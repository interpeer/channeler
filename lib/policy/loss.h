/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_POLICY_LOSS_H
#define CHANNELER_POLICY_LOSS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include "params.h"

namespace channeler::policy {

/**
 * Loss policies interact with ordering and resend policies.
 *
 * Loss policies are also simple to the point of being dumb: either a loss
 * event needs to be emitted to the caller (which could man closing a channel/
 * connection), or not. Loss policies are essentially a boolean flag.
 *
 * We make them very little more than that here.
 *
 * Note: at this point, the policy_paramsT is not used other than exposing it
 * as a nested definition. That's all it's intended for at the moment, as
 * packet_queue tries to ensure compatible policy params. This is a compile-time
 * check, so it does not hurt here, but it means future changes to loss
 * policies should get off the ground with the right kind of parameters.
 */
template <
  typename policy_paramsT = policy_params<>
>
struct return_next_policy
{
  using params = policy_paramsT;

  static constexpr bool emit_close_event()
  {
    return false;
  }
};


template <
  typename policy_paramsT = policy_params<>
>
struct abort_policy
{
  using params = policy_paramsT;

  static constexpr bool emit_close_event()
  {
    return true;
  }
};

} // namespace channeler::policy

#endif // guard
