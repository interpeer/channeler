/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_POLICY_ORDERING_H
#define CHANNELER_POLICY_ORDERING_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <list>
#include <optional>

#include <channeler/packet.h>

#include "params.h"

#include <liberate/logging.h>

namespace channeler::policy {

/**
 * Packet ordering policies interact with resend and loss policies.
 *
 * The resend policy detects if a packet is late or out-of-order, and can
 * initiate resends. The resend policy therefore operates on the incoming
 * end of a packet queue.
 *
 * The outgoing end, the ordering policy, determines if the queue contains
 * a valid next element or not. It then interacts with the loss policy to
 * determine what to do when it can't find a valid element.
 *
 * A third state is that there may yet arrive a valid next element. This is
 * where the ordering policy must interact with the resend policy.
 *
 * The choices of ordering policies, hence the name, revolve largely around
 * which received packet to yield up to the reader. Choices included:
 *  - In order of packet sequence number, without gaps. This is roughly how
 *    TCP orders packets.
 *  - In order of packet sequence number, with gaps. This offers some semblance
 *    of ordering without blocking for delayed packets. It may be the best
 *    policy for streaming media.
 *  - In order of arrival. Sequence numbers do not matter. This is the same
 *    behaviour as UDP exhibits.
 */

/**
 * This policy models UDP behaviour and returns packets in order of arrival. It
 * is by far the simplest policy.
 */
template <
  typename policy_paramsT
>
struct order_of_arrival_policy
{
  using params = policy_paramsT;

  using slot = typename params::slot;
  using queue_entry = typename params::queue_entry;

  using sequence_no_t = typename params::sequence_no_t;

  /**
   * Constructor. Parameters are unused in this policy.
   */
  inline order_of_arrival_policy(
      typename params::unsigned_window const & window [[maybe_unused]]
    )
    : m_window{window}
  {
  }


  /**
   * The policy may need to update state when a packet is inserted into the
   * pool. This function needs to be called only when the resend policy decides
   * the packet was ok to process.
   */
  inline error_t
  ordered_insert(typename params::time_point now,
      sequence_no_t const & lower_bound,
      packet_wrapper const & packet,
      slot const & _slot)
  {
    return ordered_insert(now, lower_bound, { packet, _slot });
  }

  inline error_t
  ordered_insert(typename params::time_point now [[maybe_unused]],
      sequence_no_t const & lower_bound,
      queue_entry const & entry)
  {
    // Clear any outdated entries, based on the lower bound. We need to order
    // only what's within the window.
    clear_outdated_entries(lower_bound);

    // For ordering on arrival, we have two choices: order by time_point,
    // or order by invocations of ordered_insert(). With a monotonically
    // increasing clock, the two would be identical. We make the simplyfing
    // assumption that this is the case. That means, we can simply push newly
    // arrived entries onto the back of the buffer.
    m_entries.push_back(entry);

    LIBLOG_DEBUG("Inserted " << static_cast<uint64_t>(entry.packet.sequence_no()));
    return ERR_SUCCESS;
  }


  /**
   * Retrieving a queue entry also removes it from the queue as well. Removal
   * from the queue does not imply removal from the packet_pool, however. It
   * does mean advancing the read state of the policy, though.
   * Note: this modification of the read state means that a peek operation is
   *       harder to implement.
   */
  inline std::optional<queue_entry>
  retrieve(sequence_no_t const & lower_bound,
      sequence_no_t const & seqno [[maybe_unused]] = 0,
      bool erase_after_read = true)
  {
    // We don't wwant to return outdated entries.
    clear_outdated_entries(lower_bound);

    // Retrieval is relatively simple, too. If we have an entry, we'll return
    // it. If we don't, we don't.
    if (m_entries.empty()) {
      return {};
    }
    auto ret = m_entries.front();
    if (erase_after_read) {
      m_entries.pop_front();
    }
    return {ret};
  }


  /**
   * Retrieve the currently held number of entries.
   */
  inline std::size_t size() const
  {
    return m_entries.size();
  }

  inline bool empty() const
  {
    return m_entries.empty();
  }


  /**
   * The function clears all "outdated" entries, i.e. entries before the window
   * lower bound. This is comparable to the resend policy's update_timeouts()
   * functions. The main difference is that the ordering policy keeps references
   * to actual packets.
   */
  inline void clear_outdated_entries(sequence_no_t const & lower_bound)
  {
    size_t count = 0;
    auto iter = m_entries.begin();
    while (iter != m_entries.end()) {
      if (m_window.before(lower_bound, iter->packet.sequence_no())) {
        // Erase this.
        iter = m_entries.erase(iter);
        ++count;
      }
      else {
        ++iter;
      }
    }
    LIBLOG_DEBUG("Erased " << count << " outdated entries.");
  }
private:

  typename params::unsigned_window const &  m_window;

  // See ordered_insert().
  using entry_list = std::list<queue_entry>;
  entry_list  m_entries;
};



/**
 * This policy returns packets in sequence number order, but accepts gaps. The
 * upshot is that it will always return the packet with the lowest sequence
 * number it currently knows.
 */
template <
  typename policy_paramsT
>
struct sequential_with_gaps_policy
{
  using params = policy_paramsT;

  using slot = typename params::slot;
  using queue_entry = typename params::queue_entry;

  using sequence_no_t = typename params::sequence_no_t;

  /**
   * We need a constructor with a const reference to a comparison function
   * for the map to do what it's supposed to do. The map function type is
   * defined in the policy params.
   */
  inline sequential_with_gaps_policy(
      typename params::unsigned_window const & window
    )
    : m_window{window}
  {
  }


  /**
   * See above.
   */
  inline error_t
  ordered_insert(typename params::time_point now,
      sequence_no_t const & lower_bound,
      packet_wrapper const & packet,
      slot const & _slot)
  {
    return ordered_insert(now, lower_bound, { packet, _slot });
  }

  inline error_t
  ordered_insert(typename params::time_point now [[maybe_unused]],
      sequence_no_t const & lower_bound,
      queue_entry const & entry)
  {
    // Clear any outdated entries, based on the lower bound. We need to order
    // only what's within the window.
    clear_outdated_entries(lower_bound);

    // The logic for insert is much the same as in an unordered situation;
    // we just collect entries and return the lower bound again.
    // Order enforcement happens at retrieval. The only reason we put the
    // entry into a map is so that we don't need to do a linear search after
    // odering.
    auto [iter, success] = m_entries.insert({entry.packet.sequence_no(), entry});
    if (!success) {
      LIBLOG_ERROR("Could not insert duplicate packet; this indicates bad use.");
      return ERR_INVALID_VALUE;
    }

    LIBLOG_DEBUG("Inserted " << static_cast<uint64_t>(entry.packet.sequence_no()));
    return ERR_SUCCESS;
  }


  /**
   * See above.
   */
  inline std::optional<queue_entry>
  retrieve(sequence_no_t const & lower_bound, sequence_no_t const & seqno = 0,
      bool erase_after_read = true)
  {
    // We don't wwant to return outdated entries.
    clear_outdated_entries(lower_bound);

    if (m_entries.empty()) {
      return {};
    }

    // Either return the given sequence number, or find the first one in order.
    sequence_no_t to_retrieve = seqno;
    if (!to_retrieve) {
      // Only at this point can we be sure that there are no outdated items in
      // use, because only here can we be sure we have the right lower bound.
      // Given this lower bound, then, we can sort entries into a set, and return
      // the best match.
      // TODO: this algorithm is probably not very efficient. But it's readable.
      using sort_set = std::set<
        typename params::sequence_no_t,
        typename params::sequence_less
      >;
      typename params::sequence_less less{
        m_window,
        [lower_bound] { return lower_bound; }
      };
      sort_set sorter{less};

      for (auto & [seq, entry] : m_entries) {
        sorter.insert(seq);
      }

      to_retrieve = *sorter.begin();
    }
    LIBLOG_DEBUG("Need to return sequence number: " << static_cast<uint64_t>(to_retrieve));

    // Knowing which sequence number we need, extract the appropriate entry.
    auto iter = m_entries.find(to_retrieve);
    auto ret = iter->second;
    if (erase_after_read) {
      m_entries.erase(iter);
    }
    return {ret};
  }


  /**
   * Retrieve the currently held number of entries.
   */
  inline std::size_t size() const
  {
    return m_entries.size();
  }

  inline bool empty() const
  {
    return m_entries.empty();
  }


  /**
   * Clear outdated entries
   */
  inline void clear_outdated_entries(sequence_no_t const & lower_bound)
  {
    size_t count = 0;
    auto iter = m_entries.begin();
    while (iter != m_entries.end()) {
      if (m_window.before(lower_bound, iter->first)) {
        // Erase this.
        iter = m_entries.erase(iter);
        ++count;
      }
      else {
        ++iter;
      }
    }
    LIBLOG_DEBUG("Erased " << count << " outdated entries.");
  }

private:
  typename params::unsigned_window const &  m_window;

  // See ordered_insert().
  using entry_map = std::map<
    typename params::sequence_no_t,
    queue_entry
  >;
  entry_map                                 m_entries;
};



/**
 * This policy returns packets in sequence number order and does not accept
 * gaps. If it encounters a gap, it will return no entry. For the purposes
 * of this policy, a gap occurs when the next value to read is not one it knows
 * about.
 */
template <
  typename policy_paramsT
>
struct sequential_without_gaps_policy
{
  using params = policy_paramsT;

  using slot = typename params::slot;
  using queue_entry = typename params::queue_entry;

  using sequence_no_t = typename params::sequence_no_t;

  /**
   * We need a constructor with a const reference to a comparison function
   * for the map to do what it's supposed to do. The map function type is
   * defined in the policy params.
   */
  inline sequential_without_gaps_policy(
      typename params::unsigned_window const & window
    )
    : m_window{window}
  {
  }

  /**
   * See above.
   */
  inline error_t
  ordered_insert(typename params::time_point now,
      sequence_no_t const & lower_bound,
      packet_wrapper const & packet,
      slot const & _slot)
  {
    return ordered_insert(now, lower_bound, { packet, _slot });
  }


  inline error_t
  ordered_insert(typename params::time_point now [[maybe_unused]],
      sequence_no_t const & lower_bound,
      queue_entry const & entry)
  {
    clear_outdated_entries(lower_bound);

    // Much as above, we only fail on inserting duplicates, and ordering will
    // occur later.
    auto [iter, success] = m_entries.insert({entry.packet.sequence_no(), entry});
    if (!success) {
      LIBLOG_ERROR("Could not insert duplicate packet; this indicates bad use.");
      return ERR_INVALID_VALUE;
    }

    LIBLOG_DEBUG("Inserted " << static_cast<uint64_t>(entry.packet.sequence_no()));
    return ERR_SUCCESS;
  }


  /**
   * See above.
   */
  inline std::optional<queue_entry>
  retrieve(sequence_no_t const & lower_bound, sequence_no_t const & seqno = 0,
      bool erase_after_read = true)
  {
    auto to_retrieve = seqno;
    if (!to_retrieve) {
      to_retrieve = lower_bound;
    }

    clear_outdated_entries(lower_bound);

    // If there are no entries, we can't retrieve anything.
    if (m_entries.empty()) {
      LIBLOG_DEBUG("No entries.");
      return {};
    }

    // We don't have to perform any particular sorting here.
    // Reading happens at the lower bound. If a corresponding entry does
    // not exist, we have a gap, and that must be passed to the caller via
    // an empty result.
    auto iter = m_entries.find(to_retrieve);
    if (iter == m_entries.end()) {
      LIBLOG_DEBUG("Entry " << to_retrieve << " not found.");
      return {};
    }

    auto ret = iter->second;
    if (erase_after_read) {
      m_entries.erase(iter);
    }
    return {ret};
  }


  /**
   * Retrieve the currently held number of entries.
   */
  inline std::size_t size() const
  {
    return m_entries.size();
  }

  inline bool empty() const
  {
    return m_entries.empty();
  }

  /**
   * Clear outdated entries
   */
  inline void clear_outdated_entries(sequence_no_t const & lower_bound)
  {
    size_t count = 0;
    auto iter = m_entries.begin();
    while (iter != m_entries.end()) {
      if (m_window.before(lower_bound, iter->first)) {
        // Erase this.
        iter = m_entries.erase(iter);
        ++count;
      }
      else {
        ++iter;
      }
    }
    LIBLOG_DEBUG("Erased " << count << " outdated entries.");
  }
private:

  typename params::unsigned_window const &  m_window;

  // See ordered_insert().
  using entry_map = std::map<
    typename params::sequence_no_t,
    queue_entry
  >;
  entry_map                                 m_entries;
};


} // namespace channeler::policy

#endif // guard
