/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_FSM_REGISTRY_H
#define CHANNELER_FSM_REGISTRY_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include "base.h"

#include "../macros.h"

#include <liberate/logging.h>

namespace channeler::fsm {

/**
 * A run-time registry of FSMs, to simplify state handling across multiple
 * FSM implementations.
 */
class registry
{
public:

  /**
   * Instanciate and add FSM. There is no removal of FSMs, as it is expected
   * that the number and composition of FSMs does not change during the run-time
   * of the protocol. The registry only matters in compositing; for this reason,
   * we also subsume the construction into the add() function.
   */
  template <
    typename fsmT,
    typename... ctor_argsT
  >
  inline void add(ctor_argsT &&... ctor_args)
  {
    static_assert(std::is_base_of<fsm_base, fsmT>::value,
        "fsmT must be a descendant of fsm_base");

    m_fsms.insert(std::move(
          std::make_unique<fsmT>(std::forward<ctor_argsT>(ctor_args)...)
    ));
  }

  template <
    typename fsmT
  >
  inline void add_move(std::unique_ptr<fsmT> fsm)
  {
    static_assert(std::is_base_of<fsm_base, fsmT>::value,
        "fsmT must be a descendant of fsm_base");
    m_fsms.insert(std::move(fsm));
  }


  /**
   * Process the input event across all FSMs. Returns true if at least one
   * registered FSM processed the input event.
   */
  inline fsm_base::process_result
  process(::channeler::pipe::event const & to_process,
      ::channeler::pipe::action_list_type & result_actions,
      ::channeler::pipe::event_list_type & output_events)
  {
    ::channeler::pipe::action_list_type actions;
    auto [err, status] = process_inner(to_process, actions, output_events);

    // The action list may contain an action for the registry to
    // perform, for which there really is mostly one, i.e. to process
    // an event returned in the action.
    for (auto iter = actions.begin() ; iter != actions.end() ; ) {
      if ((*iter)->type != pipe::AT_PROCESS_EVENT) {
        ++iter;
        continue;
      }

      // We have an event to process. Move it out of the action first.
      auto actconv = reinterpret_cast<pipe::process_event_action *>(iter->get());
      auto inner_to_process = std::move(actconv->to_process);

      // Remove & advance iterator; we're done with the action.
      iter = actions.erase(iter);

      // Pass this event to the FSMs as well. By adding to the actions,
      // we ensure that any results of this processing will also get
      // processed, etc.
      auto [inner_err, inner_status] = process_inner(*inner_to_process.get(),
          actions, output_events);

      // The only thing we're really interested here is if this inner event
      // errored; all other statuses we can ignore. We'll log the inner
      // status but report the outer.
      if (ERR_SUCCESS != inner_err) {
        LIBLOG_ET("Inner event caused status: " << inner_status,
            inner_err);
        err = inner_err;
        break;
      }
    }

    // With the action list processed like this, we can append it to the
    // result actions.
    merge_actions(result_actions, actions);

    // Finfally, if the event hasn't been handled, that's technically
    // a programming error and we should error appropriately.
    if (fsm_base::PS_UNHANDLED == status) {
      LIBLOG_ERROR("No registered FSM handled event: " << to_process.type);
      return {ERR_STATE, status};
    }
    return {ERR_SUCCESS, status};
  }

private:

  inline fsm_base::process_result
  process_inner(::channeler::pipe::event const & to_process,
      ::channeler::pipe::action_list_type & result_actions,
      ::channeler::pipe::event_list_type & output_events)
  {
    auto status = fsm_base::PS_UNHANDLED;

    // Pass the event through all FSMs
    for (auto & fsm : m_fsms) {
      auto [err, res] = fsm->process(to_process, result_actions, output_events);

      // On failure, stop immediately and pass on result.
      if (ERR_SUCCESS != err) {
        return {err, res};
      }

      // Just skip when the FSM didn't handle the event.
      if (fsm_base::PS_UNHANDLED == res) {
        continue;
      }

      // Whether the FSM ignored or handled the event successfull is
      // not particularly interesting (except for logging). But any message
      // that remains entirely unhandled should be logged.
      if (fsm_base::PS_SUCCESS == res) {
        status = fsm_base::PS_SUCCESS;
        continue;
      }

      // Set the overall status if it's still unset.
      if (fsm_base::PS_UNHANDLED == status) {
        status = res;
      }
    }

    return {ERR_SUCCESS, status};
  }


  using fsm_ptr = std::unique_ptr<fsm_base>;
  using fsm_set = std::set<fsm_ptr>;

  fsm_set     m_fsms;
};


} // namespace channeler::fsm

#endif // guard
