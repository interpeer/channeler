/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_FSM_BASE_H
#define CHANNELER_FSM_BASE_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include "../pipe/event.h"
#include "../pipe/action.h"

namespace channeler::fsm {

/**
 * This file defines a base class for finite state machines that handle
 * parts of the channeler sub-protocols.
 *
 * The general interface is similar to the filter interface (see ../pipe),
 * in that FSMs consume events, whose type is defined in the pipe submodule.
 *
 * Unlike filters, each FSM produces actions (to be passed back down the
 * input filter pipe) as well as events (to be passed to the output filter
 * pipe). A FSM also does not consume the event it is being passed, but returns
 * whether or not it processed the event.
 *
 * As a result of this, actions and output events are appended to modifiable
 * list parameters, and only the processing status is returned.
 *
 * Also unlike filters, there is no statically compiled pipeline to FSMs.
 * Rather, each instance may or may not process an event and produce some
 * results. We therefore need an abstract base class.
 *
 * Finally, the input event types are fairly limited. They are invoked at the
 * end of the input filter pipe, where events are ET_MESSAGE (or in future,
 * timeouts TODO). This is checked at run-time however.
 * https://codeberg.org/interpeer/channeler/issues/10
 */
struct fsm_base
{
  enum processing_status : uint8_t
  {
    PS_SUCCESS    = 0,  // Fully processed
    PS_IGNORED    = 1,  // Relevant, but ignored in the current state
    PS_UNHANDLED  = 2,  // Not relevant to this FSM
    PS_ERRORED    = 3,  // Event led to some kind of unrecoverable error.
  };

  // The process results consists of an error code and an event processing
  // status. If the error code is anything other than ERR_SUCCESS, the
  // connection should be closed. FSMs should only return anything other
  // than ERR_SUCESS if they irrecoverably failed.
  //
  // For any other kind of state, set the processing status accordingly. The
  // status will fall into one of the above classes.
  using process_result = std::tuple<error_t, processing_status>;

  /**
   * Process function, see above.
   *
   * The return value is an error code and processing status. Anything other
   * than ERR_SUCCESS will terminate the FSM and connection associated with
   * it. If you want to do that, return such an error.
   *
   * If you want to proceed, provide more information via the processing_status
   * code.
   *
   * Note: events are moved as std::unique_ptr within the event pipeline, but
   *       then handed as const & to process() - this is for the benefit of the
   *       FSM registry (see registry.h). We need to be able to allow multiple
   *       FSMs a stab at processing each event.
   *
   *       The event may be a message event that condains a std::unique_ptr of
   *       a message. If that message needs to be processed somehow, it has to
   *       be copied. The copy should therefore be lightweight (which the
   *       filter pipeline guarantees to be feasible by using the packet backing
   *       buffer).
   *
   *       In short, copy messages from message events if you do anything with
   *       them.
   */
  virtual process_result
  process(::channeler::pipe::event const & to_process,
      ::channeler::pipe::action_list_type & result_actions,
      ::channeler::pipe::event_list_type & output_events) = 0;

  virtual ~fsm_base() = default;
};

} // namespace channeler::fsm

#endif // guard
