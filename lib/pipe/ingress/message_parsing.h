/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_PIPE_INGRESS_MESSAGE_PARSING_H
#define CHANNELER_PIPE_INGRESS_MESSAGE_PARSING_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <memory>
#include <set>

#include "../../memory/packet_pool.h"
#include "../../channels.h"
#include "../event.h"
#include "../action.h"
#include "../event_as.h"

#include <channeler/packet.h>
#include <channeler/error.h>


namespace channeler::pipe {


/**
 * Parses messages in a packet. Follow-on filters will receive individual
 * messages.
 *
 * Expects the next_eventT constructor to take
 * - a message wrapper rerpresenting the parsed message
 * - an (optional) channel data structure pointer
 * - packet_wrapper
 * - a pool slot
 *
 * such as e.g. message_event
 */
template <
  std::size_t POOL_BLOCK_SIZE,
  typename next_filterT,
  typename next_eventT,
  typename channelT
>
struct message_parsing_filter
{
  using input_event = dequeued_packet_event<POOL_BLOCK_SIZE, channelT>;
  using channel_set = ::channeler::channels<channelT>;

  inline message_parsing_filter(
      std::shared_ptr<::channeler::message_registry> registry,
      next_filterT * next)
    : m_registry{registry}
    , m_next{next}
  {
  }


  template <typename... argsT>
  inline action_list_type
  consume(std::unique_ptr<event> unchecked, argsT && ...args)
  {
    auto in = event_as<input_event>("ingress:message_parsing", unchecked, ET_DEQUEUED_PACKET);

    // If there is no data passed, we should also throw.
    if (nullptr == in->data.data()) {
      throw exception{ERR_INVALID_REFERENCE};
    }

    // We have a packet and it belongs to a channel. Now we need to push
    // messages down the pipeline.
    // TODO if the packet channel is pending, then this packet should
    //      contain a MSG_PACKET_COOKIE. We need to process that message
    //      *first* in order to be able to process the others. This may
    //      require two iterations.
    //      https://codeberg.org/interpeer/channeler/issues/13
    action_list_type actions;
    for (auto msg : in->packet.get_messages(m_registry)) {
      // Need to construct a new event per message
      auto next = std::make_unique<next_eventT>(
          in->packet,
          in->data,
          in->channel,
          std::move(extract_message(msg))
      );
      auto ret = m_next->consume(std::move(next), std::forward<argsT>(args)...);
      merge_actions(actions, ret);
    }

    // The nice thing about the iterator interface is that if there were no
    // messages in the packet, or the packet has trailing junk, we don't have
    // to care. If in->data slot hasn't been copied and the slot goes out
    // of scope when this input event is abandoned at the end of this function,
    // we've freed the pool slot effectively.

    return actions;
  }


  std::shared_ptr<::channeler::message_registry>  m_registry;
  next_filterT *                                  m_next;
};


} // namespace channeler::pipe

#endif // guard
