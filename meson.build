##############################################################################
# Project
project('channeler', 'cpp',
  version: '0.1.0',
  license: 'GPLv3',
  meson_version: '>=0.55.0',
  default_options: [
    'cpp_std=c++17',
    'default_library=both',
  ])


##############################################################################
# Versioning, project and libraries

# There are so many versioning schemes, each of which is incompatible with
# others. We'll manage best by keeping things simple:
#
# - The package version follows semver
# - The library version is the package version
# - The ABI version, i.e. the compatibility we expect, is the major
#   component of the package
_splitver = meson.project_version().split('.')
PACKAGE_MAJOR = _splitver[0]
PACKAGE_MINOR = _splitver[1]
PACKAGE_PATCH = _splitver[2]

ABI_VERSION = PACKAGE_MAJOR
LIB_VERSION = meson.project_version()
PACKAGE_VERSION = meson.project_version()

##############################################################################
# Configuration

conf_data = configuration_data()
compiler = meson.get_compiler('cpp')

host_type = ''
if host_machine.system() in [ 'cygwin', 'darwin', 'dragonfly', 'freebsd', 'gnu', 'linux', 'netbsd' ]
  host_type = 'posix'
elif host_machine.system() == 'windows'
  host_type = 'win32'
elif host_machine.system().startswith('android')
  host_type = 'android'
endif
summary('Host platform', host_type, section: 'Platform')

# For Windows, try to determine the SDK version.
winsdk_ver = -1
if compiler.has_header('ntverp.h')
  code = '''#include <iostream>
#include <ntverp.h>

int main()
{
  std::cout << VER_PRODUCTMAJORVERSION;
}
'''
  result = compiler.run(code, name: 'Windows SDK version check.')
  winsdk_ver = result.stdout().to_int()
  summary('Windows SDK major version', winsdk_ver, section: 'Platform')
endif


### Build configuration
bt = get_option('buildtype')

libtype = get_option('default_library')
build_shared = false
if libtype in ['shared', 'both']
  build_shared = true
endif

build_static = false
if libtype in ['static', 'both']
  build_static = true
endif


### Compiler flags
compiler_id = compiler.get_id()

cpp_args = []
link_args = []
static_link_args = []
cpp_defines = []
cpp_define_args = []
define_prefix = '-D'

posix_common_args = [
  '-Wall', '-Wextra', '-Wpedantic', '-Wshadow', '-Wstrict-aliasing',
  '-Wstrict-overflow=5', '-Wcast-align',
  '-Wpointer-arith', '-Wcast-qual', '-Wold-style-cast',
]
if compiler_id == 'clang'
  cpp_args += posix_common_args + [
    '-Wabi', '-flto',
  ]
  link_args += ['-flto']
  static_link_args = [
    '-lstdc++', '-lm',
  ]
  cpp_defines += ['CHANNELER_IS_BUILDING=1']
elif compiler_id == 'gcc'
  cpp_args += posix_common_args + [
    '-Wstrict-null-sentinel', '-flto',
  ]
  link_args += ['-flto']
  static_link_args = [
    '-lstdc++', '-lm',
  ]
  cpp_defines += ['CHANNELER_IS_BUILDING=1']
elif compiler_id == 'msvc'
  cpp_args += [
    '/wd4250', '/wd4251', '/wd4275',
  ]
  cpp_defines += ['CHANNELER_IS_BUILDING=1']
  define_prefix = '/D'
endif

if bt in ['debug', 'debugoptimized']
  cpp_defines += ['DEBUG=1']

  if compiler_id == 'clang'
    cpp_args += ['-ggdb']
  elif compiler_id == 'gcc'
    cpp_args += ['-g3']
  elif compiler_id == 'msvc'
    cpp_args += ['/FS']
  endif
else
  cpp_defines += ['NDEBUG=1']

  posix_release_args = [
    '-fvisibility=hidden', '-fvisibility-inlines-hidden',
  ]
  if compiler_id == 'clang'
    cpp_args += posix_release_args
  elif compiler_id == 'gcc'
    cpp_args += posix_release_args
  elif compiler_id == 'msvc'
    cpp_args += ['/Oi', '/DNDEBUG=1']
  endif
endif

if host_type == 'android'
  # Only posix compilers supported (?)
  cpp_defines += ['ANDROID_STL=c++_shared']

  cpp_args += [
    '-fexceptions', '-frtti',
  ]
  link_args = static_link_args
endif


# Make things work with MSVC and Windows SDK <10
if compiler_id == 'msvc' and winsdk_ver < 10
  cpp_args += ['/permissive']
endif

# Turn defines into cpp args
foreach def : cpp_defines
  cpp_define_args += [define_prefix + def]
endforeach


add_project_arguments(cpp_args, language: 'cpp')


### Version and package information
conf_data.set_quoted('CHANNELER_PACKAGE_MAJOR', PACKAGE_MAJOR)
conf_data.set_quoted('CHANNELER_PACKAGE_MINOR', PACKAGE_MINOR)
conf_data.set_quoted('CHANNELER_PACKAGE_PATCH', PACKAGE_PATCH)
conf_data.set_quoted('CHANNELER_PACKAGE_VERSION', PACKAGE_VERSION)
conf_data.set_quoted('CHANNELER_ABI_VERSION', ABI_VERSION)
conf_data.set_quoted('CHANNELER_LIB_VERSION', LIB_VERSION)

conf_data.set_quoted('CHANNELER_PACKAGE_NAME', meson.project_name())
conf_data.set_quoted('CHANNELER_PACKAGE_URL', 'https://codeberg.org/interpeer/channeler')

### Host platform details
conf_data.set('CHANNELER_BIGENDIAN', host_machine.endian() == 'big')

### Headers

# TODO?

### Types

compiler.sizeof('int32_t', prefix: '#include <stdint.h>')
compiler.sizeof('uint32_t', prefix: '#include <stdint.h>')
compiler.sizeof('int64_t', prefix: '#include <stdint.h>')
compiler.sizeof('uint64_t', prefix: '#include <stdint.h>')
compiler.sizeof('size_t', prefix: '#include <stdint.h>')
compiler.sizeof('ssize_t', prefix: '#include <stdint.h>')


### Set values from options
default_packet_pool_block_size_bytes = get_option('default_packet_pool_block_size_bytes')
summary('Packet pool block size (Bytes)', default_packet_pool_block_size_bytes, section: 'Build options')
conf_data.set('CHANNELER_DEFAULT_PACKET_POOL_BLOCK_SIZE_BYTES', default_packet_pool_block_size_bytes)

default_receive_window_size = get_option('default_receive_window_size')
summary('Receive window size (Packets)', default_receive_window_size, section: 'Build options')
conf_data.set('CHANNELER_DEFAULT_RECEIVE_WINDOW_SIZE', default_receive_window_size)

default_round_trip_timeout_nsec = get_option('default_round_trip_timeout_nsec')
summary('Round trip timeout (nsec)', default_round_trip_timeout_nsec, section: 'Build options')
conf_data.set('CHANNELER_DEFAULT_ROUND_TRIP_TIMEOUT_NSEC', default_round_trip_timeout_nsec)

default_maximum_resend_collisions = get_option('default_maximum_resend_collisions')
summary('Packet resend maximum collisions (attempts)', default_maximum_resend_collisions, section: 'Build options')
conf_data.set('CHANNELER_DEFAULT_MAXIMUM_RESEND_COLLISIONS', default_maximum_resend_collisions)

default_channel_creation_timeout_nsec = get_option('default_channel_creation_timeout_nsec')
summary('Default channel creation timeout (nsec)', default_channel_creation_timeout_nsec, section: 'Build options')
conf_data.set('CHANNELER_DEFAULT_CHANNEL_CREATION_TIMEOUT_NSEC', default_channel_creation_timeout_nsec)

default_channel_creation_retries = get_option('default_channel_creation_retries')
summary('Default channel creation attempts (max)', default_channel_creation_retries, section: 'Build options')
conf_data.set('CHANNELER_DEFAULT_CHANNEL_CREATION_RETRIES', default_channel_creation_retries)

default_channel_timeout_nsec = get_option('default_channel_timeout_nsec')
summary('Default channel (in)activity timeout (nsec)', default_channel_timeout_nsec, section: 'Build options')
conf_data.set('CHANNELER_DEFAULT_CHANNEL_TIMEOUT_NSEC', default_channel_timeout_nsec)

default_channel_progress_timeout_nsec = get_option('default_channel_progress_timeout_nsec')
summary('Default channel progress message timeout (max, nsec)', default_channel_progress_timeout_nsec, section: 'Build options')
conf_data.set('CHANNELER_DEFAULT_CHANNEL_PROGRESS_TIMEOUT_NSEC', default_channel_progress_timeout_nsec)

default_channel_congestion_update_timeout_nsec = get_option('default_channel_congestion_update_timeout_nsec')
summary('Default channel congestion update message timeout (nsec)', default_channel_congestion_update_timeout_nsec, section: 'Build options')
conf_data.set('CHANNELER_DEFAULT_CHANNEL_CONGESTION_UPDATE_TIMEOUT_NSEC', default_channel_congestion_update_timeout_nsec)

##############################################################################
# Dependencies
subproject_opts = [
  'default_library=' + libtype,
]

libvar_postfix = ''
if libtype in ['static', 'both']
  libvar_postfix = '_static_dep'
else
  libvar_postfix = '_dep'
endif

deps = []

##### liberate

liberate_dep = dependency(
  'liberate',
  fallback: ['liberate', 'liberate' + libvar_postfix],
  default_options: subproject_opts,
)

deps += [liberate_dep]

summary('liberate', liberate_dep.version(), section: 'Other Dependencies')



##############################################################################
# Vendored dependencies
# subdir('vendor')
# deps += [vendored_deps]

##############################################################################
# Set values from options
configure_file(
  input: 'build-config.h.in',
  output: 'build-config.h',
  configuration: conf_data
)

main_build_dir = declare_dependency(
  include_directories: include_directories('.')
)


##############################################################################
# Library

includes = include_directories(
  'include',
)

libincludes = include_directories(
  'lib',
)


install_headers(
  'include' / 'channeler.h',
)

install_headers(
  'include' / 'channeler' / 'version.h',
  'include' / 'channeler' / 'visibility.h',
  'include' / 'channeler' / 'error.h',
  'include' / 'channeler' / 'channelid.h',
  'include' / 'channeler' / 'packet.h',
  'include' / 'channeler' / 'peerid.h',
  'include' / 'channeler' / 'protoid.h',
  'include' / 'channeler' / 'message.h',

  subdir: 'channeler',
)

install_headers(
  'include' / 'channeler' / 'message' / 'basic.h',
  'include' / 'channeler' / 'message' / 'channels.h',
  'include' / 'channeler' / 'message' / 'data.h',
  'include' / 'channeler' / 'message' / 'congestion.h',

  subdir: 'channeler' / 'message',
)


libsrc = [
  'lib' / 'channelid.cpp',
  'lib' / 'error.cpp',
  'lib' / 'packet.cpp',
  'lib' / 'peerid.cpp',
  'lib' / 'message.cpp',
  'lib' / 'cookie.cpp',
  'lib' / 'timeouts.cpp',
  'lib' / 'message' / 'channels.cpp',
  'lib' / 'message' / 'data.cpp',
  'lib' / 'message' / 'congestion.cpp',
  'lib' / 'version.cpp',
]


dep_internal = false

# Shared library?
if build_shared
  so_lib = shared_library('channeler', libsrc,
      include_directories: [includes],
      dependencies: deps,
      link_args: link_args,
      cpp_args: cpp_define_args,
      version: LIB_VERSION,
      soversion: ABI_VERSION,
      install: true
  )

  channeler_dep = declare_dependency(
      include_directories: [includes],
      dependencies: deps,
      compile_args: [],
      link_with: [so_lib],
      link_args: link_args,
      version: LIB_VERSION,
  )

  dep_internal = channeler_dep
endif

# Static library?
if build_static
  static_args = [define_prefix + 'CHANNELER_STATIC=1']

  sta_lib = static_library('channeler', libsrc,
      include_directories: [includes],
      dependencies: deps,
      link_args: link_args,
      cpp_args: cpp_define_args + static_args,
      install: true,
  )

  channeler_static_dep = declare_dependency(
      include_directories: [includes],
      dependencies: deps,
      compile_args: static_args,
      link_with: [sta_lib],
      link_args: link_args + static_link_args,
      version: LIB_VERSION,
  )

  dep_internal = channeler_static_dep
endif


##############################################################################
# Subdirectories
subdir('test')

##############################################################################
# Linter, etc.
cppcheck = find_program('cppcheck', required: false)

if cppcheck.found()
  run_target('cppcheck', command: [
    cppcheck.full_path(),
    '--cppcheck-build-dir=@0@/cppcheck'.format(meson.current_build_dir()),
    '--enable=all', '-v',
    '--suppress=missingIncludeSystem',
    '--inline-suppr',
    '-I', '@0@/include'.format(meson.current_source_dir()),
    '-I', '@0@/lib'.format(meson.current_source_dir()),
    '-I', meson.current_build_dir(),
    '--std=c++17',
    cpp_define_args,
    '--quiet',
    '@0@/lib'.format(meson.current_source_dir()),
  ])
endif

oclint = find_program('oclint', required: false)

if oclint.found()
  oclint_config = custom_target('oclint_config',
      output: '.oclint',
      input: '.oclint',
      command: ['cp', '@INPUT@', '@OUTPUT@'],
      install: false,
  )

  oclintsrc = []
  foreach fname : libsrc
    oclintsrc += [meson.current_source_dir() / fname]
  endforeach

  run_target('oclint', command: [
      oclint.full_path(),
      oclintsrc,
      '-o', '@0@/oclint.log'.format(meson.current_build_dir()),
      '--',
      '-I', '@0@/include'.format(meson.current_source_dir()),
      '-I', '@0@/lib'.format(meson.current_source_dir()),
      '-I', meson.current_build_dir(),
      '--std=@0@'.format(get_option('cpp_std')),
      cpp_args,
      cpp_define_args,
      '-DOCLINT_IS_RUNNING',
    ],
    depends: oclint_config,
  )
endif
