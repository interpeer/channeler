/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef TEST_PACKETS_H
#define TEST_PACKETS_H

#include <channeler/packet.h>

namespace test {


/**
 * Packet definitions
 */
extern channeler::byte const packet_default_channel_trailing_bytes[];
extern std::size_t const packet_default_channel_trailing_bytes_size;

extern channeler::byte const packet_default_channel[];
extern std::size_t const packet_default_channel_size;

extern channeler::byte const packet_partial_channelid_initiator[];
extern std::size_t const packet_partial_channelid_initiator_size;

extern channeler::byte const packet_partial_channelid_responder[];
extern std::size_t const packet_partial_channelid_responder_size;

extern channeler::byte const packet_regular_channelid[];
extern std::size_t const packet_regular_channelid_size;

extern channeler::byte const packet_with_messages[];
extern std::size_t const packet_with_messages_size;

} // namespace test

#endif // guard
