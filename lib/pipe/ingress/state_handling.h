/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_PIPE_INGRESS_STATE_HANDLING_H
#define CHANNELER_PIPE_INGRESS_STATE_HANDLING_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <functional>
#include <map>

#include "../../lock_policy.h"
#include "../../fsm/base.h"

#include "../event.h"
#include "../action.h"
#include "../event_as.h"



namespace channeler::pipe {


/**
 * Handles the state machines for messages.
 *
 * This filter is the *end* of the ingress pipe, and the *beginning* of the
 * egress pipe -- the latter albeit only for messages produced by the FSMs
 * it encapsulates.
 *
 * At the same time, the FSMs may produce notifications to the API user, such
 * as of errors or data being available.
 *
 * The filter therefore requires as a paremter a mapping of event categories
 * to recipient functions. At this point, it seems unlikely that individual
 * event types need to be routed differently.
 *
 * The function prototype follows that of the filter's consume function,
 * in that it can produce actions. It cannot be injecting more events
 * into *this* pipe.
 */
template <typename... argsT>
using event_route_function = std::function<
  action_list_type (std::unique_ptr<event>, argsT && ...)
>;

template <typename... argsT>
using event_route_map = std::map<
  event_category,
  event_route_function<argsT...>
>;

template <
  std::size_t POOL_BLOCK_SIZE,
  typename channelT,
  typename fsm_registryT,
  typename... map_argsT
>
struct state_handling_filter
{
  using input_event = message_event<POOL_BLOCK_SIZE, channelT>;
  using event_route_map_t = event_route_map<map_argsT...>;

  inline state_handling_filter(fsm_registryT & registry,
      event_route_map_t & route_map)
    : m_registry{registry}
    , m_event_route_map{route_map}
  {
  }



  template <typename... argsT>
  inline action_list_type
  consume(std::unique_ptr<event> unchecked, argsT && ...args)
  {
    auto in = event_as<input_event>("ingress:state_handling", unchecked, ET_MESSAGE);

    action_list_type actions;
    event_list_type events;

    auto [err, status] = m_registry.process(*in, actions, events);
    if (ERR_SUCCESS != err) {
      // TODO this is a hard error; close the channel/connection
      LIBLOG_ET("FSMs entered an error state of sorts.", err);
      return {}; // FIXME
    }

    if (::channeler::fsm::fsm_base::PS_SUCCESS != status) {
      LIBLOG_WARN("Message was not processed by registry: " << in->message->type
          << " (status: " << status << ")");
    }

    for (auto & shared_ev : events) {
      auto iter = m_event_route_map.find(shared_ev->category);
      if (iter == m_event_route_map.end()) {
        LIBLOG_ERROR("Do not know how to dispatch result event of category: "
            << shared_ev->category);
        continue;
      }

      // Pass arguments to callbacks
      auto acts = iter->second(std::move(shared_ev), std::forward<argsT>(args)...);
      merge_actions(actions, acts);
    }

    LIBLOG_DEBUG("Returning actions: " << actions.size());
    return actions;
  }


  fsm_registryT &     m_registry;
  event_route_map_t & m_event_route_map;
};


} // namespace channeler::pipe

#endif // guard
