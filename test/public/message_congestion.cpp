/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <channeler/message/congestion.h>

#include <gtest/gtest.h>

#include "../messages.h"

using namespace test;

namespace {

inline void test_congestion_window(std::shared_ptr<channeler::message_registry> reg,
    std::vector<channeler::byte> const & buf,
    std::size_t length, std::size_t expected)
{
  assert_single_byte_type_variable_size_message(reg, buf,
      channeler::MSG_CONGESTION_RECEIVE_WINDOW, 1);

  auto [ret, msg] = reg->parse(buf.data(), buf.size(), false);
  ASSERT_EQ(channeler::ERR_SUCCESS, ret);
  ASSERT_TRUE(msg);
  ASSERT_EQ(msg->type, channeler::MSG_CONGESTION_RECEIVE_WINDOW);

  auto ptr = reinterpret_cast<channeler::message_congestion_receive_window *>(msg.get());
  ASSERT_EQ(ptr->payload_size, length);

  ASSERT_EQ(ptr->window_size, expected);

  // Serialize
  std::vector<channeler::byte> out;
  out.resize(200);
  assert_serialization_ok(out, msg, buf);
}

} // anonymous namespace

TEST(MessageCongestion, parse_and_serialize_congestion_receive_window_small)
{
  auto reg = channeler::message_registry::create();

  std::vector<channeler::byte> b{message_congestion_receive_window_small, message_congestion_receive_window_small + message_congestion_receive_window_small_size};

  test_congestion_window(reg, b, 1, 18);
}


TEST(MessageCongestion, parse_and_serialize_congestion_receive_window_large)
{
  auto reg = channeler::message_registry::create();

  std::vector<channeler::byte> b{message_congestion_receive_window_large, message_congestion_receive_window_large + message_congestion_receive_window_large_size};

  test_congestion_window(reg, b, 3, 67968);
}
