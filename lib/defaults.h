/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG.
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_DEFAULTS_H
#define CHANNELER_DEFAULTS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <build-config.h>

namespace channeler {

/**
 * Compile-time defaults, from meson_options.txt
 * FIXME move them into timeout_config? rename that?
 * pool block size must remain static
 */
template <
  typename durationT,
  std::size_t _POOL_BLOCK_SIZE = CHANNELER_DEFAULT_PACKET_POOL_BLOCK_SIZE_BYTES,
  std::size_t _RECEIVE_WINDOW_SIZE = CHANNELER_DEFAULT_RECEIVE_WINDOW_SIZE,
  durationT _ROUND_TRIP_TIMEOUT_USEC = durationT{1000 * CHANNELER_DEFAULT_ROUND_TRIP_TIMEOUT_NSEC},
  std::size_t _MAX_RESEND_COLLISIONS = CHANNELER_DEFAULT_MAXIMUM_RESEND_COLLISIONS
>
struct defaults
{
  // *** Compile time constants
  constexpr static std::size_t POOL_BLOCK_SIZE = _POOL_BLOCK_SIZE;

  // *** Compile time defaults; these can be overridden at run-time.
  constexpr static std::size_t RECEIVE_WINDOW_SIZE = _RECEIVE_WINDOW_SIZE;
  constexpr static durationT ROUND_TRIP_TIMEOUT_USEC = _ROUND_TRIP_TIMEOUT_USEC;
  constexpr static std::size_t MAX_RESEND_COLLISIONS = _MAX_RESEND_COLLISIONS;
};

} // namespace channeler

#endif // guard
