/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_FSM_DATA_H
#define CHANNELER_FSM_DATA_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <functional>

#include "base.h"
#include "timeouts.h"

#include <channeler/message.h>
#include <channeler/message/data.h>
#include <channeler/timeouts.h>

#include "../macros.h"
#include "../channels.h"
#include "../channel_data.h"
#include "../support/timeouts.h"

namespace channeler::fsm {

/**
 * Implement the "data transport" state machine.
 *
 * In principle, this is actually so simple that calling it a FSM is wrong and
 * weird. In practice, though, it fits the code structure well to do so.
 *
 * This "state machine" does two jobs:
 *
 * - It translates data the user writes to a channel into outgoing data
 *   messages.
 * - It translates incoming data messages on a channel into a user
 *   notification that data is available for reading.
 *
 * That's pretty much it.
 *
 * Of course, it can only do that if the referenced channels are known. So if
 * they are not known, it may also generate some kind of errors. This last part
 * is complicated by the fact that a channel may be known but pending.
 */
template <
  std::size_t POOL_BLOCK_SIZE,
  typename channelT
>
struct fsm_data
  : public fsm_base
{
  using channel_set = ::channeler::channels<channelT>;

  using message_event_type = ::channeler::pipe::message_event<POOL_BLOCK_SIZE, channelT>;
  using data_written_event_type = ::channeler::pipe::user_data_written_event;
  using data_to_read_event_type = ::channeler::pipe::user_data_to_read_event<POOL_BLOCK_SIZE>;
  using progress_event_type = ::channeler::pipe::progress_event;
  using timeout_event_type = ::channeler::pipe::timeout_event<
    ::channeler::support::timeout_scoped_tag_type
  >;
  using packet_out_enqueued_event_type = ::channeler::pipe::packet_out_enqueued_event<
    channelT
  >;


  /**
   * Need to keep a reference to a channel_set.
   */
  inline fsm_data(
      std::shared_ptr<::channeler::message_registry> registry,
      ::channeler::support::timeouts & timeouts,
      ::channeler::timeout_config const & to_config,
      channel_set & channels)
    : m_registry{registry}
    , m_timeouts{timeouts}
    , m_to_config{to_config}
    , m_channels{channels}
  {
  }



  /**
   * Process function, see above.
   */
  virtual fsm_base::process_result
  process(::channeler::pipe::event const & to_process,
      ::channeler::pipe::action_list_type & result_actions,
      ::channeler::pipe::event_list_type & output_events)
  {
    namespace pipe = channeler::pipe;
    LIBLOG_DEBUG("Data FSM got event of type: " << to_process.type);

    // Distinguish incoming event types
    switch (to_process.type) {
      case pipe::ET_MESSAGE:
        {
          auto & msg_ev = ::channeler::pipe::event_cast<message_event_type>(to_process);
          return handle_message(msg_ev, result_actions, output_events);
        }

      case pipe::ET_USER_DATA_WRITTEN:
        {
          auto & data_ev = ::channeler::pipe::event_cast<data_written_event_type>(to_process);
          return handle_user_data_written(data_ev, result_actions, output_events);
        }

      case pipe::ET_PROGRESS:
        {
          auto & data_ev = ::channeler::pipe::event_cast<progress_event_type>(to_process);
          return handle_progress(data_ev, result_actions, output_events);
        }

      case pipe::ET_TIMEOUT:
        {
          auto & to_ev = ::channeler::pipe::event_cast<timeout_event_type>(to_process);
          return handle_timeout(to_ev, result_actions, output_events);
        }


      default:
        LIBLOG_WARN("Data FSM does not handle messages of type: " << to_process.type);
        break;
    }

    // Otherwise we can't really do anything here.
    return {ERR_SUCCESS, PS_UNHANDLED};
  }


  inline fsm_base::process_result
  handle_message(message_event_type const & event,
      ::channeler::pipe::action_list_type & result_actions [[maybe_unused]],
      ::channeler::pipe::event_list_type & output_events)
  {
    switch (event.message->type) {
      case MSG_DATA:
        return handle_data_message(event, result_actions, output_events);

      case MSG_DATA_PROGRESS:
        return handle_data_progress_message(event, result_actions, output_events);

      default:
        LIBLOG_DEBUG("Data FSM handles only data/progress messages.");
        return {ERR_SUCCESS, PS_UNHANDLED};
    }
  }



  inline fsm_base::process_result
  handle_data_message(message_event_type const & event,
      ::channeler::pipe::action_list_type & result_actions [[maybe_unused]],
      ::channeler::pipe::event_list_type & output_events)
  {
    // The main difference in how we handle things here is by the channel
    // state. We can only process messages on established channels.
    // We know this from the channel_assign filter by looking at the
    // channel pointer we're being passed.
    if (!m_channels.has_established_channel(event.packet.channel())) {
      LIBLOG_DEBUG("Cannot handle data message; channel is not established. Dropping message.");
      if (m_channels.has_pending_channel(event.packet.channel())) {
        // XXX: This should never happen. It's a programming bug, due to how this
        //      class *should* be used. But things that should never happen have
        //      a way of happening anyway, so... maybe we can return an action
        //      here if we do find an occurrence?
        return {ERR_STATE, PS_ERRORED};
      }

      // However, if the channel is simply unknown, we should ignore the
      // message. The reason is to simply give would-be faked packets no
      // way to close a connection by erroring out.
      return {ERR_SUCCESS, PS_IGNORED};
    }

    // If we have data to read, we also need to report read progress, at
    // least eventually.
    auto [active, to] = m_timeouts.is_timeout_active(
        {CHANNEL_PROGRESS_TIMEOUT_TAG, event.packet.channel().initiator}
    );
    if (!active) {
      m_timeouts.add({CHANNEL_PROGRESS_TIMEOUT_TAG, event.packet.channel().initiator},
          m_to_config.channel_progress_timeout);
    }

    // Since this is for a channel we know, we need to copy the data payload
    // into an event for the user to consume.
    // Note that we copy the message, hoping that it's appropriate copy method
    // is suitably lightweight.
    // We need to copy the message here (not the backing buffer) so we can
    // move it into the output event.
    auto raw = dynamic_cast<message_data *>(event.message.get());
    auto msg = std::make_unique<message_data>(*raw);
    auto result = std::make_unique<data_to_read_event_type>(
        event.packet.channel(),
        event.data,
        std::move(msg)
    );
    output_events.push_back(std::move(result));

    LIBLOG_DEBUG("Generated data event.");
    return {ERR_SUCCESS, PS_SUCCESS};
  }



  inline fsm_base::process_result
  handle_timeout(timeout_event_type const & event,
      ::channeler::pipe::action_list_type & result_actions [[maybe_unused]],
      ::channeler::pipe::event_list_type & output_events [[maybe_unused]])
  {
    // TODO
    // - set timeouts for any kind of activity, reset by incoming messages
    // - https://codeberg.org/interpeer/channeler/issues/2 ? or somewhere else?

    // The first thing to check is the tag of the timeout.
    if (event.context.tag.scope != CHANNEL_PROGRESS_TIMEOUT_TAG) {
      // Not a timeout for us.
      LIBLOG_DEBUG("Ignoring timeout; we didn't ask for it.");
      return {ERR_SUCCESS, PS_IGNORED};
    }

    // Remove and add timeout for progress.
    m_timeouts.remove({CHANNEL_PROGRESS_TIMEOUT_TAG, event.context.tag.name});
    m_timeouts.add({CHANNEL_PROGRESS_TIMEOUT_TAG, event.context.tag.name},
        m_to_config.channel_progress_timeout);

    // Missing packets trigger a data progress message already. If we reach
    // this timeout, it's not that we're missing packets, but perhaps the read
    // progress has updated.
    auto channel = m_channels.get_by_initiator(event.context.tag.name);
    auto msg = std::make_unique<message_data_progress>(m_registry,
        channel->ingress_buffer().window_start(),
        typename channelT::queue_params::missing_set{} // Nothing known to be missing
    );
    auto ev = std::make_unique<channeler::pipe::message_out_event>(
        channel->id(),
        std::move(msg)
    );
    output_events.push_back(std::move(ev));

    LIBLOG_DEBUG("Progress sent to egress.");
    return {ERR_SUCCESS, PS_SUCCESS};
  }


  inline fsm_base::process_result
  handle_data_progress_message(message_event_type const & event,
      ::channeler::pipe::action_list_type & result_actions [[maybe_unused]],
      ::channeler::pipe::event_list_type & output_events [[maybe_unused]])
  {
    if (!m_channels.has_established_channel(event.packet.channel())) {
      LIBLOG_ERROR("Progress message received for unknown or pending channel.");
      return {ERR_STATE, PS_ERRORED};
    }

    auto channel = m_channels.get(event.packet.channel());
    auto converted = dynamic_cast<message_data_progress *>(event.message.get());

    // First handle the good news: we can purge the egress buffer of everything
    // that preceeds the peer's receive window start.
    channel->egress_buffer().truncate_before(converted->receive_window_start);

    // Next, for every missing packet, we'll try to resend it. Since the packet
    // is in the queue by definition, we'll re-use the packet_out_enqueued_event
    for (auto seq : converted->resend_requests) {
      auto result = std::make_unique<packet_out_enqueued_event_type>(
          channel, seq
      );
      output_events.push_back(std::move(result));
    }

    LIBLOG_DEBUG("Progress message handled.");
    return {ERR_SUCCESS, PS_SUCCESS};
  }



  inline fsm_base::process_result
  handle_user_data_written(data_written_event_type const & event,
      ::channeler::pipe::action_list_type & result_actions,
      ::channeler::pipe::event_list_type & output_events)
  {
    LIBLOG_DEBUG("User wants to write " << event.data.size() << " Bytes.");

    // If we don't know the given channel, then we must error out via an
    // action.
    if (!m_channels.has_channel(event.channel)) {
      result_actions.push_back(std::make_unique<channeler::pipe::error_action>(
            ERR_INVALID_CHANNELID));
      LIBLOG_ERROR("Received data for unknown channel.");
      return {ERR_SUCCESS, PS_IGNORED};
    }

    // If we have a channel, we need to wrap the data into a data message and
    // produce an appropriate output event.
    // TODO split data here into individual messages if it's too large.
    auto msg = message_data::create_with_payload(m_registry, event.data);
    auto ev = std::make_unique<channeler::pipe::message_out_event>(
        event.channel,
        std::move(msg)
    );
    output_events.push_back(std::move(ev));

    LIBLOG_DEBUG("Sending data to egress.");
    return {ERR_SUCCESS, PS_SUCCESS};
  }


  inline fsm_base::process_result
  handle_progress(progress_event_type const & event,
      ::channeler::pipe::action_list_type & result_actions,
      ::channeler::pipe::event_list_type & output_events)
  {
    // If we don't know the given channel, then we must error out via an
    // action.
    if (!m_channels.has_channel(event.channel)) {
      result_actions.push_back(std::make_unique<channeler::pipe::error_action>(
            ERR_INVALID_CHANNELID));
      LIBLOG_ERROR("Requested progress for unknown channel.");
      return {ERR_SUCCESS, PS_IGNORED};
    }

    // Remove and add timeout for progress.
    m_timeouts.remove({CHANNEL_PROGRESS_TIMEOUT_TAG, event.channel.initiator});
    m_timeouts.add({CHANNEL_PROGRESS_TIMEOUT_TAG, event.channel.initiator},
        m_to_config.channel_progress_timeout);

    // If we do have a channel, create a data progress message; this triggers
    // progress reports.
    auto msg = std::make_unique<message_data_progress>(m_registry,
        event.receive_window_start, event.missing);
    auto ev = std::make_unique<channeler::pipe::message_out_event>(
        event.channel,
        std::move(msg)
    );
    output_events.push_back(std::move(ev));

    LIBLOG_DEBUG("Progress sent to egress.");
    return {ERR_SUCCESS, PS_SUCCESS};
  }


  virtual ~fsm_data() = default;

private:

  std::shared_ptr<::channeler::message_registry>  m_registry;
  ::channeler::support::timeouts &                m_timeouts;
  ::channeler::timeout_config const &             m_to_config;
  channel_set &                                   m_channels;
};

} // namespace channeler::fsm

#endif // guard
