/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG.
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_PARAMS_H
#define CHANNELER_PARAMS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <liberate/net/socket_address.h>

#include "defaults.h"
#include "pipe/filter_classifier.h"
#include "memory/packet_pool.h"
#include "support/timeouts.h"
#include "clock_params.h"
#include "channel_data.h"
#include "channels.h"

namespace channeler {

/**
 * This struct provides compile-time options (and defaults) for parameters
 * common to many different templates in the code base.
 */
template <
  typename clock_paramsT = clock_params<>,
  typename defaultsT = ::channeler::defaults<typename clock_paramsT::duration>,
  typename lock_policyT = null_lock_policy,
  typename transport_addressT = liberate::net::socket_address,
  typename transport_failure_policyT = ::channeler::pipe::null_policy<transport_addressT>,
  typename peer_failure_policyT = ::channeler::pipe::null_policy<peerid_wrapper>,
  typename timeoutsT = ::channeler::support::timeouts,
  typename sleep_funcT = typename timeoutsT::sleep_function,
  typename secretT = std::vector<byte>
>
struct params
{
  using self_type = params<
    clock_paramsT,
    defaultsT,
    lock_policyT,
    transport_addressT,
    transport_failure_policyT,
    peer_failure_policyT,
    timeoutsT,
    sleep_funcT,
    secretT
  >;

  // Clock parameters are dependent on each other, and should be used
  // grouped.
  using clock_params = clock_paramsT;

  // Encapsulate the defaults as well
  using defaults = defaultsT;

  // We should use the same lock policy everywhere.
  using lock_policy = lock_policyT;

  // The transport address type
  using transport_address = transport_addressT;

  // We also need to configure failure policies for the transport and
  // peer level.
  using transport_failure_policy = transport_failure_policyT;
  using peer_failure_policy = peer_failure_policyT;

  // Sleep and timeouts related
  using timeouts = timeoutsT;
  using sleep_function = sleep_funcT;

  // Types for secret generation
  using secret = secretT;
  using secret_generator = std::function<secret ()>;

  // Pool type to use - this is entirely derived from the above parameters.
  using pool = ::channeler::memory::packet_pool<
    defaults::POOL_BLOCK_SIZE,
    lock_policy
  >;
  using slot = typename pool::slot;

  // Channel and channel set to use - these, too, are derived
  using channel = ::channeler::channel_data<self_type>;
  using channel_set = ::channeler::channels<channel>;
};


} // namespace channeler

#endif // guard
