/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/pipe/ingress/channel_assign.h"
#include "../lib/channel_data.h"
#include "../lib/params.h"

#include <gtest/gtest.h>

#include "../../../packets.h"
#include "../../../clock.h"

using namespace test;

namespace {

// For testing
std::size_t PACKET_SIZE = packet_default_channel_size;

using test_clock_params = test::clock_params;
using test_defaults = channeler::defaults<
  typename test_clock_params::duration,
  3 // POOL_BLOCK_SIZE
>;
using address_t = uint16_t;

using test_params = ::channeler::params<
  test_clock_params,
  test_defaults,
  channeler::null_lock_policy,
  address_t
>;


struct next
{
  using input_event = channeler::pipe::queue_drain_event;

  inline channeler::pipe::action_list_type consume(std::unique_ptr<channeler::pipe::event> event)
  {
    m_event = std::move(event);
    return {};
  }

  std::unique_ptr<channeler::pipe::event> m_event = {};
};



using simple_filter_t = channeler::pipe::channel_assign_filter<
  address_t,
  test_params::defaults::POOL_BLOCK_SIZE,
  next,
  next::input_event,
  typename test_params::channel
>;


} // anonymous namespace



TEST(PipeIngressChannelAssignFilter, throw_on_invalid_event)
{
  using namespace channeler::pipe;

  next n;
  typename test_params::channel_set chs;
  simple_filter_t filter{&n, &chs};

  // Create a default event; this should not be handled.
  auto ev = std::make_unique<event>();
  ASSERT_THROW(filter.consume(std::move(ev)), ::channeler::exception);

  // The filter should also throw on a null event
  ASSERT_THROW(filter.consume(nullptr), ::channeler::exception);
}



TEST(PipeIngressChannelAssignFilter, pass_packet_default_channel)
{
  using namespace channeler::pipe;

  typename test_params::pool pool{PACKET_SIZE};

  // Copy packet data before parsing header
  auto data = pool.allocate();
  ::memcpy(data.data(), packet_default_channel, packet_default_channel_size);
  channeler::packet_wrapper packet{data.data(), data.size()};

  next n;
  typename test_params::channel_set chs;
  simple_filter_t filter{&n, &chs};

  // No data added to event.
  auto ev = std::make_unique<simple_filter_t::input_event>(123, 321, packet, data);
  ASSERT_NO_THROW(filter.consume(std::move(ev)));

  // We expect the event to be passed on verbatim, so we'll test what there
  // is in the output event.
  ASSERT_EQ(n.m_event->type, ET_QUEUE_DRAIN);
  next::input_event * ptr = reinterpret_cast<next::input_event *>(n.m_event.get());
  ASSERT_EQ(channeler::DEFAULT_CHANNELID, ptr->channel);
}



TEST(PipeIngressChannelAssignFilter, drop_packet_unknown_channel)
{
  using namespace channeler::pipe;

  typename test_params::pool pool{PACKET_SIZE};

  // Copy packet data before parsing header
  auto data = pool.allocate();
  ::memcpy(data.data(), packet_regular_channelid, packet_regular_channelid_size);
  channeler::packet_wrapper packet{data.data(), data.size()};

  next n;
  typename test_params::channel_set chs;
  simple_filter_t filter{&n, &chs};

  // No data added to event.
  auto ev = std::make_unique<simple_filter_t::input_event>(123, 321, packet, data);
  ASSERT_NO_THROW(filter.consume(std::move(ev)));

  // We expect the event to be passed on verbatim, so we'll test what there
  // is in the output event.
  ASSERT_FALSE(n.m_event);
}


TEST(PipeIngressChannelAssignFilter, pass_packet_known_channel)
{
  using namespace channeler::pipe;

  typename test_params::pool pool{PACKET_SIZE};

  // Copy packet data before parsing header
  auto data = pool.allocate();
  ::memcpy(data.data(), packet_regular_channelid, packet_regular_channelid_size);
  channeler::packet_wrapper packet{data.data(), data.size()};

  next n;
  typename test_params::channel_set chs;
  simple_filter_t filter{&n, &chs};

  // Before consuming the packet, make sure that the channel is already
  // known.
  chs.add(packet.channel());
  EXPECT_TRUE(chs.has_established_channel(packet.channel()));

  // No data added to event.
  auto ev = std::make_unique<simple_filter_t::input_event>(123, 321, packet, data);
  ASSERT_NO_THROW(filter.consume(std::move(ev)));

  // We expect the event to be passed on verbatim, so we'll test what there
  // is in the output event.
  ASSERT_TRUE(n.m_event);
  // XXX skip other verification, it's already covered above
}



// FIXME: see https://codeberg.org/interpeer/channeler/issues/13
#if 0
TEST(PipeIngressChannelAssignFilter, pass_packet_pending_channel)
{
  using namespace channeler::pipe;

  typename test_params::pool pool{PACKET_SIZE};

  // Copy packet data before parsing header
  auto data = pool.allocate();
  ::memcpy(data.data(), packet_regular_channelid, packet_regular_channelid_size);
  channeler::packet_wrapper packet{data.data(), data.size()};

  next n;
  typename test_params::channel_set chs;
  simple_filter_t filter{&n, &chs};

  // Before consuming the packet, make sure that the channel is already
  // known. We use a partial channel identifier, though.
  channeler::channelid id{packet.channel()};
  id.responder = channeler::DEFAULT_CHANNELID.responder;
  chs.add(id);
  EXPECT_TRUE(chs.has_pending_channel(id));

  // No data added to event.
  auto ev = std::make_unique<simple_filter_t::input_event>(123, 321, packet, data);
  ASSERT_NO_THROW(filter.consume(std::move(ev)));

  // We expect the packet to be passed on without a channel structure. This is
  // to avoid creating buffers, but is necessary for support of early data.
  ASSERT_TRUE(n.m_event);
  ASSERT_EQ(n.m_event->type, ET_QUEUE_DRAIN);

  next::input_event * ptr = reinterpret_cast<next::input_event *>(n.m_event.get());
  ASSERT_FALSE(ptr->channel);

  FAIL() << "TBD";
}
#endif
