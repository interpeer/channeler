/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef TEST_MESSAGES_H
#define TEST_MESSAGES_H

#include <gtest/gtest.h>

#include <channeler/packet.h>

namespace test {

/**
 * Message definitions
 */
extern channeler::byte const message_unknown[];
extern std::size_t const message_unknown_size;

extern channeler::byte const message_channel_new[];
extern std::size_t const message_channel_new_size;

extern channeler::byte const message_channel_acknowledge[];
extern std::size_t const message_channel_acknowledge_size;

extern channeler::byte const message_channel_finalize[];
extern std::size_t const message_channel_finalize_size;

extern channeler::byte const message_channel_cookie[];
extern std::size_t const message_channel_cookie_size;

extern channeler::byte const message_data[];
extern std::size_t const message_data_size;

extern channeler::byte const message_data_progress[];
extern std::size_t const message_data_progress_size;

extern channeler::byte const message_congestion_receive_window_small[];
extern std::size_t const message_congestion_receive_window_small_size;

extern channeler::byte const message_congestion_receive_window_large[];
extern std::size_t const message_congestion_receive_window_large_size;

// A block of several messages
extern channeler::byte const message_block[];
extern std::size_t const message_block_size;


/**
 * Test helper macros
 */
inline void
assert_message(std::shared_ptr<channeler::message_registry> reg,
    std::vector<channeler::byte> const & buf,
    channeler::message_type type,
    std::size_t type_bytes,
    std::size_t length_bytes)
{
  // Heavy weight parsing must succeed
  ASSERT_NO_THROW(reg->parse(buf.data(), buf.size(), false));

  // Lightweight with delayed validation
  auto [res, msg] = reg->parse(buf.data(), buf.size(), true);
  ASSERT_EQ(channeler::ERR_SUCCESS, res);

  ASSERT_EQ(msg->buffer, buf.data());
  ASSERT_EQ(msg->input_size, buf.size());

  // Payload should start after the buffer.
  ASSERT_EQ(msg->buffer + type_bytes + length_bytes, msg->payload);
  ASSERT_EQ(msg->payload_size + type_bytes + length_bytes, msg->buffer_size);
  ASSERT_EQ(msg->buffer, buf.data());
  ASSERT_EQ(msg->buffer_size, buf.size());

  // Assert type
  ASSERT_EQ(msg->type, type);
}



inline void
assert_single_byte_type_variable_size_message(std::shared_ptr<channeler::message_registry> reg,
    std::vector<channeler::byte> const & buf,
    channeler::message_type type, std::size_t length_bytes)
{
  assert_message(reg, buf, type, 1, length_bytes);
}



inline void
assert_single_byte_type_fixed_size_message(std::shared_ptr<channeler::message_registry> reg,
    std::vector<channeler::byte> const & buf,
    channeler::message_type type)
{
  assert_message(reg, buf, type, 1, 0);
}



template <typename messageT>
inline void
assert_serialization_ok(std::vector<channeler::byte> & out,
    std::unique_ptr<messageT> const & msg, std::vector<channeler::byte> const & buf)
{
  auto res = msg->serialize(&out[0], out.size());
  ASSERT_EQ(res, buf.size());

  for (std::size_t i = 0 ; i < buf.size() ; ++i) {
    ASSERT_EQ(out[i], buf[i]) << "Failure at index " << i;
  }
}


} // namespace test

#endif // guard
