/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_PIPE_EVENT_AS_H
#define CHANNELER_PIPE_EVENT_AS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include "../macros.h"

#include "event.h"

namespace channeler::pipe {

inline void
event_assert_set(char const * caller, std::unique_ptr<event> & input)
{
#if defined(DEBUG) && !defined(NDEBUG)
  if (!input) {
    LIBLOG_ERROR(caller << " received empty event.");
    throw exception{ERR_INVALID_REFERENCE};
  }
  LIBLOG_DEBUG(caller << " received event type: " << input->type);
#endif // DEBUG
}

/**
 * Helper function for asserting that a given filter receives an event of a
 * given type.
 *
 * When DEBUG is defined, this is active code. When NDEBUG is defined, only
 * the conversion is performed.
 */
template <
  typename eventT
>
inline std::unique_ptr<eventT>
event_as(char const * caller, std::unique_ptr<event> & input, event_type expected_type)
{
  event_assert_set(caller, input);

#if defined(DEBUG) && !defined(NDEBUG)
  if (input->type != expected_type) {
    LIBLOG_ERROR(caller << " received unexpected event type: " << input->type
      << " (wanted: " << expected_type << ")");
    throw exception{ERR_INVALID_PIPE_EVENT};
  }
#endif // DEBUG

  // Safe dynamic cast
  event * raw = input.release();
  std::unique_ptr<eventT> res{dynamic_cast<eventT *>(raw)};
  if (!res) {
    input.reset(raw); // For cleanup
#if defined(DEBUG) && !defined(NDEBUG)
    LIBLOG_ERROR(caller << " could not cast event type, aborting.");
    throw exception{ERR_INVALID_PIPE_EVENT};
#endif
  }
  return res;
}


/**
 * Same as above, but if the event did not match, it is instead passed through
 * to the given function.
 */
template <
  typename eventT,
  typename next_filterT,
  typename... forward_argsT
>
inline std::unique_ptr<eventT>
event_as_or_passthrough(
    char const * caller,
    std::unique_ptr<event> & input,
    event_type expected_type,
    action_list_type & actions,
    next_filterT * next,
    forward_argsT && ...forward_args)
{
  event_assert_set(caller, input);

  if (input->type != expected_type) {
#if defined(DEBUG) && !defined(NDEBUG)
    LIBLOG_DEBUG("Passing through event of type: " << input->type);
#endif
    actions = next->consume(std::move(input),
        std::forward<forward_argsT>(forward_args)...);
    return {};
  }

  // Safe dynamic cast
  event * raw = input.release();
  std::unique_ptr<eventT> res{dynamic_cast<eventT *>(raw)};
  if (!res) {
    input.reset(raw); // For cleanup
#if defined(DEBUG) && !defined(NDEBUG)
    LIBLOG_ERROR(caller << " could not cast event type, aborting.");
    throw exception{ERR_INVALID_PIPE_EVENT};
#endif
  }
  return res;
}


} // namespace channeler::pipe

#endif // guard
