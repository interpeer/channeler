/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <channeler/channelid.h>

#include <liberate/random/unsafe_bits.h>

namespace channeler {

channelid create_new_channelid()
{
  liberate::random::unsafe_bits<uint16_t> rng;

  uint16_t cur;
  do {
    cur = rng.get();
  } while (cur == DEFAULT_CHANNELID.initiator);

  channelid id{};
  id.initiator = cur;
  return id;
}



error_t complete_channelid(channelid & id)
{
  // We can only complete a channel identifier if
  // a) the initiator part *is not* default, and
  // b) the responder part *is* default.
  if (!id.has_initiator()) {
    return ERR_INVALID_CHANNELID;
  }
  if (id.has_responder()) {
    return ERR_INVALID_CHANNELID;
  }

  // Generate
  liberate::random::unsafe_bits<uint16_t> rng;

  uint16_t cur;
  do {
    cur = rng.get();
  } while (cur == DEFAULT_CHANNELID.initiator);

  id.responder = cur;
  return ERR_SUCCESS;
}


} // namespace channeler
