/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_CHANNEL_DATA_H
#define CHANNELER_CHANNEL_DATA_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <map>
#include <vector>

#include <channeler/channelid.h>

#include <liberate/random/unsafe_bits.h>

#include "memory/packet_queue.h"

namespace channeler {

using namespace std::chrono_literals;

namespace {

template <
  typename policy_paramsT,
  typename... ctor_argsT
>
inline std::unique_ptr<memory::packet_queue_base<policy_paramsT>>
create_best_packet_queue(capabilities_t const & caps, ctor_argsT && ...ctor_args)
{
  LIBLOG_DEBUG("Supposed to create packet buffer with capabilities: " << caps);

  // Capabilities contain more bits than we're handling here. Let's just strip
  // them to the three relevant ones.
  std::bitset<3> bits;
  bits[0] = caps[CAP_RESEND];
  bits[1] = caps[CAP_ORDERED];
  bits[2] = caps[CAP_CLOSE_ON_LOSS];

  // With this, we can create simpler if blocks
  if (bits == 0b000) {
    return std::make_unique<
      memory::no_resend_fifo_ignore_loss_queue<policy_paramsT>
    >(std::forward<ctor_argsT>(ctor_args)...);
  }

  if (bits == 0b100) {
    return std::make_unique<
      memory::no_resend_fifo_close_on_loss_queue<policy_paramsT>
    >(std::forward<ctor_argsT>(ctor_args)...);
  }

  if (bits == 0b010) {
    return std::make_unique<
      memory::no_resend_sequential_ignore_loss_queue<policy_paramsT>
    >(std::forward<ctor_argsT>(ctor_args)...);
  }

  if (bits == 0b110) {
    return std::make_unique<
      memory::no_resend_sequential_close_on_loss_queue<policy_paramsT>
    >(std::forward<ctor_argsT>(ctor_args)...);
  }

  if (bits == 0b001) {
    return std::make_unique<
      memory::exponential_backoff_fifo_ignore_loss_queue<policy_paramsT>
    >(std::forward<ctor_argsT>(ctor_args)...);
  }

  if (bits == 0b101) {
    return std::make_unique<
      memory::exponential_backoff_fifo_close_on_loss_queue<policy_paramsT>
    >(std::forward<ctor_argsT>(ctor_args)...);
  }

  if (bits == 0b011) {
    return std::make_unique<
      memory::exponential_backoff_sequential_ignore_loss_queue<policy_paramsT>
    >(std::forward<ctor_argsT>(ctor_args)...);
  }

  if (bits == 0b111) {
    return std::make_unique<
      memory::exponential_backoff_sequential_close_on_loss_queue<policy_paramsT>
    >(std::forward<ctor_argsT>(ctor_args)...);
  }

  // TODO
  // Extend capabilities to gapped variants
  LIBLOG_ERROR("Unhandled capability combination: " << caps);
  return {};
}

} // anonymous namespace

/**
 * This data structure holds internal channel information, such as the
 * channel's buffers.
 *
 * TODO this is expected to change a lot.
 */
template <
  typename paramsT
>
struct channel_data
{
  using params = paramsT;

  using queue_params = policy::policy_params<
    params::defaults::POOL_BLOCK_SIZE,
    typename params::clock_params::clock, // FIXME just pass clock params, not individually
    sequence_no_t,
    typename params::clock_params::time_point,
    typename params::clock_params::duration,
    typename params::lock_policy
  >;

  using queue_entry = typename queue_params::queue_entry;
  using slot_type = typename queue_params::slot;
  using clock = typename queue_params::clock;
  using time_point = typename queue_params::time_point;

  using resend_callback = std::function<
    // - A timestamp
    // - Channel ID
    // - Numer of collissions
    // - Sequence number of the window start
    // - Set of missing sequence numbers in the window
    error_t
    (
      typename params::clock_params::time_point const &,
      channelid const &,
      std::size_t,
      sequence_no_t,
      typename queue_params::missing_set const &
    )
  >;

  /***************************************************************************
   * Basic channel information
   */
  inline channel_data(channelid const & id,
      capabilities_t const & caps,
      resend_callback resend = {},
      typename params::lock_policy * lock = nullptr
    )
    : m_id{id}
    , m_capabilities{caps}
    , m_lock{lock}
    , m_ingress_buffer{false, caps,
        std::bind(&channel_data<params>::resend_forwarder, this,
            std::placeholders::_1,
            std::placeholders::_2,
            std::placeholders::_3,
            std::placeholders::_4
        )
      }
    , m_egress_buffer{true, caps}
    , m_resend{resend}
  {
  }



  inline channelid const & id() const
  {
    return m_id;
  }


  inline capabilities_t const & capabilities() const
  {
    return m_capabilities;
  }


  /***************************************************************************
   * Packet buffers
   *
   * The class is a simple wrapper around the internal container providing the
   * common operations.
   */
  struct packet_buffer
  {
  public:
    inline packet_buffer(
        bool manage_sequence,
        capabilities_t const & caps,
        typename queue_params::resend_callback resend = {}
      )
      : m_buffer{create_best_packet_queue<queue_params>(
          caps,
          params::defaults::RECEIVE_WINDOW_SIZE,
          params::defaults::ROUND_TRIP_TIMEOUT_USEC,
          params::defaults::MAX_RESEND_COLLISIONS,
          typename params::clock_params::time_point{}, // Initial clock; is overridden later (before use)
          resend
        )}
      , m_manage_sequence{manage_sequence}
      , m_next{0}
    {
      // Generate a random initial egress packet number
      liberate::random::unsafe_bits<typename queue_params::sequence_no_t> rng;
      do {
        m_next = rng.get();
      } while (0 == m_next);
    }


    inline error_t push(packet_wrapper const & packet, slot_type slot,
        time_point const & now)
    {
      // If we're not managing the sequence, this is easy.
      if (!m_manage_sequence) {
        return m_buffer->push_back(now, packet, slot);
      }

      // Otherwise, create a (shallow) copy of the input packet, and update
      // the sequence number.
      packet_wrapper pkt{packet};
      pkt.sequence_no() = m_next;
      m_next = support::detail::window_increment(m_next);

      auto err = pkt.update_checksum();
      if (ERR_SUCCESS != err) {
        LIBLOG_DEBUG("Error updating packet checksum to reflect sequence number!");
        return err;
      }

      return m_buffer->push_back(now, pkt, slot);
    }


    using retrieve_result = typename memory::packet_queue_base<queue_params>::retrieve_result;

    /**
     * In ingress buffers, retrieval automatically moves the window forward, so
     * we use pop() instead of retrieve() and truncate_before().
     */
    inline retrieve_result pop(time_point const & now)
    {
      return m_buffer->pop_front(now);
    }

    /**
     * Egress buffers are manually managed; here, retrieve() and
     * truncate_before() are used to manage the window.
     */
    inline retrieve_result retrieve(sequence_no_t const & seqno, time_point const & now)
    {
      return m_buffer->retrieve(now, seqno);
    }


    // The wrapper functions are for keeping filter tests simpler
    inline error_t push(packet_wrapper const & packet, slot_type slot)
    {
      LIBLOG_WARN("This function is for internal testing purposes *only*!");
      return push(packet, slot, clock::now());
    }

    inline retrieve_result pop()
    {
      LIBLOG_WARN("This function is for internal testing purposes *only*!");
      return pop(clock::now());
    }

    inline retrieve_result retrieve(sequence_no_t const & seqno)
    {
      LIBLOG_WARN("This function is for internal testing purposes *only*!");
      return retrieve(seqno, clock::now());
    }


    // STL-ish access
    inline bool empty() const
    {
      return m_buffer->empty();
    }


    inline std::size_t size() const
    {
      return m_buffer->size();
    }

    inline sequence_no_t window_start() const
    {
      return m_buffer->window_start();
    }

    inline void truncate_before(sequence_no_t window_start)
    {
      // TODO: it would be good to get the current time as a parameter, but FSMs
      //       which trigger this function have no concept of time.
      m_buffer->shift_window(clock::now(), window_start);
    }

    inline std::size_t window_size() const
    {
      return m_buffer->window_size();
    }

    inline bool set_window_size(std::size_t window_size)
    {
      return m_buffer->set_window_size(window_size);
    }

    inline std::size_t available_size() const
    {
      auto cur = size();
      auto max = window_size();
      if (cur > max) {
        return 0;
      }
      return max - cur;
    }

  private:
    friend struct channel_data<paramsT>;

    std::unique_ptr<
      memory::packet_queue_base<queue_params>
    >                                     m_buffer;
    bool                                  m_manage_sequence;
    typename queue_params::sequence_no_t  m_next;
  };


  inline packet_buffer const & egress_buffer() const
  {
    return m_egress_buffer;
  }


  inline packet_buffer const & ingress_buffer() const
  {
    return m_ingress_buffer;
  }


  inline packet_buffer & egress_buffer()
  {
    return m_egress_buffer;
  }


  inline packet_buffer & ingress_buffer()
  {
    return m_ingress_buffer;
  }


  inline void set_initial_clock(typename queue_params::time_point const & initial)
  {
    m_ingress_buffer.m_buffer->set_initial_clock(initial);
    m_egress_buffer.m_buffer->set_initial_clock(initial);
  }

  inline void set_initial_clock()
  {
    LIBLOG_WARN("This function is for internal testing purposes *only*!");
    return set_initial_clock(clock::now());
  }


  /***************************************************************************
   * Message buffers
   *
   * The class is a simple wrapper around the internal container providing the
   * common operations.
   */
  struct message_buffer
  {
  public:
    inline bool have_pending() const
    {
      return !m_list.empty();
    }

    inline void enqueue(std::unique_ptr<message> msg)
    {
      // TODO order e.g. by timestamp? Most likely by insertion time is
      // good enough.
      m_list.push_back(std::move(msg));
    }

    inline std::unique_ptr<message> dequeue()
    {
      auto ret = std::move(m_list.front());
      m_list.pop_front();
      return ret;
    }

    inline std::size_t next_size() const
    {
      auto iter = m_list.begin();
      if (iter == m_list.end()) {
        return 0;
      }

      return (*iter)->serialized_size();
    }

  private:
    using list_type = std::list<std::unique_ptr<message>>;
    list_type m_list = {};
  };


  inline message_buffer & egress_messages()
  {
    return m_egress_messages;
  }

  inline message_buffer const & egress_messages() const
  {
    return m_egress_messages;
  }

  inline message_buffer & ingress_messages()
  {
    return m_ingress_messages;
  }

  inline message_buffer const & ingress_messages() const
  {
    return m_ingress_messages;
  }


  /***************************************************************************
   * API hooks
   */
  inline void resend_forwarder(
      typename queue_params::time_point const & now,
      std::size_t const & collisions,
      sequence_no_t const & window_start,
      typename queue_params::missing_set const & missing
  )
  {
    if (!m_resend) {
      return;
    }

    auto err = m_resend(now, m_id, collisions, window_start, missing);
    if (ERR_SUCCESS != err) {
      LIBLOG_ET("Requesting a resend for missing packets failed!", err);
    }
  }


  inline void set_resend_callback(resend_callback resend)
  {
    m_resend = resend;
  }



private:

  channelid                             m_id;
  capabilities_t                        m_capabilities;
  typename params::lock_policy *        m_lock;

  // Packet buffers for ingress and egress
  packet_buffer                         m_ingress_buffer;
  packet_buffer                         m_egress_buffer;

  // Message buffers for ingress and egress
  message_buffer                        m_egress_messages = {};
  message_buffer                        m_ingress_messages = {}; // Typically store user data!

  // Resend function
  resend_callback                       m_resend;
};

} // namespace channeler

#endif // guard
