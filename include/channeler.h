/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_CHANNELER_H
#define CHANNELER_CHANNELER_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

/**
 * Which platform are we on?
 */
#if !defined(CHANNELER_PLATFORM_DEFINED)
#  if defined(_WIN32) || defined(__WIN32__) || defined(__WINDOWS__)
#    define CHANNELER_WIN32
#  else
#    define CHANNELER_POSIX
#  endif
#  define CHANNELER_PLATFORM_DEFINED
#endif

/**
 * Anonymous structs are a C++ extension for GNU
 */
#if defined(__GNUC__)
#  define CHANNELER_ANONYMOUS __extension__
#else
#  define CHANNELER_ANONYMOUS
#endif

/**
 * Decide what to include globally
 **/
#if defined(CHANNELER_WIN32)
// Include windows.h with minimal definitions
#  ifndef WIN32_LEAN_AND_MEAN
#    define WIN32_LEAN_AND_MEAN
#    define __UNDEF_LEAN_AND_MEAN
#  endif
#  define NOMINMAX
// Unicode builds
#  define UNICODE
#  define _UNICODE
#  include <windows.h>
#  include <WinDef.h>
#  ifdef __UNDEF_LEAN_AND_MEAN
#    undef WIN32_LEAN_AND_MEAN
#    undef __UNDEF_LEAN_AND_MEAN
#  endif
#endif

// Visibility macros are used by all, so they must come first.
#include <channeler/visibility.h>

// We want standard int types across the board. We also want a standard
// byte type.
#include <liberate/types.h>
namespace channeler {
  using byte = ::liberate::types::byte;
} // namespace channeler

// Not all, but the very basic headers are always included.
#include <channeler/error.h>
#include <channeler/version.h>

// TODO: include API headers if necessary

#endif // guard
