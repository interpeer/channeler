/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_MESSAGE_DATA_H
#define CHANNELER_MESSAGE_DATA_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <set>

#include <channeler/message.h>
#include <channeler/message/basic.h>

#include <channeler/channelid.h>
#include <channeler/capabilities.h>
#include <channeler/cookie.h>
#include <channeler/packet.h>

namespace channeler {

/**
 * Register message types used by the data FSM.
 */
CHANNELER_API
error_t
register_data_types(std::shared_ptr<message_registry> registry);


/**
 * The data message transports arbitrary payloads across its channel.
 *
 * Unlike the base message type, data messages can carry (large) payloads.
 * In order to make them inexpensive to copy, the payload (if present) is
 * treated as copy-on-write. That is, copies of message_data share the same
 * backing payload, but set_payload() overwrites this.
 */
struct CHANNELER_API message_data
  : public message
{
  // TODO
  // - maybe make constructors in other messages private and offer
  //   the same kind of interface
  // - deal with ownership somehow

  inline message_data(message && msg)
    : message{std::forward<message &&>(msg)}
  {
  }

  inline message_data(message_data const &) = default;
  inline message_data(message_data &&) = default;

  virtual ~message_data() = default;

  /**
   * Create a data message with a payload backing buffer of the given size.
   * The second version behaves as if set_payload() had also been called.
   */

  static std::unique_ptr<message_data>
  create_with_payload(std::shared_ptr<message_registry> registry,
      byte const * input, std::size_t size);

  template <
    typename byteT,
    typename = typename std::enable_if<
      std::is_integral<byteT>::value
    >::type,
    typename = typename std::enable_if<
      sizeof(byte) == sizeof(byteT)
    >::type
  >
  static inline std::unique_ptr<message_data>
  create_with_payload(std::shared_ptr<message_registry> registry,
      byteT const * input, std::size_t size)
  {
    return create_with_payload(registry,
        reinterpret_cast<byte const *>(input), size);
  }


  template <
    typename byteT,
    typename = typename std::enable_if<
      std::is_integral<byteT>::value
    >::type,
    typename = typename std::enable_if<
      sizeof(byte) == sizeof(byteT)
    >::type
  >
  static inline std::unique_ptr<message_data>
  create_with_payload(std::shared_ptr<message_registry> registry,
      std::vector<byteT> const & container)
  {
    return create_with_payload(
        registry,
        reinterpret_cast<byte const *>(container.data()),
        container.size()
    );
  }


  /**
   * Serialize, as in the base message type.
   */
  virtual std::size_t serialize(byte * output, std::size_t output_max) const;

  template <
    typename byteT,
    typename = typename std::enable_if<
      std::is_integral<byteT>::value
    >::type,
    typename = typename std::enable_if<
      sizeof(byte) == sizeof(byteT)
    >::type
  >
  inline std::size_t
  serialize(byteT * output, std::size_t output_max) const
  {
    return serialize(reinterpret_cast<byte *>(output), output_max);
  }


  template <
    typename byteT,
    typename = typename std::enable_if<
      std::is_integral<byteT>::value
    >::type,
    typename = typename std::enable_if<
      sizeof(byte) == sizeof(byteT)
    >::type
  >
  inline std::size_t
  serialize(std::vector<byteT> & container) const
  {
    return serialize(
        reinterpret_cast<byte *>(container.data()),
        container.size()
    );
  }


  /**
   * Set the payload. This is a glorified memcpy(). Inputs larger than the
   * payload size will be truncated. Inputs smaller than the payload size
   * will *not* be padded.
   *
   * set_payload() reallocates the backing payload buffer.
   */
  std::size_t set_payload(byte const * input, std::size_t _input_size);

  template <
    typename byteT,
    typename = typename std::enable_if<
      std::is_integral<byteT>::value
    >::type,
    typename = typename std::enable_if<
      sizeof(byte) == sizeof(byteT)
    >::type
  >
  inline std::size_t
  set_payload(byteT const * input, std::size_t _input_size)
  {
    return set_payload(reinterpret_cast<byte const *>(input), _input_size);
  }


  template <
    typename byteT,
    typename = typename std::enable_if<
      std::is_integral<byteT>::value
    >::type,
    typename = typename std::enable_if<
      sizeof(byte) == sizeof(byteT)
    >::type
  >
  inline std::size_t
  set_payload(std::vector<byteT> & container)
  {
    return set_payload(
        reinterpret_cast<byte *>(container.data()),
        container.size()
    );
  }


private:
  // Used with create_wth_payload
  message_data(std::shared_ptr<message_registry> _registry);

  using buffer_type = std::vector<byte>;
  std::shared_ptr<buffer_type>  owned_buffer;
};


/**
 * Report progress, i.e. basic information about the state of the
 * receive buffer.
 */
struct CHANNELER_API message_data_progress
  : public message
{
public:
  using request_set = std::set<sequence_no_t>;

  sequence_no_t   receive_window_start;
  request_set     resend_requests;

  message_data_progress(
      std::shared_ptr<message_registry> _registry,
      sequence_no_t const & winstart,
      request_set const & requests);

  inline message_data_progress(message && msg)
    : message{std::forward<message &&>(msg)}
  {
  }

  virtual ~message_data_progress() = default;

  virtual std::size_t serialize(byte * output, std::size_t output_max) const;
};


} // namespace channeler

#endif // guard
