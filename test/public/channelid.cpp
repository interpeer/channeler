/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <channeler/channelid.h>

#include <gtest/gtest.h>

TEST(ChannelID, default_constructed)
{
  auto id = channeler::channelid{};
  ASSERT_EQ(id, channeler::DEFAULT_CHANNELID);
  ASSERT_EQ(id.full, channeler::DEFAULT_CHANNELID.full);
  ASSERT_EQ(id.initiator, channeler::DEFAULT_CHANNELID.initiator);
  ASSERT_EQ(id.responder, channeler::DEFAULT_CHANNELID.responder);

  ASSERT_FALSE(id.has_initiator());
  ASSERT_FALSE(id.has_responder());
}


TEST(ChannelID, create)
{
  auto id = channeler::create_new_channelid();
  ASSERT_NE(id, channeler::DEFAULT_CHANNELID);
  ASSERT_TRUE(id.has_initiator());
  ASSERT_FALSE(id.has_responder());
}


TEST(ChannelID, complete_bad_initiator)
{
  auto id = channeler::DEFAULT_CHANNELID;
  ASSERT_EQ(channeler::ERR_INVALID_CHANNELID,
      channeler::complete_channelid(id));
}


TEST(ChannelID, complete_bad_responder)
{
  channeler::channelid id{};
  id.full = 0xdeadd00duL;
  ASSERT_EQ(channeler::ERR_INVALID_CHANNELID,
      channeler::complete_channelid(id));
}


TEST(ChannelID, complete)
{
  auto id = channeler::create_new_channelid();

  ASSERT_EQ(channeler::ERR_SUCCESS,
      channeler::complete_channelid(id));

  ASSERT_TRUE(id.has_initiator());
  ASSERT_TRUE(id.has_responder());
}
