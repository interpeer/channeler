/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_PIPE_INGRESS_QUEUE_DRAINING_H
#define CHANNELER_PIPE_INGRESS_QUEUE_DRAINING_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <memory>
#include <set>

#include "../../memory/packet_pool.h"
#include "../../channels.h"
#include "../event.h"
#include "../action.h"
#include "../filter_classifier.h"
#include "../event_as.h"

#include <channeler/packet.h>
#include <channeler/error.h>


namespace channeler::pipe {


/**
 * Drains a channel's ingress queue.
 *
 * Expects the next_eventT constructor to take
 * - an (optional) channel data structure pointer
 * - transport source address
 * - transport destination address
 * - packet_wrapper
 * - a pool slot
 *
 * such as e.g. dequeued_packet_event
 */
template <
  std::size_t POOL_BLOCK_SIZE,
  typename next_filterT,
  typename next_eventT,
  typename channelT
>
struct queue_draining_filter
{
  using input_event = queue_drain_event;
  using channel_set = ::channeler::channels<channelT>;

  inline queue_draining_filter(next_filterT * next, channel_set * chs)
    : m_next{next}
    , m_channel_set{chs}
  {
    // TODO make reference then, not pointer
    if (nullptr == m_channel_set) {
      throw exception{ERR_INVALID_REFERENCE};
    }
  }


  template <typename... argsT>
  inline action_list_type
  consume(std::unique_ptr<event> unchecked, argsT && ...args)
  {
    auto in = event_as<input_event>("ingress:queue_draining", unchecked, ET_QUEUE_DRAIN);

    action_list_type actions;

    // First, we need a channel for the given input event. This filter should
    // only be invoked for established channels, so let's make sure that's the
    // case here.
    auto ptr = m_channel_set->get(in->channel);
    if (!ptr) {
      LIBLOG_ERROR("queue_drain filter invoked with an unknown channel id.");
      actions.push_back(std::make_unique<error_action>(ERR_INVALID_CHANNELID));
      return actions;
    }

    // We need to assemble the action list from invoking the next filter once
    // per packet in the queue.
    while (true) {
      auto [optval, close] = ptr->ingress_buffer().pop(
          std::forward<argsT>(args)...);

      // Let's see if we need to close this channel due to packet loss, etc.
      if (close) {
        // Remove the channel from the channel set
        m_channel_set->remove(ptr->id());

        // Report packet loss and channel closure
        actions.push_back(std::make_unique<notify_channel_closed_action>(
              ptr->id(),
              ERR_PACKET_LOSS
        ));
        return actions;
      }

      // If there is no value, we can safely assume that at this time, there
      // is no new event to construct, and we do not need to read from the
      // buffer further.
      if (!optval.has_value()) {
        if (!ptr->ingress_buffer().empty()) {
          LIBLOG_DEBUG("Ingress buffer cannot be processed at the moment, and "
              "contains " << ptr->ingress_buffer().size() << " entries.");
        }
        break;
      }

      auto & val = optval.value();

      // Construct and pass on a new event.
      auto next = std::make_unique<next_eventT>(
          val.packet,
          val.data,
          ptr // TODO maybe we don't need to pass this on. Which could mean we
              //      may not need the next_eventT type.
      );

      auto acts = m_next->consume(std::move(next), std::forward<argsT>(args)...);

      // TODO we should probably release the pop'd packet only here, if we know acts does
      // not contain errors.
      //
      merge_actions(actions, acts);
    }

    // TODO actually, it would be best to release all processed packets once, here, since
    // we'll be truncating??

    // We're done
    return actions;
  }


  next_filterT *  m_next;
  channel_set *   m_channel_set;
};


} // namespace channeler::pipe

#endif // guard
