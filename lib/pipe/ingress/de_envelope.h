/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_PIPE_INGRESS_DE_ENVELOPE_H
#define CHANNELER_PIPE_INGRESS_DE_ENVELOPE_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <memory>

#include "../../memory/packet_pool.h"
#include "../event.h"
#include "../action.h"
#include "../event_as.h"

#include <channeler/packet.h>
#include <channeler/error.h>


namespace channeler::pipe {

/**
 * The de-envelope filter raw buffers, parses packet headers, and passes on the
 * result.
 *
 * Expects the next_eventT constructor to take
 * - transport source address
 * - transport destination address
 * - public headers
 * - a pool slot
 *
 * such as e.g. parsed_header_event
 */
template <
  typename addressT,
  std::size_t POOL_BLOCK_SIZE,
  typename next_filterT,
  typename next_eventT
>
struct de_envelope_filter
{
  using input_event = raw_buffer_event<addressT, POOL_BLOCK_SIZE>;

  inline de_envelope_filter(next_filterT * next)
    : m_next{next}
  {
  }


  template <typename... argsT>
  inline action_list_type
  consume(std::unique_ptr<event> unchecked, argsT && ...args)
  {
    auto in = event_as<input_event const>("ingress:de_envelope", unchecked, ET_RAW_BUFFER);

    // If there is no data passed, we should also throw.
    if (nullptr == in->data.data()) {
      throw exception{ERR_INVALID_REFERENCE};
    }

    // Parse header data, and pass on a new event to the next filter
    ::channeler::public_header_fields header{in->data.data()};

    auto next = std::make_unique<next_eventT>(
        in->transport.source,
        in->transport.destination,
        header,
        in->data);
    return m_next->consume(std::move(next), std::forward<argsT>(args)...);
  }


  next_filterT *  m_next;
};


} // namespace channeler::pipe

#endif // guard
