/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_MESSAGE_BASIC_H
#define CHANNELER_MESSAGE_BASIC_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <ostream>

#include <channeler/message.h>

namespace channeler {

/**
 * Basic message types used by the core FSMs, e.g. for channel negotation and
 * sending data, etc.
 */
enum CHANNELER_API basic_message_type : message_type
{
  // Channel negotiation
  MSG_CHANNEL_NEW = 10,
  MSG_CHANNEL_ACKNOWLEDGE,
  MSG_CHANNEL_FINALIZE,
  MSG_CHANNEL_COOKIE,

  // Transmission related
  MSG_DATA = 20,
  MSG_DATA_PROGRESS,

  // Congestion control related
  MSG_CONGESTION_RECEIVE_WINDOW = 30,

  // TODO
  // MSG_START_ENCRYPTION
  // https://codeberg.org/interpeer/channeler/issues/3
};


inline std::ostream &
operator<<(std::ostream & os, basic_message_type type)
{
  os << static_cast<message_type>(type);
  return os;
}


} // namespace channeler

#endif // guard
