/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_PIPE_INGRESS_CHANNEL_ASSIGN_H
#define CHANNELER_PIPE_INGRESS_CHANNEL_ASSIGN_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <memory>
#include <set>

#include "../../memory/packet_pool.h"
#include "../../channels.h"
#include "../event.h"
#include "../action.h"
#include "../filter_classifier.h"
#include "../event_as.h"

#include <channeler/packet.h>
#include <channeler/error.h>


namespace channeler::pipe {


/**
 * Assigns a packet to a channel.
 *
 * This maps packets to the internal channel data structure held in the channel
 * set. It also contains the logic that marks the packet as beloging to the
 * default channel an existing or a pending channel - and if none of that is
 * true, it drops the packet and issues punitive actions against the sender.
 *
 * In future (e.g. https://codeberg.org/interpeer/channeler/issues/2 ) it should
 * also handle flow control messages.
 *
 * Expects the next_eventT constructor to take
 * - a channel id
 *
 * such as e.g. queue_drain_event
 */
template <
  typename addressT,
  std::size_t POOL_BLOCK_SIZE,
  typename next_filterT,
  typename next_eventT,
  typename channelT,
  typename peer_failure_policyT = null_policy<peerid_wrapper>,
  typename transport_failure_policyT = null_policy<addressT>
>
struct channel_assign_filter
{
  using input_event = decrypted_packet_event<addressT, POOL_BLOCK_SIZE>;
  using channel_set = ::channeler::channels<channelT>;
  using classifier = filter_classifier<addressT, peer_failure_policyT, transport_failure_policyT>;

  inline channel_assign_filter(next_filterT * next, channel_set * chs,
      peer_failure_policyT * peer_p = nullptr,
      transport_failure_policyT * trans_p = nullptr)
    : m_next{next}
    , m_channel_set{chs}
    , m_classifier{peer_p, trans_p}
  {
    // TODO make reference then, not pointer
    if (nullptr == m_channel_set) {
      throw exception{ERR_INVALID_REFERENCE};
    }
  }


  template <typename... argsT>
  inline action_list_type
  consume(std::unique_ptr<event> unchecked, argsT && ...args)
  {
    auto in = event_as<input_event>("ingress:channel_assign", unchecked, ET_DECRYPTED_PACKET);

    // If there is no data passed, we should also throw.
    if (nullptr == in->data.data()) {
      throw exception{ERR_INVALID_REFERENCE};
    }

    // Channels are *added* in the protocol handling filter. Here, we
    // short-circuit this for just the default channel.
    typename channel_set::channel_ptr ptr;
    if (in->packet.channel() == DEFAULT_CHANNELID) {
      auto [err, chanopt] = m_channel_set->add(in->packet.channel());
      if (chanopt.has_value()) {
        ptr = chanopt.value();
        // If the channel was newly created, set the initial clock on it
        if (ERR_SUCCESS == err) {
          LIBLOG_DEBUG("New channel: " << in->packet.channel()
              << "; setting initial clock.");
          ptr->set_initial_clock(std::forward<argsT>(args)...);
        }
        else {
          LIBLOG_DEBUG("Existing channel: " << in->packet.channel());
        }
      }
      else {
        LIBLOG_ERROR("Channel does not exist: " << in->packet.channel());
        // The packet is invalid, and we should drop it.
        return m_classifier.process(in->transport.source,
            in->transport.destination, in->packet);
      }
    }
    else {
      ptr = m_channel_set->get(in->packet.channel());
    }

    // There are three cases we need to consider:
    // a) We have a full channel. In that case, we need to assign the packet
    //    to said channel.
    // b) We have a pending channel and early data. This can only happen to
    //    initiators. Since the responder then clearly accepted our channel,
    //    we could pass the channel structure. But later filters should
    //    be able to distinguish this, so we pass an empty channel pointer.
    // c) We do not have either kind of channel. This we must reject.
    if (!ptr) {
      return m_classifier.process(in->transport.source,
          in->transport.destination, in->packet);
    }

    // If we have an established channel here, we can place the packet in a
    // buffer. Otherwise, we clear the channel structure again to indicate
    // pending status.
    if (m_channel_set->has_established_channel(in->packet.channel())) {
      auto err = ptr->ingress_buffer().push(in->packet, in->data,
          std::forward<argsT>(args)...);
      if (ERR_PACKET_INVALID == err) {
        // If the packet was invalid, inform the filter classifier. But also
        // produce an error here.
        auto ret = m_classifier.process(in->transport.source,
            in->transport.destination, in->packet);
        ret.push_back(std::make_unique<error_action>(err));
        return ret;
      }

      if (ERR_SUCCESS != err) {
        // TODO handle ERR_PACKET_REJECTED
        // TODO: in future versions, we'll need to return flow control information
        //       to the sender.
        //       https://codeberg.org/interpeer/channeler/issues/2
        return {};
      }

      // Construct and pass on a new event.
      auto next = std::make_unique<next_eventT>(ptr->id());
      return m_next->consume(std::move(next), std::forward<argsT>(args)...);
    }

    // TODO This may be only interesting for early data and the cookie message;
    //      we'll have to see about this.
    //      https://codeberg.org/interpeer/channeler/issues/13

    // TODO: Pass directly to message parsing/state handling instead.

#if 0
    // If we do not have an established channel, we still need to pass the
    // packet on, but we need to bypass the queue. This may seem a little
    // odd (what's the queue for after all?), but is valid for e.g. channel
    // handshakes.
    ptr.reset();

    // Construct and pass on a new event.
    auto next = std::make_unique<next_eventT>(
        in->transport.source,
        in->transport.destination,
        in->packet,
        in->data,
        ptr // TODO maybe we don't need to pass this on. Which could mean we
            //      may not need the next_eventT type.
    );

    return m_next->consume(std::move(next), std::forward<argsT>(args)...);
#endif
    LIBLOG_ERROR("TBD: " << in->packet.channel());
    throw std::runtime_error("aaaaaaaaaaaaaaaaah");
    return {};
  }


  next_filterT *  m_next;
  channel_set *   m_channel_set;
  classifier      m_classifier; // TODO ptr or ref for shared state?
                                // https://codeberg.org/interpeer/channeler/issues/21
};


} // namespace channeler::pipe

#endif // guard
