/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <channeler/message/channels.h>

#include <channeler/channelid.h>
#include <channeler/capabilities.h>
#include <channeler/cookie.h>

#include <liberate/serialization/integer.h>
#include <liberate/serialization/varint.h>

#include <liberate/logging.h>

namespace channeler {


error_t
register_channel_types(std::shared_ptr<message_registry> registry)
{
  static message_registry::message_info info[] = {
    // *****************************************
    {
      MSG_CHANNEL_NEW,
      []() -> ssize_t
      {
        // - channelid.initiator
        // - cookie1
        return sizeof(channelid::half_type) + sizeof(cookie_serialize);
      },
      [](std::unique_ptr<message> basic) -> std::tuple<error_t, std::unique_ptr<message>>
      {
        auto ret = upgrade_message<message_channel_new>(std::move(basic));

        byte const * offset = ret->payload;
        std::size_t remaining = ret->payload_size;

        // First comes the partial channel id
        auto used = liberate::serialization::deserialize_int(ret->initiator_part,
            offset, remaining);
        if (used != sizeof(ret->initiator_part)) {
          LIBLOG_ERROR("Could not deserialize initiator part of channel identiifer.");
          return {ERR_DECODE, nullptr};
        }
        offset += used;
        remaining -= used;

        // And the cookie
        cookie_serialize s;
        used = liberate::serialization::deserialize_int(s,
            offset, remaining);
        if (used != sizeof(s)) {
          LIBLOG_ERROR("Could not deserialize cookie1.");
          return {ERR_DECODE, nullptr};
        }
        ret->cookie1 = s;

        offset += used;
        remaining -= used;

        if (remaining > 0) {
          // We didn't consume the entire payload
          LIBLOG_ERROR("Unused payload part.");
          return {ERR_DECODE, nullptr};
        }

        return {ERR_SUCCESS, std::move(ret)};
      },
    },
    // *****************************************
    {
      MSG_CHANNEL_ACKNOWLEDGE,
      []() -> ssize_t
      {
        // - channelid.full
        // - cookie1
        // - cookie2
        return sizeof(channelid::full_type) + sizeof(cookie_serialize) * 2;
      },
      [](std::unique_ptr<message> basic) -> std::tuple<error_t, std::unique_ptr<message>>
      {
        auto ret = upgrade_message<message_channel_acknowledge>(std::move(basic));

        byte const * offset = ret->payload;
        std::size_t remaining = ret->payload_size;

        // First comes the channel id
        auto used = liberate::serialization::deserialize_int(ret->id.full,
            offset, remaining);
        if (used != sizeof(ret->id.full)) {
          LIBLOG_ERROR("Unable to decode channel identifier.");
          return {ERR_DECODE, nullptr};
        }
        offset += used;
        remaining -= used;

        // And cookie1
        cookie_serialize s;
        used = liberate::serialization::deserialize_int(s,
            offset, remaining);
        if (used != sizeof(s)) {
          LIBLOG_ERROR("Unable to decode cookie1.");
          return {ERR_DECODE, nullptr};
        }
        ret->cookie1 = s;

        offset += used;
        remaining -= used;

        // cookie2
        used = liberate::serialization::deserialize_int(s,
            offset, remaining);
        if (used != sizeof(s)) {
          LIBLOG_ERROR("Unable to decode cookie2.");
          return {ERR_DECODE, nullptr};
        }
        ret->cookie2 = s;

        offset += used;
        remaining -= used;

        if (remaining > 0) {
          // We didn't consume the entire payload
          LIBLOG_ERROR("Unused payload part.");
          return {ERR_DECODE, nullptr};
        }

        return {ERR_SUCCESS, std::move(ret)};
      },
    },
    // *****************************************
    {
      MSG_CHANNEL_FINALIZE,
      []() -> ssize_t
      {
        // - channelid.full
        // - cookie2
        // - capability bits
        return sizeof(channelid::full_type) + sizeof(cookie_serialize)
          + sizeof(capability_bits_t);
      },
      [](std::unique_ptr<message> basic) -> std::tuple<error_t, std::unique_ptr<message>>
      {
        auto ret = upgrade_message<message_channel_finalize>(std::move(basic));

        byte const * offset = ret->payload;
        std::size_t remaining = ret->payload_size;

        // First comes the channel id
        auto used = liberate::serialization::deserialize_int(ret->id.full,
            offset, remaining);
        if (used != sizeof(ret->id.full)) {
          LIBLOG_ERROR("Unable to decode channel identifier.");
          return {ERR_DECODE, nullptr};
        }
        offset += used;
        remaining -= used;

        // And the cookie
        cookie_serialize s;
        used = liberate::serialization::deserialize_int(s,
            offset, remaining);
        if (used != sizeof(s)) {
          LIBLOG_ERROR("Unable to decode cookie2.");
          return {ERR_DECODE, nullptr};
        }
        ret->cookie2 = s;

        offset += used;
        remaining -= used;

        // Also the capabilities
        capability_bits_t bits;
        used = liberate::serialization::deserialize_int(bits,
            offset, remaining);
        if (used != sizeof(bits)) {
          LIBLOG_ERROR("Unable to decode capabilities.");
          return {ERR_DECODE, nullptr};
        }
        ret->capabilities = bits;

        offset += used;
        remaining -= used;

        if (remaining > 0) {
          // We didn't consume the entire payload
          LIBLOG_ERROR("Unused payload part.");
          return {ERR_DECODE, nullptr};
        }

        return {ERR_SUCCESS, std::move(ret)};
      },
    },
    // *****************************************
    {
      MSG_CHANNEL_COOKIE,
      []() -> ssize_t
      {
        // - channelid.full // In header, not counted
        // - either cookie
        // - capability bits
        return sizeof(cookie_serialize) + sizeof(capability_bits_t);
      },
      [](std::unique_ptr<message> basic) -> std::tuple<error_t, std::unique_ptr<message>>
      {
        auto ret = upgrade_message<message_channel_cookie>(std::move(basic));

        byte const * offset = ret->payload;
        std::size_t remaining = ret->payload_size;

        // The cookie...
        cookie_serialize s;
        auto used = liberate::serialization::deserialize_int(s,
            offset, remaining);
        if (used != sizeof(s)) {
          LIBLOG_ERROR("Unable to decode cookie.");
          return {ERR_DECODE, nullptr};
        }
        ret->either_cookie = s;

        offset += used;
        remaining -= used;

        // Also the capabilities
        capability_bits_t bits;
        used = liberate::serialization::deserialize_int(bits,
            offset, remaining);
        if (used != sizeof(bits)) {
          LIBLOG_ERROR("Unable to decode capabilities.");
          return {ERR_DECODE, nullptr};
        }
        ret->capabilities = bits;

        offset += used;
        remaining -= used;

        if (remaining > 0) {
          // We didn't consume the entire payload
          LIBLOG_ERROR("Unused payload part.");
          return {ERR_DECODE, nullptr};
        }

        return {ERR_SUCCESS, std::move(ret)};
      },
    },
  };

  if (!registry) {
    LIBLOG_ERROR("Cannot register types with a non-existent registry.");
    return ERR_INVALID_VALUE;
  }

  error_t err = ERR_SUCCESS;
  for (std::size_t i = 0 ; i < sizeof(info) / sizeof(message_registry::message_info) ; ++i) {
    err = registry->register_message(info[i].type, info[i]);
    if (ERR_SUCCESS != err) {
      return err;
    }
  }

  return err;
}




/**
 * message_channel_new
 */
std::size_t
message_channel_new::serialize(byte * output, std::size_t output_max) const
{
  // We know the buffer size, as it's fixed.
  auto remaining = serialized_size();
  if (!remaining) {
    LIBLOG_ERROR("No idea how to serialize this message.");
    return 0;
  }
  if (remaining > output_max) {
    LIBLOG_ERROR("Serialized size is larger than output buffer.");
    return 0;
  }

  byte * offset = output;

  // Serialize message header
  auto used = this->message::serialize(output, output_max);
  if (used <= 0) {
    LIBLOG_ERROR("Could not serialize message header.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Serialize the half id.
  used = liberate::serialization::serialize_int(offset, remaining,
      initiator_part);
  if (used != sizeof(initiator_part)) {
    LIBLOG_ERROR("Could not serialize initiator part.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Serialize the cookie
  cookie_serialize tmp = cookie1;
  used = liberate::serialization::serialize_int(offset, remaining,
      tmp);
  if (used != sizeof(tmp)) {
    LIBLOG_ERROR("Could not serialize cookie1.");
    return 0;
  }
  offset += used;
  remaining -= used;

  if (remaining != 0) {
    LIBLOG_ERROR("Did not fill the entire serialized size with data.");
    return 0;
  }
  return (offset - output);
}



/**
 * message_channel_acknowledge
 */
std::size_t
message_channel_acknowledge::serialize(byte * output, std::size_t output_max) const
{
  // We know the buffer size, as it's fixed.
  auto remaining = serialized_size();
  if (!remaining) {
    LIBLOG_ERROR("No idea how to serialize this message.");
    return 0;
  }
  if (remaining > output_max) {
    LIBLOG_ERROR("Serialized size is larger than output buffer.");
    return 0;
  }

  byte * offset = output;

  // Serialize message header
  auto used = this->message::serialize(output, output_max);
  if (used <= 0) {
    LIBLOG_ERROR("Could not serialize message header.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Serialize the full id.
  used = liberate::serialization::serialize_int(offset, remaining,
      id.full);
  if (used != sizeof(id.full)) {
    LIBLOG_ERROR("Could not serialize channel identifier.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Serialize cookie1
  cookie_serialize tmp = cookie1;
  used = liberate::serialization::serialize_int(offset, remaining,
      tmp);
  if (used != sizeof(tmp)) {
    LIBLOG_ERROR("Could not serialize cookie1");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Serialize cookie2
  tmp = cookie2;
  used = liberate::serialization::serialize_int(offset, remaining,
      tmp);
  if (used != sizeof(tmp)) {
    LIBLOG_ERROR("Could not serialize cookie2");
    return 0;
  }
  offset += used;
  remaining -= used;

  if (remaining != 0) {
    LIBLOG_ERROR("Did not fill the entire serialized size with data.");
    return 0;
  }
  return (offset - output);
}



/**
 * message_channel_finalize
 */
std::size_t
message_channel_finalize::serialize(byte * output, std::size_t output_max) const
{
  // We know the buffer size, as it's fixed.
  auto remaining = serialized_size();
  if (!remaining) {
    LIBLOG_ERROR("No idea how to serialize this message.");
    return 0;
  }
  if (remaining > output_max) {
    LIBLOG_ERROR("Serialized size is larger than output buffer.");
    return 0;
  }

  byte * offset = output;

  // Serialize message header
  auto used = this->message::serialize(output, output_max);
  if (used <= 0) {
    LIBLOG_ERROR("Could not serialize message header.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Serialize the full id.
  used = liberate::serialization::serialize_int(offset, remaining,
      id.full);
  if (used != sizeof(id.full)) {
    LIBLOG_ERROR("Could not serialize channel identifier.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Serialize the cookie
  cookie_serialize tmp = cookie2;
  used = liberate::serialization::serialize_int(offset, remaining,
      tmp);
  if (used != sizeof(tmp)) {
    LIBLOG_ERROR("Could not serialize channel cookie2.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Capabilities
  capability_bits_t tmp2 = capabilities.to_ullong();
  used = liberate::serialization::serialize_int(offset, remaining,
      tmp2);
  if (used != sizeof(tmp2)) {
    LIBLOG_ERROR("Could not serialize channel capabilities");
    return 0;
  }
  offset += used;
  remaining -= used;

  if (remaining != 0) {
    LIBLOG_ERROR("Did not fill the entire serialized size with data.");
    return 0;
  }
  return (offset - output);
}



/**
 * message_channel_cookie
 */
std::size_t
message_channel_cookie::serialize(byte * output, std::size_t output_max) const
{
  // We know the buffer size, as it's fixed.
  auto remaining = serialized_size();
  if (!remaining) {
    LIBLOG_ERROR("No idea how to serialize this message.");
    return 0;
  }
  if (remaining > output_max) {
    LIBLOG_ERROR("Serialized size is larger than output buffer.");
    return 0;
  }

  byte * offset = output;

  // Serialize message header
  auto used = this->message::serialize(output, output_max);
  if (used <= 0) {
    LIBLOG_ERROR("Could not serialize message header.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Serialize the cookie
  cookie_serialize tmp = either_cookie;
  used = liberate::serialization::serialize_int(offset, remaining,
      tmp);
  if (used != sizeof(tmp)) {
    return 0;
  }
  offset += used;
  remaining -= used;

  // Capabilities
  capability_bits_t tmp2 = capabilities.to_ullong();
  used = liberate::serialization::serialize_int(offset, remaining,
      tmp2);
  if (used != sizeof(tmp2)) {
    return 0;
  }
  offset += used;
  remaining -= used;

  if (remaining != 0) {
    LIBLOG_ERROR("Did not fill the entire serialized size with data.");
    return 0;
  }
  return (offset - output);
}



} // namespace channeler
