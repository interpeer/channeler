/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_FSM_CONGESTION_SIMPLE_H
#define CHANNELER_FSM_CONGESTION_SIMPLE_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <functional>

#include "base.h"
#include "timeouts.h"

#include <channeler/message.h>
#include <channeler/message/congestion.h>
#include <channeler/timeouts.h>

#include "../macros.h"
#include "../channels.h"
#include "../channel_data.h"
#include "../support/timeouts.h"

namespace channeler::fsm {

/**
 * Simple congestion management state machine.
 *
 * When a channel is established, a timeout periodically reports the channel's
 * receive window to the peer. This is of course the *available* window, reduced
 * from the configured window by any out-of-order packets that have already
 * arrived.
 *
 * Upon receipt of a peer's receive window, we adjust our own send window and
 * notify the API.
 */
template <
  std::size_t POOL_BLOCK_SIZE,
  typename channelT
>
struct fsm_congestion_simple
  : public fsm_base
{
  using channel_set = ::channeler::channels<channelT>;

  using message_event_type = ::channeler::pipe::message_event<POOL_BLOCK_SIZE, channelT>;
  using timeout_event_type = ::channeler::pipe::timeout_event<
    ::channeler::support::timeout_scoped_tag_type
  >;
  using channel_established_event_type = ::channeler::pipe::channel_established_event;
  using channel_closed_event_type = ::channeler::pipe::channel_closed_event;


  /**
   * Need to keep a reference to a channel_set; this FSM only acts on
   * established channels.
   */
  inline fsm_congestion_simple(
      std::shared_ptr<::channeler::message_registry> registry,
      ::channeler::support::timeouts & timeouts,
      ::channeler::timeout_config const & to_config,
      channel_set & channels)
    : m_registry{registry}
    , m_timeouts{timeouts}
    , m_to_config{to_config}
    , m_channels{channels}
  {
  }



  /**
   * Process function, see above.
   */
  virtual fsm_base::process_result
  process(::channeler::pipe::event const & to_process,
      ::channeler::pipe::action_list_type & result_actions,
      ::channeler::pipe::event_list_type & output_events)
  {
    namespace pipe = channeler::pipe;
    LIBLOG_DEBUG("Simple congestion FSM got event of type: " << to_process.type);

    // Distinguish incoming event types
    switch (to_process.type) {
      case pipe::ET_MESSAGE:
        {
          auto & msg_ev = ::channeler::pipe::event_cast<message_event_type>(to_process);
          return handle_message(msg_ev, result_actions, output_events);
        }

      case pipe::ET_TIMEOUT:
        {
          auto & to_ev = ::channeler::pipe::event_cast<timeout_event_type>(to_process);
          return handle_timeout(to_ev, result_actions, output_events);
        }

      case pipe::ET_CHANNEL_ESTABLISHED:
        {
          auto & to_ev = ::channeler::pipe::event_cast<channel_established_event_type>(to_process);
          return handle_channel_established(to_ev, result_actions, output_events);
        }

      case pipe::ET_CHANNEL_CLOSED:
        {
          auto & to_ev = ::channeler::pipe::event_cast<channel_closed_event_type>(to_process);
          return handle_channel_closed(to_ev, result_actions, output_events);
        }

      default:
        LIBLOG_WARN("Simple congestion FSM does not handle messages of type: " << to_process.type);
        break;
    }

    // Otherwise we can't really do anything here.
    return {ERR_SUCCESS, PS_UNHANDLED};
  }


  inline fsm_base::process_result
  handle_message(message_event_type const & event,
      ::channeler::pipe::action_list_type & result_actions [[maybe_unused]],
      ::channeler::pipe::event_list_type & output_events)
  {
    switch (event.message->type) {
      case MSG_CONGESTION_RECEIVE_WINDOW:
        return handle_receive_window_message(
            event.channel,
            *reinterpret_cast<message_congestion_receive_window *>(event.message.get()),
            result_actions, output_events);

      default:
        LIBLOG_DEBUG("Simple congestion FSM handles only congestion messages.");
        return {ERR_SUCCESS, PS_UNHANDLED};
    }
  }



  inline fsm_base::process_result
  handle_receive_window_message(
      typename channel_set::channel_ptr const & channel,
      message_congestion_receive_window const & message,
      ::channeler::pipe::action_list_type & result_actions,
      ::channeler::pipe::event_list_type & output_events [[maybe_unused]])
  {
    LIBLOG_DEBUG("Peer's ingress window size of channel " << channel->id()
        << " holds " << message.window_size << " more packets, adjusting our"
        << " own egress window accordingly.");
    channel->egress_buffer().set_window_size(message.window_size);

    // Also create an action notifying the API/user of this.
    result_actions.push_back(std::make_unique<channeler::pipe::notify_channel_window_action>(
          channel->id(), message.window_size));

    return {ERR_SUCCESS, PS_SUCCESS};
  }



  inline fsm_base::process_result
  handle_timeout(timeout_event_type const & event,
      ::channeler::pipe::action_list_type & result_actions [[maybe_unused]],
      ::channeler::pipe::event_list_type & output_events [[maybe_unused]])
  {
    // The first thing to check is the tag of the timeout.
    if (event.context.tag.scope != CHANNEL_CONGESTION_TIMEOUT_TAG) {
      // Not a timeout for us.
      LIBLOG_DEBUG("Ignoring timeout; we didn't ask for it.");
      return {ERR_SUCCESS, PS_IGNORED};
    }

    // Try to grab channel data for this channel
    auto channel = m_channels.get_by_initiator(event.context.tag.name);
    if (!channel) {
      LIBLOG_DEBUG("No channel matching timeout found, ignoring event.");
      return {ERR_SUCCESS, PS_IGNORED};
    }

    // Refresh the timeout
    m_timeouts.remove({CHANNEL_CONGESTION_TIMEOUT_TAG, event.context.tag.name});
    m_timeouts.add({CHANNEL_CONGESTION_TIMEOUT_TAG, event.context.tag.name},
        m_to_config.channel_congestion_update_timeout);

    // With the channel data in hand, we can produce a message with our
    // own ingress window's current *available* size.
    auto msg = std::make_unique<message_congestion_receive_window>(m_registry,
      channel->ingress_buffer().available_size());
    auto ev = std::make_unique<channeler::pipe::message_out_event>(
        channel->id(),
        std::move(msg)
    );
    output_events.push_back(std::move(ev));

    LIBLOG_DEBUG("Sending receive window message to egress.");
    return {ERR_SUCCESS, PS_SUCCESS};
  }


  inline fsm_base::process_result
  handle_channel_established(channel_established_event_type const & event,
      ::channeler::pipe::action_list_type & result_actions [[maybe_unused]],
      ::channeler::pipe::event_list_type & output_events [[maybe_unused]])
  {
    if (!event.channel.is_complete()) {
      LIBLOG_DEBUG("Ignoring any but full channel IDs.");
      return {ERR_SUCCESS, PS_IGNORED};
    }

    if (!m_channels.has_established_channel(event.channel)) {
      LIBLOG_DEBUG("No channel with id " << event.channel << " found to be "
          "established, ignoring event.");
      return {ERR_SUCCESS, PS_IGNORED};
    }

    // It shouldn't be possible for this event to be sent when there already is
    // a timeout active. We can either ignore that case, or ignore the event.
    // It's probably safer to ignore the event.
    auto [active, to] = m_timeouts.is_timeout_active(
        {CHANNEL_CONGESTION_TIMEOUT_TAG, event.channel.initiator}
    );
    if (active) {
      LIBLOG_DEBUG("Ignoring duplicate channel establishment!");
      return {ERR_SUCCESS, PS_IGNORED};
    }

    // Create a timeout
    m_timeouts.remove({CHANNEL_CONGESTION_TIMEOUT_TAG, event.channel.initiator});
    m_timeouts.add({CHANNEL_CONGESTION_TIMEOUT_TAG, event.channel.initiator},
        m_to_config.channel_congestion_update_timeout);

    return {ERR_SUCCESS, PS_SUCCESS};
  }



  inline fsm_base::process_result
  handle_channel_closed(channel_closed_event_type const & event,
      ::channeler::pipe::action_list_type & result_actions [[maybe_unused]],
      ::channeler::pipe::event_list_type & output_events [[maybe_unused]])
  {
    if (!event.channel.is_complete()) {
      LIBLOG_DEBUG("Ignoring any but full channel IDs.");
      return {ERR_SUCCESS, PS_IGNORED};
    }

    if (m_channels.has_established_channel(event.channel)) {
      LIBLOG_DEBUG("Channel with id " << event.channel << " found to be "
          "established, ignoring event.");
      return {ERR_SUCCESS, PS_IGNORED};
    }

    // Just remove the timeout
    m_timeouts.remove({CHANNEL_CONGESTION_TIMEOUT_TAG, event.channel.initiator});

    return {ERR_SUCCESS, PS_SUCCESS};
  }

  virtual ~fsm_congestion_simple() = default;

private:

  std::shared_ptr<::channeler::message_registry>  m_registry;
  ::channeler::support::timeouts &                m_timeouts;
  ::channeler::timeout_config const &             m_to_config;
  channel_set &                                   m_channels;
};

} // namespace channeler::fsm

#endif // guard
