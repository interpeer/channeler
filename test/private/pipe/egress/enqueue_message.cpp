/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/pipe/egress/enqueue_message.h"
#include "../lib/channel_data.h"
#include "../lib/params.h"

#include <channeler/message/data.h>

#include <gtest/gtest.h>

#include "../../../clock.h"

using namespace liberate::types::literals;

namespace {

// For testing
using test_clock_params = test::clock_params;
using test_defaults = channeler::defaults<
  typename test_clock_params::duration,
  3 // POOL_BLOCK_SIZE
>;
using test_params = ::channeler::params<
  test_clock_params,
  test_defaults
>;


struct next
{
  using input_event = channeler::pipe::message_out_enqueued_event;

  inline channeler::pipe::action_list_type consume(std::unique_ptr<channeler::pipe::event> event)
  {
    m_event = std::move(event);
    return {};
  }
  std::unique_ptr<channeler::pipe::event> m_event;
};


using filter_t = channeler::pipe::enqueue_message_filter<
  typename test_params::channel,
  next,
  next::input_event
>;

} // anonymous namespace



TEST(PipeEgressEnqueueMessageFilter, passthrough_invalid_event)
{
  using namespace channeler::pipe;

  next n;
  filter_t::channel_set chs;
  filter_t filter{&n, chs};

  // Create a default event; this should not be handled.
  auto ev = std::make_unique<event>();
  auto res = filter.consume(std::move(ev));
  ASSERT_TRUE(res.empty());
  ASSERT_TRUE(n.m_event);
  ASSERT_EQ(ET_UNKNOWN, n.m_event->type);
}


TEST(PipeEgressEnqueueMessageFilter, enqueue_message)
{
  using namespace channeler::pipe;

  next n;
  filter_t::channel_set chs;
  filter_t filter{&n, chs};

  auto channel = channeler::create_new_channelid();
  channeler::complete_channelid(channel);
  auto [err, chopt] = chs.add(channel);
  EXPECT_EQ(channeler::ERR_SUCCESS, err);
  auto ch = chopt.value();

  using namespace liberate::types::literals;
  channeler::byte buf[130] = { 0x0f_b };

  auto reg = channeler::message_registry::create();
  auto msg = channeler::message_data::create_with_payload(reg, buf, sizeof(buf));
  auto ev = std::make_unique<message_out_event>(
      channel,
      std::move(msg)
  );
  ASSERT_FALSE(ch->egress_messages().have_pending());
  auto ret = filter.consume(std::move(ev));
  ASSERT_TRUE(ch->egress_messages().have_pending());

  ASSERT_EQ(0, ret.size());

  // Ensure the message has been moved.
  ASSERT_TRUE(n.m_event);
  ASSERT_EQ(n.m_event->type, ET_MESSAGE_OUT_ENQUEUED);
  next::input_event * ptr = reinterpret_cast<next::input_event *>(n.m_event.get());
  ASSERT_EQ(channel, ptr->channel);

  auto res = ch->egress_messages().dequeue();
  ASSERT_TRUE(res);
  ASSERT_EQ(channeler::MSG_DATA, res->type);
  auto resconv = reinterpret_cast<channeler::message_data *>(res.get());
  ASSERT_EQ(resconv->payload_size, sizeof(buf));

  for (std::size_t i = 0 ; i < resconv->payload_size ; ++i) {
    ASSERT_EQ(buf[i], resconv->payload[i]);
  }
}
