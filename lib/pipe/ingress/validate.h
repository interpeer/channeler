/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_PIPE_INGRESS_VALIDATE_H
#define CHANNELER_PIPE_INGRESS_VALIDATE_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <memory>
#include <set>

#include "../../memory/packet_pool.h"
#include "../event.h"
#include "../action.h"
#include "../filter_classifier.h"
#include "../event_as.h"

#include <channeler/packet.h>
#include <channeler/error.h>


namespace channeler::pipe {

/**
 * Validate packets.
 *
 * On the face of it, this filter merely validates packet checksums or similar
 * cryptographic mechanisms in order to decide whether a packet should be
 * processed further.
 *
 * But beyond that, this is also the place in which it should be decided
 * what consequences receiving an invalid packet has.
 *
 * If a packet is known to be invalid, the filter consults the peer failure
 * policy to decide whether to institute filtering at the level of the peerid,
 * and the transport failure policy whether to filter at the transport level.
 *
 * Expects the next_eventT constructor to take
 * - transport source address
 * - transport destination address
 * - packet_wrapper
 * - a pool slot
 *
 * such as e.g. decrypted_packet_event
 */
template <
  typename addressT,
  std::size_t POOL_BLOCK_SIZE,
  typename next_filterT,
  typename next_eventT,
  typename peer_failure_policyT = null_policy<peerid_wrapper>,
  typename transport_failure_policyT = null_policy<addressT>
>
struct validate_filter
{
  using input_event = decrypted_packet_event<addressT, POOL_BLOCK_SIZE>;
  using classifier = filter_classifier<addressT, peer_failure_policyT, transport_failure_policyT>;

  inline validate_filter(next_filterT * next,
      peer_failure_policyT * peer_p = nullptr,
      transport_failure_policyT * trans_p = nullptr)
    : m_next{next}
    , m_classifier{peer_p, trans_p}
  {
  }


  template <typename... argsT>
  inline action_list_type
  consume(std::unique_ptr<event> unchecked, argsT && ...args)
  {
    auto in = event_as<input_event>("ingress:validate", unchecked, ET_DECRYPTED_PACKET);

    // If there is no data passed, we should also throw.
    if (nullptr == in->data.data()) {
      LIBLOG_DEBUG("Invalid reference");
      throw exception{ERR_INVALID_REFERENCE};
    }

    // We need to validate the packet. For now, this just means verifying the
    // checksum.
    if (!in->packet.has_valid_checksum()) {
      LIBLOG_DEBUG("Packet checksum is invalid!");
      // If the checksum is invalid, we know we want to exit the pipe here. The
      // classifier provides actions, if so desired.
      return m_classifier.process(in->transport.source,
          in->transport.destination, in->packet);
    }

    // At the next filter, we require full packets again. Let's just move
    // the event, then.
    return m_next->consume(std::move(in), std::forward<argsT>(args)...);
  }


  next_filterT *  m_next;
  classifier      m_classifier; // TODO ptr or ref for shared state?
                                // https://codeberg.org/interpeer/channeler/issues/21
};


} // namespace channeler::pipe

#endif // guard
