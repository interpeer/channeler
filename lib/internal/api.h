/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_INTERNAL_API_H
#define CHANNELER_INTERNAL_API_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <functional>

#include <channeler/channelid.h>
#include <channeler/error.h>
#include <channeler/capabilities.h>

#include "../macros.h"

#include "../fsm/default.h"
#include "../pipe/ingress.h"
#include "../pipe/egress.h"
#include "../pipe/action.h"

namespace channeler::internal {

/**
 * This file contains the *internal* API for channeler, i.e. an API that is
 * allows the user to add channeler's protocol support to an existing
 * connection management system.
 *
 * The API requires a context which encapsulates all the platform support
 * the stack requires.
 *
 * The expected usage of this class is for the caller to establish a
 * connection of sorts, and to instanciate one of these classes per
 * connection.
 */
template <
  typename paramsT,
  typename connection_contextT
>
struct connection_api
{
  using params = paramsT;
  using connection = connection_contextT;

  static_assert(std::is_same<params, typename connection::params>::value,
      "Parameters of API and connection context must be the same!");

  using ingress_type = pipe::default_ingress<params>;
  using egress_type = pipe::default_egress<params>;

  using channel_established_callback = std::function<
    void
    (
      typename params::clock_params::time_point const &,
      error_t,
      channelid const &,
      capabilities_t const &
    )
  >;
  using channel_closed_callback = std::function<
    void
    (
      typename params::clock_params::time_point const &,
      error_t,
      channelid const &
    )
  >;
  using packet_to_send_callback = std::function<
    error_t
    (
      typename params::clock_params::time_point const &,
      channelid const &, sequence_no_t const &
    )
  >;
  using data_available_callback = std::function<
    void
    (
      typename params::clock_params::time_point const &,
      channelid const &,
      std::size_t
    )
  >;
  using channel_window_changed_callback = std::function<
    void
    (
      typename params::clock_params::time_point const &,
      channelid const &,
      std::size_t
    )
  >;

  using queue_entry = typename params::channel::queue_entry;

  /**
   * Constructor accepts:
   * TODO
   */
  inline connection_api(connection & context,
      channel_established_callback established_cb = {},
      channel_closed_callback closed_cb = {},
      packet_to_send_callback packet_cb = {},
      data_available_callback data_cb = {}
    )
    : m_context{context}
    , m_registry{
        fsm::get_standard_registry(m_context)
      }
    , m_event_route_map{}
    , m_ingress{
        m_registry,
        m_context.node().message_registry(),
        m_event_route_map,
        m_context.channels()
      }
    , m_egress{
        std::bind(&connection_api::redirect_egress_event, this,
            std::placeholders::_1,
            std::placeholders::_2),
        m_context.channels(),
        m_context.node().packet_pool(),
        [this]() { return m_context.node().id(); },
        [this]() { return m_context.peer(); }
      }
    , m_channel_established_cb{established_cb}
    , m_channel_closed_cb{closed_cb}
    , m_channel_window_changed_cb{}
    , m_packet_to_send_cb{packet_cb}
    , m_data_available_cb{data_cb}
  {
    // Populate event route map
    using namespace std::placeholders;

    m_event_route_map[pipe::EC_UNKNOWN] =
      std::bind(&connection_api::handle_unknown_event, this, _1, _2);

    m_event_route_map[pipe::EC_INGRESS] =
      std::bind(&connection_api::handle_ingress_event, this, _1, _2);

    m_event_route_map[pipe::EC_EGRESS] =
      std::bind(&connection_api::handle_egress_event, this, _1, _2);

    m_event_route_map[pipe::EC_USER] =
      std::bind(&connection_api::handle_user_event, this, _1, _2);

    m_event_route_map[pipe::EC_SYSTEM] =
      std::bind(&connection_api::handle_system_event, this, _1, _2);

    m_event_route_map[pipe::EC_NOTIFICATION] =
      std::bind(&connection_api::handle_notification_event, this, _1, _2);

    m_context.channels().set_resend_callback(
        std::bind(&connection_api::resend_callback, this, _1, _2, _3, _4, _5)
    );
  }

  // *** Channel interface

  /**
   * Initiate a channel.
   *
   * ERR_SUCCESS means the initial establishment message was sent properly,
   * and does not indicate overall success. On overall success,
   * the channel establishment callback will be invoked.
   *
   * Note that this does not permit the user to tie a *purpose* to a particular
   * channel, which is less than ideal.
   *
   * TODO: also store a user-provided tag in channel data, so that the
   *       establishment callback can be used to tie a new channel back to
   *       the user's action.
   *
   *       Also see: https://codeberg.org/interpeer/channeler/issues/32
   */
  inline error_t establish_channel(
      typename params::clock_params::time_point const & now,
      peerid const & peer,
      capabilities_t capabilities = capabilities_datagram()
    )
  {
    // Channel establishment happens on the default channel; we need to create a
    // default channel entry if we haven't already.
    auto [err, chanopt] = m_context.channels().add(DEFAULT_CHANNELID);
    if (ERR_CHANNEL_EXISTS != err && chanopt.has_value()) {
      chanopt.value()->set_initial_clock(now);
    }

    // Establishing a channel means sending an appropriate user
    // event to the FSM.
    auto event = pipe::new_channel_event(m_context.node().id(), peer,
        capabilities);

    err = process_event_and_results(now, event);
    if (ERR_SUCCESS != err) {
      return err;
    }

    LIBLOG_DEBUG("Channel initiated successfully.");
    return ERR_SUCCESS;
  }


  /**
   * Write data to a channel.
   *
   * This is raw application data; it will be wrapped in a message, packet,
   * etc. by this API.
   *
   * Note that for simplicity's sake, this API does *not* currently break down
   * too-large data chunks into individual packets. TODO
   */
  inline error_t channel_write(
      typename params::clock_params::time_point const & now,
      channelid const & id, byte const * data,
      std::size_t length, std::size_t & written
    )
  {
    written = 0;
    if (id == DEFAULT_CHANNELID || !id.has_responder()) {
      return ERR_INVALID_CHANNELID;
    }

    // The user data event carries unbounded amounts of data. It's up to the
    // FSM to split this up.
    std::vector<byte> payload{data, data + length};
    auto event = pipe::user_data_written_event(id, std::move(payload));

    auto err = process_event_and_results(now, event);
    if (ERR_SUCCESS != err) {
      return err;
    }

    LIBLOG_DEBUG("Data written successfully.");
    written = length;
    return ERR_SUCCESS;
  }

  inline error_t channel_write(
      typename params::clock_params::time_point const & now,
      channelid const & id, char const * data,
      std::size_t length, std::size_t & written
    )
  {
    return channel_write(now, id,
        reinterpret_cast<byte const *>(data),
        length, written);
  }



  /**
   * Read data from channel.
   *
   * Again, this is raw application data.
   */
  inline error_t channel_read(
      typename params::clock_params::time_point const & now [[maybe_unused]],
      channelid const & id, byte * data,
      std::size_t max, std::size_t & read
    )
  {
    // TODO if no buffer, return
    LIBLOG_DEBUG("User wants to read data of up to " << max
        << " Bytes from channel " << id);

    auto channel = m_context.channels().get(id);
    if (!channel) {
      return ERR_INVALID_CHANNELID;
    }

    // The channel data's ingress buffer should contain only user
    // data messages.
    do {
      auto err = have_pending(now, channel);
      if (ERR_SUCCESS != err) {
        read = 0;
        return err;
      }

      // TODO handle read != next_size(), essentially

      auto msg = channel->ingress_messages().dequeue();
      if (!msg) {
        LIBLOG_WARN("Strange, got an empty message.");
        continue;
      }

      // FIXME check message type or something?
      std::size_t to_copy = std::min(msg->payload_size, max);
      LIBLOG_DEBUG("Copying " << to_copy << " Bytes of " << msg->payload_size
          << " to buffer of size " << max);
      ::memcpy(data, msg->payload, to_copy);

      read = to_copy;
      return ERR_SUCCESS;
    } while (true);


    LIBLOG_ERROR("This line should be unreachable.");
    return ERR_UNEXPECTED;
  }

  inline error_t channel_read(typename params::clock_params::time_point const & now,
      channelid const & id, char * data,
      std::size_t max, std::size_t & read)
  {
    return channel_read(now, id,
        reinterpret_cast<byte *>(data),
        max, read);
  }


  /**
   * Be notified of a channel's receive window changing.
   */
  inline connection_api &
  set_channel_window_changed_callback(channel_window_changed_callback cb)
  {
    m_channel_window_changed_cb = cb;
    return *this;
  }


  /**
   * Retrieve or set channel window size. This is the ingress window, strictly
   * speaking. The egress window is managed by congestion control messages.
   */
  inline std::size_t window_size(channelid const & id) const
  {
    auto ptr = m_context.channels().get(id);
    if (!ptr) {
      LIBLOG_DEBUG("Channel with id " << id << " not found, ignoring.");
      return 0;
    }

    return ptr->ingress_buffer().window_size();
  }

  inline bool set_window_sisze(channelid const & id, std::size_t window_size)
  {
    auto ptr = m_context.channels().get(id);
    if (!ptr) {
      LIBLOG_DEBUG("Channel with id " << id << " not found, ignoring.");
      return false;
    }

    return ptr->ingress_buffer().set_window_size(window_size);
  }


  // *** I/O interface

  /**
   * Allocate a packet slot. The pool is shared between ingress and egress,
   * the queues are not.
   */
  inline typename params::slot allocate()
  {
    return m_context.node().packet_pool().allocate();
  }


  /**
   * Consume received packets.
   *
   * Buffers for incoming packets are taken from a pool, so the API
   * really just needs a pool slot.
   */
  inline error_t received_packet(
      typename params::clock_params::time_point const & now,
      typename params::transport_address const & source,
      typename params::transport_address const & destination,
      typename params::slot const & slot
    )
  {
    LIBLOG_DEBUG("Received packet: " << slot.size());
    auto ev = std::make_unique<
      pipe::raw_buffer_event<
        typename params::transport_address,
        params::defaults::POOL_BLOCK_SIZE
      >
    >(source, destination, slot);

    // Feed into default ingress pipe
    auto actions = m_ingress.consume(now, std::move(ev));
    auto err = process_ingress_actions(now, actions);

    LIBLOG_DEBUG("Packet processed after receipt.");
    return err;
  }


  /**
   * Dequeue packet ready for sending.
   *
   * Also buffers for outgoing packets are taken from a pool, so the
   * API just returns the slot. If the slot is empty, there is no more
   * data ready for sending.
   */
  inline queue_entry packet_to_send(
      typename params::clock_params::time_point const & now,
      channelid const & channel, sequence_no_t const & seqno = 0
    )
  {
    auto ptr = m_context.channels().get(channel);
    LIBLOG_DEBUG("Want to retrieve packet " << seqno << " for sending.");

    // XXX We *retrieve* the packet; the progress message will make sure it's
    //     erased.
    auto [optval, close] = ptr->egress_buffer().retrieve(seqno, now);
    if (!optval.has_value()) {
      throw exception{ERR_DATA_UNAVAILABLE,
        "There was supposed to be data to read, but nothing was found."};
    }
    return optval.value();
  }


private:

  inline error_t
  have_pending(
      typename params::clock_params::time_point const & now,
      typename params::channel_set::channel_ptr channel)
  {
    if (channel->ingress_messages().have_pending()) {
      return ERR_SUCCESS;
    }

    // If there are no immediate pending messages, we need to try and drain
    // the ingress queue, which we do by sending the appropriate event to
    // stage2 of the ingress queue.
    LIBLOG_DEBUG("No immediate pending messages, trigger queue drain.");

    auto ev = std::make_unique<typename ingress_type::stage2_t::input_event>(
        channel->id()
    );
    auto actions = m_ingress.consume_stage2(now, std::move(ev));
    auto err = process_ingress_actions(now, actions);
    if (ERR_SUCCESS != err) {
      return err;
    }

    // Now return what there is in pending data. It might still be nothing,
    // but at least we're sure.
    if (channel->ingress_messages().have_pending()) {
      return ERR_SUCCESS;
    }
    return ERR_DATA_UNAVAILABLE;
  }

  /**
   * Handle known actions. There aren't all that many we know what to do with,
   * to be honest. These are the actions we want to handle *globally*, that is,
   * at the beginning of the filter pipe.
   */
  inline error_t
  process_ingress_actions(
      typename params::clock_params::time_point const & now,
      pipe::action_list_type const & actions
    )
  {
    error_t err = ERR_SUCCESS;

    for (auto & act : actions) {
      switch (act->type) {
        case pipe::AT_NOTIFY_CHANNEL_ESTABLISHED:
          {
            auto actconv = reinterpret_cast<pipe::notify_channel_established_action *>(act.get());
            LIBLOG_DEBUG("FSM reports channel established: " << actconv->channel);
            if (m_channel_established_cb) {
              m_channel_established_cb(now, ERR_SUCCESS, actconv->channel, actconv->capabilities);
            }

            // This should be published back into the FSMs as an event.
            auto ev = pipe::channel_established_event{actconv->channel};
            auto err2 = process_event_and_results(now, ev);
            if (ERR_SUCCESS != err2) {
              return err2;
            }
          }
          break;

        case pipe::AT_NOTIFY_CHANNEL_CLOSED:
          {
            auto actconv = reinterpret_cast<pipe::notify_channel_closed_action *>(act.get());
            LIBLOG_DEBUG("FSM reports channel closed: " << actconv->channel);
            if (m_channel_closed_cb) {
              m_channel_closed_cb(now, actconv->error, actconv->channel);
            }

            // This should be published back into the FSMs as an event.
            auto ev = pipe::channel_closed_event{actconv->channel};
            auto err2 = process_event_and_results(now, ev);
            if (ERR_SUCCESS != err2) {
              return err2;
            }

            // If things went well, exit with the original error
            return actconv->error;
          }
          break;

        case pipe::AT_NOTIFY_CHANNEL_WINDOW:
          {
            auto actconv = reinterpret_cast<pipe::notify_channel_window_action *>(act.get());
            LIBLOG_DEBUG("FSM reports channel window changed: " << actconv->channel
                << " - " << actconv->window_size);
            if (m_channel_window_changed_cb) {
              m_channel_window_changed_cb(now, actconv->channel, actconv->window_size);
            }
          }
          break;

        case pipe::AT_ERROR:
          {
            auto actconv = reinterpret_cast<pipe::error_action *>(act.get());
            LIBLOG_ET("Incoming packet produced error", actconv->error);
            return actconv->error; // XXX exit loop immediately on error
          }
          break;

        case pipe::AT_PROCESS_EVENT:
          {
            auto actconv = reinterpret_cast<pipe::process_event_action *>(act.get());
            LIBLOG_DEBUG("Pushing new to_process into FSM: " << actconv->to_process->type);
            auto actions2 = m_ingress.consume(now, std::move(actconv->to_process));
            auto err2 = process_ingress_actions(now, actions2);
            if (ERR_SUCCESS != err2) {
              return err2;
            }
          }
          break;

        default:
          {
            LIBLOG_ERROR("Ingress pipe reports action we don't understand: "
                << act->type);
            err = ERR_UNEXPECTED;
          }
          break;
      }
    }

    return err;
  }


  inline error_t
  process_egress_actions(
      typename params::clock_params::time_point const & now [[maybe_unused]],
      pipe::action_list_type const & actions
    )
  {
    error_t err = ERR_SUCCESS;

    for (auto & act : actions) {
      switch (act->type) {
        case pipe::AT_ERROR:
          {
            auto actconv = reinterpret_cast<pipe::error_action *>(act.get());
            LIBLOG_ET("Outgoing packet produced error", actconv->error);
            return actconv->error; // XXX exit loop immediately on error
          }
          break;

        default:
          {
            LIBLOG_ERROR("Egress pipe reports action we don't understand: "
                << act->type);
            err = ERR_UNEXPECTED;
          }
          break;
      }
    }

    return err;
  }


  inline error_t
  process_event_and_results(
      typename params::clock_params::time_point const & now,
      pipe::event const & event
    )
  {
    pipe::action_list_type result_actions;
    pipe::event_list_type result_events;
    auto [err, res] = m_registry.process(event, result_actions, result_events);
    if (ERR_SUCCESS != err) {
      return err;
    }

    // First, process the resulting actions. If there is an error here, we
    // don't want to produce outgoing messages.
    err = process_ingress_actions(now, result_actions);
    if (ERR_SUCCESS != err) {
      return err;
    }

    // Otherwise, we need to feed each of the result events into the egress
    // pipe.
    while (!result_events.empty()) {
      auto ev = std::move(result_events.front());
      result_events.pop_front();
      if (!ev) {
        LIBLOG_ERROR("Registry produced an empty result event!");
        return ERR_STATE;
      }

      // The only really legitimate event type is ET_MESSAGE_OUT, at least
      // for feeding back into the egress pipe.
      if (ev->type != pipe::ET_MESSAGE_OUT) {
        LIBLOG_ERROR("Registry produced an event that is not an outgoing "
            "message: " << ev->type);
        return ERR_STATE;
      }

      // Feed ET_MESSAGE_OUT to the egress pipe. This also may result in
      // actions, though
      result_actions = m_egress.consume(now, std::move(ev));
      err = process_egress_actions(now, result_actions);
      if (ERR_SUCCESS != err) {
        return err;
      }
    }

    // All done and good.
    return ERR_SUCCESS;
  }



  pipe::action_list_type redirect_egress_event(
      std::unique_ptr<pipe::event> ev,
      typename params::clock_params::time_point const & now
    )
  {
    pipe::action_list_type actions;

    LIBLOG_DEBUG("Egress event produced: " << ev->category << " / " << ev->type);
    switch (ev->type) {
      case pipe::ET_PACKET_OUT_ENQUEUED:
        {
          auto converted = reinterpret_cast<
            pipe::packet_out_enqueued_event<
              typename params::channel
            > *
          >(ev.get());
            auto channel = converted->channel->id();
            LIBLOG_DEBUG("Notifying packet available on channel: " << channel
                << "; requested packet: " << static_cast<uint64_t>(converted->sequence_no));
            if (m_packet_to_send_cb) {
              auto err = m_packet_to_send_cb(now, channel, converted->sequence_no);
              if (ERR_SUCCESS != err) {
                LIBLOG_ET("Error in write callback", err);
                actions.push_back(std::make_unique<pipe::error_action>(err));
                // TODO
              }
            }
        }
        break;

      default:
        break;
    }

    // TODO
    return actions;
  }

  pipe::action_list_type handle_ingress_event(
      std::unique_ptr<pipe::event> ev,
      typename params::clock_params::time_point const & now [[maybe_unused]]
    )
  {
    LIBLOG_DEBUG("Handling ingress event of type: " << ev->type);
    // TODO
    return {};
  }

  pipe::action_list_type handle_unknown_event(
      std::unique_ptr<pipe::event> ev,
      typename params::clock_params::time_point const & now [[maybe_unused]]
    )
  {
    LIBLOG_DEBUG("Handling unknown event of type: " << ev->type);
    // TODO
    return {};
  }

  pipe::action_list_type handle_egress_event(
      std::unique_ptr<pipe::event> ev,
      typename params::clock_params::time_point const & now
    )
  {
    LIBLOG_DEBUG("Handling egress event of type: " << ev->type);
    return m_egress.consume(now, std::move(ev));
  }

  pipe::action_list_type handle_user_event(
      std::unique_ptr<pipe::event> ev,
      typename params::clock_params::time_point const & now [[maybe_unused]]
    )
  {
    LIBLOG_DEBUG("Handling user event of type: " << ev->type);
    // TODO
    return {};
  }

  pipe::action_list_type handle_system_event(
      std::unique_ptr<pipe::event> ev,
      typename params::clock_params::time_point const & now [[maybe_unused]]
    )
  {
    LIBLOG_DEBUG("Handling system event of type: " << ev->type);
    // TODO
    return {};
  }

  pipe::action_list_type handle_notification_event(
      std::unique_ptr<pipe::event> ev,
      typename params::clock_params::time_point const & now
    )
  {
    LIBLOG_DEBUG("Handling notification event of type: " << ev->type);
    switch (ev->type) {
      case pipe::ET_USER_DATA_TO_READ:
        {
          auto converted = reinterpret_cast<
            pipe::user_data_to_read_event<
              params::defaults::POOL_BLOCK_SIZE
            > *
          >(ev.get());

          if (converted->message->type != MSG_DATA) {
            LIBLOG_ERROR("Unknown message type for user data available: " << converted->message->type);
            return {};
          }

          auto id = converted->channel;
          auto channel = m_context.channels().get(id);
          if (!channel) {
            LIBLOG_ERROR("User data for unknown channel received!");
            return {};
          }

          auto size = converted->message->payload_size;
          channel->ingress_messages().enqueue(std::move(converted->message));

          LIBLOG_DEBUG("Notifying data available on channel: " << converted->channel);
          if (m_data_available_cb) {
            m_data_available_cb(now, id, size);
          }
        }
        break;

      default:
        break;
    }

    return {};
  }



  inline error_t
  resend_callback(
      typename params::clock_params::time_point const & now,
      channelid const & id,
      std::size_t const & collisions [[maybe_unused]],
      sequence_no_t const & window_start,
      typename params::channel::queue_params::missing_set const & missing
    )
  {
    // Ignore missing reports for pending channels.
    // FIXME yes?
    if (id == DEFAULT_CHANNELID || !id.has_responder()) {
      LIBLOG_ERROR("Missing packets reported for default/incomplete channel: "
          << id << ", ignoring.");
      return ERR_SUCCESS;
    }

    // Trigger a resend envent
    LIBLOG_DEBUG("Missing packets on channel: " << id);
    auto event = pipe::progress_event(id, window_start, missing);
    auto err = process_event_and_results(now, event);
    if (ERR_SUCCESS != err) {
      return err;
    }

    LIBLOG_DEBUG("Resend triggered.");
    return ERR_SUCCESS;
  }


  connection &                      m_context;
  fsm::registry                     m_registry;

  pipe::event_route_map<
    typename params::clock_params::time_point const &
  >                                 m_event_route_map;

  ingress_type                      m_ingress;
  egress_type                       m_egress;

  channel_established_callback      m_channel_established_cb;
  channel_closed_callback           m_channel_closed_cb;
  channel_window_changed_callback   m_channel_window_changed_cb;
  packet_to_send_callback           m_packet_to_send_cb;
  data_available_callback           m_data_available_cb;
};


} // namespace channeler::internal

#endif // guard
