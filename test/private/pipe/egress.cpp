/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/pipe/egress.h"
#include "../lib/context/node.h"
#include "../lib/context/connection.h"

#include <gtest/gtest.h>

#include "../../clock.h"

namespace {

constexpr static std::size_t PACKET_SIZE = 300;

using test_clock_params = test::clock_params;
using test_defaults = channeler::defaults<
  typename test_clock_params::duration,
  3 // POOL_BLOCK_SIZE
>;
using address_t = uint16_t;

using test_params = ::channeler::params<
  test_clock_params,
  test_defaults,
  channeler::null_lock_policy,
  address_t
>;


using node_t = ::channeler::context::node<test_params>;
using connection_t = ::channeler::context::connection<
  test_params,
  node_t
>;


using egress_t = channeler::pipe::default_egress<
  test_params
>;


} // anonymous namespace


TEST(PipeEgress, create)
{
  using namespace channeler::pipe;

  channeler::peerid self;
  channeler::peerid peer;

  node_t node{
    self,
    PACKET_SIZE,
    []() -> std::vector<channeler::byte> { return {}; },
    [](channeler::support::timeouts::duration d) { return d; },
  };

  channeler::timeout_config to_config;
  connection_t ctx{
    node,
    peer,
    to_config
  };

  typename test_params::channel_set chs;
  typename test_params::pool pool{PACKET_SIZE};

  std::unique_ptr<event> caught;
  egress_t egress{
    [&caught](std::unique_ptr<event> ev, typename test_clock_params::time_point const &) -> action_list_type
    {
      caught = std::move(ev);
      return {};
    },
    chs,
    pool,
    []() -> channeler::peerid { return {}; },
    []() -> channeler::peerid { return {}; }
  };
}
