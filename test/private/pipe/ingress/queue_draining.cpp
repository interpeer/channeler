/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/pipe/ingress/queue_draining.h"
#include "../lib/channel_data.h"
#include "../lib/params.h"

#include <gtest/gtest.h>

#include "../../../packets.h"
#include "../../../clock.h"

using namespace test;

namespace {

// For testing
std::size_t PACKET_SIZE = packet_with_messages_size;

using test_clock_params = test::clock_params;
using test_defaults = channeler::defaults<
  typename test_clock_params::duration,
  3 // POOL_BLOCK_SIZE
>;
using address_t = uint16_t;

using test_params = ::channeler::params<
  test_clock_params,
  test_defaults,
  channeler::null_lock_policy,
  address_t
>;


struct next
{
  using input_event = channeler::pipe::dequeued_packet_event<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  template <typename... argsT>
  inline channeler::pipe::action_list_type consume(std::unique_ptr<channeler::pipe::event> event,
      argsT && ...args [[maybe_unused]])
  {
    m_event = std::move(event);
    ++m_called;
    return {};
  }

  std::unique_ptr<channeler::pipe::event> m_event = {};
  std::size_t                             m_called = 0;
};



using simple_filter_t = channeler::pipe::queue_draining_filter<
  test_params::defaults::POOL_BLOCK_SIZE,
  next,
  next::input_event,
  typename test_params::channel
>;


} // anonymous namespace



TEST(PipeIngressQueueDrainingFilter, throw_on_invalid_event)
{
  using namespace channeler::pipe;

  next n;
  typename test_params::channel_set chs;
  simple_filter_t filter{&n, &chs};

  // Create a default event; this should not be handled.
  auto ev = std::make_unique<event>();
  ASSERT_THROW(filter.consume(std::move(ev)), ::channeler::exception);

  // The filter should also throw on a null event
  ASSERT_THROW(filter.consume(nullptr), ::channeler::exception);
}


TEST(PipeIngressQueueDrainingFilter, unknown_channel)
{
  using namespace channeler::pipe;

  next n;
  typename test_params::channel_set chs;
  simple_filter_t filter{&n, &chs};

  // Create an event with an unknown channel ID.
  auto id = channeler::create_new_channelid();
  ASSERT_EQ(channeler::ERR_SUCCESS, complete_channelid(id));
  auto ev = std::make_unique<typename simple_filter_t::input_event>(id);

  // Consuming this must fail.
  auto res = filter.consume(std::move(ev));
  ASSERT_EQ(1, res.size());
  auto & rev = *res.begin();

  ASSERT_EQ(AT_ERROR, rev->type);

  auto conv = reinterpret_cast<channeler::pipe::error_action *>(rev.get());
  ASSERT_EQ(channeler::ERR_INVALID_CHANNELID, conv->error);
}


TEST(PipeIngressQueueDrainingFilter, empty_queue)
{
  using namespace channeler::pipe;

  next n;
  typename test_params::channel_set chs;
  simple_filter_t filter{&n, &chs};

  // Create and add a new channel
  auto id = channeler::create_new_channelid();
  ASSERT_EQ(channeler::ERR_SUCCESS, complete_channelid(id));
  auto [err, chopt] = chs.add(id);
  ASSERT_EQ(channeler::ERR_SUCCESS, err);

  // When the channel is created, push an event with this ID. There should
  // be no error, but also no invocation of the next filter because the
  // channel's queue is empty.
  auto ev = std::make_unique<typename simple_filter_t::input_event>(id);
  auto res = filter.consume(std::move(ev));
  ASSERT_TRUE(res.empty());
  ASSERT_FALSE(n.m_event);
}


TEST(PipeIngressQueueDrainingFilter, drain_queue_with_loss)
{
  using namespace channeler::pipe;

  next n;
  typename test_params::channel_set chs;
  simple_filter_t filter{&n, &chs};

  // Create and add a new channel. Make this a FIFO without resend, but
  // close on loss semantics. This should mean that draining the queue leads
  // to an immediate loss event.
  auto id = channeler::create_new_channelid();
  ASSERT_EQ(channeler::ERR_SUCCESS, complete_channelid(id));

  channeler::capabilities_t caps;
  caps[channeler::CAP_RESEND] = false;
  caps[channeler::CAP_ORDERED] = false;
  caps[channeler::CAP_CLOSE_ON_LOSS] = true;
  auto [err, chopt] = chs.add(id, caps);
  ASSERT_EQ(channeler::ERR_SUCCESS, err);
  ASSERT_TRUE(chopt.has_value());

  // We need to feed a packet into queue, otherwise it won't consider a loss.
  // The second packet the drain filter will read is then triggering the close
  // event.
  auto ch = chopt.value();

  // Copy packet data before passing it on to the queue
  typename test_params::pool pool{PACKET_SIZE};
  auto data = pool.allocate();
  ASSERT_EQ(1, data.use_count());

  ::memcpy(data.data(), packet_with_messages, packet_with_messages_size);
  channeler::packet_wrapper packet{data.data(), data.size()};

  typename clock_params::time_point now = 42;
  ch->ingress_buffer().push(packet, data, now);

  // The first invocation of the filter should produce an event for the first
  // packet we just pushed.
  auto ev = std::make_unique<typename simple_filter_t::input_event>(id);
  now += 1;
  auto res = filter.consume(std::move(ev), now);
  ASSERT_TRUE(res.empty());
  ASSERT_TRUE(n.m_event);
  ASSERT_EQ(1, n.m_called);

  // A subsequent invocation with a sufficient timeout should result in a
  // close action being triggered.
  n.m_event = nullptr;
  n.m_called = 0;
  now += test_params::defaults::ROUND_TRIP_TIMEOUT_USEC;
  ev = std::make_unique<typename simple_filter_t::input_event>(id);
  res = filter.consume(std::move(ev), now);
  ASSERT_FALSE(n.m_event);
  ASSERT_EQ(0, n.m_called);

  ASSERT_EQ(1, res.size());
  auto & rev = *res.begin();

  ASSERT_EQ(AT_NOTIFY_CHANNEL_CLOSED, rev->type);

  auto conv = reinterpret_cast<channeler::pipe::notify_channel_closed_action *>(rev.get());
  ASSERT_EQ(id, conv->channel);
  ASSERT_EQ(channeler::ERR_PACKET_LOSS, conv->error);

  // After this, the channel is magically gone.
  ASSERT_FALSE(chs.has_channel(id));
}
