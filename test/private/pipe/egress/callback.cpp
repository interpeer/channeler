/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/pipe/egress/callback.h"
#include "../lib/channel_data.h"
#include "../lib/params.h"

#include <gtest/gtest.h>

#include "../../../packets.h"
#include "../../../clock.h"

namespace {

// For testing
using test_clock_params = test::clock_params;
using test_defaults = channeler::defaults<
  typename test_clock_params::duration,
  3 // POOL_BLOCK_SIZE
>;
using test_params = ::channeler::params<
  test_clock_params,
  test_defaults
>;


using filter_t = channeler::pipe::callback_filter<
  typename test_params::channel
>;

} // anonymous namespace



TEST(PipeEgressCallbackFilter, pass_events)
{
  using namespace channeler::pipe;

  std::unique_ptr<event> caught;
  filter_t filter{
    [&caught](std::unique_ptr<event> ev) -> action_list_type
    {
      caught = std::move(ev);
      return {};
    }
  };

  // Create a default event; this should not be handled.
  auto ev = std::make_unique<event>();

  ASSERT_FALSE(caught);
  auto res = filter.consume(std::move(ev));
  ASSERT_TRUE(res.empty());
  ASSERT_TRUE(caught);
}
