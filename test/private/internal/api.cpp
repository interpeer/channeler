/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/internal/api.h"
#include "../lib/context/node.h"
#include "../lib/context/connection.h"

#include <liberate/string/hexencode.h>

#include <gtest/gtest.h>

#include "../../clock.h"

#include <cstring>

namespace {

static constexpr std::size_t PACKET_SIZE = 120;

using test_clock_params = test::clock_params;
using test_defaults = channeler::defaults<
  typename test_clock_params::duration,
  3 // POOL_BLOCK_SIZE
>;
using address_t = uint16_t;

using test_params = ::channeler::params<
  test_clock_params,
  test_defaults,
  channeler::null_lock_policy,
  address_t
>;

using node_t = ::channeler::context::node<
  test_params
>;

using connection_t = ::channeler::context::connection<
  test_params,
  node_t
>;

using api_t = channeler::internal::connection_api<
  test_params,
  connection_t
>;


constexpr char const hello[] = "hello, world!";
constexpr std::size_t hello_size = sizeof(hello);


struct packet_loop_callback
{
  // Losses, not in internal sequence numbers, but in invocations of the
  // callback.
  using loss_set = std::set<std::size_t>;
  using loss_map = std::map<std::size_t, std::size_t>;

  inline packet_loop_callback(api_t *& self, api_t *& peer,
      loss_set const & losses = {})
    : m_self{self}
    , m_peer{peer}
    , m_losses{}
  {
    for (auto l : losses) {
      m_losses[l] = 0;
    }
  }

  channeler::error_t packet_to_send(
      typename test_clock_params::time_point const & now,
      channeler::channelid const & channel,
      channeler::sequence_no_t const & seqno
    )
  {
    ++m_call_count;

    // If we have a packet to send, we'll just feed it to our peer.
    auto entry = m_self->packet_to_send(now, channel, seqno);

    // liberate::string::wide_hexdump hd;
    liberate::string::canonical_hexdump hd;
    LIBLOG_DEBUG("Packet data: " << entry.packet.sequence_no() << " / "
        << entry.packet.channel()
        << " from " << entry.packet.sender() << " to " << entry.packet.recipient()
        << std::endl
        << hd(entry.packet.buffer(), entry.packet.buffer_size()));

    // See if we're supposed to lose this packet for testing purposes
    auto iter = m_losses.find(m_call_count);
    if (iter != m_losses.end()) {
      iter->second += 1;
      LIBLOG_DEBUG("Supposed to lose this packet (count): " << m_call_count);
      // We *pretend* everything is alright, because the packet is supposed to
      // be lost in transit.
      return channeler::ERR_SUCCESS;
    }
    LIBLOG_DEBUG("Pushing packet to peer: " << m_call_count);

    // Allocate a peer slot, copy packet.
    auto peer_slot = m_peer->allocate();
    EXPECT_EQ(entry.packet.buffer_size(), peer_slot.size());
    memcpy(peer_slot.data(), entry.packet.buffer(), entry.packet.buffer_size());

    // Let the peer consume its slot
    m_peer->received_packet(now, 123, 321, peer_slot);
    return channeler::ERR_SUCCESS;
  }

  std::size_t m_call_count = 0;
  api_t *&    m_self;
  api_t *&    m_peer;
  loss_map    m_losses;
};


struct channel_establishment_callback
{
  channeler::channelid      m_id = channeler::DEFAULT_CHANNELID;
  channeler::capabilities_t m_caps = {};

  void callback(typename test_clock_params::time_point const &, channeler::error_t err, channeler::channelid const & id,
      channeler::capabilities_t const & caps)
  {
    ASSERT_EQ(channeler::ERR_SUCCESS, err);
    m_id = id;
    m_caps = caps;
  }
};


struct data_available_callback
{
  channeler::channelid  m_id = channeler::DEFAULT_CHANNELID;
  std::size_t           m_size = 0;

  void callback(typename test_clock_params::time_point const &, channeler::channelid const & id, std::size_t size)
  {
    m_id = id;
    m_size = size;
  }
};


template <typename peer_apiT>
inline void
test_data_exchange(channeler::channelid const & id,
    std::string const & message,
    peer_apiT & peer1,
    data_available_callback const & callback2,
    peer_apiT & peer2)
{
  using namespace channeler;

  typename test_clock_params::time_point now{};

  // Write to peer1
  std::size_t written = 0;
  auto err = peer1.channel_write(now,
      id, message.c_str(), message.size(), written);
  ASSERT_EQ(ERR_SUCCESS, err);
  ASSERT_EQ(written, message.size());

  // Read from peer2 - we must advance the time point to permit for resends,
  // i.e. by some multiple of the round trip timeout.
  now = test_params::defaults::ROUND_TRIP_TIMEOUT_USEC * 10;

  std::vector<char> buf;
  buf.resize(message.size() * 2);

  std::size_t read = 0;
  err = peer2.channel_read(typename test_clock_params::time_point{},
      id, &buf[0], buf.size(), read);
  ASSERT_EQ(ERR_SUCCESS, err);
  ASSERT_EQ(read, message.size());
  for (std::size_t i = 0 ; i < read ; ++i) {
    ASSERT_EQ(message[i], buf[i]);
  }

  // Peer2's data callback must have been called.
  ASSERT_NE(callback2.m_id, DEFAULT_CHANNELID);
  ASSERT_EQ(callback2.m_id, id);
  ASSERT_EQ(callback2.m_size, message.size());
}

// All test cases
static channeler::peerid self;
static channeler::peerid peer;

static node_t self_node{
  self,
  PACKET_SIZE,
  []() -> std::vector<channeler::byte> { return {}; },
  [](channeler::support::timeouts::duration d) { return d; },
};

static node_t peer_node{
  peer,
  PACKET_SIZE,
  []() -> std::vector<channeler::byte> { return {}; },
  [](channeler::support::timeouts::duration d) { return d; },
};



} // anonymous namespace


TEST(InternalAPI, create)
{
  using namespace channeler::fsm;

  channeler::timeout_config to_config;
  connection_t ctx{self_node, peer, to_config};

  api_t api{ctx};
}


TEST(InternalAPI, fail_sending_data_on_default_channel)
{
  using namespace channeler::fsm;
  using namespace channeler;

  channeler::timeout_config to_config;
  connection_t ctx{self_node, peer, to_config};

  api_t api{ctx};

  std::size_t written = 0;

  auto err = api.channel_write(typename test_clock_params::time_point{},
      DEFAULT_CHANNELID, hello, hello_size, written);
  ASSERT_EQ(ERR_INVALID_CHANNELID, err);
  ASSERT_EQ(0, written);
}


TEST(InternalAPI, establish_channel)
{
  // FIXME this is failing because the default channels' egress filter
  // retains packets from the handshake; they do *not* get acknowledged,
  // so are *not* purged from the egress buffer. As a consequence, the initial
  // handshake packet gets processed twice.
  // Some packets must be dropped immediately. We can make this based on the
  // caps, I suppose, that some queues do not retain them. Specifically that
  // would have to be the no_resend_policy's decision.
  // -> give this a retain() function?
  // XXX BUT actually this is something the ordering policy
  //         enacts. SHOULD THIS BE FIFO behaviour?
  //   -> ORDERING HAS erase_after_read
  //   -> SET THIS from resend policy - does resend? do not erase. does not resend? do erase.
  using namespace channeler::fsm;
  using namespace channeler;

  channeler::timeout_config to_config;
  connection_t ctx1{self_node, peer, to_config};
  connection_t ctx2{peer_node, self, to_config};

  api_t * peer_api1 = nullptr;
  api_t * peer_api2 = nullptr;

  packet_loop_callback loop1{peer_api1, peer_api2};
  packet_loop_callback loop2{peer_api2, peer_api1};

  channel_establishment_callback ccb1;
  channel_establishment_callback ccb2;

  using namespace std::placeholders;

  peer_api1 = new api_t{
    ctx1,
    std::bind(&channel_establishment_callback::callback, &ccb1, _1, _2, _3, _4),
    {},
    std::bind(&packet_loop_callback::packet_to_send, &loop1, _1, _2, _3)
  };
  peer_api2 = new api_t{
    ctx2,
    std::bind(&channel_establishment_callback::callback, &ccb2, _1, _2, _3, _4),
    {},
    std::bind(&packet_loop_callback::packet_to_send, &loop2, _1, _2, _3)
  };

  // Before channel establishment: defaults
  ASSERT_EQ(DEFAULT_CHANNELID, ccb1.m_id);
  ASSERT_EQ(DEFAULT_CHANNELID, ccb2.m_id);

  // Establish channel
  auto err = peer_api1->establish_channel(typename test_clock_params::time_point{}, ctx2.node().id());
  ASSERT_EQ(ERR_SUCCESS, err);

  // After channel establishment: identifiers are non-default and must match
  ASSERT_NE(DEFAULT_CHANNELID, ccb1.m_id);
  ASSERT_NE(DEFAULT_CHANNELID, ccb2.m_id);
  ASSERT_EQ(ccb1.m_id, ccb2.m_id);

  // Default capabilities are datagram
  ASSERT_EQ(ccb1.m_caps, capabilities_datagram());
  ASSERT_EQ(ccb2.m_caps, capabilities_datagram());

  delete peer_api1;
  delete peer_api2;
}


TEST(InternalAPI, establish_channel_with_caps)
{
  using namespace channeler::fsm;
  using namespace channeler;

  channeler::timeout_config to_config;
  connection_t ctx1{self_node, peer, to_config};
  connection_t ctx2{peer_node, self, to_config};

  api_t * peer_api1 = nullptr;
  api_t * peer_api2 = nullptr;

  packet_loop_callback loop1{peer_api1, peer_api2};
  packet_loop_callback loop2{peer_api2, peer_api1};

  channel_establishment_callback ccb1;
  channel_establishment_callback ccb2;

  using namespace std::placeholders;

  peer_api1 = new api_t{
    ctx1,
    std::bind(&channel_establishment_callback::callback, &ccb1, _1, _2, _3, _4),
    {},
    std::bind(&packet_loop_callback::packet_to_send, &loop1, _1, _2, _3)
  };
  peer_api2 = new api_t{
    ctx2,
    std::bind(&channel_establishment_callback::callback, &ccb2, _1, _2, _3, _4),
    {},
    std::bind(&packet_loop_callback::packet_to_send, &loop2, _1, _2, _3)
  };

  // Before channel establishment: defaults
  ASSERT_EQ(DEFAULT_CHANNELID, ccb1.m_id);
  ASSERT_EQ(DEFAULT_CHANNELID, ccb2.m_id);

  // Establish channel
  auto err = peer_api1->establish_channel(typename test_clock_params::time_point{}, ctx2.node().id(),
      capabilities_stream());
  ASSERT_EQ(ERR_SUCCESS, err);

  // After channel establishment: identifiers are non-default and must match
  ASSERT_NE(DEFAULT_CHANNELID, ccb1.m_id);
  ASSERT_NE(DEFAULT_CHANNELID, ccb2.m_id);
  ASSERT_EQ(ccb1.m_id, ccb2.m_id);

  // Ensure capabilities are passed correctly
  ASSERT_EQ(ccb1.m_caps, capabilities_stream());
  ASSERT_EQ(ccb2.m_caps, capabilities_stream());

  delete peer_api1;
  delete peer_api2;
}



TEST(InternalAPI, send_data_on_established_channel)
{
  using namespace channeler::fsm;
  using namespace channeler;

  channeler::timeout_config to_config;
  connection_t ctx1{self_node, peer, to_config};
  connection_t ctx2{peer_node, self, to_config};

  api_t * peer_api1 = nullptr;
  api_t * peer_api2 = nullptr;

  packet_loop_callback loop1{peer_api1, peer_api2};
  packet_loop_callback loop2{peer_api2, peer_api1};

  channel_establishment_callback ccb1;
  channel_establishment_callback ccb2;

  data_available_callback dcb1;
  data_available_callback dcb2;

  using namespace std::placeholders;

  peer_api1 = new api_t{
    ctx1,
    std::bind(&channel_establishment_callback::callback, &ccb1, _1, _2, _3, _4),
    {},
    std::bind(&packet_loop_callback::packet_to_send, &loop1, _1, _2, _3),
    std::bind(&data_available_callback::callback, &dcb1, _1, _2, _3)
  };
  peer_api2 = new api_t{
    ctx2,
    std::bind(&channel_establishment_callback::callback, &ccb2, _1, _2, _3, _4),
    {},
    std::bind(&packet_loop_callback::packet_to_send, &loop2, _1, _2, _3),
    std::bind(&data_available_callback::callback, &dcb2, _1, _2, _3)
  };

  // *** Establish channel
  auto err = peer_api1->establish_channel(typename test_clock_params::time_point{}, ctx2.node().id());
  EXPECT_EQ(ERR_SUCCESS, err);

  // *** Send data from peer1 to peer2
  test_data_exchange(ccb1.m_id, "Test #1", *peer_api1,
      dcb2, *peer_api2);

  // *** Send data from peer2 to peer1
  test_data_exchange(ccb1.m_id, "Test #2", *peer_api2,
      dcb1, *peer_api1);

  delete peer_api1;
  delete peer_api2;
}


TEST(InternalAPI, send_data_on_established_channel_with_caps)
{
  using namespace channeler::fsm;
  using namespace channeler;

  channeler::timeout_config to_config;
  connection_t ctx1{self_node, peer, to_config};
  connection_t ctx2{peer_node, self, to_config};

  api_t * peer_api1 = nullptr;
  api_t * peer_api2 = nullptr;

  packet_loop_callback loop1{peer_api1, peer_api2};
  packet_loop_callback loop2{peer_api2, peer_api1};

  channel_establishment_callback ccb1;
  channel_establishment_callback ccb2;

  data_available_callback dcb1;
  data_available_callback dcb2;

  using namespace std::placeholders;

  peer_api1 = new api_t{
    ctx1,
    std::bind(&channel_establishment_callback::callback, &ccb1, _1, _2, _3, _4),
    {},
    std::bind(&packet_loop_callback::packet_to_send, &loop1, _1, _2, _3),
    std::bind(&data_available_callback::callback, &dcb1, _1, _2, _3)
  };
  peer_api2 = new api_t{
    ctx2,
    std::bind(&channel_establishment_callback::callback, &ccb2, _1, _2, _3, _4),
    {},
    std::bind(&packet_loop_callback::packet_to_send, &loop2, _1, _2, _3),
    std::bind(&data_available_callback::callback, &dcb2, _1, _2, _3)
  };

  // *** Establish channel
  auto err = peer_api1->establish_channel(typename test_clock_params::time_point{}, ctx2.node().id(),
      capabilities_stream());
  EXPECT_EQ(ERR_SUCCESS, err);

  // *** Send data from peer1 to peer2
  test_data_exchange(ccb1.m_id, "Test #1", *peer_api1,
      dcb2, *peer_api2);

  // *** Send data from peer2 to peer1
  test_data_exchange(ccb1.m_id, "Test #2", *peer_api2,
      dcb1, *peer_api1);

  delete peer_api1;
  delete peer_api2;
}



TEST(InternalAPI, send_data_on_established_channel_with_close_on_loss)
{
  using namespace channeler::fsm;
  using namespace channeler;

  channeler::timeout_config to_config;
  connection_t ctx1{self_node, peer, to_config};
  connection_t ctx2{peer_node, self, to_config};

  api_t * peer_api1 = nullptr;
  api_t * peer_api2 = nullptr;

  packet_loop_callback loop1{peer_api1, peer_api2, {4}};
  packet_loop_callback loop2{peer_api2, peer_api1};

  channel_establishment_callback ccb1;
  channel_establishment_callback ccb2;

  data_available_callback dcb1;
  data_available_callback dcb2;

  using namespace std::placeholders;

  bool closed = false;
  peer_api1 = new api_t{
    ctx1,
    std::bind(&channel_establishment_callback::callback, &ccb1, _1, _2, _3, _4),
    {},
    std::bind(&packet_loop_callback::packet_to_send, &loop1, _1, _2, _3),
    std::bind(&data_available_callback::callback, &dcb1, _1, _2, _3)
  };
  peer_api2 = new api_t{
    ctx2,
    std::bind(&channel_establishment_callback::callback, &ccb2, _1, _2, _3, _4),
    [&closed](typename test_params::clock_params::time_point const &, channeler::error_t err, channeler::channelid const & _id)
    {
      LIBLOG_ET("Channel close callback " << _id << ":", err);
      closed = true;
    },
    std::bind(&packet_loop_callback::packet_to_send, &loop2, _1, _2, _3),
    std::bind(&data_available_callback::callback, &dcb2, _1, _2, _3)
  };

  // *** Establish channel
  typename test_clock_params::time_point now{};

  channeler::capabilities_t caps;
  caps[channeler::CAP_RESEND] = false;
  caps[channeler::CAP_ORDERED] = false;
  caps[channeler::CAP_CLOSE_ON_LOSS] = true;

  auto err = peer_api1->establish_channel(now, ctx2.node().id(), caps);
  EXPECT_EQ(ERR_SUCCESS, err);

  // A packet can only be lost if a first packet has been sent, i.e.
  // some expectation is created of what the second one might be.

  // Write to peer1
  std::string message = "Test #1";
  std::size_t written = 0;
  err = peer_api1->channel_write(now, ccb1.m_id,
      message.c_str(), message.size(), written);
  ASSERT_EQ(ERR_SUCCESS, err);
  ASSERT_EQ(written, message.size());

  // Read from peer2
  now += test_params::defaults::ROUND_TRIP_TIMEOUT_USEC;

  std::vector<char> buf;
  buf.resize(message.size() * 2);

  std::size_t read = 0;
  err = peer_api2->channel_read(now, ccb1.m_id,
      &buf[0], buf.size(), read);
  ASSERT_EQ(ERR_SUCCESS, err);
  ASSERT_EQ(read, message.size());
  for (std::size_t i = 0 ; i < read ; ++i) {
    ASSERT_EQ(message[i], buf[i]);
  }

  // If we write again, the message will be lost (#4 above)
  now += test_params::defaults::ROUND_TRIP_TIMEOUT_USEC;

  message = "Test #2";
  written = 0;
  err = peer_api1->channel_write(now, ccb1.m_id,
      message.c_str(), message.size(), written);
  ASSERT_EQ(ERR_SUCCESS, err);
  ASSERT_EQ(written, message.size());

  ASSERT_FALSE(closed);

  // Reading a second time should result in the API reporting the
  // lost packet.
  now += test_params::defaults::ROUND_TRIP_TIMEOUT_USEC;

  std::memset(&buf[0], '\0', buf.size());

  read = 0;
  err = peer_api2->channel_read(now, ccb1.m_id,
      &buf[0], buf.size(), read);
  ASSERT_EQ(ERR_PACKET_LOSS, err);

  ASSERT_TRUE(closed);

  delete peer_api1;
  delete peer_api2;
}



TEST(InternalAPI, send_data_on_established_channel_with_resend_and_recovery)
{
  // We need 64 bits of time resolution in usec to deal with exponential backoffs,
  // otherwise timeouts may wrap around.
  static_assert(sizeof(typename test_clock_params::time_point) >= 8);

  using namespace channeler::fsm;
  using namespace channeler;

  channeler::timeout_config to_config;
  connection_t ctx1{self_node, peer, to_config};
  connection_t ctx2{peer_node, self, to_config};

  api_t * peer_api1 = nullptr;
  api_t * peer_api2 = nullptr;

  packet_loop_callback loop1{peer_api1, peer_api2, {4}};
  packet_loop_callback loop2{peer_api2, peer_api1};

  channel_establishment_callback ccb1;
  channel_establishment_callback ccb2;

  data_available_callback dcb1;
  data_available_callback dcb2;

  using namespace std::placeholders;

  bool closed = false;
  peer_api1 = new api_t{
    ctx1,
    std::bind(&channel_establishment_callback::callback, &ccb1, _1, _2, _3, _4),
    {},
    std::bind(&packet_loop_callback::packet_to_send, &loop1, _1, _2, _3),
    std::bind(&data_available_callback::callback, &dcb1, _1, _2, _3)
  };
  peer_api2 = new api_t{
    ctx2,
    std::bind(&channel_establishment_callback::callback, &ccb2, _1, _2, _3, _4),
    [&closed](typename test_params::clock_params::time_point const &, channeler::error_t err, channeler::channelid const & _id)
    {
      LIBLOG_ET("Channel close callback " << _id << ":", err);
      closed = true;
    },
    std::bind(&packet_loop_callback::packet_to_send, &loop2, _1, _2, _3),
    std::bind(&data_available_callback::callback, &dcb2, _1, _2, _3)
  };

  // *** Establish channel
  typename test_clock_params::time_point now{};

  auto caps = capabilities_stream();
  auto err = peer_api1->establish_channel(now, ctx2.node().id(), caps);
  EXPECT_EQ(ERR_SUCCESS, err);

  // A packet can only be lost if a first packet has been sent, i.e.
  // some expectation is created of what the second one might be.

  // Write to peer1
  std::string message = "Test #1";
  std::size_t written = 0;
  err = peer_api1->channel_write(now, ccb1.m_id,
      message.c_str(), message.size(), written);
  ASSERT_EQ(ERR_SUCCESS, err);
  ASSERT_EQ(written, message.size());

  // Read from peer2
  now += test_params::defaults::ROUND_TRIP_TIMEOUT_USEC;

  std::vector<char> buf;
  buf.resize(message.size() * 2);

  std::size_t read = 0;
  err = peer_api2->channel_read(now, ccb1.m_id,
      &buf[0], buf.size(), read);
  ASSERT_EQ(ERR_SUCCESS, err);
  ASSERT_EQ(read, message.size());
  for (std::size_t i = 0 ; i < read ; ++i) {
    ASSERT_EQ(message[i], buf[i]);
  }

  // If we write again, the message will be lost (#4 above)
  now += test_params::defaults::ROUND_TRIP_TIMEOUT_USEC;

  message = "Test #2";
  written = 0;
  err = peer_api1->channel_write(now, ccb1.m_id,
      message.c_str(), message.size(), written);
  ASSERT_EQ(ERR_SUCCESS, err);
  ASSERT_EQ(written, message.size());

  ASSERT_FALSE(closed);

  // Because of the way the callback above immediately writes to the recipient,
  // reading and expecting unavailable data is not possible. By the time we
  // read, the resend mechanism must have returned the data. So we must on the
  // one hand expect delivery, but on the other hand also that the original packet
  // #4 *was* lost.
  now += test_params::defaults::ROUND_TRIP_TIMEOUT_USEC;

  std::memset(&buf[0], '\0', buf.size());

  read = 0;
  err = peer_api2->channel_read(now, ccb1.m_id,
      &buf[0], buf.size(), read);
  ASSERT_EQ(ERR_SUCCESS, err);

  ASSERT_EQ(read, message.size());
  std::string tmp{buf.data(), buf.data() + read};
  ASSERT_EQ(tmp, message);

  ASSERT_EQ(loop1.m_losses[4], 1);

  delete peer_api1;
  delete peer_api2;
}


TEST(InternalAPI, send_data_on_established_channel_with_resend_and_close_on_loss)
{
  // We need 64 bits of time resolution in usec to deal with exponential backoffs,
  // otherwise timeouts may wrap around.
  static_assert(sizeof(typename test_clock_params::time_point) >= 8);

  using namespace channeler::fsm;
  using namespace channeler;

  channeler::timeout_config to_config;
  connection_t ctx1{self_node, peer, to_config};
  connection_t ctx2{peer_node, self, to_config};

  api_t * peer_api1 = nullptr;
  api_t * peer_api2 = nullptr;

  // Lose everything from #4 onwards
  packet_loop_callback loop1{peer_api1, peer_api2, {4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}};
  packet_loop_callback loop2{peer_api2, peer_api1};

  channel_establishment_callback ccb1;
  channel_establishment_callback ccb2;

  data_available_callback dcb1;
  data_available_callback dcb2;

  using namespace std::placeholders;

  bool closed = false;
  peer_api1 = new api_t{
    ctx1,
    std::bind(&channel_establishment_callback::callback, &ccb1, _1, _2, _3, _4),
    {},
    std::bind(&packet_loop_callback::packet_to_send, &loop1, _1, _2, _3),
    std::bind(&data_available_callback::callback, &dcb1, _1, _2, _3)
  };
  peer_api2 = new api_t{
    ctx2,
    std::bind(&channel_establishment_callback::callback, &ccb2, _1, _2, _3, _4),
    [&closed](typename test_params::clock_params::time_point const &, channeler::error_t err, channeler::channelid const & _id)
    {
      LIBLOG_ET("Channel close callback " << _id << ":", err);
      closed = true;
    },
    std::bind(&packet_loop_callback::packet_to_send, &loop2, _1, _2, _3),
    std::bind(&data_available_callback::callback, &dcb2, _1, _2, _3)
  };

  // *** Establish channel
  typename test_clock_params::time_point now{};

  auto caps = capabilities_stream();
  auto err = peer_api1->establish_channel(now, ctx2.node().id(), caps);
  EXPECT_EQ(ERR_SUCCESS, err);

  // A packet can only be lost if a first packet has been sent, i.e.
  // some expectation is created of what the second one might be.

  // Write to peer1
  std::string message = "Test #1";
  std::size_t written = 0;
  err = peer_api1->channel_write(now, ccb1.m_id,
      message.c_str(), message.size(), written);
  ASSERT_EQ(ERR_SUCCESS, err);
  ASSERT_EQ(written, message.size());

  // Read from peer2
  now += test_params::defaults::ROUND_TRIP_TIMEOUT_USEC;

  std::vector<char> buf;
  buf.resize(message.size() * 2);

  std::size_t read = 0;
  err = peer_api2->channel_read(now, ccb1.m_id,
      &buf[0], buf.size(), read);
  ASSERT_EQ(ERR_SUCCESS, err);
  ASSERT_EQ(read, message.size());
  for (std::size_t i = 0 ; i < read ; ++i) {
    ASSERT_EQ(message[i], buf[i]);
  }

  // If we write again, the message will be lost (#4 above)
  now += test_params::defaults::ROUND_TRIP_TIMEOUT_USEC;

  message = "Test #2";
  written = 0;
  err = peer_api1->channel_write(now, ccb1.m_id,
      message.c_str(), message.size(), written);
  ASSERT_EQ(ERR_SUCCESS, err);
  ASSERT_EQ(written, message.size());

  ASSERT_FALSE(closed);

  // Because of the way the callback above immediately writes to the recipient,
  // we immediately have to expect the message to be lost.
  now += test_params::defaults::ROUND_TRIP_TIMEOUT_USEC;

  std::memset(&buf[0], '\0', buf.size());

  read = 0;
  err = peer_api2->channel_read(now, ccb1.m_id,
      &buf[0], buf.size(), read);
  ASSERT_EQ(ERR_DATA_UNAVAILABLE, err);

  // Waiting longer for the timeouts to kick in must result in the channel
  // deciding it's unrecoverable. This timeout must take into account the
  // exponential backoff, though.
  typename test_clock_params::time_point max_wait = 0;
  auto collisions = test_params::defaults::MAX_RESEND_COLLISIONS;
  while (collisions > 0) {
    max_wait += (1 + exp2(collisions))
      * test_params::defaults::ROUND_TRIP_TIMEOUT_USEC;
    --collisions;
  }
  now += max_wait;

  std::memset(&buf[0], '\0', buf.size());

  read = 0;
  err = peer_api2->channel_read(now, ccb1.m_id,
      &buf[0], buf.size(), read);
  ASSERT_EQ(ERR_PACKET_LOSS, err);

  // At this point we'll not only need to be closed, but also we'll have to
  // have recorded some packet losses
  ASSERT_EQ(loop1.m_losses[4], 1);
  ASSERT_EQ(loop1.m_losses[5], 1);
  // etc.

  ASSERT_TRUE(closed);

  delete peer_api1;
  delete peer_api2;
}
