/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_PIPE_INGRESS_H
#define CHANNELER_PIPE_INGRESS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include "filter_classifier.h"

#include "ingress/state_handling.h"
#include "ingress/message_parsing.h"
#include "ingress/queue_draining.h"
#include "ingress/channel_assign.h"
#include "ingress/validate.h"
#include "ingress/route.h"
#include "ingress/de_envelope.h"

#include "../fsm/default.h"

namespace channeler::pipe {

/**
 * The default ingress filter pipe is split into two stages.
 * - The first stage processes from raw I/O to the ingress queue.
 * - The second stage starts by draining the ingress queue.
 */
template <
  typename paramsT,
  typename stage2T
>
struct default_ingress_stage1
{
  using params = paramsT;

  // *** Filter type aliases
  using channel_assign = channel_assign_filter<
    typename params::transport_address,
    params::defaults::POOL_BLOCK_SIZE,
    stage2T, typename stage2T::input_event,
    typename params::channel,
    typename params::peer_failure_policy,
    typename params::transport_failure_policy
  >;
  using validate = validate_filter<
    typename params::transport_address,
    params::defaults::POOL_BLOCK_SIZE,
    channel_assign, typename channel_assign::input_event,
    typename params::peer_failure_policy,
    typename params::transport_failure_policy
  >;
  using route = route_filter<
    typename params::transport_address,
    params::defaults::POOL_BLOCK_SIZE,
    validate, typename validate::input_event
  >;
  using de_envelope = de_envelope_filter<
    typename params::transport_address,
    params::defaults::POOL_BLOCK_SIZE,
    route, typename route::input_event
  >;

  inline default_ingress_stage1(
      stage2T & stage2,
      typename params::channel_set & channels,
      typename params::peer_failure_policy * peer_p = nullptr,
      typename params::transport_failure_policy * trans_p = nullptr
    )
    : m_stage2{stage2}
    , m_channel_assign{&stage2, &channels, peer_p, trans_p}
    , m_validate{&m_channel_assign, peer_p, trans_p}
    , m_route{&m_validate}
    , m_de_envelope{&m_route}
  {
  }


  inline action_list_type
  consume(
      typename params::clock_params::time_point const & now,
      std::unique_ptr<event> ev
    )
  {
    // Swap the parameters around; filters don't need to know about the time
    // point, really, except as something to pass on.
    return m_de_envelope.consume(std::move(ev), now);
  }

  stage2T &             m_stage2;

  channel_assign        m_channel_assign;
  validate              m_validate;
  route                 m_route;
  de_envelope           m_de_envelope;
};


template <
  typename paramsT
>
struct default_ingress_stage2
{
  using params = paramsT;

  using fsm_registry = ::channeler::fsm::registry;
  using message_registry = std::shared_ptr<::channeler::message_registry>;

  // *** Filter type aliases
  using state_handling = state_handling_filter<
    params::defaults::POOL_BLOCK_SIZE,
    typename params::channel,
    fsm_registry,
    // XXX: The only filter to which we have to pass this - it's the extra
    //      args to consume().
    typename params::clock_params::time_point const &
  >;
  using message_parsing = message_parsing_filter<
    params::defaults::POOL_BLOCK_SIZE,
    state_handling, typename state_handling::input_event,
    typename params::channel
  >;
  using queue_draining = queue_draining_filter<
    params::defaults::POOL_BLOCK_SIZE,
    message_parsing, typename message_parsing::input_event,
    typename params::channel
  >;


  inline default_ingress_stage2(
      fsm_registry & registry,
      message_registry msg_registry,
      typename state_handling::event_route_map_t & route_map,
      typename params::channel_set & channels
    )
    : m_message_registry{msg_registry}
    , m_state_handling{registry, route_map}
    , m_message_parsing{m_message_registry, &m_state_handling}
    , m_queue_draining{&m_message_parsing, &channels}
  {
  }


  inline action_list_type
  consume(
      typename params::clock_params::time_point const & now,
      std::unique_ptr<event> ev
    )
  {
    // Swap the parameters around; filters don't need to know about the time
    // point, really, except as something to pass on.
    return m_queue_draining.consume(std::move(ev), now);
  }


  // We have to behave a bit like a filter, which means we need an input
  // event as well as a consume() function with a filter protootype.
  using input_event = typename queue_draining::input_event;

  template <typename... argsT>
  inline action_list_type
  consume(std::unique_ptr<event> ev, argsT && ...args)
  {
    return consume(std::forward<argsT>(args)..., std::move(ev));
  }

  message_registry      m_message_registry;

  state_handling        m_state_handling;
  message_parsing       m_message_parsing;
  queue_draining        m_queue_draining;
};


/**
 * Default ingress filter pipe, combined
 */
template <
  typename paramsT
>
struct default_ingress
{
  using params = paramsT;

  using stage2_t = default_ingress_stage2<paramsT>;
  using stage1_t = default_ingress_stage1<paramsT, stage2_t>;

  inline default_ingress(
      typename stage2_t::fsm_registry & registry,
      typename stage2_t::message_registry msg_registry,
      typename stage2_t::state_handling::event_route_map_t & route_map,
      typename params::channel_set & channels,
      typename params::peer_failure_policy * peer_p = nullptr,
      typename params::transport_failure_policy * trans_p = nullptr
    )
    : m_stage2{registry, msg_registry, route_map, channels}
    , m_stage1{m_stage2, channels, peer_p, trans_p}
  {
  }


  inline action_list_type
  consume(
      typename params::clock_params::time_point const & now,
      std::unique_ptr<event> ev
    )
  {
    // Forward the event to stage1
    return m_stage1.consume(now, std::move(ev));
  }

  inline action_list_type
  consume_stage2(
      typename params::clock_params::time_point const & now,
      std::unique_ptr<event> ev
    )
  {
    // Forward the event to stage2
    return m_stage2.consume(now, std::move(ev));
  }

  stage2_t              m_stage2;
  stage1_t              m_stage1;
};


} // namespace channeler::pipe

#endif // guard
