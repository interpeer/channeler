/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef TEST_CLOCK_H
#define TEST_CLOCK_H

#include "../lib/clock_params.h"

namespace test {

/**
 * For testing purposes, use integers for time points and durations. The
 * clock's now() function always returns zero (and produces a warning when
 * used).
 *
 * This is because we *typicall* pass the current time point into any API
 * functions.
 */
namespace detail {
struct clock
{
  using time_point = uint64_t;
  using duration = uint64_t;

  static inline uint64_t now()
  {
    return 0;
  }
};

} // namespace detail

using clock_params = ::channeler::clock_params<
  detail::clock,
  detail::clock::time_point,
  detail::clock::duration
>;

} // namespace test

#endif // guard
