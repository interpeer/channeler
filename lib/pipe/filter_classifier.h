/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_PIPE_FILTER_CLASSIFIER_H
#define CHANNELER_PIPE_FILTER_CLASSIFIER_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <memory>
#include <set>

#include "../memory/packet_pool.h"
#include "event.h"
#include "action.h"

#include <channeler/packet.h>
#include <channeler/error.h>


namespace channeler::pipe {

/**
 * Null policy - does nothing, but provides the right kind of interface.
 *
 * See validate_filter for details.
 */
template <
  typename addressT = peerid_wrapper
>
struct null_policy
{
  inline bool should_filter(addressT const &, direction_type direction [[maybe_unused]])
  {
    return false;
  }
};


/**
 * The filter classifier doesn't classify filters - that's a bit of a misnomer,
 * and maybe something to fix in future. TODO
 * https://codeberg.org/interpeer/channeler/issues/23
 *
 * Rather, it provides the classification algorithm for what to do with packets
 * that need to be rejected. More precisely, if a packet should be rejected, it
 * should be fed to this class - which produces an action_list based on the
 * specific policies it's parametrized with.
 *
 * The main purpose of this class is re-usability across different filters.
 */
template <
  typename addressT,
  typename peer_failure_policyT,
  typename transport_failure_policyT
>
struct filter_classifier
{
  inline filter_classifier(
      peer_failure_policyT * peer_p = nullptr,
      transport_failure_policyT * trans_p = nullptr)
    : m_peer_policy{peer_p}
    , m_transport_policy{trans_p}
  {
  }


  inline action_list_type process(
      addressT const & transport_source,
      addressT const & transport_destination,
      ::channeler::packet_wrapper const & packet)
  {
    action_list_type res;

    if (m_peer_policy) {
      if (m_peer_policy->should_filter(packet.sender(), DIR_INGRESS)) {
        res.push_back(std::make_unique<peer_filter_request_action>(
                packet.sender(), DIR_INGRESS));
      }
      if (m_peer_policy->should_filter(packet.recipient(), DIR_EGRESS)) {
        res.push_back(std::make_unique<peer_filter_request_action>(
                packet.recipient(), DIR_EGRESS));
      }
    }

    if (m_transport_policy) {
      if (m_transport_policy->should_filter(transport_source, DIR_INGRESS)) {
        res.push_back(std::make_unique<transport_filter_request_action<addressT>>(
              transport_source, DIR_INGRESS));
      }
      if (m_transport_policy->should_filter(transport_destination, DIR_EGRESS)) {
        res.push_back(std::make_unique<transport_filter_request_action<addressT>>(
              transport_destination, DIR_EGRESS));
      }
    }

    return res;
  }


  peer_failure_policyT *      m_peer_policy;
  transport_failure_policyT * m_transport_policy;
};


} // namespace channeler::pipe

#endif // guard
