/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/fsm/congestion_simple.h"
#include "../lib/params.h"

#include <gtest/gtest.h>

#include "../../messages.h"
#include "../../packets.h"
#include "../../clock.h"

namespace {

constexpr static std::size_t TEST_PACKET_SIZE = 120;

using test_clock_params = test::clock_params;
using test_defaults = channeler::defaults<
  typename test_clock_params::duration,
  3 // POOL_BLOCK_SIZE
>;
using address_t = uint16_t;

using test_params = ::channeler::params<
  test_clock_params,
  test_defaults,
  channeler::null_lock_policy,
  address_t
>;

} // anonymous namespace


TEST(FSMCongestionSimple, process_bad_event)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_congestion_simple<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs
  };

  // If we feed the FSM anything other than the events it handles, there will
  // be an error state.
  action_list_type actions;
  event_list_type events;
  event ev;
  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_EQ(fsm_base::PS_UNHANDLED, ret);
}



TEST(FSMCongestionSimple, new_full_channel_results_in_timeout)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_congestion_simple<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs
  };

  // If we feed the FSM a new channel event, we'll have to get a timeout out
  // of it.
  auto id = create_new_channelid();
  ASSERT_EQ(ERR_SUCCESS, complete_channelid(id));
  chs.add(id);

  // Prior to the event, there should be no timeout active.
  {
    auto [active, to] = t.is_timeout_active(
        {CHANNEL_CONGESTION_TIMEOUT_TAG, id.initiator}
    );
    ASSERT_FALSE(active);
  }

  // Ok, let the FSM know there's a new channel.
  action_list_type actions;
  event_list_type events;
  channel_established_event ev{id};
  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_EQ(fsm_base::PS_SUCCESS, ret);

  // Now we need to have an active timeout.
  {
    auto [active, to] = t.is_timeout_active(
        {CHANNEL_CONGESTION_TIMEOUT_TAG, id.initiator}
    );
    ASSERT_TRUE(active);
  }
}


TEST(FSMCongestionSimple, bad_channel_results_in_no_timeout)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_congestion_simple<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs
  };

  // With either a partial channel or a channel not in the set, we won't
  // see a timeout. Let's do the first, it's simpler.
  auto id = create_new_channelid();

  // Prior to the event, there should be no timeout active.
  {
    auto [active, to] = t.is_timeout_active(
        {CHANNEL_CONGESTION_TIMEOUT_TAG, id.initiator}
    );
    ASSERT_FALSE(active);
  }

  // Ok, let the FSM know there's a new channel.
  action_list_type actions;
  event_list_type events;
  channel_established_event ev{id};
  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_EQ(fsm_base::PS_IGNORED, ret);

  // Now we need to have an active timeout.
  {
    auto [active, to] = t.is_timeout_active(
        {CHANNEL_CONGESTION_TIMEOUT_TAG, id.initiator}
    );
    ASSERT_FALSE(active);
  }
}


TEST(FSMCongestionSimple, close_channel_results_in_timeout_removal)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_congestion_simple<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs
  };

  // If we feed the FSM a close channel event, that should remove timeouts
  // for the channel.
  auto id = create_new_channelid();
  ASSERT_EQ(ERR_SUCCESS, complete_channelid(id));

  t.add({CHANNEL_CONGESTION_TIMEOUT_TAG, id.initiator},
      to_config.channel_progress_timeout);

  // Ok, let the FSM know the channel closed
  action_list_type actions;
  event_list_type events;
  channel_closed_event ev{id};
  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_EQ(fsm_base::PS_SUCCESS, ret);

  // Now the timeout we added must be gone.
  {
    auto [active, to] = t.is_timeout_active(
        {CHANNEL_CONGESTION_TIMEOUT_TAG, id.initiator}
    );
    ASSERT_FALSE(active);
  }
}


TEST(FSMCongestionSimple, timeout_produces_update_message)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_congestion_simple<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs
  };

  auto id = create_new_channelid();
  ASSERT_EQ(ERR_SUCCESS, complete_channelid(id));
  chs.add(id);

  // We push a timeout event. This should result in the FSM producing
  // a message on output. In order to test the output, let's set the
  // channel data's ingress buffer size.
  chs.get(id)->ingress_buffer().set_window_size(42);

  action_list_type actions;
  event_list_type events;
  fsm_t::timeout_event_type ev{{CHANNEL_CONGESTION_TIMEOUT_TAG, id.initiator}};
  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_EQ(fsm_base::PS_SUCCESS, ret);

  // The result should be a window size message.
  ASSERT_EQ(0, actions.size());
  ASSERT_EQ(1, events.size());

  auto & rev = *events.begin();
  ASSERT_TRUE(rev);
  ASSERT_EQ(ET_MESSAGE_OUT, rev->type);
  auto rev_conv = reinterpret_cast<message_out_event *>(rev.get());

  ASSERT_EQ(rev_conv->channel, id);
  ASSERT_EQ(rev_conv->message->type, MSG_CONGESTION_RECEIVE_WINDOW);

  auto dmsg = reinterpret_cast<message_congestion_receive_window *>(rev_conv->message.get());
  ASSERT_EQ(dmsg->window_size, 42);
}


TEST(FSMCongestionSimple, update_message_changes_ingress_size)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_congestion_simple<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  typename test_params::pool pool{TEST_PACKET_SIZE};
  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs
  };

  std::vector<channeler::byte> buf{test::packet_regular_channelid,
    test::packet_regular_channelid + test::packet_regular_channelid_size};
  packet_wrapper pkt{buf.data(), buf.size()};

  chs.add(pkt.channel());

  // Upon receipt of a congestion receive window message, the FSM needs to
  // update the egress window size. Let's set it to something testable.
  auto ch = chs.get(pkt.channel());
  ch->egress_buffer().set_window_size(42);

  action_list_type actions;
  event_list_type events;

  auto reg = channeler::message_registry::create();
  auto [err, msg] = reg->parse(test::message_congestion_receive_window_small,
      test::message_congestion_receive_window_small_size, false);
  ASSERT_EQ(channeler::ERR_SUCCESS, err);

  using event_t = message_event<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;
  event_t ev{pkt, pool.allocate(), ch,
    std::move(msg)
  };

  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_EQ(fsm_base::PS_SUCCESS, ret);

  // Our own egress window needs to have been updated.
  ASSERT_EQ(18, ch->egress_buffer().window_size());

  // We also need to have an action informing the API of this.
  ASSERT_EQ(1, actions.size());
  ASSERT_EQ(0, events.size());

  auto & act = *actions.begin();
  ASSERT_TRUE(act);
  ASSERT_EQ(AT_NOTIFY_CHANNEL_WINDOW, act->type);

  auto convact = reinterpret_cast<notify_channel_window_action *>(act.get());
  ASSERT_EQ(ch->id(), convact->channel);
  ASSERT_EQ(18, convact->window_size);
}


