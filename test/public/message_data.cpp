/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <channeler/message/data.h>

#include <gtest/gtest.h>

#include "../messages.h"

using namespace test;


TEST(MessageData, parse_and_serialize_data)
{
  auto reg = channeler::message_registry::create();

  std::vector<channeler::byte> b{message_data, message_data + message_data_size};

  assert_single_byte_type_variable_size_message(reg, b, channeler::MSG_DATA,
      1);

  auto [ret, msg] = reg->parse(b.data(), b.size(), false);
  ASSERT_EQ(channeler::ERR_SUCCESS, ret);
  ASSERT_TRUE(msg);
  ASSERT_EQ(msg->type, channeler::MSG_DATA);

  // Serialize
  std::vector<channeler::byte> out;
  out.resize(200);
  assert_serialization_ok(out, msg, b);
}



TEST(MessageData, from_message)
{
  auto reg = channeler::message_registry::create();

  std::string text{"Hello, world!"};

  auto msg = channeler::message_data::create_with_payload(reg, text.c_str(), text.size());
  ASSERT_TRUE(msg);
  ASSERT_EQ(msg->payload_size, text.size());
  ASSERT_NE(msg->payload, nullptr);

  // The created message is not backed by a buffer, but must have a payload
  ASSERT_FALSE(msg->has_buffer());
  ASSERT_TRUE(msg->has_payload());

  // The message's serialized size must be larger than the payload size
  auto sersize = msg->serialized_size();
  ASSERT_GT(sersize, msg->payload_size);

  // Serialize the message
  std::vector<channeler::byte> buf;
  buf.resize(sersize);
  auto used = msg->serialize(buf);
  ASSERT_EQ(used, sersize);

  // New message from serialized data
  auto [err, parsed] = reg->parse(buf, false);
  ASSERT_EQ(channeler::ERR_SUCCESS, err);
  ASSERT_TRUE(parsed);

  ASSERT_EQ(channeler::MSG_DATA, parsed->type);
  ASSERT_EQ(msg->payload_size, parsed->payload_size);

  std::string res{
    reinterpret_cast<char const *>(parsed->payload),
    reinterpret_cast<char const *>(parsed->payload) + parsed->payload_size
  };
  ASSERT_EQ(text, res);
}



TEST(MessageData, parse_and_serialize_data_progress)
{
  auto reg = channeler::message_registry::create();

  std::vector<channeler::byte> b{message_data_progress, message_data_progress + message_data_progress_size};

  assert_single_byte_type_variable_size_message(reg, b, channeler::MSG_DATA_PROGRESS, 1);

  auto [ret, msg] = reg->parse(b.data(), b.size(), false);
  ASSERT_EQ(channeler::ERR_SUCCESS, ret);
  ASSERT_TRUE(msg);
  ASSERT_EQ(msg->type, channeler::MSG_DATA_PROGRESS);

  auto ptr = reinterpret_cast<channeler::message_data_progress *>(msg.get());

  ASSERT_EQ(0x01a2, ptr->receive_window_start);

  ASSERT_EQ(3, ptr->resend_requests.size());
  {
    auto iter = ptr->resend_requests.find(0xbeef);
    ASSERT_NE(iter, ptr->resend_requests.end());
  }

  {
    auto iter = ptr->resend_requests.find(0xb4be);
    ASSERT_NE(iter, ptr->resend_requests.end());
  }

  {
    auto iter = ptr->resend_requests.find(0x0012);
    ASSERT_NE(iter, ptr->resend_requests.end());
  }

  // Serialize
  std::vector<channeler::byte> out;
  out.resize(200);
  assert_serialization_ok(out, msg, b);
}
