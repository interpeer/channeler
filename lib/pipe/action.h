/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_PIPE_ACTION_H
#define CHANNELER_PIPE_ACTION_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <list>
#include <memory>

#include <channeler/peerid.h>

namespace channeler::pipe {


/**
 * Actions percolate up the filter pipe. They have a type, and may carry
 * a type-dependent payload.
 */
enum action_type : uint_fast16_t
{
  AT_UNKNOWN = 0,
  AT_ERROR,               // Report error to upstream

  AT_FILTER_TRANSPORT,    // Request filtering of transport or peer address
  AT_FILTER_PEER,

  AT_PROCESS_EVENT,       // Request that a new event (included) shall be
                          // processed

  AT_NOTIFY_CHANNEL_ESTABLISHED,  // Notify the API of a newly established channel
  AT_NOTIFY_CHANNEL_CLOSED,       // Notify the API of a closed channel.
  AT_NOTIFY_CHANNEL_WINDOW,       // Notify the API that the peer's receive
                                  // window was updated.
};


/**
 * Some actions may apply to the INGRESS or EGRESS direction only (or both).
 * TODO: Note that the same may apply to events, so this header may not be
 *       the best location in the long term. Revisit this.
 */
enum direction_type : uint_fast8_t
{
  DIR_UNKNOWN = 0x00,
  DIR_INGRESS = 0x01,
  DIR_EGRESS  = 0x02,
  DIR_BOTH    = DIR_INGRESS | DIR_EGRESS,
};

inline direction_type assert_unidirectional(direction_type dir)
{
#if defined(DEBUG) && !defined(NDEBUG)
  if (DIR_INGRESS != dir && DIR_EGRESS != dir) {
    throw exception{
      ERR_INVALID_VALUE,
      "Unidirectional value required, but got: " + std::to_string(dir)
    };
  }
#endif
  return dir;
}



/**
 * Action base class
 */
struct action
{
  action_type const type = AT_UNKNOWN;

  inline action(action_type t = AT_UNKNOWN)
    : type{t}
  {
  }

  virtual ~action() = default;
};

// Action list type
using action_list_type = std::list<std::unique_ptr<action>>;

inline void
merge_actions(action_list_type & target, action_list_type & source)
{
#if defined(CHANNELER_WIN32)
  // In-place merge does not work on win32, because pointers are not less-than
  // comparable (technically). So, let's do this manually here.
  for (auto & item : source) {
    target.push_back(std::move(item));
  }
#else // CHANNELER_WIN32
  target.merge(source);
#endif // CHANNELER_WIN32
}


/**
 * Action for requesting filtering on different address types.
 */
template <typename addressT>
struct transport_filter_request_action
  : public action
{
  addressT        address;
  direction_type  direction;

  inline transport_filter_request_action(
      addressT const & addr,
      direction_type _direction = DIR_INGRESS
    )
    : action{AT_FILTER_TRANSPORT}
    , address{addr}
    , direction{assert_unidirectional(_direction)}
  {
  }

  virtual ~transport_filter_request_action() = default;
};

struct peer_filter_request_action
  : public action
{
  peerid          peer;
  direction_type  direction;

  inline peer_filter_request_action(
      peerid_wrapper const & p,
      direction_type _direction = DIR_INGRESS
    )
    : action{AT_FILTER_PEER}
    , peer{p.copy()}
    , direction{assert_unidirectional(_direction)}
  {
  }

  virtual ~peer_filter_request_action() = default;
};



/**
 * Action for reporting errors upstream
 */
struct error_action
  : public action
{
  error_t error;

  inline error_action(error_t err)
    : action{AT_ERROR}
    , error{err}
  {
  }

  virtual ~error_action() = default;
};



/**
 * Action for reporting channel established
 */
struct notify_channel_established_action
  : public action
{
  channelid       channel;
  capabilities_t  capabilities;

  inline notify_channel_established_action(channelid const & id,
      capabilities_t const & caps
    )
    : action{AT_NOTIFY_CHANNEL_ESTABLISHED}
    , channel{id}
    , capabilities{caps}
  {
  }

  virtual ~notify_channel_established_action() = default;
};



/**
 * Action for reporting channel closed
 */
struct notify_channel_closed_action
  : public action
{
  channelid channel;
  error_t   error;

  inline notify_channel_closed_action(channelid const & id,
      error_t err)
    : action{AT_NOTIFY_CHANNEL_CLOSED}
    , channel{id}
    , error{err}
  {
  }

  virtual ~notify_channel_closed_action() = default;
};


/**
 * Action for notifying a channel's receive window
 */
struct notify_channel_window_action
  : public action
{
  channelid   channel;
  std::size_t window_size;

  inline notify_channel_window_action(channelid const & id,
      std::size_t _window_size)
    : action{AT_NOTIFY_CHANNEL_WINDOW}
    , channel{id}
    , window_size{_window_size}
  {
  }

  virtual ~notify_channel_window_action() = default;
};




/**
 * Action for requesting the processing of a new event.
 */
struct process_event_action
  : public action
{
  std::unique_ptr<event> to_process;

  inline process_event_action(std::unique_ptr<event> && _to_process)
    : action{AT_PROCESS_EVENT}
    , to_process{std::move(_to_process)}
  {
  }

  virtual ~process_event_action() = default;
};

} // namespace channeler::pipe

#endif // guard
