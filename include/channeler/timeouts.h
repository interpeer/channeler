/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_TIMEOUTS_H
#define CHANNELER_TIMEOUTS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <chrono>

namespace channeler {

/**
 * Channeler internally defines timeouts in this unit type. Thanks
 * to chrono's casts, you can use a number of other resolutions.
 */
using timeout_duration = std::chrono::nanoseconds;

/**
 * This struct is the configuration structure for the API
 * defining timeouts in the rest of the code base. This struct is
 * typically passed by const reference.
 *
 * For default values, see options.txt
 */
struct CHANNELER_API timeout_config
{
  timeout_config();

  timeout_config(timeout_config const &) = default;
  timeout_config(timeout_config &&) = default;

  // Timeout for channel creation.
  timeout_duration  channel_new_timeout;

  // Maximum retries for channel creation.
  std::size_t       channel_new_max_retries;

  // Timeout for inactive channels.
  timeout_duration  channel_timeout;

  // Timeout for sending progress messages even when no packets went missing.
  timeout_duration  channel_progress_timeout;

  // Timeout for congestion update messages.
  timeout_duration  channel_congestion_update_timeout;
};

} // namespace channeler


#endif // guard
