/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <channeler/cookie.h>

#include <gtest/gtest.h>

namespace {

using namespace liberate::types::literals;

auto const secret1 = "s3kr1t"_b;
auto const secret2 = "t1rk3s"_b;

} // anonymous namespace


TEST(Cookie, initiator_cookie_calculation)
{
  using namespace channeler;

  peerid p1;
  peerid p2;

  channelid id = create_new_channelid();

  cookie c1 = create_cookie_initiator(secret1.data(), secret1.size(),
      p1, p2, id.initiator);

  ASSERT_NE(c1, cookie{});
  ASSERT_TRUE(validate_cookie(c1, secret1.data(), secret1.size(),
        p1, p2, id.initiator));
  ASSERT_FALSE(validate_cookie(c1 + 1, secret1.data(), secret1.size(),
        p1, p2, id.initiator));
}


TEST(Cookie, responder_cookie_calculation)
{
  using namespace channeler;

  peerid p1;
  peerid p2;

  channelid id = create_new_channelid();
  complete_channelid(id);

  cookie c2 = create_cookie_responder(secret2.data(), secret2.size(),
      p1, p2, id);

  ASSERT_NE(c2, cookie{});

  ASSERT_TRUE(validate_cookie(c2, secret2.data(), secret2.size(),
        p1, p2, id));
  ASSERT_FALSE(validate_cookie(c2 + 1, secret2.data(), secret2.size(),
        p1, p2, id));
}
