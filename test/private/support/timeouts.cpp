/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/support/timeouts.h"

#include <gtest/gtest.h>

namespace {

// The test "sleep" function doesn't sleep, but pretends that the clock
// progressed exactly as much as requested.
channeler::support::timeouts::duration
test_sleep(channeler::support::timeouts::duration amount)
{
  return amount;
}

} // anonymous namepsace



TEST(SupportTimeouts, simple_timeout)
{
  channeler::support::timeouts to{&test_sleep};
  using duration = channeler::support::timeouts::duration;

  // Add a timeout
  auto ret = to.add({123, 321}, duration{10});
  ASSERT_TRUE(ret);

  // Progress the platform time
  auto exp = to.wait(duration{3});
  ASSERT_EQ(0, exp.size());

  // Progress another time
  exp = to.wait(duration{3});
  ASSERT_EQ(0, exp.size());

  // Progress again - this time, the 10 duration should expire
  exp = to.wait(duration{5});
  ASSERT_EQ(1, exp.size());

  ASSERT_EQ(exp[0].tag.scope, 123);
  ASSERT_EQ(exp[0].tag.name, 321);
}



TEST(SupportTimeouts, duplicate_timeouts)
{
  channeler::support::timeouts to{&test_sleep};
  using duration = channeler::support::timeouts::duration;

  // Add a timeout
  auto ret = to.add({123, 321}, duration{10});
  ASSERT_TRUE(ret);

  // Cannot add the same timeout again, even with a different value.
  ret = to.add({123, 321}, duration{42});
  ASSERT_FALSE(ret);

  // However, a different scope or tag works.
  ret = to.add({124, 321}, duration{10});
  ASSERT_TRUE(ret);
  ret = to.add({123, 421}, duration{10});
  ASSERT_TRUE(ret);

  // Progress the platform time
  auto exp = to.wait(duration{10});
  ASSERT_EQ(3, exp.size());
}



TEST(SupportTimeouts, incremental_timeouts)
{
  channeler::support::timeouts to{&test_sleep};
  using duration = channeler::support::timeouts::duration;

  // Add timeouts
  auto ret = to.add({123, 321}, duration{10});
  ASSERT_TRUE(ret);
  ret = to.add({123, 421}, duration{11});
  ASSERT_TRUE(ret);

  auto exp = to.wait(duration{10});
  ASSERT_EQ(1, exp.size());

  // Just another unit more should provide the other timeout
  exp = to.wait(duration{1});
  ASSERT_EQ(1, exp.size());
}



TEST(SupportTimeouts, remove_timeouts)
{
  channeler::support::timeouts to{&test_sleep};
  using duration = channeler::support::timeouts::duration;

  // Add timeouts
  auto ret = to.add({123, 321}, duration{10});
  ASSERT_TRUE(ret);
  to.remove({123, 321});

  auto exp = to.wait(duration{10});
  ASSERT_EQ(0, exp.size());
}


TEST(SupportTimeouts, incremental_backoff_fail)
{
  channeler::support::timeouts to{&test_sleep};
  using duration = channeler::support::timeouts::duration;

  // We can't add a maximum number of collisions < 1
  auto ret = to.add({123, 321}, duration{10}, 0);
  ASSERT_FALSE(ret);
}


TEST(SupportTimeouts, incremental_backoff)
{
  channeler::support::timeouts to{&test_sleep};
  using duration = channeler::support::timeouts::duration;

  // If we add an timeout with max_collisions > 1, it should expire that
  // many times.
  auto ret = to.add({123, 321}, duration{10}, 3);
  ASSERT_TRUE(ret);

  auto exp = to.wait(duration{100});
  ASSERT_EQ(1, exp.size());
  {
    auto [active, _] = to.is_timeout_active({123, 321});
    ASSERT_TRUE(active);
  }

  exp = to.wait(duration{100});
  ASSERT_EQ(1, exp.size());
  {
    auto [active, _] = to.is_timeout_active({123, 321});
    ASSERT_TRUE(active);
  }

  exp = to.wait(duration{100});
  ASSERT_EQ(1, exp.size());
  {
    auto [active, _] = to.is_timeout_active({123, 321});
    ASSERT_FALSE(active); // XXX timeout no longer active after this
  }

  // It expired three times; the next time it must be removed.
  exp = to.wait(duration{100});
  ASSERT_EQ(0, exp.size());
}


TEST(SupportTimeouts, immediate_expire)
{
  channeler::support::timeouts to{&test_sleep};
  using duration = channeler::support::timeouts::duration;

  // This works best with a timeout with collisions.
  auto ret = to.add({123, 321}, duration{10}, 3);
  ASSERT_TRUE(ret);

  // Expire twice. Each time should succeed.
  ret = to.expire_now({123, 321});
  ASSERT_TRUE(ret);
  ret = to.expire_now({123, 321});
  ASSERT_TRUE(ret);

  // Waiting should expire once more, and then stop.
  auto exp = to.wait(duration{100});
  ASSERT_EQ(1, exp.size());
  {
    auto [active, _] = to.is_timeout_active({123, 321});
    ASSERT_FALSE(active); // XXX timeout no longer active after this
  }

  exp = to.wait(duration{100});
  ASSERT_EQ(0, exp.size());
}


TEST(SupportSimpleTimeout, expiry_and_advancement)
{
  constexpr std::size_t TIMEOUT = 15;

  channeler::support::simple_timeout<int, int> sto{TIMEOUT, 0};

  ASSERT_FALSE(sto.expired(0));
  ASSERT_FALSE(sto.expired(TIMEOUT - 1));
  ASSERT_TRUE(sto.expired(TIMEOUT));
  ASSERT_TRUE(sto.expired(TIMEOUT + 1));

  sto.advance(TIMEOUT);
  ASSERT_FALSE(sto.expired(TIMEOUT + 1));
  ASSERT_FALSE(sto.expired((TIMEOUT * 2) - 1));
  ASSERT_TRUE(sto.expired(TIMEOUT * 2));
  ASSERT_TRUE(sto.expired((TIMEOUT * 2) + 1));
}


TEST(SupportExponentialBackoffTimeout, expiry_and_advancement)
{
  constexpr std::size_t TIMEOUT = 15;
  constexpr std::size_t BACKOFF = 3;

  std::size_t invocations = 0;
  auto cb = [&invocations](std::size_t, std::size_t) -> void {
    ++invocations;
  };
  channeler::support::exponential_backoff_timeout<int, int> ebto{TIMEOUT, 0, BACKOFF, cb};

  int timeout = 0;
  while (!ebto.expired(timeout++)) {
    // Pass.
  }
  ASSERT_GE(timeout, TIMEOUT); // Absolute minimum, very much likely higher
  ASSERT_EQ(invocations, BACKOFF);

  ebto.advance(timeout);
  timeout = 0;
  while (!ebto.expired(timeout++)) {
    // Pass.
  }
  ASSERT_GE(timeout, TIMEOUT); // Absolute minimum, very much likely higher
  ASSERT_EQ(invocations, 2 * BACKOFF);
}
