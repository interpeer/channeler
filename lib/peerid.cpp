/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <channeler/peerid.h>
#include <channeler/error.h>

#include <cstring>

#include <stdexcept>

#include <liberate/string/hexencode.h>
#include <liberate/cpp/hash.h>
#include <liberate/random/unsafe_bits.h>

namespace channeler {


peerid_wrapper::peerid_wrapper(byte const * start, size_t bufsize)
  : raw{start}
{
  if (!raw || bufsize < size()) {
    throw exception{ERR_INSUFFICIENT_BUFFER_SIZE,
      "Input buffer too small for a peer identifier."};
  }
}



peerid_wrapper::peerid_wrapper(peerid_wrapper const & other)
  : raw{other.raw}
{
}



std::string
peerid_wrapper::display() const
{
  std::string res = "0x";
  return res + liberate::string::hexencode(raw, PEERID_SIZE_BYTES);
}



size_t
peerid_wrapper::hash() const
{
  return liberate::cpp::range_hash(raw, raw+ PEERID_SIZE_BYTES);
}



bool
peerid_wrapper::is_equal_to(peerid_wrapper const & other) const
{
  return (0 == ::memcmp(raw, other.raw, PEERID_SIZE_BYTES));
}



bool
peerid_wrapper::is_less_than(peerid_wrapper const & other) const
{
  return (0 > ::memcmp(raw, other.raw, PEERID_SIZE_BYTES));
}



peerid
peerid_wrapper::copy() const
{
  return {raw, size()};
}



peerid_wrapper &
peerid_wrapper::operator=(peerid_wrapper const & other)
{
  if (!raw || !other.raw) {
    throw std::logic_error("Should never happen; see regular ctor.");
  }
  if (raw != other.raw) {
    memcpy(const_cast<byte*>(raw), other.raw, size());
  }
  return *this;
}




peerid::peerid()
  : peerid_wrapper{buffer, PEERID_SIZE_BYTES}
{
  liberate::random::unsafe_bits<unsigned char> rng;
  byte * offset = buffer;
  while (offset < buffer + PEERID_SIZE_BYTES) {
    *offset = static_cast<byte>(rng.get());
    ++offset;
  }
}



peerid::peerid(byte const * buf, size_t bufsize)
  : peerid_wrapper{buffer, bufsize}
{
  ::memcpy(buffer, buf, PEERID_SIZE_BYTES);
}



peerid::peerid(char const * buf, size_t bufsize)
  : peerid_wrapper{buffer, sizeof(buffer)}
{
  char const * start = buf;
  size_t buflen = bufsize;
  if (bufsize > 2 && buf[0] == '0' && (buf[1] == 'x' || buf[1] == 'X')) {
    start += 2;
    buflen -= 2;
    if (bufsize < (PEERID_SIZE_BYTES * 2) + 2) {
      throw exception{ERR_INSUFFICIENT_BUFFER_SIZE,
        "Peer identifier buffer is too small."};
    }
  }
  else {
    if (bufsize < PEERID_SIZE_BYTES * 2) {
      throw exception{ERR_INSUFFICIENT_BUFFER_SIZE,
        "Peer identifier buffer is too small."};
    }
  }

  auto used = liberate::string::hexdecode(buffer, PEERID_SIZE_BYTES,
      reinterpret_cast<byte const *>(start), buflen);
  if (used != PEERID_SIZE_BYTES) {
    throw exception{ERR_DECODE,
      "Could not decode hexadecimal peer identifier."};
  }
}



peerid::peerid(peerid const & other)
  : peerid_wrapper{buffer, PEERID_SIZE_BYTES}
{
  ::memcpy(buffer, other.buffer, PEERID_SIZE_BYTES);
}



peerid &
peerid::operator=(peerid const & other)
{
  ::memcpy(buffer, other.buffer, PEERID_SIZE_BYTES);
  return *this;
}



} // namespace channeler
