/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/pipe/egress/out_buffer.h"
#include "../lib/channel_data.h"
#include "../lib/params.h"

#include <gtest/gtest.h>

#include "../../../packets.h"
#include "../../../clock.h"

namespace {

// For testing
static constexpr std::size_t PACKET_SIZE = 200;

using test_clock_params = test::clock_params;
using test_defaults = channeler::defaults<
  typename test_clock_params::duration,
  3 // POOL_BLOCK_SIZE
>;
using test_params = ::channeler::params<
  test_clock_params,
  test_defaults
>;

struct next
{
  using input_event = channeler::pipe::packet_out_enqueued_event<
    typename test_params::channel
  >;

  inline channeler::pipe::action_list_type consume(std::unique_ptr<channeler::pipe::event> event)
  {
    m_event = std::move(event);
    return {};
  }
  std::unique_ptr<channeler::pipe::event> m_event;
};


using filter_t = channeler::pipe::out_buffer_filter<
  typename test_params::transport_address,
  test_params::defaults::POOL_BLOCK_SIZE,
  typename test_params::channel,
  next,
  next::input_event
>;

} // anonymous namespace



TEST(PipeEgressOutBufferFilter, passthrough_invalid_event)
{
  using namespace channeler::pipe;

  typename test_params::channel_set chs;
  next n;
  filter_t filter{&n, chs};

  // Create a default event; this should not be handled.
  auto ev = std::make_unique<event>();
  auto res = filter.consume(std::move(ev));
  ASSERT_TRUE(res.empty());
  ASSERT_TRUE(n.m_event);
  ASSERT_EQ(ET_UNKNOWN, n.m_event->type);
}


// TODO enqueue_bad_channel
TEST(PipeEgressOutBufferFilter, enqueue)
{
  using namespace channeler::pipe;

  typename test_params::pool pool{PACKET_SIZE};
  typename test_params::channel_set chs;
  next n;
  filter_t filter{&n, chs};

  // Create packet
  auto slot = pool.allocate();
  memcpy(slot.data(), test::packet_default_channel,
      test::packet_default_channel_size);
  auto packet = channeler::packet_wrapper(slot.data(), slot.size(), true);

  // Make sure channel exists for this test case
  chs.add(packet.channel());

  // Pass through filter
  auto ev = std::make_unique<packet_out_event<test_params::defaults::POOL_BLOCK_SIZE>>(
      std::move(slot),
      std::move(packet)
  );
  auto ret = filter.consume(std::move(ev));

  ASSERT_EQ(0, ret.size());

  // No need to actually test packet header parsing - that's been tested
  // elsewhere. This tests that the filter passes on things well.
  ASSERT_TRUE(n.m_event);
  ASSERT_EQ(n.m_event->type, ET_PACKET_OUT_ENQUEUED);
  next::input_event * ptr = reinterpret_cast<next::input_event *>(n.m_event.get());
  ASSERT_TRUE(ptr->channel);

  // We could check that the packet is in the buffer
  ASSERT_FALSE(ptr->channel->egress_buffer().empty());
}
