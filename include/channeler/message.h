/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_MESSAGE_H
#define CHANNELER_MESSAGE_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <functional>
#include <map>

#include <liberate/types.h>

namespace channeler {

/**
 * Forward declaration
 */
struct CHANNELER_API message_registry;

/**
 * Messages have a type.
 *
 * Note that the message_type is 16 bits, but the type is encoded as a
 * variable length integer.
 *
 * We define two constants:
 * - MSG_UNKNOWN is a "message type" for unknown messages. It is sometimes
 *   returned instead of raising errors.
 * - CUSTOM_MESSAGES_START is the start of the message types that users can
 *   define themselves. This library reserves message types less than this
 *   value.
 */
using message_type = uint16_t;
static constexpr message_type MSG_UNKNOWN = 0;
static constexpr message_type CUSTOM_MESSAGES_START = 128;


/**
 * We define a message class as just taking a buffer and providing an interface
 * for basic member access.
 *
 * The class needs to provide an interface for two distinct types of situation:
 *
 * 1. In the simpler kind of instance, code creates explicit messages derived
 *    from this class with constructors or other means of creation that best
 *    fit the class. A virtual serialize() function then provides the means by
 *    which to serialize it into a byte buffer suitable for transmission.
 * 2. In the slightly more complex situation, we need to parse messages of
 *    unknown types from a byte buffer. To do this, we can parse the message
 *    header reasonably well in a generic fashion. But to further parse message
 *    specific data fields, we need to know how to do this. In order to keep
 *    things extensible, this is delegated to a message_registry class.
 *
 * When it comes to parsing, some basic members such as the buffer pointer and
 * input size can be set explicitly from code. The message type and potentially
 * the payload length are part of the message header, and may be parsed. This is
 * the use case for the main constructor that sets each data field.
 *
 * Converting from the basic message type to some derived type may be done via
 * the upgrade_message() function below. This involves a new allocation of the
 * specific message structure, which should be reasonably lightweight. Derived
 * classes must implement an appropriate move constructor that can just delegate
 * to the base classe's move constructor. The rest of message parsing is handled
 * by the registry (see below).
 *
 * For the simpler use case, create your derived message type as desired. The
 * only requirement that remains is to implement the serialize() function. Note
 * that the base class's serialize() function serializes an appropriate message
 * header including payload size (for which it consults the registry), so
 * calling this super function in your serialize() implementation is a good idea.
 *
 * Note that the message class is lightweight and could be efficiently copied.
 * Derived classes may not be constructed that way, however. Furthermore, for the
 * registry below to work well, we need to use run-time polymorphism.
 * Accidentally erasing the derived type's additional data by making copies of
 * it should not happen. So in most instances, we pass unique_ptr of messages
 * around.
 */
struct CHANNELER_API message
{
  std::shared_ptr<message_registry> registry;

  // Message metadata
  message_type    type = MSG_UNKNOWN;

  // Buffer
  byte const *    buffer = nullptr;
  std::size_t     input_size = 0;
  std::size_t     buffer_size = 0;

  // Payload
  byte const *    payload = nullptr;
  std::size_t     payload_size = 0;

  /**
   * Constructor just sets the basic data members.
   */
  inline message(std::shared_ptr<message_registry> _registry,
      message_type _type,
      byte const * _buffer, std::size_t _input_size, std::size_t _buffer_size,
      byte const * _payload, std::size_t _payload_size)
    : registry{_registry}
    , type{_type}
    , buffer{_buffer}
    , input_size{_input_size}
    , buffer_size{_buffer_size}
    , payload{_payload}
    , payload_size{_payload_size}
  {
  }

  // Mostly trivial, except for the virtual functions (which we need)
  virtual ~message() = default;
  message(message &&) = default;
  message(message const &) = default;
  message & operator=(message const &) = default;
  message & operator=(message &&) = default;


  /**
   * Serialized size of the message. This is type dependent, and will consult
   * registry. May return 0 if the message type is not known to the registry.
   * In order to provide an accurate value for variable sized messages, the
   * payload_size member must be set correctly.
   */
  std::size_t serialized_size() const;

  /**
   * As described above, serialize the message. This base class function
   * serializes the header.
   *
   * If the registry indicates that the message is of static length, only the
   * message type is encoded. If it indicates the message has dynamic length,
   * the current value of payload_size is encoded.
   *
   * The result is the number of serialized/consumed bytes, or zero if some
   * error occurred.
   */
  virtual std::size_t serialize(byte * output, std::size_t output_max) const;

  template <
    typename byteT,
    typename = typename std::enable_if<
      std::is_integral<byteT>::value
    >::type,
    typename = typename std::enable_if<
      sizeof(byte) == sizeof(byteT)
    >::type
  >
  inline std::size_t
  serialize(byteT * output, std::size_t output_max) const
  {
    return serialize(reinterpret_cast<byte *>(output), output_max);
  }


  template <
    typename byteT,
    typename = typename std::enable_if<
      std::is_integral<byteT>::value
    >::type,
    typename = typename std::enable_if<
      sizeof(byte) == sizeof(byteT)
    >::type
  >
  inline std::size_t
  serialize(std::vector<byteT> & container) const
  {
    return serialize(
        reinterpret_cast<byte *>(container.data()),
        container.size()
    );
  }



  /**
   * Simple accessors
   */
  inline bool has_buffer() const
  {
    return buffer && buffer_size;
  }

  inline bool has_payload() const
  {
    return payload && payload_size;
  }

protected:
  /**
   * Derived classes will want to set the message type in their respective
   * constructors, but perhaps set other members by more complex means.
   */
  inline message(std::shared_ptr<message_registry> _registry, message_type _type)
    : registry{_registry}
    , type{_type}
  {
  }
};



/**
 * "Upgrade" a message. This creates a new message object of the given
 * type from the basic message object, each as unique_ptr. The move
 * constructor is called for the conversion; see class message above.
 *
 * Note that this only creates the derived *type*; it doesn't yet extract
 * any useful metadata from it. It also does not check that the message
 * type and derived class are in any way compatible. Treat this as a
 * function for easing the development of feature extraction functions
 * as provided to the registry (below).
 */
template <
  typename derivedT,
  typename = typename std::enable_if<
    std::is_base_of<message, derivedT>::value
  >::type
>
std::unique_ptr<derivedT>
upgrade_message(std::unique_ptr<message> input)
{
  message * raw = input.release();
  auto ret = std::unique_ptr<derivedT>(new derivedT{std::move(*raw)});
  delete raw;
  return ret;
}



/**
 * The message_registry is the mechanism by which one can extend the known
 * message types in the protocol stack.
 *
 * For each message type, it's numeric type value, a payload_size() function
 * and a feature extractor function is registered. Duplicate registrations
 * are not permitted.
 */
struct CHANNELER_API message_registry
  : public std::enable_shared_from_this<message_registry>
{
  /**
   * For a message type, return the payload size or a negative value if the
   * payload is variably sized. In such an instance, the payload_size member of
   * the message will be consulted for serialization.
   */
  using payload_size_func = std::function<ssize_t ()>;

  /**
   * For a mesage type, given a base message pointer, return an instance
   * message pointer of the derived type. For polymorphism reasons, this is
   * still returned as a base pointer. Furthermore, the return value is a tuple
   * of an error code and such a pointer. If the error code is anything other
   * than ERR_SUCCESS, the message pointer must not be consulted.
   */
  using extractor_func = std::function<
    std::tuple<error_t, std::unique_ptr<message>>
    (std::unique_ptr<message>)
  >;

  /**
   * Message information in the registry comprises the type and the above
   * functions.
   */
  struct message_info
  {
    message_type type;
    payload_size_func payload_size;
    extractor_func    extractor;
  };


  /**
   * Create registry instance. Basic message types are already registered
   * automatically.
   */
  static std::shared_ptr<message_registry> create();

  /**
   * Register a message type.
   */
  error_t register_message(message_type type, message_info const & info);

  /**
   * Retrieve message type information
   */
  bool is_registered(message_type type) const;
  message_info const & get_info(message_type type) const;

  /**
   * Parse a message from a buffer.
   *
   * This function operates in a number of modes.
   *
   * In its lightweight mode, it does not try to parse more of the message than
   * necessary. It reads the message type and deduces the payload size based on
   * that. This uses the payload_size function above, and if that returns a
   * negative value, the payload_size member is parsed from the input buffer.
   *
   * In its heavier weight mode, all featurs of a message will be extracted.
   * After parsing the header, the resulting base message is handed of to the
   * message type's extractor function (above). The result is then returned
   * here.
   */
  std::tuple<error_t, std::unique_ptr<message>>
  parse(byte const * input, std::size_t input_size, bool lightweight);

  template <
    typename byteT,
    typename = typename std::enable_if<
      std::is_integral<byteT>::value
    >::type,
    typename = typename std::enable_if<
      sizeof(byte) == sizeof(byteT)
    >::type
  >
  inline std::tuple<error_t, std::unique_ptr<message>>
  parse(byteT const * input, std::size_t input_size, bool lightweight)
  {
    return parse(reinterpret_cast<byte const *>(input), input_size, lightweight);
  }


  template <
    typename byteT,
    typename = typename std::enable_if<
      std::is_integral<byteT>::value
    >::type,
    typename = typename std::enable_if<
      sizeof(byte) == sizeof(byteT)
    >::type
  >
  inline std::tuple<error_t, std::unique_ptr<message>>
  parse(std::vector<byteT> const & container, bool lightweight)
  {
    return parse(
        reinterpret_cast<byte const *>(container.data()),
        container.size(),
        lightweight
    );
  }



private:
  inline message_registry() = default;

  std::map<message_type, message_info> m_registered = {};
};




/**
 * Provide an iterator interface for blocks of messages in an input
 * buffer. The struct provides lightweight parsing of messages, such
 * that iteration itself is reasonably fast. For full parsing, use e.g.
 * the upgrade_message() function above.
 * FIXME that doesn't work!
 */
struct messages
{
  using value_type = std::unique_ptr<message>;

  struct iterator : public std::iterator<
    std::input_iterator_tag,   // iterator_category
    value_type,                // value_type
    long,                      // difference_type
    value_type const *,        // pointer
    value_type const &         // reference
  >
  {
    inline value_type operator*() const
    {
      if (m_offset >= m_messages.m_size) {
        return {};
      }
      auto [err, ret] = m_messages.m_registry->parse(
        m_messages.m_buffer + m_offset,
        m_messages.m_size - m_offset,
        true // lightweight
      );
      if (ERR_SUCCESS != err) {
        return {};
      }
      return std::move(ret);
    }


    inline iterator & operator++()
    {
      auto [err, ret] = m_messages.m_registry->parse(
        m_messages.m_buffer + m_offset,
        m_messages.m_size - m_offset,
        true // lightweight
      );
      if (ERR_SUCCESS == err) {
        m_offset += ret->buffer_size;
      }
      return *this;
    }


    inline iterator operator++(int) // postfix
    {
      iterator ret{*this};
      ++ret;
      return ret;
    }


    inline std::size_t remaining() const
    {
      auto [err, ret] = m_messages.m_registry->parse(
        m_messages.m_buffer + m_offset,
        m_messages.m_size - m_offset,
        true // lightweight
      );

      std::size_t remain = m_messages.m_size - m_offset;
      if (ERR_SUCCESS == err) {
        remain -= ret->buffer_size;
      }

      return remain;
    }


    inline bool operator==(iterator const & other) const
    {
      return (**this) == (*other);
    }


    inline bool operator!=(iterator const & other) const
    {
      return (**this) != (*other);
    }

  private:
    friend struct messages;

    inline iterator(messages const & msg, std::size_t offset)
      : m_messages{msg}
      , m_offset{offset}
    {
    }

    messages const &  m_messages;
    std::size_t       m_offset;
  };

  using const_iterator = iterator const;

  inline messages(std::shared_ptr<message_registry> registry,
      byte const * buffer, std::size_t size)
    : m_registry{registry}
    , m_buffer{buffer}
    , m_size{size}
  {
  }

  inline iterator begin()
  {
    return {*this, 0};
  }

  inline const_iterator begin() const
  {
    return {*this, 0};
  }

  inline iterator end()
  {
    return {*this, m_size};
  }

  inline const_iterator end() const
  {
    return {*this, m_size};
  }

  inline bool operator==(messages const & other) const
  {
    return m_buffer == other.m_buffer &&
      m_size == other.m_size;
  }

  inline bool operator!=(messages const & other) const
  {
    return m_buffer != other.m_buffer ||
      m_size != other.m_size;
  }

private:
  std::shared_ptr<message_registry> m_registry;
  byte const *                      m_buffer;
  std::size_t                       m_size;
};



/**
 * Like dynamic_cast, but for std::unique_ptr<message> only. You must
 * know that the pointer is actually a derivedT.
 *
 * Uses the registry information to extract features from the message
 * if the extract parameter is true (the default).
 *
 * For returning an extracted message as a base pointer, use extract_message
 * directly.
 */
inline std::unique_ptr<message>
extract_message(std::unique_ptr<message> & input)
{
  // We need to get the extractor from the message's own registry
  if (!input->registry->is_registered(input->type)) {
    throw exception{ERR_INVALID_MESSAGE_TYPE, "Failed to convert mesage because messsage type is unknown."};
  }

  auto [err, extracted] = input->registry->get_info(input->type).extractor(std::move(input));
  if (ERR_SUCCESS != err) {
    throw exception{err, "Could not extract message features."};
  }

  return std::move(extracted);
}


template <
  typename derivedT,
  typename = typename std::enable_if<
    std::is_base_of<message, derivedT>::value
  >::type
>
std::unique_ptr<derivedT>
dynamic_message_cast(std::unique_ptr<message> & input)
{
  auto extracted = extract_message(input);

  message * raw = extracted.release();
  auto result = std::unique_ptr<derivedT>(
      dynamic_cast<derivedT *>(raw)
  );
  return result;
}


} // namespace channeler

#endif // guard
