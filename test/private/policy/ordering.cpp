/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/policy/ordering.h"
#include "../lib/policy/resend.h"
#include "../lib/policy/loss.h"

#include <gtest/gtest.h>

namespace {

constexpr static std::size_t PACKET_SIZE =
  30  // Payload
  + channeler::public_header_layout::PUB_SIZE
  + channeler::private_header_layout::PRIV_SIZE
  + channeler::footer_layout::FOOT_SIZE;

constexpr static std::size_t TEST_WINDOW_SIZE = 50;

template <typename poolT>
inline std::tuple<typename poolT::slot, channeler::packet_wrapper>
create_packet(poolT & pool, channeler::sequence_no_t sequence_no)
{
  auto slot = pool.allocate();
  auto pkt = channeler::packet_wrapper{slot.data(), slot.size(), false};
  pkt.sequence_no() = sequence_no;
  return {slot, pkt};
}


template <typename timeT, typename poolT, typename policyT>
inline void
fill_standard_packets(timeT const & now,
    poolT & pool,
    policyT & policy,
    typename policyT::sequence_no_t lower_bound
    )
{
  {
    auto [slot, pkt] = create_packet(pool, 10);
    auto err = policy.ordered_insert(now, lower_bound, pkt, slot);
    EXPECT_EQ(channeler::ERR_SUCCESS, err);
    EXPECT_EQ(1, policy.size());
  }

  {
    auto [slot, pkt] = create_packet(pool, 9);
    auto err = policy.ordered_insert(now, lower_bound, pkt, slot);
    EXPECT_EQ(channeler::ERR_SUCCESS, err);
    EXPECT_EQ(2, policy.size());
  }

  {
    auto [slot, pkt] = create_packet(pool, 12);
    auto err = policy.ordered_insert(now, lower_bound, pkt, slot);
    EXPECT_EQ(channeler::ERR_SUCCESS, err);
    EXPECT_EQ(3, policy.size());
  }
}


template <typename timeT, typename poolT, typename policyT>
inline channeler::error_t
fill_missing_packet(timeT const & now,
    poolT & pool,
    policyT & policy,
    typename policyT::sequence_no_t lower_bound
    )
{
  auto [slot, pkt] = create_packet(pool, 11);
  auto err = policy.ordered_insert(now, lower_bound, pkt, slot);
  return err;
}


} // anonymous namespace


TEST(PolicyOrderingOrderOfArrival, retrieve_in_expected_order)
{
  using params_t = channeler::policy::policy_params<>;
  using policy_t = channeler::policy::order_of_arrival_policy<
    params_t
  >;

  params_t::unsigned_window window{TEST_WINDOW_SIZE};

  params_t::pool pool{PACKET_SIZE};
  constexpr params_t::sequence_no_t LOWER_BOUND = 1;

  policy_t pol{window};
  fill_standard_packets(params_t::clock::now(), pool, pol, LOWER_BOUND);

  // The order of arrival policy is a FIFO, it just returns packets as they
  // were received.
  auto opt = pol.retrieve(LOWER_BOUND);
  ASSERT_TRUE(opt.has_value());
  ASSERT_EQ(10, opt.value().packet.sequence_no());

  opt = pol.retrieve(LOWER_BOUND);
  ASSERT_TRUE(opt.has_value());
  ASSERT_EQ(9, opt.value().packet.sequence_no());

  opt = pol.retrieve(LOWER_BOUND);
  ASSERT_TRUE(opt.has_value());
  ASSERT_EQ(12, opt.value().packet.sequence_no());

  opt = pol.retrieve(LOWER_BOUND);
  ASSERT_FALSE(opt.has_value());

  // We can always insert the missing packet, the queue does not care.
  ASSERT_EQ(channeler::ERR_SUCCESS, fill_missing_packet(params_t::clock::now(), pool, pol, LOWER_BOUND));
}


TEST(PolicyOrderingSequentialWithGaps, retrieve_in_expected_order)
{
  using params_t = channeler::policy::policy_params<>;
  using policy_t = channeler::policy::sequential_with_gaps_policy<
    params_t
  >;

  params_t::unsigned_window window{TEST_WINDOW_SIZE};

  params_t::pool pool{PACKET_SIZE};
  constexpr params_t::sequence_no_t LOWER_BOUND = 1;

  policy_t pol{window};
  fill_standard_packets(params_t::clock::now(), pool, pol, LOWER_BOUND);

  // Retrieve in seuqence order
  auto opt = pol.retrieve(LOWER_BOUND);
  ASSERT_TRUE(opt.has_value());
  ASSERT_EQ(9, opt.value().packet.sequence_no());

  opt = pol.retrieve(LOWER_BOUND);
  ASSERT_TRUE(opt.has_value());
  ASSERT_EQ(10, opt.value().packet.sequence_no());

  opt = pol.retrieve(LOWER_BOUND);
  ASSERT_TRUE(opt.has_value());
  ASSERT_EQ(12, opt.value().packet.sequence_no());

  opt = pol.retrieve(LOWER_BOUND);
  ASSERT_FALSE(opt.has_value());

  // The policy doesn't know that it would make sense for the lower_bound to advance.
  ASSERT_EQ(channeler::ERR_SUCCESS, fill_missing_packet(params_t::clock::now(), pool, pol, LOWER_BOUND));
}



TEST(PolicyOrderingSequentialWithoutGaps, retrieve_in_expected_order)
{
  using params_t = channeler::policy::policy_params<>;
  using policy_t = channeler::policy::sequential_without_gaps_policy<
    params_t
  >;

  params_t::unsigned_window window{TEST_WINDOW_SIZE};

  params_t::pool pool{PACKET_SIZE};
  constexpr params_t::sequence_no_t LOWER_BOUND = 1;

  policy_t pol{window};
  fill_standard_packets(params_t::clock::now(), pool, pol, LOWER_BOUND);

  // Retrieve in sequence order. But we do not receive the last, because the
  // one prior to that is missing. We move the lower bound up to 9, because
  // we'll only retrieve empty results up until there.
  auto dynamic_lower_bound = 9;
  auto opt = pol.retrieve(dynamic_lower_bound);
  ASSERT_TRUE(opt.has_value());
  ASSERT_EQ(9, opt.value().packet.sequence_no());
  dynamic_lower_bound = window.increment(opt.value().packet.sequence_no());

  opt = pol.retrieve(dynamic_lower_bound);
  ASSERT_TRUE(opt.has_value());
  ASSERT_EQ(10, opt.value().packet.sequence_no());
  dynamic_lower_bound = window.increment(opt.value().packet.sequence_no());

  opt = pol.retrieve(dynamic_lower_bound);
  ASSERT_FALSE(opt.has_value());

  // We can insert the missing packet, because the read position is blocked here.
  ASSERT_EQ(channeler::ERR_SUCCESS, fill_missing_packet(params_t::clock::now(), pool, pol, LOWER_BOUND));
}
