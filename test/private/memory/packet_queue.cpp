/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/memory/packet_queue.h"

#include <gtest/gtest.h>

namespace {

using params = channeler::policy::policy_params<>;

constexpr std::size_t TEST_WINDOW_SIZE = 13;
using namespace std::literals::chrono_literals;
constexpr typename params::duration TEST_MAX_RTT = 50ms;
constexpr std::size_t TEST_MAX_COLLISIONS = 5;

constexpr static std::size_t PACKET_SIZE =
  30  // Payload
  + channeler::public_header_layout::PUB_SIZE
  + channeler::private_header_layout::PRIV_SIZE
  + channeler::footer_layout::FOOT_SIZE;


template <typename poolT>
inline std::tuple<typename poolT::slot, channeler::packet_wrapper>
create_packet(poolT & pool, channeler::sequence_no_t sequence_no)
{
  auto slot = pool.allocate();
  auto pkt = channeler::packet_wrapper{slot.data(), slot.size(), false};
  pkt.sequence_no() = sequence_no;
  return {slot, pkt};
}

} // anonymous namespace

template <typename T>
class GenericPacketQueue : public ::testing::Test {};
TYPED_TEST_SUITE_P(GenericPacketQueue);


TYPED_TEST_P(GenericPacketQueue, empty_queue)
{
  TypeParam queue{TEST_WINDOW_SIZE, TEST_MAX_RTT, TEST_MAX_COLLISIONS};

  // Just retrieving an item should not yield results, but also not fail
  // to close the queue. For that, we'll need to reach a timeout.
  auto now = params::clock::now();
  auto [entry, close] = queue.pop_front(now);
  ASSERT_FALSE(entry.has_value());
  ASSERT_FALSE(close);
}



TYPED_TEST_P(GenericPacketQueue, simple_test)
{
  TypeParam queue{TEST_WINDOW_SIZE, TEST_MAX_RTT, TEST_MAX_COLLISIONS};

  // A single packet pushed and popped must work for all policy combinations
  // if it's done immediately.
  typename TypeParam::pool pool{PACKET_SIZE};
  auto [slot, pkt] = create_packet(pool, 42);
  auto now = params::clock::now();

  auto err = queue.push_back(now, pkt, slot);
  ASSERT_EQ(channeler::ERR_SUCCESS, err);

  auto [entry, close] = queue.pop_front(now);
  ASSERT_TRUE(entry.has_value());
  ASSERT_EQ(42, entry.value().packet.sequence_no());
  ASSERT_FALSE(close);
}



TYPED_TEST_P(GenericPacketQueue, sequence_test)
{
  // All queues are expected to handle a short sequence of strictly
  // consecutive sequence numbers well.
  typename TypeParam::pool pool{PACKET_SIZE};
  TypeParam queue{TEST_WINDOW_SIZE, TEST_MAX_RTT, TEST_MAX_COLLISIONS};

  {
    auto [slot, pkt] = create_packet(pool, 42);
    auto now = params::clock::now();
    auto err = queue.push_back(now, pkt, slot);
    ASSERT_EQ(channeler::ERR_SUCCESS, err);
  }

  {
    auto [slot, pkt] = create_packet(pool, 43);
    auto now = params::clock::now();
    auto err = queue.push_back(now, pkt, slot);
    ASSERT_EQ(channeler::ERR_SUCCESS, err);
  }

  {
    auto [slot, pkt] = create_packet(pool, 47);
    auto now = params::clock::now();
    auto err = queue.push_back(now, pkt, slot);
    ASSERT_EQ(channeler::ERR_SUCCESS, err);
  }

  {
    auto [slot, pkt] = create_packet(pool, 45);
    auto now = params::clock::now();
    auto err = queue.push_back(now, pkt, slot);
    ASSERT_EQ(channeler::ERR_SUCCESS, err);
  }

  // At minimum the first two in-sequence packets should be retrievable by
  // all kinds of queues.
  {
    auto now = params::clock::now();
    auto [res, close] = queue.pop_front(now);
    ASSERT_TRUE(res.has_value());
    ASSERT_FALSE(close);
  }

  {
    auto now = params::clock::now();
    auto [res, close] = queue.pop_front(now);
    ASSERT_TRUE(res.has_value());
    ASSERT_FALSE(close);
  }

  // The rest of the entries are still in there, but maybe not accessible
  // so easily.
  ASSERT_FALSE(queue.empty());
  ASSERT_EQ(2, queue.size());
}

REGISTER_TYPED_TEST_SUITE_P(GenericPacketQueue,
    empty_queue,
    simple_test,
    sequence_test
);


typedef ::testing::Types<
  // Queue types for common tests
  channeler::memory::no_resend_fifo_ignore_loss_queue<params>,
  channeler::memory::exponential_backoff_fifo_ignore_loss_queue<params>,
  channeler::memory::no_resend_gapped_ignore_loss_queue<params>,
  channeler::memory::exponential_backoff_gapped_ignore_loss_queue<params>,
  channeler::memory::no_resend_sequential_ignore_loss_queue<params>,
  channeler::memory::exponential_backoff_sequential_ignore_loss_queue<params>,
  channeler::memory::no_resend_fifo_close_on_loss_queue<params>,
  channeler::memory::exponential_backoff_fifo_close_on_loss_queue<params>,
  channeler::memory::no_resend_gapped_close_on_loss_queue<params>,
  channeler::memory::exponential_backoff_gapped_close_on_loss_queue<params>,
  channeler::memory::no_resend_sequential_close_on_loss_queue<params>,
  channeler::memory::exponential_backoff_sequential_close_on_loss_queue<params>
> generic_test_types;


INSTANTIATE_TYPED_TEST_SUITE_P(memory, GenericPacketQueue, generic_test_types);
