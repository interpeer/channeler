/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_SUPPORT_TIMEOUTS_H
#define CHANNELER_SUPPORT_TIMEOUTS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <chrono>
#include <cmath>
#include <iomanip>
#include <functional>
#include <map>
#include <unordered_set>
#include <vector>

#include <liberate/timeout/exponential_backoff.h>
#include <liberate/cpp/hash.h>
#include <liberate/cpp/operators.h>
#include <liberate/logging.h>

#include <channeler/timeouts.h>


namespace channeler::support {


/**
 * We want to be able to output time_point without modifying the code to
 * expect anything in particular. This operator<< overload helps.
 */
template <
  typename timeT,
  class Clock = typename timeT::clock,
  class Duration = typename timeT::clock::duration,
  std::enable_if_t<
    std::is_same<
      timeT,
      std::chrono::time_point<Clock, Duration>
    >::value,
    bool
  > = true
>
inline std::ostream &
operator<<(std::ostream & os, timeT const & t)
{
  uint64_t seconds = t.time_since_epoch().count() * timeT::period::num;
  time_t tt = seconds / timeT::period::den;
  os << std::put_time(std::localtime(&tt), "%Y-%m-%d %H:%M:%S.")
     << std::setw(static_cast<std::streamsize>(std::log10(timeT::period::den)))
     << std::setfill('0')
     << (seconds % timeT::period::den);
  return os;
}


/**
 * In terms of context, each timeout is registered with a scope and tag value.
 * Each of those is a simple integer value. Scopes should be e.g. something
 * like a CHANNEL_ESTABLISHMENT value, while the tag might be the channel
 * identifier in question, or some similar context.
 */
using timeout_scope_type = uint16_t;
using timeout_name_type = uint16_t;

union timeout_scoped_tag_type
{
  uint32_t _hash;
  struct {
    timeout_scope_type  scope;
    timeout_name_type   name;
  } tag;

  inline timeout_scoped_tag_type(
      timeout_scope_type _scope,
      timeout_name_type _name
    )
    : tag{_scope, _name}
  {
  }

  inline bool operator==(timeout_scoped_tag_type const & other) const
  {
    return _hash == other._hash;
  }

  inline bool operator<(timeout_scoped_tag_type const & other) const
  {
    return _hash < other._hash;
  }

  inline std::size_t hash() const
  {
    return _hash;
  }
};

} // namespace channeler::support

LIBERATE_MAKE_HASHABLE(channeler::support::timeout_scoped_tag_type)
LIBERATE_MAKE_COMPARABLE(channeler::support::timeout_scoped_tag_type)


namespace channeler::support {

/**
 * We want a support class for handling timeouts. This support class takes
 * care of assiging some kind of meaning to a timeout, while it expects some
 * platform-provided functionality (e.g. in tests, a test-platform) with
 * minimal expectations.
 *
 * In the case of timeouts, the best we can really expect from a platform is
 * a function that sleeps for a maximum given time period, and returns the
 * actual duration slept. We'll simplify the API a little by assuming that
 * this is given in duration - each platform implementation
 * can provide its own interpretation of that, then.
 *
 * The sleep function takes the duration to sleep for, and returns the actually
 * elapsed duration.
 */
class timeouts
{
public:
  using duration = ::channeler::timeout_duration;

  using sleep_function = std::function<
    duration (duration)
  >;

  using expired = std::vector<timeout_scoped_tag_type>;

  inline timeouts(sleep_function func)
    : m_sleep{func}
  {
  }

  /**
   * Add a *transient* timeout with the scoped tag, i.e. when it expires,
   * it also gets removed from the timeouts class.
   */
  inline bool add(timeout_scoped_tag_type scoped_tag, duration amount,
      std::size_t max_collisions = 1)
  {
    if (max_collisions < 1) {
      return false;
    }

    if (m_tags.find(scoped_tag) != m_tags.end()) {
      return false;
    }

    m_tags.insert(scoped_tag);
    m_timeouts.insert({amount, {scoped_tag, amount, max_collisions}});
    return true;
  }

  /**
   * Remove a transient timeout without expiring it.
   */
  inline void remove(timeout_scoped_tag_type scoped_tag)
  {
    auto iter = m_tags.find(scoped_tag);
    if (iter == m_tags.end()) {
      return;
    }

    m_tags.erase(iter);

    auto iter2 = m_timeouts.begin();
    for ( ; iter2 != m_timeouts.end() ; ++iter2) {
      if (iter2->second.scoped_tag == scoped_tag) {
        // We can only erase in the loop because we exit the loop!
        m_timeouts.erase(iter2);
        break;
      }
    }
  }

  /**
   * Return true (and the current expiry time) if a timeout is active,
   * false otherwise.
   */
  inline
  std::tuple<bool, duration>
  is_timeout_active(timeout_scoped_tag_type scoped_tag)
  {
    auto iter = m_tags.find(scoped_tag);
    if (iter == m_tags.end()) {
      return {false, {}};
    }

    auto iter2 = m_timeouts.begin();
    for ( ; iter2 != m_timeouts.end() ; ++iter2) {
      if (iter2->second.scoped_tag == scoped_tag) {
        return {true, iter2->first};
      }
    }

    return {false, {}};
  }

  /**
   * Expire a timeout immediately. When the timeout is on its last collision,
   * it's the same as calling remove(). Otherwise, the timeout entry is
   * treated as if it expired during wait().
   *
   * Returns true if the tiemout was found, otherwise false.
   */
  inline bool
  expire_now(timeout_scoped_tag_type scoped_tag)
  {
    bool result = false;
    map_t remaining;
    for (auto entry : m_timeouts) {
      if (entry.second.scoped_tag == scoped_tag) {
        // It's possible we need to re-add the timeout with exponential backoff.
        if (++entry.second.collisions < entry.second.max_collisions) {
          duration new_amount = entry.second.initial_duration
            * liberate::timeout::backoff_multiplier(entry.second.collisions);
          remaining.insert({new_amount, entry.second});
        }
        else {
          m_tags.erase(scoped_tag);
        }
        result = true;
      }
      else {
        remaining.insert(entry);
      }
    }

    m_timeouts = remaining;

    return result;
  }

  /**
   * Wait for the given duration, and return all expired timeouts.
   */
  inline expired wait(duration amount)
  {
    // This implementation is a bit brute force, but it allows the platform
    // expectations to be relatively low. If it becomes a performance issue,
    // we can always optimize it.
    // TODO:
    // - check if this needs optimization
    // - may need an entirely different approach to integrate well with a
    //   platform's I/O system
    //   https://codeberg.org/interpeer/channeler/issues/10

    auto elapsed = m_sleep(amount);
    map_t remaining;
    expired result;
    for (auto entry : m_timeouts) {
      // If the entry's duration is less or equal to elapsed, then
      // return it.
      if (entry.first <= elapsed) {
        result.push_back(entry.second.scoped_tag);

        // It's possible we need to re-add the timeout with exponential backoff.
        if (++entry.second.collisions < entry.second.max_collisions) {
          duration new_amount = entry.second.initial_duration
            * liberate::timeout::backoff_multiplier(entry.second.collisions);
          remaining.insert({new_amount, entry.second});
        }
        else {
          m_tags.erase(entry.second.scoped_tag);
        }
      }
      else {
        // Otherwise, the entry has to remain - but we adjust the expectation
        // of the duration downwards.
        remaining.insert({entry.first - elapsed, entry.second});
      }
    }

    m_timeouts = remaining;

    return result;
  }

private:
  sleep_function  m_sleep;

  using set_t = std::unordered_set<timeout_scoped_tag_type>;
  set_t           m_tags = {};

  struct timeout_params
  {
    timeout_scoped_tag_type scoped_tag;
    duration                initial_duration;
    std::size_t             max_collisions;
    std::size_t             collisions = 0;
  };

  using map_t = std::multimap<duration, timeout_params>;
  map_t           m_timeouts = {};
};


/**
 * The simple_timeout and exponential_backoff_timeout classes implement
 * timeout handling functionality via common, simple interface.
 *
 * At any given time_pointT after the start value given to the constructor,
 * the expired() function may return true if the timeout has expired, false
 * otherwise.
 *
 * By calling the advance() function, the timeout and start value are reset.
 *
 * The simple_timeout class simply expires the timeout when the distance
 * between the start and current time is larger than the duration passed to
 * the constructor.
 *
 * By contrast, the exponential_backoff_timeout waits for a number of
 * "collisions" (i.e. timeout expirations) to have occurred before giving up.
 * At each collision, a callback is invoked up to the maximum number of
 * backoffs passed to the constructor.
 */
template <typename time_pointT, typename durationT>
struct simple_timeout
{
  inline simple_timeout(durationT timeout, time_pointT start)
    : m_simple_timeout{timeout}
    , m_next{start + m_simple_timeout}
  {
  }

  inline bool expired(time_pointT now) const
  {
    LIBLOG_DEBUG("[simple] Expired? Now: " << now << "; next: " << m_next);
    return now >= m_next;
  }

  inline void advance(time_pointT now)
  {
    m_next = now + m_simple_timeout;
  }

  inline void set_initial_clock(time_pointT initial)
  {
    m_next = initial + m_simple_timeout;
  }

  durationT const   m_simple_timeout;
  time_pointT       m_next;
};


template <typename time_pointT, typename durationT>
struct exponential_backoff_timeout
{
  using callback = std::function<void (time_pointT, std::size_t)>;

  inline exponential_backoff_timeout(durationT timeout, time_pointT start,
      std::size_t max_collisions, callback _callback)
    : m_simple_timeout{timeout}
    , m_callback{_callback}
    , m_max_collisions{max_collisions}
    , m_next{start + m_simple_timeout}
  {
  }


  inline bool expired(time_pointT now)
  {
    LIBLOG_DEBUG("[exponential_backoff] Expired? Now: " << now << "; next: " << m_next);
    // If we're still waiting, the timeout is not expired.
    if (now < m_next) {
      // Not expired
      return false;
    }

    // Keep waiting. We break out of the loop when the current timeout has
    // *not* been reached yet, or when the maximum collision number is reached.
    while (now >= m_next && m_collisions < m_max_collisions) {
      LIBLOG_DEBUG("Timed out, but there are collisions left, re-checking.");
      ++m_collisions;
      m_next = m_next + liberate::timeout::backoff(m_simple_timeout, m_collisions, m_simple_timeout);
      m_callback(now, m_collisions);
    }

    LIBLOG_DEBUG("Expired based on collisions? " << (m_collisions >= m_max_collisions));
    return (m_collisions >= m_max_collisions);
  }

  inline void advance(time_pointT now)
  {
    m_next = now + m_simple_timeout;
    m_collisions = 0;
  }

  inline void set_initial_clock(time_pointT initial)
  {
    m_next = initial + m_simple_timeout;
  }

  std::size_t       m_collisions = 0;

  durationT const   m_simple_timeout;
  callback const    m_callback;
  std::size_t const m_max_collisions;

  time_pointT       m_next;
};


} // namespace channeler::support


#endif // guard
