/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <channeler/message.h>

#include <channeler/channelid.h>
#include <channeler/capabilities.h>
#include <channeler/cookie.h>

#include <liberate/serialization/integer.h>
#include <liberate/serialization/varint.h>

#include <channeler/message/channels.h>
#include <channeler/message/data.h>
#include <channeler/message/congestion.h>

#include <liberate/logging.h>

namespace channeler {


/**
 * message
 */
std::size_t
message::serialized_size() const
{
  if (!registry->is_registered(type)) {
    LIBLOG_ERROR("Message type not registered: " << type);
    return 0;
  }

  auto t = static_cast<::liberate::types::varint>(type);
  std::size_t result = ::liberate::serialization::serialized_size(t);

  auto & info = registry->get_info(type);
  auto pl_size = info.payload_size();
  if (pl_size >= 0) {
    result += pl_size;
  }
  else if (pl_size < 0) {
    t = static_cast<::liberate::types::varint>(payload_size);
    result += ::liberate::serialization::serialized_size(t);
    result += payload_size;
  }
  return result;
}



std::size_t
message::serialize(byte * output, std::size_t output_max) const
{
  if (!registry->is_registered(type)) {
    LIBLOG_ERROR("Message type not registered: " << type);
    return 0;
  }

  std::size_t total = 0;

  // We know the message type, and need to serialize it.
  auto tmp = static_cast<liberate::types::varint>(type);
  auto used = liberate::serialization::serialize_varint(
      output + total,
      output_max - total,
      tmp);
  if (used <= 0) {
    LIBLOG_ERROR("Could not serialize message type.");
    return 0;
  }
  total += used;

  // Also serialize size, if it is given.
  auto & info = registry->get_info(type);
  auto pl_size = info.payload_size();
  if (pl_size < 0) {
    tmp = static_cast<liberate::types::varint>(payload_size);
    used = liberate::serialization::serialize_varint(
        output + total,
        output_max - total,
        tmp);
    if (used <= 0) {
      LIBLOG_ERROR("Could not serialize payload size.");
      return 0;
    }
    total += used;
  }

  return total;
}




std::shared_ptr<message_registry>
message_registry::create()
{
  auto ret = std::shared_ptr<message_registry>(new message_registry{});

  auto err = register_channel_types(ret);
  if (ERR_SUCCESS != err) {
    throw exception{err, "Could not register channel message types!"};
  }

  err = register_data_types(ret);
  if (ERR_SUCCESS != err) {
    throw exception{err, "Could not register data message types!"};
  }

  err = register_congestion_types(ret);
  if (ERR_SUCCESS != err) {
    throw exception{err, "Could not register congestion message types!"};
  }

  return ret;
}



error_t
message_registry::register_message(message_type type, message_info const & info)
{
  auto iter = m_registered.find(type);
  if (iter != m_registered.end()) {
    LIBLOG_ERROR("Message type is already registered: " << type);
    return ERR_INVALID_MESSAGE_TYPE;
  }

  if (!info.payload_size) {
    LIBLOG_ERROR("When registering message type " << type
        << ", the payload_size() function was not set.");
    return ERR_INVALID_VALUE;
  }
  if (!info.extractor) {
    LIBLOG_ERROR("When registering message type " << type
        << ", the extractor() function was not set.");
    return ERR_INVALID_VALUE;
  }

  m_registered[type] = info;
  return ERR_SUCCESS;
}



bool
message_registry::is_registered(message_type type) const
{
  auto iter = m_registered.find(type);
  return iter != m_registered.end();
}


message_registry::message_info const &
message_registry::get_info(message_type type) const
{
  auto iter = m_registered.find(type);
  if (iter == m_registered.end()) {
    throw exception{ERR_INVALID_MESSAGE_TYPE, "No such message type."};
  }
  return iter->second;
}



std::tuple<error_t, std::unique_ptr<message>>
message_registry::parse(byte const * input, std::size_t input_size, bool lightweight)
{
  byte const * current = input;
  std::size_t remaining = input_size;

  // *** First, try to extract the basic message header here.
  // Extract the type, and ensure it is known.
  liberate::types::varint tmp;
  auto used = liberate::serialization::deserialize_varint(tmp, current, remaining);
  if (!used) {
    LIBLOG_ERROR("Could not decode message type.");
    return {ERR_DECODE, nullptr};
  }
  current += used;
  remaining -= used;

  message_type type = static_cast<message_type>(tmp);

  // Ok, find the registry entry for this type
  auto iter = m_registered.find(type);
  if (iter == m_registered.end()) {
    LIBLOG_ERROR("Message type " << type << " is unknown.");
    return {ERR_INVALID_MESSAGE_TYPE, nullptr};
  }

  // Determine the payload size from this. This can either be static, or
  // dynamic.
  auto payload_size = iter->second.payload_size();
  if (payload_size < 0) {
    // The size is dynamic. Which means we need at least two more bytes in
    // the input buffer!
    used = liberate::serialization::deserialize_varint(tmp, current, remaining);
    if (!used) {
      LIBLOG_ERROR("Could not decode message length.");
      return {ERR_DECODE, nullptr};
    }
    current += used;
    remaining -= used;

    payload_size = static_cast<ssize_t>(tmp);
  }

  // Validate input buffer size
  std::size_t allocated = (current - input) + payload_size;
  if (allocated > input_size) {
    LIBLOG_ERROR("Expected message size exceeds input buffer; need "
        << allocated << " and input is " << input_size);
    return {ERR_DECODE, nullptr};
  }

  // The way we manage to get around the fact that specific messages may want
  // a specific kind of ctor is to require a copy constructor from the basic
  // message type. This basic message has a constructor for a type and size.
  auto msg = std::make_unique<message>(shared_from_this(),
    type,
    input, input_size, allocated,
    current, payload_size);

  // If we wanted only a lightweight message, this is where we're done.
  if (lightweight) {
    return {ERR_SUCCESS, std::move(msg)};
  }

  // *** When we know the message header, we know the type and can query the
  // registry for a matching extractor
  return iter->second.extractor(std::move(msg));
}


} // namespace channeler
