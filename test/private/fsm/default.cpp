/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/fsm/default.h"
#include "../lib/context/node.h"
#include "../lib/context/connection.h"

#include <gtest/gtest.h>

#include "../../clock.h"

namespace {

constexpr static std::size_t TEST_PACKET_SIZE = 120;

using test_clock_params = test::clock_params;
using test_defaults = channeler::defaults<
  typename test_clock_params::duration,
  3 // POOL_BLOCK_SIZE
>;
using address_t = uint16_t;

using test_params = ::channeler::params<
  test_clock_params,
  test_defaults,
  channeler::null_lock_policy,
  address_t
>;

using node_t = ::channeler::context::node<
  test_params
>;

using connection_t = ::channeler::context::connection<
  test_params,
  node_t
>;

using packet_t = std::pair<
  channeler::packet_wrapper,
  std::shared_ptr<std::vector<channeler::byte>>
>;

inline packet_t
make_packet(channeler::peerid const & sender,
    channeler::peerid const & recipient)
{
  constexpr std::size_t packet_size = channeler::packet_wrapper::envelope_size();

  auto buf = std::make_shared<std::vector<channeler::byte>>();
  buf->resize(packet_size, channeler::byte{0});

  channeler::packet_wrapper pkt{buf->data(), buf->size(), false};
  pkt.sender() = sender;
  pkt.recipient() = recipient;

  return std::make_pair(pkt, buf);
}

} // anonymous namespace


TEST(FSMStandardRegistry, create)
{
  using namespace channeler::fsm;

  channeler::peerid self;
  channeler::peerid peer;
  channeler::timeout_config to_config;

  node_t node{
    self,
    TEST_PACKET_SIZE,
    []() -> std::vector<channeler::byte> { return {}; },
    [](channeler::support::timeouts::duration d) { return d; },
  };

  connection_t ctx{
    node,
    peer,
    to_config
  };

  auto reg = get_standard_registry(ctx);
}


TEST(FSMStandardRegistry, negotiate_channel)
{
  // We should be able to negotiate a channel between two default registries
  using namespace channeler::fsm;
  using namespace channeler::pipe;

  using pool_t = channeler::memory::packet_pool<
    test_params::defaults::POOL_BLOCK_SIZE
  >;
  using message_event_t = message_event<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  pool_t pool{TEST_PACKET_SIZE};

  // Copying the context is fine here, we should just have two
  // unique contexts.
  channeler::peerid self;
  channeler::peerid peer;

  node_t node1{
    self,
    TEST_PACKET_SIZE,
    []() -> std::vector<channeler::byte> { return {}; },
    [](channeler::support::timeouts::duration d) { return d; },
  };
  node_t node2{
    peer,
    TEST_PACKET_SIZE,
    []() -> std::vector<channeler::byte> { return {}; },
    [](channeler::support::timeouts::duration d) { return d; },
  };

  channeler::timeout_config to_config;

  connection_t peer1_ctx{
    node1,
    peer,
    to_config
  };
  connection_t peer2_ctx{
    node2,
    self,
    to_config
  };

  // Create two registries
  auto peer1_reg = get_standard_registry(peer1_ctx);
  auto peer2_reg = get_standard_registry(peer2_ctx);

  action_list_type actions;
  event_list_type events;

  // Step 1: one peer initiates a new channel
  //         We need to have a message event as the output.
  auto ev1 = new_channel_event(
      peer1_ctx.node().id(), peer1_ctx.peer());
  auto [err, res] = peer1_reg.process(ev1, actions, events);
  ASSERT_EQ(channeler::ERR_SUCCESS, err);
  ASSERT_EQ(channeler::fsm::fsm_base::PS_SUCCESS, res);

  ASSERT_EQ(0, actions.size());
  ASSERT_EQ(1, events.size());

  auto & result_ev1 = *events.begin();
  ASSERT_TRUE(result_ev1);
  ASSERT_EQ(result_ev1->type, ET_MESSAGE_OUT);
  auto result_ev1_conv = reinterpret_cast<message_out_event *>(result_ev1.get());

  ASSERT_EQ(channeler::MSG_CHANNEL_NEW, result_ev1_conv->message->type);
  auto result_ev1_msg = reinterpret_cast<channeler::message_channel_new *>(result_ev1_conv->message.get());

  auto half_id = result_ev1_msg->initiator_part;
  ASSERT_TRUE(peer1_ctx.channels().has_pending_channel(half_id));
  ASSERT_FALSE(peer2_ctx.channels().has_pending_channel(half_id));

  // Step 2: the other peer returns a cookie.
  //         We have to feed the output message to the other peer's registry.
  auto pkt2 = make_packet(peer2_ctx.peer(), peer2_ctx.node().id());
  auto ev2 = message_event_t{pkt2.first, pool.allocate(), {},
    std::move(result_ev1_conv->message)
  };

  actions.clear();
  events.clear();

  auto [err2, res2] = peer2_reg.process(ev2, actions, events);
  ASSERT_EQ(channeler::ERR_SUCCESS, err2);
  ASSERT_EQ(channeler::fsm::fsm_base::PS_SUCCESS, res2);

  ASSERT_EQ(0, actions.size());
  ASSERT_EQ(1, events.size());

  auto & result_ev2 = *events.begin();
  ASSERT_TRUE(result_ev2);
  ASSERT_EQ(result_ev2->type, ET_MESSAGE_OUT);
  auto result_ev2_conv = reinterpret_cast<message_out_event *>(result_ev2.get());

  ASSERT_EQ(channeler::MSG_CHANNEL_ACKNOWLEDGE, result_ev2_conv->message->type);
  auto result_ev2_msg = reinterpret_cast<channeler::message_channel_acknowledge *>(result_ev2_conv->message.get());

  auto id = result_ev2_msg->id;
  ASSERT_EQ(half_id, id.initiator);
  ASSERT_TRUE(peer1_ctx.channels().has_pending_channel(id));
  ASSERT_FALSE(peer2_ctx.channels().has_pending_channel(id));

  // Step 3: the ack message should finalize the channel on peer1
  auto pkt3 = make_packet(peer2_ctx.node().id(), peer2_ctx.peer());
  auto ev3 = message_event_t{pkt3.first, pool.allocate(), {},
    std::move(result_ev2_conv->message)
  };

  actions.clear();
  events.clear();

  auto [err3, res3] = peer1_reg.process(ev3, actions, events);
  ASSERT_EQ(channeler::ERR_SUCCESS, err3);
  ASSERT_EQ(channeler::fsm::fsm_base::PS_SUCCESS, res3);

  ASSERT_EQ(1, actions.size()); // Expect one action; tested in individual FSMs
  ASSERT_EQ(1, events.size());

  auto & result_ev3 = *events.begin();
  ASSERT_TRUE(result_ev3);
  ASSERT_EQ(result_ev3->type, ET_MESSAGE_OUT);
  auto result_ev3_conv = reinterpret_cast<message_out_event *>(result_ev3.get());

  ASSERT_EQ(channeler::MSG_CHANNEL_FINALIZE, result_ev3_conv->message->type);
  auto result_ev3_msg = reinterpret_cast<channeler::message_channel_finalize *>(result_ev3_conv->message.get());
  ASSERT_TRUE(result_ev3_msg);

  ASSERT_TRUE(peer1_ctx.channels().has_established_channel(id));
  ASSERT_FALSE(peer2_ctx.channels().has_pending_channel(id));

  // Step 4: peer 2 needs to process the finalize message to also have an
  //         established channel
  auto pkt4 = make_packet(peer1_ctx.node().id(), peer1_ctx.peer());
  auto ev4 = message_event_t{pkt4.first, pool.allocate(), {},
    std::move(result_ev3_conv->message)
  };

  actions.clear();
  events.clear();

  auto [err4, res4] = peer2_reg.process(ev4, actions, events);
  ASSERT_EQ(channeler::ERR_SUCCESS, err4);
  ASSERT_EQ(channeler::fsm::fsm_base::PS_SUCCESS, res4);

  ASSERT_EQ(1, actions.size()); // Expect one action; tested in individual FSMs
  ASSERT_EQ(0, events.size());

  ASSERT_TRUE(peer1_ctx.channels().has_established_channel(id));
  ASSERT_TRUE(peer2_ctx.channels().has_established_channel(id));
}
