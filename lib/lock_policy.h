/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_LOCK_POLICY_H
#define CHANNELER_LOCK_POLICY_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

namespace channeler {

/**
 * Since we want the packet_pool (below) to serialize access only if necessary,
 * we provide a null_lock_policy that does nothing, and use that by default.
 */
struct null_lock_policy
{
  inline void lock() {}
  inline void unlock() {}
};


/**
 * Simple guard for the lock policy
 */
template <typename lockT>
struct guard
{
  inline guard(lockT * lock)
    : m_lock(lock)
  {
    if (m_lock) {
      m_lock->lock();
    }
  }

  inline ~guard()
  {
    if (m_lock) {
      m_lock->unlock();
    }
  }

  lockT * m_lock;
};

/**
 * Specalization of guard for when lockT is the null_lock_policy, because we
 * can optimize for space and branches here.
 */
template <>
struct guard<null_lock_policy>
{
  inline guard(null_lock_policy *)
  {
  }

  inline ~guard()
  {
  }
};


} // namespace channeler

#endif // guard
