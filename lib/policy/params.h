/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_POLICY_PARAMS_H
#define CHANNELER_POLICY_PARAMS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <build-config.h>

#include <chrono>
#include <set>
#include <functional>

#include <channeler/packet.h>

#include "../lock_policy.h"
#include "../support/unsigned_window.h"
#include "../memory/packet_pool.h"

namespace channeler::policy {

/**
 * All policy params are templates, but they interact with each other. Rather
 * than have conflicting definitions in the different policies, just force them
 * to the same definitions by making the definitions part of a shared struct.
 *
 * Tests can create their own struct, or accept an instanciation of this one
 * which defines the correct definitions.
 */
template <
  std::size_t POOL_BLOCK_SIZE = CHANNELER_DEFAULT_PACKET_POOL_BLOCK_SIZE_BYTES,
  typename clockT = std::chrono::steady_clock,
  typename sequenceT = sequence_no_t,
  typename timeT = typename clockT::time_point,
  typename durationT = typename clockT::duration,
  typename lock_policyT = null_lock_policy
>
struct policy_params
{
  // The time point for this structure. The duration and clock are dependent
  // types, if std::chrono is used. You can provide them separately in the
  // template parmeters, however.
  // The std::chrono::steady_clock is usually the best choice, so we'll default
  // to that.
  using time_point = timeT;
  using clock = clockT;
  using duration = durationT;

  // All dependent policies should use the same sequence number type.
  // FIXME remove _t
  using sequence_no_t = sequenceT;

  // We'll also likely use the same lock policy.
  using lock_policy = lock_policyT;

  // This isn't so much a parameter, but based on a parameter. We need a
  // definition of a decent sequence_no_t window, and the only thing that
  // makes sense is the dynamic_unsigned_window.
  using unsigned_window = channeler::support::dynamic_unsigned_window<
    sequence_no_t
  >;
  using sequence_less = channeler::support::window_less<
    sequence_no_t,
    unsigned_window
  >;

  // For managing actual packets rather than merely sequence numbers, we need
  // to know something about the storage of packets in a pool.
  using pool = channeler::memory::packet_pool<
    POOL_BLOCK_SIZE,
    lock_policy
  >;
  using slot = typename pool::slot;

  // We need some trivially copyable combined packet and slot structure for
  // queues and individual policies.
  struct queue_entry
  {
    packet_wrapper  packet;
    slot            data;
  };

  // Similarly, the queue and resend policy both define a missing set and
  // resend callback; we just put both here.
  using missing_set = std::set<sequence_no_t>;
  using resend_callback = std::function<
    // - Time point
    // - Numer of collisions
    // - Sequence number of the window start
    // - Set of missing sequence numbers in the window
    void (time_point, std::size_t, sequence_no_t, missing_set const &)
  >;
  using lower_bound_callback = std::function<sequence_no_t ()>;

};


} // namespace channeler::policy

#endif // guard
