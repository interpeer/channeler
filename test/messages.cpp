/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "messages.h"

#include <gtest/gtest.h>
#include <iostream>

namespace test {

using namespace liberate::types::literals;

channeler::byte const message_unknown[] = {
  0x7f_b, // Nothing, but below one byte

  0xbe_b, 0xef_b, 0xb4_b, 0xbe_b, // junk
};
std::size_t const message_unknown_size = sizeof(message_unknown);



channeler::byte const message_channel_new[] = {
  0x0a_b, // MSG_CHANNEL_NEW

  0xbe_b, 0xef_b, // Half channel ID

  0xbe_b, 0xef_b, 0xb4_b, 0xbe_b, // crc32 (cookie)
};
std::size_t const message_channel_new_size = sizeof(message_channel_new);



channeler::byte const message_channel_acknowledge[] = {
  0x0b_b, // MSG_CHANNEL_ACKNOWLEDGE

  0xbe_b, 0xef_b, 0xd0_b, 0x0d_b, // Channel ID

  0xbe_b, 0xef_b, 0xb4_b, 0xbe_b, // crc32 (cookie1)
  0xde_b, 0xad_b, 0xd0_b, 0x0d_b, // crc32 (cookie2)
};
std::size_t const message_channel_acknowledge_size = sizeof(message_channel_acknowledge);



channeler::byte const message_channel_finalize[] = {
  0x0c_b, // MSG_CHANNEL_FINALIZE

  0xbe_b, 0xef_b, 0xd0_b, 0x0d_b, // Channel ID

  0x39_b, 0x87_b, 0x88_b, 0x6e_b, // crc32 (cookie); used in FSM for channel responder

  0x00_b, 0x00_b, // Capabilities
};
std::size_t const message_channel_finalize_size = sizeof(message_channel_finalize);



channeler::byte const message_channel_cookie[] = {
  0x0d_b, // MSG_CHANNEL_COOKIE

  // Channel ID is in header

  0xbe_b, 0xef_b, 0xb4_b, 0xbe_b, // crc32 (cookie)

  0x00_b, 0x00_b, // Capabilities
};
std::size_t const message_channel_cookie_size = sizeof(message_channel_cookie);




channeler::byte const message_data[] = {
  0x14_b, // MSG_DATA

  0x06_b, // *Payload* size

  // Payload
  0xbe_b, 0xef_b, 0xb4_b, 0xbe_b, 0x00_b, 0x00_b,
};
std::size_t const message_data_size = sizeof(message_data);



channeler::byte const message_data_progress[] = {
  0x15_b, // MSG_DATA_PROGRESS

  0x08_b, // *Payload* size

  // Channel ID is in header

  0x01_b, 0xa2_b, // Receive window start

  // Payload - these must be unique sequence numbers.
  // XXX: The values don't make much sense; they're just
  //      unique. However, they *must* be in order!
  0x00_b, 0x12_b, 0xb4_b, 0xbe_b, 0xbe_b, 0xef_b,
};
std::size_t const message_data_progress_size = sizeof(message_data_progress);


channeler::byte const message_congestion_receive_window_small[] = {
  0x1e_b, // MSG_CONGESTION_RECEIVE_WINDOW

  0x01_b, // *Payload* size

  0x12_b, // Single-Byte window size.
};
std::size_t const message_congestion_receive_window_small_size = sizeof(message_congestion_receive_window_small);

channeler::byte const message_congestion_receive_window_large[] = {
  0x1e_b, // MSG_CONGESTION_RECEIVE_WINDOW

  0x03_b, // *Payload* size

  0x80_b, 0x93_b, 0x04_b, // Three-Byte window size
};
std::size_t const message_congestion_receive_window_large_size = sizeof(message_congestion_receive_window_large);






channeler::byte const message_block[] = {
  0x14_b, // MSG_DATA

  0x06_b, // *Payload* size

  // Payload
  0xbe_b, 0xef_b, 0xb4_b, 0xbe_b, 0x00_b, 0x00_b,

  // ---
  0x0a_b, // MSG_CHANNEL_NEW

  0xbe_b, 0xef_b, // Half channel ID

  0xbe_b, 0xef_b, 0xb4_b, 0xbe_b, // crc32 (cookie)

  // ---
  0x0d_b, // MSG_CHANNEL_COOKIE

  // Channel ID is in header

  0xbe_b, 0xef_b, 0xb4_b, 0xbe_b, // crc32 (cookie)

  0x00_b, 0x00_b, // Capabilities

  // ---
  0xbe_b, 0xef_b, 0xb4_b, 0xbe_b, // junk
};
std::size_t const message_block_size = sizeof(message_block);




} // namespace test
