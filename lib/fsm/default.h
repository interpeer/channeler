/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_FSM_DEFAULT_H
#define CHANNELER_FSM_DEFAULT_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include "registry.h"
#include "channel_initiator.h"
#include "channel_responder.h"
#include "data.h"
#include "congestion_simple.h"

namespace channeler::fsm {

/**
 * Return a pre-configured registry. We may have different versions of these, or
 * pass some kind of enum as input in future.
 *
 * We'll pass this a context; the context contains all type definitions, etc.
 * necessary for constructing FSMs.
 *
 * TODO
 */
template <
  typename connection_contextT
>
inline registry
get_standard_registry(connection_contextT & conn_ctx)
{
  using params = typename connection_contextT::params;

  registry reg;

  // Channel initiator
  using init_fsm_t = fsm_channel_initiator<
    params::defaults::POOL_BLOCK_SIZE,
    typename params::channel
  >;
  auto init = std::make_unique<init_fsm_t>(
      conn_ctx.node().message_registry(),
      conn_ctx.timeouts(),
      conn_ctx.timeout_config(),
      conn_ctx.channels(),
      conn_ctx.node().secret_generator()
  );
  reg.add_move(std::move(init));

  // Channel responder
  using resp_fsm_t = fsm_channel_responder<
    params::defaults::POOL_BLOCK_SIZE,
    typename params::channel
  >;
  auto resp = std::make_unique<resp_fsm_t>(
      conn_ctx.node().message_registry(),
      conn_ctx.timeouts(),
      conn_ctx.timeout_config(),
      conn_ctx.channels(),
      conn_ctx.node().secret_generator()
  );
  reg.add_move(std::move(resp));

  // Data
  using data_fsm_t = fsm_data<
    params::defaults::POOL_BLOCK_SIZE,
    typename params::channel
  >;
  auto data = std::make_unique<data_fsm_t>(
      conn_ctx.node().message_registry(),
      conn_ctx.timeouts(),
      conn_ctx.timeout_config(),
      conn_ctx.channels()
  );
  reg.add_move(std::move(data));

  // Congestion
  using congestion_fsm_t = fsm_congestion_simple<
    params::defaults::POOL_BLOCK_SIZE,
    typename params::channel
  >;
  auto cong = std::make_unique<congestion_fsm_t>(
      conn_ctx.node().message_registry(),
      conn_ctx.timeouts(),
      conn_ctx.timeout_config(),
      conn_ctx.channels()
  );
  reg.add_move(std::move(cong));

  return reg;
}

} // namespace channeler::fsm

#endif // guard
