/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/support/unsigned_window.h"

#include <gtest/gtest.h>

namespace {

// A test window size of 5 allows testing one value at each
// boundary of the window, one at a potential rollover point,
// and two possible "regular" values.
static constexpr size_t TEST_WINDOW_SIZE = 5;

template <typename unsignedT, typename windowT>
inline void
test_window_within(windowT & window, unsignedT const & start)
{
  // Before the window is not within
  ASSERT_FALSE(window.within(start, start - 1));

  // In the window is
  for (size_t i = 0 ; i < TEST_WINDOW_SIZE ; ++i) {
    unsignedT current = start + i;
    if (current == window.invalid_value()) {
      continue;
    }
    ASSERT_TRUE(window.within(start, current))
      << "For start " << start << " and current " << current;
  }

  // After the window is again not within
  ASSERT_FALSE(window.within(start, start + TEST_WINDOW_SIZE + 1));
}



template <typename unsignedT, typename windowT>
inline void
test_window_before(windowT & window, unsignedT const & start)
{
  // If we take a value before the test window, it should be "before".
  unsignedT before = window.decrement(start);

  ASSERT_TRUE(window.before(start, before))
    << "For start " << start << " and before " << before;
  ASSERT_FALSE(window.within(start, before))
    << "For start " << start << " and before " << before;
  ASSERT_FALSE(window.after(start, before))
    << "For start " << start << " and before " << before;
}



template <typename unsignedT, typename windowT>
inline void
test_window_after(windowT & window, unsignedT const & start)
{
  // If we take a value after the test window, it should be "after".
  unsignedT after = window.increment(start + TEST_WINDOW_SIZE);

  ASSERT_FALSE(window.before(start, after))
    << "For start " << start << " and after " << after;
  ASSERT_FALSE(window.within(start, after))
    << "For start " << start << " and after " << after;
  ASSERT_TRUE(window.after(start, after))
    << "For start " << start << " and after " << after;
}

} // anonymous namespace



template <typename T>
class UnsignedWindow : public ::testing::Test {};
TYPED_TEST_SUITE_P(UnsignedWindow);

TYPED_TEST_P(UnsignedWindow, within_window_start_at_beginning)
{
  using namespace channeler::support;
  using window_t = static_unsigned_window<TypeParam, TEST_WINDOW_SIZE>;
  window_t window;

  test_window_within<TypeParam, window_t>(window, 1);
}

TYPED_TEST_P(UnsignedWindow, within_window_start_at_middle)
{
  using namespace channeler::support;
  using window_t = static_unsigned_window<TypeParam, TEST_WINDOW_SIZE>;
  window_t window;

  test_window_within<TypeParam, window_t>(window, std::numeric_limits<TypeParam>::max() / 2);
}

TYPED_TEST_P(UnsignedWindow, within_window_start_near_end)
{
  using namespace channeler::support;
  using window_t = static_unsigned_window<TypeParam, TEST_WINDOW_SIZE>;
  window_t window;

  test_window_within<TypeParam, window_t>(window, std::numeric_limits<TypeParam>::max() - (TEST_WINDOW_SIZE / 2));
}

TYPED_TEST_P(UnsignedWindow, before_window_start_at_beginning)
{
  using namespace channeler::support;
  using window_t = static_unsigned_window<TypeParam, TEST_WINDOW_SIZE>;
  window_t window;

  test_window_before<TypeParam, window_t>(window, 1);
}

TYPED_TEST_P(UnsignedWindow, before_window_start_at_middle)
{
  using namespace channeler::support;
  using window_t = static_unsigned_window<TypeParam, TEST_WINDOW_SIZE>;
  window_t window;

  test_window_before<TypeParam, window_t>(window, std::numeric_limits<TypeParam>::max() / 2);
}

TYPED_TEST_P(UnsignedWindow, before_window_start_near_end)
{
  using namespace channeler::support;
  using window_t = static_unsigned_window<TypeParam, TEST_WINDOW_SIZE>;
  window_t window;

  test_window_before<TypeParam, window_t>(window, std::numeric_limits<TypeParam>::max() - (TEST_WINDOW_SIZE / 2));
}

TYPED_TEST_P(UnsignedWindow, after_window_start_at_beginning)
{
  using namespace channeler::support;
  using window_t = static_unsigned_window<TypeParam, TEST_WINDOW_SIZE>;
  window_t window;

  test_window_after<TypeParam, window_t>(window, 1);
}

TYPED_TEST_P(UnsignedWindow, after_window_start_at_middle)
{
  using namespace channeler::support;
  using window_t = static_unsigned_window<TypeParam, TEST_WINDOW_SIZE>;
  window_t window;

  test_window_after<TypeParam, window_t>(window, std::numeric_limits<TypeParam>::max() / 2);
}

TYPED_TEST_P(UnsignedWindow, after_window_start_near_end)
{
  using namespace channeler::support;
  using window_t = static_unsigned_window<TypeParam, TEST_WINDOW_SIZE>;
  window_t window;

  test_window_after<TypeParam, window_t>(window, std::numeric_limits<TypeParam>::max() - (TEST_WINDOW_SIZE / 2));
}


REGISTER_TYPED_TEST_SUITE_P(UnsignedWindow,
    within_window_start_at_beginning,
    within_window_start_at_middle,
    within_window_start_near_end,
    before_window_start_at_beginning,
    before_window_start_at_middle,
    before_window_start_near_end,
    after_window_start_at_beginning,
    after_window_start_at_middle,
    after_window_start_near_end
);

typedef ::testing::Types<
  unsigned char,
  unsigned short,
  unsigned int,
  unsigned long,
  unsigned long long
> test_types;

INSTANTIATE_TYPED_TEST_SUITE_P(support, UnsignedWindow, test_types);


template <typename T>
class DynamicUnsignedWindow : public ::testing::Test {};
TYPED_TEST_SUITE_P(DynamicUnsignedWindow);

TYPED_TEST_P(DynamicUnsignedWindow, within_window_start_at_beginning)
{
  using namespace channeler::support;
  using window_t = dynamic_unsigned_window<TypeParam>;
  window_t window{TEST_WINDOW_SIZE};

  test_window_within<TypeParam, window_t>(window, 1);
}

TYPED_TEST_P(DynamicUnsignedWindow, within_window_start_at_middle)
{
  using namespace channeler::support;
  using window_t = dynamic_unsigned_window<TypeParam>;
  window_t window{TEST_WINDOW_SIZE};

  test_window_within<TypeParam, window_t>(window, std::numeric_limits<TypeParam>::max() / 2);
}

TYPED_TEST_P(DynamicUnsignedWindow, within_window_start_near_end)
{
  using namespace channeler::support;
  using window_t = dynamic_unsigned_window<TypeParam>;
  window_t window{TEST_WINDOW_SIZE};

  test_window_within<TypeParam, window_t>(window, std::numeric_limits<TypeParam>::max() - (TEST_WINDOW_SIZE / 2));
}

TYPED_TEST_P(DynamicUnsignedWindow, before_window_start_at_beginning)
{
  using namespace channeler::support;
  using window_t = dynamic_unsigned_window<TypeParam>;
  window_t window{TEST_WINDOW_SIZE};

  test_window_before<TypeParam, window_t>(window, 1);
}

TYPED_TEST_P(DynamicUnsignedWindow, before_window_start_at_middle)
{
  using namespace channeler::support;
  using window_t = dynamic_unsigned_window<TypeParam>;
  window_t window{TEST_WINDOW_SIZE};

  test_window_before<TypeParam, window_t>(window, std::numeric_limits<TypeParam>::max() / 2);
}

TYPED_TEST_P(DynamicUnsignedWindow, before_window_start_near_end)
{
  using namespace channeler::support;
  using window_t = dynamic_unsigned_window<TypeParam>;
  window_t window{TEST_WINDOW_SIZE};

  test_window_before<TypeParam, window_t>(window, std::numeric_limits<TypeParam>::max() - (TEST_WINDOW_SIZE / 2));
}

TYPED_TEST_P(DynamicUnsignedWindow, after_window_start_at_beginning)
{
  using namespace channeler::support;
  using window_t = dynamic_unsigned_window<TypeParam>;
  window_t window{TEST_WINDOW_SIZE};

  test_window_after<TypeParam, window_t>(window, 1);
}

TYPED_TEST_P(DynamicUnsignedWindow, after_window_start_at_middle)
{
  using namespace channeler::support;
  using window_t = dynamic_unsigned_window<TypeParam>;
  window_t window{TEST_WINDOW_SIZE};

  test_window_after<TypeParam, window_t>(window, std::numeric_limits<TypeParam>::max() / 2);
}

TYPED_TEST_P(DynamicUnsignedWindow, after_window_start_near_end)
{
  using namespace channeler::support;
  using window_t = dynamic_unsigned_window<TypeParam>;
  window_t window{TEST_WINDOW_SIZE};

  test_window_after<TypeParam, window_t>(window, std::numeric_limits<TypeParam>::max() - (TEST_WINDOW_SIZE / 2));
}


REGISTER_TYPED_TEST_SUITE_P(DynamicUnsignedWindow,
    within_window_start_at_beginning,
    within_window_start_at_middle,
    within_window_start_near_end,
    before_window_start_at_beginning,
    before_window_start_at_middle,
    before_window_start_near_end,
    after_window_start_at_beginning,
    after_window_start_at_middle,
    after_window_start_near_end
);


INSTANTIATE_TYPED_TEST_SUITE_P(support, DynamicUnsignedWindow, test_types);


namespace {


template <typename lessT>
inline void
bounds_test_within(lessT const & less,
    typename lessT::value_type lower_bound,
    typename lessT::value_type max
  )
{
  // Lower than the max
  ASSERT_TRUE(less(lower_bound, max));
  ASSERT_TRUE(less(static_cast<typename lessT::value_type>(lower_bound + 1), max));
  ASSERT_TRUE(less(static_cast<typename lessT::value_type>(lower_bound + 2), max));

  // Equal to the max
  ASSERT_FALSE(less(static_cast<typename lessT::value_type>(lower_bound + TEST_WINDOW_SIZE), max));

  // Above the max
  ASSERT_FALSE(less(static_cast<typename lessT::value_type>(lower_bound + TEST_WINDOW_SIZE + 1), max));
}


template <typename lessT>
inline void
bounds_test_before(lessT const & less,
    typename lessT::value_type lower_bound
  )
{
  // Can we compare values strictly before the lower_bound?
  auto below_first = static_cast<typename lessT::value_type>(lower_bound - 2);
  auto below_second = static_cast<typename lessT::value_type>(lower_bound - 1);
  ASSERT_TRUE(less(below_first, below_second));
  ASSERT_FALSE(less(below_first, below_first));
  ASSERT_FALSE(less(below_second, below_first));
}


template <typename lessT>
inline void
bounds_test_after(lessT const & less,
    typename lessT::value_type lower_bound
  )
{
  // Testing in the region after should also work.
  auto above_first = static_cast<typename lessT::value_type>(lower_bound + TEST_WINDOW_SIZE + 1);
  auto above_second = static_cast<typename lessT::value_type>(lower_bound + TEST_WINDOW_SIZE + 2);
  ASSERT_TRUE(less(above_first, above_second));
  ASSERT_FALSE(less(above_first, above_first));
  ASSERT_FALSE(less(above_second, above_first));
}



template <typename lessT>
inline void
bounds_test(lessT const & less,
    typename lessT::value_type lower_bound,
    typename lessT::value_type max
  )
{
  bounds_test_within(less, lower_bound, max);
  bounds_test_before(less, lower_bound);
  bounds_test_after(less, lower_bound);
}

} // anonymous namespace


template <typename T>
class WindowLess : public ::testing::Test {};
TYPED_TEST_SUITE_P(WindowLess);

TYPED_TEST_P(WindowLess, dynamic_simple_window)
{
  using namespace channeler::support;
  using window_t = dynamic_unsigned_window<TypeParam>;
  window_t window{TEST_WINDOW_SIZE};

  using less_t = window_less<TypeParam, window_t>;

  // We use a simple lower bound.
  less_t less{window, [] { return TypeParam{42}; }};

  bounds_test(less, TypeParam{42}, TypeParam{42 + TEST_WINDOW_SIZE});
}



TYPED_TEST_P(WindowLess, dynamic_boundary_window)
{
  using namespace channeler::support;
  using window_t = dynamic_unsigned_window<TypeParam>;
  window_t window{TEST_WINDOW_SIZE};

  using less_t = window_less<TypeParam, window_t>;

  // We use a lower bound near the overflow edge.
  constexpr auto lower_bound = std::numeric_limits<TypeParam>::max() - (TEST_WINDOW_SIZE / 2);
  less_t less{window, [&] { return lower_bound; }};

  auto max = static_cast<TypeParam>(lower_bound + TEST_WINDOW_SIZE);

  bounds_test(less, lower_bound, max);

  // Ensure we tested enough (in case someone plays with TEST_WINDOW_SIZE)
  ASSERT_LT(static_cast<TypeParam>(lower_bound + TEST_WINDOW_SIZE - 1), lower_bound);
}



TYPED_TEST_P(WindowLess, static_simple_window)
{
  using namespace channeler::support;
  using window_t = static_unsigned_window<TypeParam, TEST_WINDOW_SIZE>;
  window_t window;

  using less_t = window_less<TypeParam, window_t>;

  // We use a simple lower bound.
  less_t less{window, [] { return TypeParam{42}; }};

  bounds_test(less, TypeParam{42}, TypeParam{42 + TEST_WINDOW_SIZE});
}



TYPED_TEST_P(WindowLess, static_boundary_window)
{
  using namespace channeler::support;
  using window_t = static_unsigned_window<TypeParam, TEST_WINDOW_SIZE>;
  window_t window;

  using less_t = window_less<TypeParam, window_t>;

  // We use a lower bound near the overflow edge.
  constexpr auto lower_bound = std::numeric_limits<TypeParam>::max() - (TEST_WINDOW_SIZE / 2);
  less_t less{window, [&] { return lower_bound; }};

  auto max = static_cast<TypeParam>(lower_bound + TEST_WINDOW_SIZE);

  bounds_test(less, lower_bound, max);

  // Ensure we tested enough (in case someone plays with TEST_WINDOW_SIZE)
  ASSERT_LT(static_cast<TypeParam>(lower_bound + TEST_WINDOW_SIZE - 1), lower_bound);
}



REGISTER_TYPED_TEST_SUITE_P(WindowLess,
    static_simple_window,
    static_boundary_window,
    dynamic_simple_window,
    dynamic_boundary_window
);


INSTANTIATE_TYPED_TEST_SUITE_P(support, WindowLess, test_types);
