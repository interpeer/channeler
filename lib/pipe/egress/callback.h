/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_PIPE_CALLBACK_H
#define CHANNELER_PIPE_CALLBACK_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <memory>
#include <functional>

#include "../event.h"
#include "../action.h"
#include "../event_as.h"

namespace channeler::pipe {

/**
 * The callback filter breaks the compile-time dependencies, and introduces a callback
 * for pipeline events.
 */
template <
  typename channelT,
  typename... cb_argsT
>
struct callback_filter
{
  using input_event = packet_out_enqueued_event<channelT>;
  using channel_set = ::channeler::channels<channelT>;
  using channel_ptr = typename channel_set::channel_ptr;

  using callback = std::function<
    action_list_type (std::unique_ptr<event>, cb_argsT && ...)
  >;

  inline callback_filter(callback cb)
    : m_callback{cb}
  {
  }


  template <typename... argsT>
  inline action_list_type
  consume(std::unique_ptr<event> unchecked, argsT && ...args)
  {
    event_assert_set("egress:callback", unchecked);
    return m_callback(std::move(unchecked), std::forward<argsT>(args)...);
  }

  callback  m_callback;
};


} // namespace channeler::pipe

#endif // guard
