/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/policy/resend.h"

#include <gtest/gtest.h>

namespace {

static constexpr channeler::timeout_duration TIMEOUT{20};
static constexpr std::size_t TEST_BLOCK_SIZE{42};


template <
  typename seqT
>
inline seqT
inc_seq(seqT const & base, int offset)
{
  seqT result = base;
  for (int i = 0 ; i < offset ; ++i) {
    result = channeler::support::detail::window_increment(result);
  }
  return result;
}



template <
  typename seqT
>
inline seqT
dec_seq(seqT const & base, int offset)
{
  seqT result = base;
  for (int i = 0 ; i < offset ; ++i) {
    result = channeler::support::detail::window_decrement(result);
  }
  return result;
}



template <typename timeT>
inline constexpr timeT
max_timeout(timeT base_timeout, std::size_t max_collisions)
{
  auto factor = static_cast<std::size_t>(std::nearbyint(exp2(max_collisions))) + 1;
  return base_timeout * factor;
}



template <
  typename policyT
>
inline void
test_window(policyT & policy, typename policyT::params::time_point now,
    typename policyT::params::sequence_no_t const & first,
    typename policyT::params::sequence_no_t const & window_size)
{
  using params = typename policyT::params;
  using duration = typename params::duration;

  auto win_start = first;
  bool has_expected_start = static_cast<bool>(win_start);

  // We're still waiting for the win_start entry.
  if (win_start) {
    ASSERT_TRUE(policy.keep_waiting_for(now, win_start, win_start));
  }

  // Add a couple of entries
  auto first_arrived = inc_seq(win_start, 1);
  auto ret = policy.packet_arrived(now + duration{1}, win_start, first_arrived);
  if (!has_expected_start) {
    // Special case: if there is no expectation, we can't really go
    // wrong with the win_start arrival.
    ASSERT_EQ(channeler::policy::RR_OK, ret);
    win_start = first_arrived;
  }
  else {
    ASSERT_EQ(channeler::policy::RR_OUT_OF_ORDER, ret);
  }

  auto second_arrived = inc_seq(win_start, 3);
  ret = policy.packet_arrived(now + duration{1}, win_start, second_arrived);
  ASSERT_EQ(channeler::policy::RR_OUT_OF_ORDER, ret);

  // A packet before the window should be ignored.
  auto current = dec_seq(win_start, 1);
  ret = policy.packet_arrived(now + duration{1}, win_start, current);
  ASSERT_EQ(channeler::policy::RR_OUTDATED, ret);

  // A packet past the window should be ignored.
  current = inc_seq(win_start, window_size + 2);
  ret = policy.packet_arrived(now + duration{1}, win_start, current);
  ASSERT_EQ(channeler::policy::RR_UNEXPECTED, ret);

  // If we then add packets in the window *again*, they must be duplicates.
  ret = policy.packet_arrived(now + duration{2}, win_start, second_arrived);
  ASSERT_EQ(channeler::policy::RR_DUPLICATE, ret);

  // etc.
}


template <typename policyT>
inline void
test_no_expected_wait_until_arrived(policyT & policy,
    typename policyT::params::time_point now)
{
  using params = typename policyT::params;
  using duration = typename params::duration;

  // Advance for the start of the test
  now += duration{1};

  // In the simple sequence scenario, we ensure that when a packet hasn't
  // arrived yet, we need to keep waiting for it until it has arrived.

  // The expectation is unset, so waiting for an packet must return true.
  ASSERT_TRUE(policy.keep_waiting_for(now, 0, 42));
  ASSERT_TRUE(policy.keep_waiting_for(now, 0, 5));

  // Signal that a packet has arrived. We pick any, not the above, because
  // that's how a "random start number" works.
  auto ret = policy.packet_arrived(now + duration{5}, 0, 13);
  ASSERT_EQ(channeler::policy::RR_OK, ret);

  // At this point, we still want to keep waiting for larger sequence numbers,
  // but lower ones must be rejected.
  ASSERT_TRUE(policy.keep_waiting_for(now + duration{15}, 14, 42));
  ASSERT_TRUE(policy.keep_waiting_for(now + duration{15}, 14, 14));

  ASSERT_FALSE(policy.keep_waiting_for(now + duration{15}, 14, 13));
  ASSERT_FALSE(policy.keep_waiting_for(now + duration{15}, 14, 5));
}



template <typename policyT>
inline void
test_no_expected_wait_until_timeout(policyT & policy,
    typename policyT::params::time_point now)
{
  using params = typename policyT::params;
  using duration = typename params::duration;

  // Advance for the start of the test
  now += duration{1};

  // In the simple sequence scenario, we ensure that when a packet hasn't
  // arrived yet, we need to keep waiting for it until it has arrived.

  // The expectation is unset, so waiting for an packet must return true.
  ASSERT_TRUE(policy.keep_waiting_for(now, 0, 42));
  ASSERT_TRUE(policy.keep_waiting_for(now, 0, 5));

  // Nothing arrived - after the timeout, we *still* need to keep waiting,
  // because there is no expectation of a previous packet here.
  ASSERT_TRUE(policy.keep_waiting_for(now + TIMEOUT, 0, 42));
  ASSERT_TRUE(policy.keep_waiting_for(now + TIMEOUT, 0, 5));

  ASSERT_TRUE(policy.keep_waiting_for(now + TIMEOUT + duration{1}, 0, 42));
  ASSERT_TRUE(policy.keep_waiting_for(now + TIMEOUT + duration{1}, 0, 5));
}



template <typename policyT>
inline void
test_expected_wait_until_arrived(policyT & policy,
    typename policyT::params::sequence_no_t const & first,
    typename policyT::params::time_point now)
{
  using params = typename policyT::params;
  using duration = typename params::duration;

  // Advance for the start of the test
  now += duration{1};

  // If we have an expectation for a first sequence number, then we must
  // be waiting for it - we also must be waiting for later ones. But we
  // can't be waiting for earlier ones.
  ASSERT_TRUE(policy.keep_waiting_for(now, first, 42));
  ASSERT_TRUE(policy.keep_waiting_for(now, first, 43));

  ASSERT_FALSE(policy.keep_waiting_for(now, first, 41));

  // When the packet has arrived, we won't be waiting for it any longer
  auto ret = policy.packet_arrived(now + duration{5}, first, 42);
  ASSERT_EQ(channeler::policy::RR_OK, ret);

  auto new_win = inc_seq(first, 1);
  ASSERT_FALSE(policy.keep_waiting_for(now + duration{10}, new_win, 42));
  ASSERT_TRUE(policy.keep_waiting_for(now + duration{10}, new_win, 43));
}



template <typename policyT>
inline void
test_expected_wait_until_timeout(policyT & policy,
    typename policyT::params::sequence_no_t const & first,
    typename policyT::params::time_point now,
    typename policyT::params::time_point past_timeout)
{
  using params = typename policyT::params;
  using duration = typename params::duration;

  // Advance for the start of the test
  now += duration{1};

  // If we have an expectation for a first sequence number, then we must
  // be waiting for it - we also must be waiting for later ones. But we
  // can't be waiting for earlier ones.
  ASSERT_TRUE(policy.keep_waiting_for(now, first, 42));
  ASSERT_TRUE(policy.keep_waiting_for(now, first, 43));

  ASSERT_FALSE(policy.keep_waiting_for(now, first, 41));

  // Past the timeout, we would expect packet 42 to no longer be relevant,
  // but 43 needs to still be waited for.
  ASSERT_FALSE(policy.keep_waiting_for(past_timeout, first, 42));
  ASSERT_TRUE(policy.keep_waiting_for(past_timeout, first, 43));
}



template <typename policyT>
inline void
test_gaps_in_received(policyT & policy,
    typename policyT::params::time_point now)
{
  using params = typename policyT::params;
  using duration = typename params::duration;

  // Advance for the start of the test
  now += duration{1};

  // If we feed in some future packets (let's say every second), then we can
  // check to make sure timeouts still apply, but the future packets are
  // acknowledged.
  typename policyT::params::sequence_no_t win_start = 42;
  auto ret = policy.packet_arrived(now + duration{5}, win_start, 43);
  ASSERT_EQ(channeler::policy::RR_OUT_OF_ORDER, ret);
  ret = policy.packet_arrived(now + duration{5}, win_start, 45);
  ASSERT_EQ(channeler::policy::RR_OUT_OF_ORDER, ret);
  ret = policy.packet_arrived(now + duration{5}, win_start, 47);
  ASSERT_EQ(channeler::policy::RR_OUT_OF_ORDER, ret);

  ASSERT_EQ(3, policy.num_future_packets_arrived());

  // We should still be waiting for 42, 44 and 46 but not the arrived packets.
  ASSERT_TRUE(policy.keep_waiting_for(now + duration{5}, win_start, 42));
  ASSERT_FALSE(policy.keep_waiting_for(now + duration{5}, win_start, 43));
  ASSERT_TRUE(policy.keep_waiting_for(now + duration{5}, win_start, 44));
  ASSERT_FALSE(policy.keep_waiting_for(now + duration{5}, win_start, 45));
  ASSERT_TRUE(policy.keep_waiting_for(now + duration{5}, win_start, 46));
  ASSERT_FALSE(policy.keep_waiting_for(now + duration{5}, win_start, 47));
  ASSERT_TRUE(policy.keep_waiting_for(now + duration{5}, win_start, 48));

  // Whether by timeout or received, if 42 is no longer the future packet,
  // then we shouldn't be waiting for 42 *or* 43 any longer, and move on to
  // waiting for 44.
  ret = policy.packet_arrived(now + duration{10}, win_start, 42);
  ASSERT_EQ(channeler::policy::RR_OK, ret);

  win_start = 44;
  policy.update_timeouts(now + duration{10}, win_start);

  ASSERT_FALSE(policy.keep_waiting_for(now + duration{15}, win_start, 42));
  ASSERT_FALSE(policy.keep_waiting_for(now + duration{15}, win_start, 43));
  ASSERT_TRUE(policy.keep_waiting_for(now + duration{15}, win_start, 44));
  ASSERT_FALSE(policy.keep_waiting_for(now + duration{15}, win_start, 45));
  ASSERT_TRUE(policy.keep_waiting_for(now + duration{15}, win_start, 46));
  ASSERT_FALSE(policy.keep_waiting_for(now + duration{15}, win_start, 47));
  ASSERT_TRUE(policy.keep_waiting_for(now + duration{15}, win_start, 48));

  // That means we just have two future set packets remaining.
  ASSERT_EQ(2, policy.num_future_packets_arrived());
}



typedef ::testing::Types<
  // Main type to use in production
  channeler::sequence_no_t,
  // Small type for boundary testing
  uint8_t
> test_types;



} // anonymous namespace


/****************************************************************************
 * No Resend
 */

template <typename T>
class NoResend : public ::testing::Test {};
TYPED_TEST_SUITE_P(NoResend);

TYPED_TEST_P(NoResend, no_expected_wait_until_arrived)
{
  constexpr TypeParam WINDOW_SIZE = 100;

  using params = channeler::policy::policy_params<
    TEST_BLOCK_SIZE,
    std::chrono::steady_clock,
    TypeParam
  >;
  using policy_t = channeler::policy::no_resend_policy<params>;

  auto now = params::clock::now();
  typename params::unsigned_window window{WINDOW_SIZE};
  policy_t policy{window, now, TIMEOUT};

  test_no_expected_wait_until_arrived<policy_t>(policy, now);
}


TYPED_TEST_P(NoResend, no_expected_wait_until_timeout)
{
  constexpr TypeParam WINDOW_SIZE = 5;

  using params = channeler::policy::policy_params<
    TEST_BLOCK_SIZE,
    std::chrono::steady_clock,
    TypeParam
  >;
  using policy_t = channeler::policy::no_resend_policy<params>;

  auto now = params::clock::now();
  typename params::unsigned_window window{WINDOW_SIZE};
  policy_t policy{window, now, TIMEOUT};

  test_no_expected_wait_until_timeout<policy_t>(policy, now);
}



TYPED_TEST_P(NoResend, expected_wait_until_arrived)
{
  constexpr TypeParam WINDOW_SIZE = 5;

  using params = channeler::policy::policy_params<
    TEST_BLOCK_SIZE,
    std::chrono::steady_clock,
    TypeParam
  >;
  using policy_t = channeler::policy::no_resend_policy<params>;

  auto now = params::clock::now();
  typename params::unsigned_window window{WINDOW_SIZE};
  policy_t policy{window, now, TIMEOUT};

  test_expected_wait_until_arrived<policy_t>(policy, 42, now);
}



TYPED_TEST_P(NoResend, expected_wait_until_timeout)
{
  constexpr TypeParam WINDOW_SIZE = 5;

  using params = channeler::policy::policy_params<
    TEST_BLOCK_SIZE,
    std::chrono::steady_clock,
    TypeParam
  >;
  using policy_t = channeler::policy::no_resend_policy<params>;

  auto now = params::clock::now();
  typename params::unsigned_window window{WINDOW_SIZE};
  policy_t policy{window, now, TIMEOUT};

  test_expected_wait_until_timeout<policy_t>(policy, 42, now,
      now + TIMEOUT + typename params::duration{1});
}



TYPED_TEST_P(NoResend, gaps_in_received)
{
  constexpr TypeParam WINDOW_SIZE = 100;

  using params = channeler::policy::policy_params<
    TEST_BLOCK_SIZE,
    std::chrono::steady_clock,
    TypeParam
  >;
  using policy_t = channeler::policy::no_resend_policy<params>;

  auto now = params::clock::now();
  typename params::unsigned_window window{WINDOW_SIZE};
  policy_t policy{window, now, TIMEOUT};

  test_gaps_in_received<policy_t>(policy, now);
}



TYPED_TEST_P(NoResend, expected_window_with_gaps)
{
  constexpr TypeParam WINDOW_SIZE = 5;
  constexpr TypeParam FIRST = 10;

  using params = channeler::policy::policy_params<
    TEST_BLOCK_SIZE,
    std::chrono::steady_clock,
    TypeParam
  >;
  using policy_t = channeler::policy::no_resend_policy<
    params
  >;

  auto now = params::clock::now();
  typename params::unsigned_window window{WINDOW_SIZE};
  policy_t policy{window, now, TIMEOUT};

  // If we add values from the window, but not the first value, that should
  // put those into the future set. If we then add all values from the window
  // in sequence, it should result in generally progressing the window.
  test_window<policy_t>(policy, now, FIRST, WINDOW_SIZE);
}



TYPED_TEST_P(NoResend, no_expected_window_with_gaps)
{
  constexpr TypeParam WINDOW_SIZE = 5;
  constexpr TypeParam FIRST = 0;

  using params = channeler::policy::policy_params<
    TEST_BLOCK_SIZE,
    std::chrono::steady_clock,
    TypeParam
  >;
  using policy_t = channeler::policy::no_resend_policy<
    params
  >;

  auto now = params::clock::now();
  typename params::unsigned_window window{WINDOW_SIZE};
  policy_t policy{window, now, TIMEOUT};

  // Same as above, but we don't expect a first packet
  test_window<policy_t>(policy, now, FIRST, WINDOW_SIZE);
}


TYPED_TEST_P(NoResend, roll_over_window_with_gaps)
{
  constexpr TypeParam WINDOW_SIZE = 5;

  using params = channeler::policy::policy_params<
    TEST_BLOCK_SIZE,
    std::chrono::steady_clock,
    TypeParam
  >;
  using policy_t = channeler::policy::no_resend_policy<
    params
  >;

  auto now = params::clock::now();
  typename params::unsigned_window window{WINDOW_SIZE};

  // Same as above, but we start the test a little before the end of the value
  // range, and roll over the start until after.
  for (TypeParam i = 0 ; i < WINDOW_SIZE ; ++i) {
    TypeParam first = std::numeric_limits<TypeParam>::max() - (WINDOW_SIZE / 2) + i;
    if (!first) {
      // Skipping, same as no_expected_window_with_gaps
      continue;
    }
    policy_t policy{window, now, TIMEOUT};
    test_window<policy_t>(policy, now, first, WINDOW_SIZE);
  }
}


REGISTER_TYPED_TEST_SUITE_P(NoResend,
    no_expected_wait_until_arrived,
    no_expected_wait_until_timeout,
    expected_wait_until_arrived,
    expected_wait_until_timeout,
    gaps_in_received,
    expected_window_with_gaps,
    no_expected_window_with_gaps,
    roll_over_window_with_gaps
);


INSTANTIATE_TYPED_TEST_SUITE_P(policy, NoResend, test_types);


/****************************************************************************
 * Exponential Backoff Resend
 */

template <typename T>
class ExponentialBackoffResend : public ::testing::Test {};
TYPED_TEST_SUITE_P(ExponentialBackoffResend);

TYPED_TEST_P(ExponentialBackoffResend, no_expected_wait_until_arrived)
{
  constexpr TypeParam WINDOW_SIZE = 100;
  constexpr std::size_t MAX_COLLISIONS = 3;

  std::size_t invocations = 0;
  auto cb = [&invocations, MAX_COLLISIONS](
      std::chrono::steady_clock::time_point const &,
      std::size_t collisions, TypeParam const &, std::set<TypeParam> const &) -> void
  {
    ASSERT_LE(collisions, MAX_COLLISIONS);
    ++invocations;
  };

  using params = channeler::policy::policy_params<
    TEST_BLOCK_SIZE,
    std::chrono::steady_clock,
    TypeParam
  >;
  using policy_t = channeler::policy::exponential_backoff_resend_policy<params>;

  auto now = params::clock::now();
  typename params::unsigned_window window{WINDOW_SIZE};
  policy_t policy{window, now, TIMEOUT, MAX_COLLISIONS, cb};

  test_no_expected_wait_until_arrived<policy_t>(policy, now);

  // Since the test is *not* waiting for any kind of timeout, we expect the
  // number of callback invocations to be zero.
  ASSERT_EQ(0, invocations);
}



TYPED_TEST_P(ExponentialBackoffResend, no_expected_wait_until_timeout)
{
  constexpr TypeParam WINDOW_SIZE = 5;
  constexpr std::size_t MAX_COLLISIONS = 3;

  std::size_t invocations = 0;
  auto cb = [&invocations, MAX_COLLISIONS](
      std::chrono::steady_clock::time_point const &,
      std::size_t collisions, TypeParam const &, std::set<TypeParam> const &) -> void
  {
    ASSERT_LE(collisions, MAX_COLLISIONS);
    ++invocations;
  };

  using params = channeler::policy::policy_params<
    TEST_BLOCK_SIZE,
    std::chrono::steady_clock,
    TypeParam
  >;
  using policy_t = channeler::policy::exponential_backoff_resend_policy<params>;

  auto now = params::clock::now();
  typename params::unsigned_window window{WINDOW_SIZE};
  policy_t policy{window, now, TIMEOUT, MAX_COLLISIONS, cb};

  test_no_expected_wait_until_timeout<policy_t>(policy, now);

  // Nothing arrived in this test, so the expectation is that the timeout
  // mechanism never managed to trigger.
  ASSERT_EQ(0, invocations);
}



TYPED_TEST_P(ExponentialBackoffResend, expected_wait_until_arrived)
{
  constexpr TypeParam WINDOW_SIZE = 5;
  constexpr std::size_t MAX_COLLISIONS = 3;

  std::size_t invocations = 0;
  auto cb = [&invocations, MAX_COLLISIONS](
      std::chrono::steady_clock::time_point const &,
      std::size_t collisions, TypeParam const &, std::set<TypeParam> const &) -> void
  {
    ASSERT_LE(collisions, MAX_COLLISIONS);
    ++invocations;
  };

  using params = channeler::policy::policy_params<
    TEST_BLOCK_SIZE,
    std::chrono::steady_clock,
    TypeParam
  >;
  using policy_t = channeler::policy::exponential_backoff_resend_policy<params>;

  auto now = params::clock::now();
  typename params::unsigned_window window{WINDOW_SIZE};
  policy_t policy{window, now, TIMEOUT, MAX_COLLISIONS, cb};

  test_expected_wait_until_arrived<policy_t>(policy, 42, now);

  // Again, the test is that eventually the packet arrives, so there should be
  // no timeout issue.
  ASSERT_EQ(0, invocations);
}





TYPED_TEST_P(ExponentialBackoffResend, expected_wait_until_timeout)
{
  constexpr TypeParam WINDOW_SIZE = 5;
  constexpr std::size_t MAX_COLLISIONS = 3;

  std::size_t invocations = 0;
  std::set<TypeParam> missing;
  auto cb = [&invocations, MAX_COLLISIONS, &missing](
      std::chrono::steady_clock::time_point const &,
      std::size_t collisions, TypeParam const &, std::set<TypeParam> const & m) -> void
  {
    ASSERT_LE(collisions, MAX_COLLISIONS);
    ASSERT_FALSE(m.empty());
    ++invocations;
    for (auto elem : m) {
      missing.insert(elem);
    }
  };

  using params = channeler::policy::policy_params<
    TEST_BLOCK_SIZE,
    std::chrono::steady_clock,
    TypeParam
  >;
  using policy_t = channeler::policy::exponential_backoff_resend_policy<params>;

  auto now = params::clock::now();
  typename params::unsigned_window window{WINDOW_SIZE};
  policy_t policy{window, now, TIMEOUT, MAX_COLLISIONS, cb,
    [] { return TypeParam{42}; }
  };

  // We have to use a higher timeout due to the number of collisions here.
  auto past_timeout = max_timeout(TIMEOUT, MAX_COLLISIONS);

  test_expected_wait_until_timeout<policy_t>(policy, 42, now,
      now + past_timeout);

  // Since we waited to past_timeout, we expect the number of invocations
  // made to be equal to MAX_COLLISIONS.
  ASSERT_EQ(MAX_COLLISIONS, invocations);

  // We also should have only the packet 42 reported as missing; this is a
  // merge across all invocations.
  ASSERT_FALSE(missing.empty());
  ASSERT_EQ(1, missing.size());
  ASSERT_EQ(42, *missing.begin());
}



TYPED_TEST_P(ExponentialBackoffResend, gaps_in_received)
{
  constexpr TypeParam WINDOW_SIZE = 100;
  constexpr std::size_t MAX_COLLISIONS = 3;

  std::size_t invocations = 0;
  auto cb = [&invocations, MAX_COLLISIONS](
      std::chrono::steady_clock::time_point const &,
      std::size_t collisions, TypeParam const &, std::set<TypeParam> const &) -> void
  {
    ASSERT_LE(collisions, MAX_COLLISIONS);
    ++invocations;
  };

  using params = channeler::policy::policy_params<
    TEST_BLOCK_SIZE,
    std::chrono::steady_clock,
    TypeParam
  >;
  using policy_t = channeler::policy::exponential_backoff_resend_policy<params>;

  auto now = params::clock::now();
  typename params::unsigned_window window{WINDOW_SIZE};
  policy_t policy{window, now, TIMEOUT, MAX_COLLISIONS, cb};

  test_gaps_in_received<policy_t>(policy, now);

  // The way the test is structured, there should not be any collisions
  // - timeouts do not factor into the test case.
  ASSERT_EQ(0, invocations);
}



TYPED_TEST_P(ExponentialBackoffResend, expected_window_with_gaps)
{
  constexpr TypeParam WINDOW_SIZE = 5;
  constexpr TypeParam FIRST = 10;
  constexpr std::size_t MAX_COLLISIONS = 3;

  std::size_t invocations = 0;
  auto cb = [&invocations, MAX_COLLISIONS](
      std::chrono::steady_clock::time_point const &,
      std::size_t collisions, TypeParam const &, std::set<TypeParam> const &) -> void
  {
    ASSERT_LE(collisions, MAX_COLLISIONS);
    ++invocations;
  };

  using params = channeler::policy::policy_params<
    TEST_BLOCK_SIZE,
    std::chrono::steady_clock,
    TypeParam
  >;
  using policy_t = channeler::policy::exponential_backoff_resend_policy<
    params
  >;

  auto now = params::clock::now();
  typename params::unsigned_window window{WINDOW_SIZE};
  policy_t policy{window, now, TIMEOUT, MAX_COLLISIONS, cb};

  // If we add values from the window, but not the first value, that should
  // put those into the future set. If we then add all values from the window
  // in sequence, it should result in generally progressing the window.
  test_window<policy_t>(policy, now, FIRST, WINDOW_SIZE);

  // Again, we expect zero invocations due to collisions.
  ASSERT_EQ(0, invocations);
}



TYPED_TEST_P(ExponentialBackoffResend, no_expected_window_with_gaps)
{
  constexpr TypeParam WINDOW_SIZE = 5;
  constexpr TypeParam FIRST = 10;
  constexpr std::size_t MAX_COLLISIONS = 3;

  std::size_t invocations = 0;
  auto cb = [&invocations, MAX_COLLISIONS](
      std::chrono::steady_clock::time_point const &,
      std::size_t collisions, TypeParam const &, std::set<TypeParam> const &) -> void
  {
    ASSERT_LE(collisions, MAX_COLLISIONS);
    ++invocations;
  };

  using params = channeler::policy::policy_params<
    TEST_BLOCK_SIZE,
    std::chrono::steady_clock,
    TypeParam
  >;
  using policy_t = channeler::policy::exponential_backoff_resend_policy<
    params
  >;

  auto now = params::clock::now();
  typename params::unsigned_window window{WINDOW_SIZE};
  policy_t policy{window, now, TIMEOUT, MAX_COLLISIONS, cb};

  // Same as above, but we don't expect a first packet
  test_window<policy_t>(policy, now, FIRST, WINDOW_SIZE);

  // Again, we expect zero invocations due to collisions.
  ASSERT_EQ(0, invocations);
}



TYPED_TEST_P(ExponentialBackoffResend, roll_over_window_with_gaps)
{
  constexpr std::size_t MAX_COLLISIONS = 3;
  constexpr TypeParam WINDOW_SIZE = 5;

  std::size_t invocations = 0;
  auto cb = [&invocations, MAX_COLLISIONS](
      std::chrono::steady_clock::time_point const &,
      std::size_t collisions, TypeParam const &, std::set<TypeParam> const &) -> void
  {
    ASSERT_LE(collisions, MAX_COLLISIONS);
    ++invocations;
  };

  using params = channeler::policy::policy_params<
    TEST_BLOCK_SIZE,
    std::chrono::steady_clock,
    TypeParam
  >;
  using policy_t = channeler::policy::exponential_backoff_resend_policy<
    params
  >;

  auto now = params::clock::now();
  typename params::unsigned_window window{WINDOW_SIZE};

  // Same as above, but we start the test a little before the end of the value
  // range, and roll over the start until after.
  for (TypeParam i = 0 ; i < WINDOW_SIZE ; ++i) {
    TypeParam first = std::numeric_limits<TypeParam>::max() - (WINDOW_SIZE / 2) + i;
    if (!first) {
      // Skipping, same as no_expected_window_with_gaps
      continue;
    }
    policy_t policy{window, now, TIMEOUT, MAX_COLLISIONS, cb};
    test_window<policy_t>(policy, now, first, WINDOW_SIZE);
  }

  // Again, we expect zero invocations due to collisions.
  ASSERT_EQ(0, invocations);
}



REGISTER_TYPED_TEST_SUITE_P(ExponentialBackoffResend,
    no_expected_wait_until_arrived,
    no_expected_wait_until_timeout,
    expected_wait_until_arrived,
    expected_wait_until_timeout,
    gaps_in_received,
    expected_window_with_gaps,
    no_expected_window_with_gaps,
    roll_over_window_with_gaps
);


INSTANTIATE_TYPED_TEST_SUITE_P(policy, ExponentialBackoffResend, test_types);


