/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_CONTEXT_CONNECTION_H
#define CHANNELER_CONTEXT_CONNECTION_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <functional>
#include <vector>

#include <channeler/timeouts.h>

#include "../memory/packet_pool.h"
#include "../pipe/filter_classifier.h"


namespace channeler::context {

/**
 * The connection context is instanciated once per connection. It should be
 * as lightweight as possible.
 */
template <
  typename paramsT,
  typename nodeT
>
struct connection
{
  // *** Template type aliases
  using params = paramsT;
  using node_type = nodeT;

  static_assert(std::is_same<
      params,
      typename node_type::params
  >::value,
  "Connection and node context parameters must be identical!");

  inline connection(node_type & node, peerid const & peer,
      ::channeler::timeout_config const & to_config)
    : m_node{node}
    , m_peer{peer}
    , m_channels{}
    , m_timeouts{m_node.sleep()} // TODO is this correct?
    , m_to_config{to_config}
  {
  }

  inline peerid const & peer() const
  {
    return m_peer;
  }

  inline typename params::channel_set & channels()
  {
    return m_channels;
  }

  inline typename params::timeouts & timeouts()
  {
    return m_timeouts;
  }

  inline ::channeler::timeout_config const & timeout_config() const
  {
    return m_to_config;
  }

  inline node_type & node()
  {
    return m_node;
  }

private:
  // *** Data members
  node_type &                         m_node;
  peerid                              m_peer;
  typename params::channel_set        m_channels;
  typename params::timeouts           m_timeouts;
  ::channeler::timeout_config const & m_to_config;
};


} // namespace channeler::context

#endif // guard
