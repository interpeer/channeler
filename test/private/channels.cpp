/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/channels.h"

#include <gtest/gtest.h>

namespace {

struct channel
{
  // We just need the type.
  using resend_callback = int;

  inline channel(
      channeler::channelid const &,
      channeler::capabilities_t const &,
      resend_callback
    )
  {
  }
};


} // anonymous namespace



TEST(Channels, empty_set)
{
  using namespace channeler;

  channels<channel> chs;

  channelid id = create_new_channelid();

  ASSERT_FALSE(chs.has_established_channel(id));
  ASSERT_FALSE(chs.has_pending_channel(id));
  ASSERT_FALSE(chs.has_channel(id));
}



TEST(Channels, add_bad_id_to_set)
{
  using namespace channeler;

  channels<channel> chs;

  // An identifier with just a responder is not valid
  channelid id;
  id.responder = 0xd00d;

  auto [res, chanopt] = chs.add(id);

  ASSERT_EQ(ERR_INVALID_CHANNELID, res);
}



TEST(Channels, add_partial_to_set)
{
  using namespace channeler;

  channels<channel> chs;

  channelid id = create_new_channelid();

  EXPECT_FALSE(chs.has_established_channel(id));
  EXPECT_FALSE(chs.has_pending_channel(id));
  EXPECT_FALSE(chs.has_channel(id));

  // Now add a channel.
  auto [res, chanopt] = chs.add(id);

  ASSERT_EQ(ERR_SUCCESS, res);

  ASSERT_FALSE(chs.has_established_channel(id));
  ASSERT_TRUE(chs.has_pending_channel(id));
  ASSERT_TRUE(chs.has_channel(id));
}



TEST(Channels, add_full_to_set)
{
  using namespace channeler;

  channels<channel> chs;

  channelid id = create_new_channelid();
  complete_channelid(id);

  EXPECT_FALSE(chs.has_established_channel(id));
  EXPECT_FALSE(chs.has_pending_channel(id));
  EXPECT_FALSE(chs.has_channel(id));

  // Now add a channel.
  auto [res, chanopt] = chs.add(id);

  ASSERT_EQ(ERR_SUCCESS, res);

  ASSERT_TRUE(chs.has_established_channel(id));
  ASSERT_FALSE(chs.has_pending_channel(id));
  ASSERT_TRUE(chs.has_channel(id));
}



TEST(Channels, upgrade_partial)
{
  using namespace channeler;

  channels<channel> chs;

  // First add partial
  channelid id = create_new_channelid();

  auto [res, chanopt] = chs.add(id);
  ASSERT_EQ(ERR_SUCCESS, res);

  ASSERT_TRUE(chs.has_pending_channel(id));
  ASSERT_FALSE(chs.has_established_channel(id));

  // Try upgrading a partial
  res = chs.make_full(id);
  ASSERT_EQ(ERR_INVALID_CHANNELID, res);

  // Upgrade for real
  complete_channelid(id);

  res = chs.make_full(id);
  ASSERT_EQ(ERR_SUCCESS, res);

  auto ptr = chs.get(id);
  ASSERT_TRUE(ptr);
}



TEST(Channels, add_default_channel)
{
  // Adding the default channel should always result in a full, established
  // channel object being returned.

  using namespace channeler;

  channels<channel> chs;

  channelid id = DEFAULT_CHANNELID;
  auto [res, chanopt] = chs.add(id);
  ASSERT_EQ(ERR_SUCCESS, res);

  ASSERT_TRUE(chs.has_established_channel(id));
  ASSERT_FALSE(chs.has_pending_channel(id));
  ASSERT_TRUE(chs.has_channel(id));

  auto ptr = chs.get(id);
  ASSERT_TRUE(ptr);
}
