/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/pipe/egress/message_bundling.h"
#include "../lib/channel_data.h"
#include "../lib/params.h"

#include <channeler/message/data.h>

#include <gtest/gtest.h>

#include "../../../clock.h"

namespace {

// For testing
static constexpr std::size_t PACKET_SIZE = 200;

using test_clock_params = test::clock_params;
using test_defaults = channeler::defaults<
  typename test_clock_params::duration,
  3 // POOL_BLOCK_SIZE
>;
using test_params = ::channeler::params<
  test_clock_params,
  test_defaults
>;


struct next
{
  using input_event = channeler::pipe::packet_out_event<
    test_params::defaults::POOL_BLOCK_SIZE
  >;

  inline channeler::pipe::action_list_type consume(std::unique_ptr<channeler::pipe::event> event)
  {
    m_event = std::move(event);
    return {};
  }
  std::unique_ptr<channeler::pipe::event> m_event;
};


using filter_t = channeler::pipe::message_bundling_filter<
  typename test_params::transport_address,
  test_params::defaults::POOL_BLOCK_SIZE,
  typename test_params::channel,
  next,
  next::input_event
>;

} // anonymous namespace



TEST(PipeEgressMessageBundlingFilter, passthrough_invalid_event)
{
  using namespace channeler::pipe;

  typename test_params::pool pool{PACKET_SIZE};
  filter_t::channel_set chs;
  next n;
  filter_t filter{&n, chs, pool,
    []() { return channeler::peerid{}; },
    []() { return channeler::peerid{}; }
  };

  // Create a default event; this should not be handled.
  auto ev = std::make_unique<event>();
  auto res = filter.consume(std::move(ev));
  ASSERT_TRUE(res.empty());
  ASSERT_TRUE(n.m_event);
  ASSERT_EQ(ET_UNKNOWN, n.m_event->type);
}


TEST(PipeEgressMessageBundlingFilter, bundle_message)
{
  using namespace channeler::pipe;

  typename test_params::pool pool{PACKET_SIZE};
  filter_t::channel_set chs;
  next n;
  channeler::peerid self;
  channeler::peerid peer;
  filter_t filter{&n, chs, pool,
    [&self]() { return self; },
    [&peer]() { return peer; }
  };

  // Create input event
  channeler::peerid sender;
  channeler::peerid recipient;
  auto channel = channeler::create_new_channelid();
  channeler::complete_channelid(channel);
  auto [err, chopt] = chs.add(channel);
  EXPECT_EQ(channeler::ERR_SUCCESS, err);
  auto ch = chopt.value();

  channeler::byte buf[130]; // It does not matter what's in this memory, but it
                            // does matter somewhat that the size of the buffer
                            // exceeds a length encodable in a single byte.
  auto reg = channeler::message_registry::create();
  auto msg = channeler::message_data::create_with_payload(reg, buf, sizeof(buf));
  ch->egress_messages().enqueue(std::move(msg));

  auto ev = std::make_unique<message_out_enqueued_event>(
      channel
  );
  auto ret = filter.consume(std::move(ev));

  ASSERT_EQ(0, ret.size());

  // No need to actually test packet header parsing - that's been tested
  // elsewhere. This tests that the filter passes on things well.
  ASSERT_EQ(n.m_event->type, ET_PACKET_OUT);
  next::input_event * ptr = reinterpret_cast<next::input_event *>(n.m_event.get());
  ASSERT_EQ(self, ptr->packet.sender());
  ASSERT_EQ(peer, ptr->packet.recipient());
  ASSERT_EQ(channel, ptr->packet.channel());

  // We have one message type and two length bytes
  ASSERT_EQ(sizeof(buf) + 3, ptr->packet.payload_size());
}
