/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG.
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_CLOCK_PARAMS_H
#define CHANNELER_CLOCK_PARAMS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <chrono>

#include "macros.h"

namespace channeler {

/**
 * This struct provides compile-time options (and defaults) for
 * clock related parameters.
 *
 * The default uses the system steady clock, but for testing purposes,
 * we might find it easier to provide a simpler implementation.
 */
template <
  typename clockT = std::chrono::steady_clock,
  typename timeT = typename clockT::time_point,
  typename durationT = typename clockT::duration
>
struct clock_params
{
  using clock = clockT;
  using time_point = timeT;
  using duration = durationT;
};

} // namespace channeler

#endif // guard
