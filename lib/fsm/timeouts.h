/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_FSM_TIMEOUTS_H
#define CHANNELER_FSM_TIMEOUTS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

namespace channeler::fsm {

/**
 * This is a central location for defining non-conflicting
 * timeout tags.
 */
constexpr uint16_t CHANNEL_NEW_TIMEOUT_TAG{0xc411};
constexpr uint16_t CHANNEL_TIMEOUT_TAG{0x114c};
constexpr uint16_t CHANNEL_PROGRESS_TIMEOUT_TAG{0xc114};
constexpr uint16_t CHANNEL_CONGESTION_TIMEOUT_TAG{0x41c1};

} // namespace channeler::fsm

#endif // guard
