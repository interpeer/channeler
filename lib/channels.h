/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_CHANNELS_H
#define CHANNELER_CHANNELS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <optional>

#include <channeler/channelid.h>
#include <channeler/capabilities.h>

#include "macros.h"

namespace channeler {

/**
 * The channels class is a holder for channels.
 *
 * In particular, it takes care that within each instance (aka connection),
 * a) Channel identifiers are not duplicated, and
 * b) The (abstract) state of channels is tracked.
 *
 * Channel state can take the form of one of three states:
 * a) unknown/absent,
 * b) pending (under establishment), and
 * c) established.
 *
 * For the latter state, a channel instance will be created and maintained
 * as long as the channel is alive.
 *
 * As the channelid is defined as having two parts, one chosen by initiator,
 * the other by responder, each party is tasked with selecting a unique part
 * for their role. It is technically sufficient for uniquely identifying a
 * channel.
 *
 * Potential for conflict arises when both peers open a channel simultaneously
 * and choose the same initiator part. In that case, both will log a partial
 * channelid, and send a MSG_CHANNEL_NEW. When they each receive the other
 * peer's message, they will find a pending channel. The correct response is
 * to not respond - this is a fairly rare case, and should be handled by both
 * sides trying again for new channels.
 *
 * But what it means for this class is that we're actually looking up channels
 * only by the initiator part.
 *
 * TODO:
 * https://codeberg.org/interpeer/channeler/issues/8
 */
template <
  typename channelT

  // TODO Lock policy? null policy should be in its own header
  // https://codeberg.org/interpeer/channeler/issues/7
>
class channels
{
public:
  using channel_ptr = std::shared_ptr<channelT>;

  using resend_callback = typename channelT::resend_callback;

  inline channels()
    : m_pending{}
    , m_established{}
    , m_resend{}
  {
  }

  inline void set_resend_callback(resend_callback resend)
  {
    m_resend = resend;
  }

  inline bool has_channel(channelid::half_type const & id) const
  {
    return has_established_channel(id) || has_pending_channel(id);
  }

  inline bool has_channel(channelid const & id) const
  {
    return has_established_channel(id) || has_pending_channel(id);
  }

  inline bool has_established_channel(channelid const & id) const
  {
    auto iter = m_established.find(id.initiator);
    if (iter == m_established.end()) {
      // Initiator not found
      return false;
    }

    if (iter->second.id.responder != id.responder) {
      // Initiator is found, but we have a different channelid remembered.
      // XXX This could actually be a problem.
      return false;
    }
    return true;
  }

  inline bool has_established_channel(channelid::half_type const & initiator) const
  {
    return m_established.end() != m_established.find(initiator);
  }


  inline bool has_pending_channel(channelid const & id) const
  {
    // Just consider the initiator part here.
    return has_pending_channel(id.initiator);
  }

  inline bool has_pending_channel(channelid::half_type const & initiator) const
  {
    // Just consider the initiator part here.
    return m_pending.end() != m_pending.find(initiator);
  }

  inline void drop_pending_channel(channelid::half_type const & initiator)
  {
    // TODO we can use remove() for that
    m_pending.erase(initiator);
  }

  inline channelid get_established_id(channelid::half_type const & initiator) const
  {
    auto iter = m_established.find(initiator);
    if (iter != m_established.end()) {
      return iter->second.id;
    }
    return DEFAULT_CHANNELID;
  }


  /**
   * Add - if it's a partial channel identifier, it will be added to
   * the pending set. If it's a full channel identifier, it will create
   * a channel. Otherwise, it's rejected.
   */
  using add_result = std::tuple<
    error_t,
    std::optional<channel_ptr>
  >;
  inline add_result
  add(channelid const & id,
      capabilities_t const & capabilities = capabilities_datagram()
    )
  {
    if (id == DEFAULT_CHANNELID || id.is_complete()) {
      auto iter = m_established.find(id.initiator);
      if (iter != m_established.end()) {
        if (iter->second.id == id) {
          // We can ignore this; the channel is already added. No need to error
          // out, either?
          return {ERR_CHANNEL_EXISTS, iter->second.data};
        }
        // However, if we currently think that the full channelid is different,
        // we should produce an error.
        return {ERR_INVALID_CHANNELID, {}};
      }

      // TODO lock policy pointer
      LIBLOG_DEBUG("Adding full channel: " << id);
      auto created = std::make_shared<channelT>(id, capabilities, m_resend);
      m_established[id.initiator] = {
        id,
        created
      };
      return {ERR_SUCCESS, created};
    }

    if (id.has_initiator()) {
      LIBLOG_DEBUG("Adding partial channel: " << id);
      // This is pending, so we can just overwrite existing identifiers. It
      // makes no difference.
      auto created = std::make_shared<channelT>(id, capabilities, m_resend);
      m_pending[id.initiator] = created;
      return {ERR_SUCCESS, created};
    }

    // An uninitalized identifier or one with just the responder part is
    // invalid.
    return {ERR_INVALID_CHANNELID, {}};
  }

  /**
   * Update a channel identifier. This is tricky, because it's mostly about
   * moving a partial channel from the pending set to a fully established
   * state.
   */
  inline error_t make_full(channelid const & id,
      capabilities_t const & capabilities = {})
  {
    // First, the passed id must be a full id.
    if (!id.is_complete()) {
      return ERR_INVALID_CHANNELID;
    }

    // If the id is already in the set, we can just ignore the call.
    auto iter = m_established.find(id.initiator);
    if (iter != m_established.end()) {
      if (iter->second.id == id) {
        return ERR_SUCCESS;
      }
      return ERR_INVALID_CHANNELID;
    }

    // If this partial identifier is in the pending set, remove it from there
    // and make a full entry.
    auto iter2 = m_pending.find(id.initiator);
    if (iter2 != m_pending.end()) {
      m_pending.erase(iter2);
    }

    // TODO lock policy pointer
    m_established[id.initiator] = {
      id,
      std::make_shared<channelT>(id, capabilities, m_resend),
    };

    return ERR_SUCCESS;
  }


  /**
   * Retrieve a channel or a nullptr-equivalent for the channel identifier,
   * depending on whether this channel has been established already.
   */
  inline channel_ptr get(channelid const & id) const
  {
    auto iter = m_established.find(id.initiator);
    if (iter != m_established.end()) {
      if (iter->second.id == id) {
        return iter->second.data;
      }
    }

    return get(id.initiator);
  }

  inline channel_ptr get(channelid::half_type const & initiator) const
  {
    auto piter = m_pending.find(initiator);
    if (piter != m_pending.end()) {
      return piter->second;
    }
    return {};
  }

  inline channel_ptr get_by_initiator(channelid::half_type const & initiator) const
  {
    auto iter = m_established.find(initiator);
    if (iter != m_established.end()) {
      return iter->second.data;
    }

    return get(initiator);
  }



  /**
   * Create a new pending channel identifier not currently in the set.
   */
  inline channelid::half_type new_pending_channel()
  {
    channelid id;
    do {
      id = create_new_channelid();
    } while (has_channel(id));

    // We don't have this channel, so add it.
    add(id);
    return id.initiator;
  }


  inline void remove(channelid const & id)
  {
    m_pending.erase(id.initiator);
    m_established.erase(id.initiator);
  }

  inline void remove(channelid::half_type const & initiator)
  {
    m_pending.erase(initiator);
    m_established.erase(initiator);
  }

private:
  // We need to keep a set of channel identifiers under establishment.
  // TODO instead of having two maps, we can have one map in which pending channels are marked by
  //      not containing a full channelid?
  //      https://codeberg.org/interpeer/channeler/issues/9
  using pending_channel_map_t = std::unordered_map<channelid::half_type, channel_ptr>;
  pending_channel_map_t      m_pending;

  // For established channels, we also need to keep a set of channel
  // instances.
  struct value_type
  {
    channelid   id;
    channel_ptr data;
  };

  using channel_map_t = std::unordered_map<channelid::half_type, value_type>;
  channel_map_t   m_established;

  resend_callback m_resend;
};

} // namespace channeler

#endif // guard
