/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/fsm/registry.h"

#include <gtest/gtest.h>

namespace {

struct foo {};

using namespace channeler::pipe;

struct test_fsm : public ::channeler::fsm::fsm_base
{
  virtual ::channeler::fsm::fsm_base::process_result
  process(event const & to_process [[maybe_unused]],
      action_list_type & result_actions,
      event_list_type & output_events)
  {
    // For the test case, we need to add one action and two
    // events.
    result_actions.push_back(std::make_unique<action>());

    output_events.push_back(std::make_unique<event>());
    output_events.push_back(std::make_unique<event>());

    using namespace channeler;
    using namespace channeler::fsm;
    return {ERR_SUCCESS, PS_SUCCESS};
  }
};


struct new_event_fsm : public ::channeler::fsm::fsm_base
{
  bool invoked = false;

  virtual ::channeler::fsm::fsm_base::process_result
  process(event const & to_process [[maybe_unused]],
      action_list_type & result_actions,
      event_list_type & output_events [[maybe_unused]])
  {
    using namespace channeler;
    using namespace channeler::fsm;

    // If we have not been invoked yet, we want to create a result
    // action that requests the FSM registry to process it.
    if (invoked) {
      return {ERR_SUCCESS, PS_IGNORED};
    }
    invoked = true;

    auto ev = std::make_unique<event>();
    result_actions.push_back(std::make_unique<::channeler::pipe::process_event_action>(std::move(ev)));

    return {ERR_SUCCESS, PS_SUCCESS};
  }
};




struct test_fsm_with_ctor : public ::channeler::fsm::fsm_base
{
  inline test_fsm_with_ctor(int unused [[maybe_unused]])
  {
  }

  virtual ::channeler::fsm::fsm_base::process_result
  process(event const & to_process [[maybe_unused]],
      action_list_type & result_actions [[maybe_unused]],
      event_list_type & output_events [[maybe_unused]])
  {
    using namespace channeler;
    using namespace channeler::fsm;
    return {ERR_SUCCESS, PS_SUCCESS};
  }
};

} // anonymous namespace


TEST(FSMRegistry, add)
{
  using namespace channeler::fsm;

  registry reg;

  // Fail at compile time
  // reg.add<foo>();

  // Succeed!
  ASSERT_NO_THROW(reg.add<test_fsm>());
}


TEST(FSMRegistry, add_with_ctor_args)
{
  using namespace channeler::fsm;

  registry reg;

  // Fail at compile time
  // reg.add<test_fsm_with_ctor>();

  // Succeed!
  ASSERT_NO_THROW(reg.add<test_fsm_with_ctor>(42));
}


TEST(FSMRegistry, with_ctx_add)
{
  using namespace channeler::fsm;

  registry reg;

  // Fail at compile time
  // reg.add<foo>();

  // Succeed!
  ASSERT_NO_THROW(reg.add<test_fsm>());
}


TEST(FSMRegistry, with_ctx_add_with_ctor_args)
{
  using namespace channeler::fsm;

  registry reg;

  // Fail at compile time
  // reg.add<test_fsm_with_ctor>();

  // Succeed!
  ASSERT_NO_THROW(reg.add<test_fsm_with_ctor>(42));
}
TEST(FSMRegistry, process_without_fsm)
{
  using namespace channeler::pipe;
  using namespace channeler::fsm;

  registry reg;

  // Without a FSM registered, process() must return false.
  event ev;
  action_list_type actions;
  event_list_type outputs;

  auto [err, res] = reg.process(ev, actions, outputs);
  ASSERT_EQ(channeler::ERR_STATE, err);
  ASSERT_EQ(0, actions.size());
  ASSERT_EQ(0, outputs.size());
}


TEST(FSMRegistry, process_with_fsm)
{
  using namespace channeler::pipe;
  using namespace channeler::fsm;

  registry reg;
  EXPECT_NO_THROW(reg.add<test_fsm>());

  // With a FSM registered, process() must produce results
  event ev;
  action_list_type actions;
  event_list_type outputs;

  auto [err, res] = reg.process(ev, actions, outputs);
  ASSERT_EQ(channeler::ERR_SUCCESS, err);
  ASSERT_EQ(channeler::fsm::fsm_base::PS_SUCCESS, res);

  ASSERT_EQ(1, actions.size());
  ASSERT_EQ(2, outputs.size());
}


TEST(FSMRegistry, process_with_new_event_action)
{
  using namespace channeler::pipe;
  using namespace channeler::fsm;

  registry reg;
  EXPECT_NO_THROW(reg.add<new_event_fsm>());
  EXPECT_NO_THROW(reg.add<test_fsm>());

  // With a FSM registered, process() must produce results.
  // The new event will be sent once, which should double
  // the actions and events as compared to test_fsm on its own
  // (see above).
  event ev;
  action_list_type actions;
  event_list_type outputs;

  auto [err, res] = reg.process(ev, actions, outputs);
  ASSERT_EQ(channeler::ERR_SUCCESS, err);
  ASSERT_EQ(channeler::fsm::fsm_base::PS_SUCCESS, res);

  ASSERT_EQ(2, actions.size());
  ASSERT_EQ(4, outputs.size());
}
