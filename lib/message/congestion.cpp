/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <channeler/message/congestion.h>

#include <liberate/serialization/varint.h>

#include <liberate/logging.h>

namespace channeler {


error_t
register_congestion_types(std::shared_ptr<message_registry> registry)
{
  static message_registry::message_info info[] = {
    // *****************************************
    {
      MSG_CONGESTION_RECEIVE_WINDOW,
      []() -> ssize_t
      {
        return -1;
      },
      [](std::unique_ptr<message> basic) -> std::tuple<error_t, std::unique_ptr<message>>
      {
        auto ret = upgrade_message<message_congestion_receive_window>(std::move(basic));

        byte const * offset = ret->payload;
        std::size_t remaining = ret->payload_size;

        // Deserialize the window size; it's a variable integer.
        liberate::types::varint tmp;
        auto used = liberate::serialization::deserialize_varint(tmp, offset, remaining);
        if (!used) {
          LIBLOG_ERROR("Could not decode window size.");
          return {ERR_DECODE, nullptr};
        }
        ret->window_size = static_cast<std::size_t>(tmp);

        offset += used;
        remaining -= used;

        if (remaining > 0) {
          // We didn't consume the entire payload
          LIBLOG_ERROR("Unused payload part.");
          return {ERR_DECODE, nullptr};
        }

        return {ERR_SUCCESS, std::move(ret)};
      },
    },
  };

  if (!registry) {
    LIBLOG_ERROR("Cannot register types with a non-existent registry.");
    return ERR_INVALID_VALUE;
  }

  error_t err = ERR_SUCCESS;
  for (std::size_t i = 0 ; i < sizeof(info) / sizeof(message_registry::message_info) ; ++i) {
    err = registry->register_message(info[i].type, info[i]);
    if (ERR_SUCCESS != err) {
      return err;
    }
  }

  return err;
}




/**
 * message_congestion_receive_window
 */
std::size_t
message_congestion_receive_window::serialize(byte * output, std::size_t output_max) const
{
  // The serialized size is based on the payload size, so let's make sure
  // that is up-to-date
  auto winsize = static_cast<liberate::types::varint>(window_size);

  *const_cast<std::size_t *>(&payload_size) = liberate::serialization::serialized_size(winsize);
  auto remaining = serialized_size();

  if (!remaining) {
    LIBLOG_ERROR("No idea how to serialize this message.");
    return 0;
  }
  if (remaining > output_max) {
    LIBLOG_ERROR("Serialized size is larger than output buffer.");
    return 0;
  }

  byte * offset = output;

  // Serialize message header
  auto used = this->message::serialize(output, output_max);
  if (used <= 0) {
    LIBLOG_ERROR("Could not serialize message header.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Serialize the window size
  used = liberate::serialization::serialize_varint(offset, remaining,
      winsize);
  if (!used) {
    LIBLOG_ERROR("Could not serialize window size.");
    return 0;
  }
  offset += used;
  remaining -= used;

  if (remaining != 0) {
    LIBLOG_ERROR("Did not fill the entire serialized size with data.");
    return 0;
  }
  return (offset - output);
}


} // namespace channeler
