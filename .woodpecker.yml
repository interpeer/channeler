###############################################################################
# DEFINITIONS
##############################################################################

host_image: &host_image "codeberg.org/native-ci/cpp-meson:release-20230502.24"
ndk_image: &ndk_image "codeberg.org/native-ci/android-ndk:release-20230502.15"

###############################################################################
# CLONE
##############################################################################

clone:
  git:
    image: woodpeckerci/plugin-git
    settings:
      partial: false
      depth: 50

##############################################################################
# MATRIX
##############################################################################
matrix:
  include:
  # Linux/compilers
  - TARGET_TYPE: host
    CC: gcc
    CXX: g++
    IMAGE: *host_image
#    ANALYSIS: true
# FIXME see https://codeberg.org/interpeer/channeler/issues/36
  - TARGET_TYPE: host
    CC: clang
    CXX: clang++
    IMAGE: *host_image

  # Android
  - TARGET_TYPE: cross
    IMAGE: *ndk_image
    CONAN_HOST_PROFILE: android-arm64-v8a.conan
  - TARGET_TYPE: cross
    IMAGE: *ndk_image
    CONAN_HOST_PROFILE: android-armeabi-v7a.conan
  - TARGET_TYPE: cross
    IMAGE: *ndk_image
    CONAN_HOST_PROFILE: android-x86.conan
  - TARGET_TYPE: cross
    IMAGE: *ndk_image
    CONAN_HOST_PROFILE: android-x86_64.conan

##############################################################################
# PIPELINE
##############################################################################

pipeline:
  ############################################################################
  # Dependencies

  deps:
    image: ${IMAGE}
    environment:
    - PIPENV_VENV_IN_PROJECT=1
    commands:
    - CI=true pipenv install --ignore-pipfile

  ############################################################################
  # Build

  build-meson:
    image: ${IMAGE}
    commands:
    - mkdir build && cd build
    - meson .. -Db_coverage=${ANALYSIS:=false}
    - ninja -v
    when:
      matrix:
        TARGET_TYPE: host

  build-conan:
    image: ${IMAGE}
    commands:
    - conan profile detect --name default
    - conan remote add codeberg https://codeberg.org/api/packages/interpeer/conan
    - >-
      conan create
      --profile:host=/usr/local/share/conan/profiles/${CONAN_HOST_PROFILE}
      --profile:build=default
      --test-folder=
      -s build_type=Release
      --build=missing
      .
    when:
      matrix:
        TARGET_TYPE: cross

  appveyor:
    image: ${IMAGE}
    commands:
    - CI=true pipenv run python ./scripts/appveyor.py
    secrets:
    - APPVEYOR_TOKEN
    when:
      matrix:
        CC: gcc
        IMAGE: *host_image
        ANALYSIS: true

  ############################################################################
  # Tests

  test-private:
    image: ${IMAGE}
    group: test
    commands:
    - cd build
    - ./test/private_tests
    when:
      matrix:
        TARGET_TYPE: host

  test-public:
    image: ${IMAGE}
    group: test
    commands:
    - cd build
    - ./test/public_tests
    when:
      matrix:
        TARGET_TYPE: host

  ############################################################################
  # Analysis & Checks

  towncrier:
    image: ${IMAGE}
    commands:
    - git fetch origin main
    - CI=true pipenv run towncrier check --compare-with origin/main
    when:
      event:
      - pull_request
      matrix:
        CC: gcc
        IMAGE: *host_image
        ANALYSIS: true

  semgrep:
    image: ${IMAGE}
    group: analysis
    commands:
    - CI=true pipenv run semgrep ci --config auto --exclude '*.gcov'
    when:
      event:
      - pull_request
      matrix:
        CC: gcc
        IMAGE: *host_image
        ANALYSIS: true

  flawfinder:
    image: ${IMAGE}
    group: analysis
    commands:
    - CI=true pipenv run flawfinder include/ lib/ examples/
    when:
      event:
      - pull_request
      matrix:
        CC: gcc
        IMAGE: *host_image
        ANALYSIS: true

  coverage:
    image: ${IMAGE}
    group: analysis
    commands:
    - cd build
    - if /usr/bin/test ${TARGET_TYPE:=host} == host ; then
        ninja coverage
        && cat meson-logs/coverage.txt ;
      fi
    when:
      event:
      - pull_request
      matrix:
        CC: gcc
        IMAGE: *host_image
        ANALYSIS: true
