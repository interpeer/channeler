/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/fsm/data.h"
#include "../lib/params.h"

#include <gtest/gtest.h>

#include "../../messages.h"
#include "../../packets.h"
#include "../../clock.h"

namespace {

constexpr static std::size_t TEST_PACKET_SIZE = 120;

using test_clock_params = test::clock_params;
using test_defaults = channeler::defaults<
  typename test_clock_params::duration,
  3 // POOL_BLOCK_SIZE
>;
using address_t = uint16_t;

using test_params = ::channeler::params<
  test_clock_params,
  test_defaults,
  channeler::null_lock_policy,
  address_t
>;

} // anonymous namespace


TEST(FSMData, process_bad_event)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_data<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs
  };

  // If we feed the FSM anything other than a ET_MESSAGE event, it will return
  // that the event wasn't handled.
  action_list_type actions;
  event_list_type events;
  event ev;
  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_EQ(fsm_base::PS_UNHANDLED, ret);
}


TEST(FSMData, process_bad_message)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_data<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;
  using event_t = message_event<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  typename test_params::pool pool{TEST_PACKET_SIZE};
  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs
  };

  // If we feed the FSM anything other than a ET_MESSAGE event, it will return
  // false.
  action_list_type actions;
  event_list_type events;

  auto reg = channeler::message_registry::create();
  auto [err, msg] = reg->parse(test::message_channel_cookie, test::message_channel_cookie_size, false);
  ASSERT_EQ(channeler::ERR_SUCCESS, err);

  event_t ev{{nullptr, 0, false}, pool.allocate(), {},
    std::move(msg)
  };
  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_EQ(fsm_base::PS_UNHANDLED, ret);
}


TEST(FSMData, remote_data_existing_channel)
{
  // - MSG_DATA on existing channel
  //  -> produce user data event
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_data<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;
  using event_t = message_event<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  typename test_params::pool pool{TEST_PACKET_SIZE};
  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs
  };

  // We need a packet with the above channel ID. Which won't exist, ever,
  // because we only have half a channel ID. But the important part is that
  // the initiator is the same.
  std::vector<channeler::byte> buf{test::packet_regular_channelid,
    test::packet_regular_channelid + test::packet_regular_channelid_size};
  packet_wrapper pkt{buf.data(), buf.size()};

  chs.add(pkt.channel());
  auto ch = chs.get(pkt.channel());

  // If we feed the FSM anything other than a ET_MESSAGE event, it will return
  // false.
  action_list_type actions;
  event_list_type events;

  auto reg = channeler::message_registry::create();
  auto [err, msg] = reg->parse(test::message_data, test::message_data_size, false);
  ASSERT_EQ(channeler::ERR_SUCCESS, err);

  event_t ev{pkt, pool.allocate(), ch,
    std::move(msg)
  };
  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_EQ(fsm_base::PS_SUCCESS, ret);

  ASSERT_EQ(0, actions.size());
  ASSERT_EQ(1, events.size());

  auto & rev = *events.begin();
  ASSERT_TRUE(rev);
  ASSERT_EQ(ET_USER_DATA_TO_READ, rev->type);
  auto rev_conv = reinterpret_cast<user_data_to_read_event<test_params::defaults::POOL_BLOCK_SIZE> *>(rev.get());

  ASSERT_EQ(rev_conv->channel, pkt.channel());
  ASSERT_EQ(rev_conv->message->type, MSG_DATA);

  auto dmsg = reinterpret_cast<message_data *>(rev_conv->message.get());
  ASSERT_EQ(test::message_data_size, dmsg->buffer_size);

  for (size_t idx = 0 ; idx < dmsg->buffer_size ; ++idx) {
    ASSERT_EQ(test::message_data[idx], dmsg->buffer[idx]);
  }
}


TEST(FSMData, remote_data_pending_channel)
{
  // - MSG_DATA on pending channel
  //  -> ??? Should be prevented by previous filter's re-ordering
  //     of messages, but that is not implemented yet
  //  -> In context of the FSM, discard, possibly create a result
  //    action
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_data<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;
  using event_t = message_event<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  typename test_params::pool pool{TEST_PACKET_SIZE};
  fsm_t::channel_set chs;
  auto cid = chs.new_pending_channel();
  auto ch = chs.get(cid);
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs
  };


  // We need a packet with the above channel ID. Which won't exist, ever,
  // because we only have half a channel ID. But the important part is that
  // the initiator is the same.
  std::vector<channeler::byte> buf{test::packet_regular_channelid,
    test::packet_regular_channelid + test::packet_regular_channelid_size};
  packet_wrapper pkt{buf.data(), buf.size()};
  pkt.channel().initiator = cid;

  // If we feed the FSM anything other than a ET_MESSAGE event, it will return
  // false.
  action_list_type actions;
  event_list_type events;

  auto reg = channeler::message_registry::create();
  auto [err, msg] = reg->parse(test::message_data, test::message_data_size, false);
  ASSERT_EQ(channeler::ERR_SUCCESS, err);

  event_t ev{pkt, pool.allocate(), ch,
    std::move(msg)
  };
  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_STATE, perr);
  ASSERT_EQ(fsm_base::PS_ERRORED, ret);

  ASSERT_EQ(0, actions.size());
  ASSERT_EQ(0, events.size());
}


TEST(FSMData, remote_data_unknown_channel)
{
  // - MSG_DATA on unknown channel
  //  -> discard, possibly create a result action
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_data<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;
  using event_t = message_event<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  typename test_params::pool pool{TEST_PACKET_SIZE};
  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs
  };

  // If we feed the FSM anything other than a ET_MESSAGE event, it will return
  // false.
  action_list_type actions;
  event_list_type events;

  auto reg = channeler::message_registry::create();
  auto [err, msg] = reg->parse(test::message_data, test::message_data_size, false);
  ASSERT_EQ(channeler::ERR_SUCCESS, err);

  event_t ev{{nullptr, 0, false}, pool.allocate(), {},
    std::move(msg)
  };
  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_EQ(fsm_base::PS_IGNORED, ret);

  ASSERT_EQ(0, actions.size());
  ASSERT_EQ(0, events.size());
}


TEST(FSMData, local_data_existing_channel)
{
  // - User data on existing channel
  //  -> produce MSG_DATA event
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_data<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;
  using event_t = fsm_t::data_written_event_type;

  typename test_params::pool pool{TEST_PACKET_SIZE};
  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs
  };


  // Random channelid, make pending
  auto id = create_new_channelid();
  complete_channelid(id);
  chs.add(id);
  auto ch = chs.get(id);
  ASSERT_TRUE(ch);

  // If we feed the FSM anything other than a ET_MESSAGE event, it will return
  // false.
  action_list_type actions;
  event_list_type events;
  std::vector<channeler::byte> data;
  event_t ev{id, data};

  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_EQ(fsm_base::PS_SUCCESS, ret);

  ASSERT_EQ(0, actions.size());
  ASSERT_EQ(1, events.size());

  auto & out = *events.begin();
  ASSERT_TRUE(out);
  ASSERT_EQ(ET_MESSAGE_OUT, out->type);

  auto outconv = reinterpret_cast<message_out_event *>(out.get());
  ASSERT_EQ(id, outconv->channel);
}


TEST(FSMData, local_data_pending_channel)
{
  // - User data on pending channel
  //  -> channel_data.has_pending_output() must be true
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_data<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;
  using event_t = fsm_t::data_written_event_type;

  typename test_params::pool pool{TEST_PACKET_SIZE};
  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs
  };

  // Random channelid, make pending
  auto id = create_new_channelid();
  chs.add(id);
  auto ch = chs.get(id);
  ASSERT_TRUE(ch);

  // If we feed the FSM anything other than a ET_MESSAGE event, it will return
  // false.
  action_list_type actions;
  event_list_type events;
  std::vector<channeler::byte> data;
  event_t ev{id, data};

  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);

  // We processed it, but are discarding the results
  ASSERT_EQ(fsm_base::PS_SUCCESS, ret);

  ASSERT_EQ(0, actions.size());
  ASSERT_EQ(1, events.size());

  auto & out = *events.begin();
  ASSERT_TRUE(out);
  ASSERT_EQ(ET_MESSAGE_OUT, out->type);

  auto outconv = reinterpret_cast<message_out_event *>(out.get());
  ASSERT_EQ(id, outconv->channel);
}


TEST(FSMData, local_data_unknown_channel)
{
  // - User data on unknown channel
  //  -> error response of some sort
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_data<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;
  using event_t = fsm_t::data_written_event_type;

  typename test_params::pool pool{TEST_PACKET_SIZE};
  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs
  };

  // Random channelid
  auto id = create_new_channelid();
  complete_channelid(id);

  // If we feed the FSM anything other than a ET_MESSAGE event, it will return
  // false.
  action_list_type actions;
  event_list_type events;
  std::vector<channeler::byte> data;
  event_t ev{id, data};

  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_EQ(fsm_base::PS_IGNORED, ret);

  ASSERT_EQ(1, actions.size());
  ASSERT_EQ(0, events.size());

  auto & act = *actions.begin();
  ASSERT_TRUE(act);
  ASSERT_EQ(AT_ERROR, act->type);

  auto convact = reinterpret_cast<error_action *>(act.get());
  ASSERT_EQ(ERR_INVALID_CHANNELID, convact->error);
}
