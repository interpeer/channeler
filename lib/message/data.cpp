/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <channeler/message/data.h>

#include <liberate/serialization/integer.h>
#include <liberate/serialization/varint.h>

#include <liberate/logging.h>

namespace channeler {


error_t
register_data_types(std::shared_ptr<message_registry> registry)
{
  static message_registry::message_info info[] = {
    // *****************************************
    {
      MSG_DATA,
      []() -> ssize_t
      {
        // Basic variable sized message
        return -1;
      },
      [](std::unique_ptr<message> basic) -> std::tuple<error_t, std::unique_ptr<message>>
      {
        return {ERR_SUCCESS, upgrade_message<message_data>(std::move(basic))};
      },
    },
    {
      MSG_DATA_PROGRESS,
      []() -> ssize_t
      {
        // Variable sized.
        return -1;
      },
      [](std::unique_ptr<message> basic) -> std::tuple<error_t, std::unique_ptr<message>>
      {
        auto ret = upgrade_message<message_data_progress>(std::move(basic));

        byte const * offset = ret->payload;
        std::size_t remaining = ret->payload_size;

        // First comes receive window
        auto used = liberate::serialization::deserialize_int(ret->receive_window_start,
            offset, remaining);
        if (used != sizeof(ret->receive_window_start)) {
          LIBLOG_ERROR("Unable to decode receive window start.");
          return {ERR_DECODE, nullptr};
        }
        offset += used;
        remaining -= used;

        // The remaining payload is sequence numbers. As such, it must be
        // divisible without remainder by the size of sequence numbers.
        if (remaining % sizeof(sequence_no_t)) {
          LIBLOG_ERROR("Remaining payload is not a multiple of sequence numbers.");
          return {ERR_DECODE, nullptr};
        }

        typename message_data_progress::request_set requests;
        while (remaining) {

          // Decode sequence number after sequence number.
          sequence_no_t resend = 0;
          used = liberate::serialization::deserialize_int(resend,
              offset, remaining);
          if (used != sizeof(resend)) {
            LIBLOG_ERROR("Unable to decode resend request.");
            return {ERR_DECODE, nullptr};
          }

          // There should not be duplication here.
          auto iter = requests.find(resend);
          if (iter != requests.end()) {
            LIBLOG_ERROR("Duplicate resend request in message.");
            return {ERR_DECODE, nullptr};
          }

          requests.insert(resend);

          offset += used;
          remaining -= used;
        }

        ret->resend_requests = requests;

        return {ERR_SUCCESS, std::move(ret)};
      }
    },
  };

  if (!registry) {
    LIBLOG_ERROR("Cannot register types with a non-existent registry.");
    return ERR_INVALID_VALUE;
  }

  error_t err = ERR_SUCCESS;
  for (std::size_t i = 0 ; i < sizeof(info) / sizeof(message_registry::message_info) ; ++i) {
    err = registry->register_message(info[i].type, info[i]);
    if (ERR_SUCCESS != err) {
      return err;
    }
  }

  return err;
}



/**
 * message_data
 */
message_data::message_data(std::shared_ptr<message_registry> _registry)
  : message{_registry, MSG_DATA}
  , owned_buffer{}
{
}



std::unique_ptr<message_data>
message_data::create_with_payload(std::shared_ptr<message_registry> registry, byte const * input,
    std::size_t size)
{
  auto ret = std::unique_ptr<message_data>(new message_data{registry});
  ret->set_payload(input, size);
  return ret;
}




std::size_t
message_data::set_payload(byte const * input, std::size_t _input_size)
{
  if (input && _input_size) {
    owned_buffer = std::make_shared<buffer_type>();
    owned_buffer->resize(_input_size);
    std::memcpy(owned_buffer->data(), input, _input_size);

    *const_cast<byte const **>(&payload) = owned_buffer->data();
    *const_cast<std::size_t *>(&payload_size) = _input_size;

    return _input_size;
  }

  // Reset buffer
  *const_cast<byte const **>(&payload) = nullptr;
  *const_cast<std::size_t *>(&payload_size) = 0;

  return 0;
}



std::size_t
message_data::serialize(byte * output, std::size_t output_max) const
{
  if (!payload || !payload_size) {
    LIBLOG_ERROR("No payload set, won't serialize data message.");
    return 0;
  }

  // We know the buffer size, as it's fixed.
  auto remaining = serialized_size();
  if (!remaining) {
    LIBLOG_ERROR("No idea how to serialize this message.");
    return 0;
  }
  if (remaining > output_max) {
    LIBLOG_ERROR("Serialized size is larger than output buffer.");
    return 0;
  }

  byte * offset = output;

  // Serialize message header
  auto used = this->message::serialize(output, output_max);
  if (used <= 0) {
    LIBLOG_ERROR("Could not serialize message header.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Copy payload
  ::memcpy(offset, payload, payload_size);

  offset += payload_size;
  remaining -= payload_size;

  if (remaining != 0) {
    LIBLOG_ERROR("Did not fill the entire serialized size with data.");
    return 0;
  }
  return (offset - output);
}



/**
 * message_data_progress
 */
message_data_progress::message_data_progress(
    std::shared_ptr<message_registry> _registry,
    sequence_no_t const & winstart,
    request_set const & requests)
  : message{_registry, MSG_DATA_PROGRESS}
  , receive_window_start{winstart}
  , resend_requests{requests}
{
  // The payload size is the number of sequence numbers we encode, which
  // is the set contents plus the window start.
  *const_cast<std::size_t *>(&payload_size) =
    sizeof(sequence_no_t) * (1 + requests.size());
}


std::size_t
message_data_progress::serialize(byte * output, std::size_t output_max) const
{
  // We can quickly calculate the required size.
  auto remaining = serialized_size();
  if (!remaining) {
    LIBLOG_ERROR("No idea how to serialize this message.");
    return 0;
  }
  if (remaining > output_max) {
    LIBLOG_ERROR("Serialized size is larger than output buffer.");
    return 0;
  }

  byte * offset = output;

  // Serialize message header
  auto used = this->message::serialize(output, output_max);
  if (used <= 0) {
    LIBLOG_ERROR("Could not serialize message header.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Serialize the window start
  used = liberate::serialization::serialize_int(offset, remaining,
      receive_window_start);
  if (used != sizeof(receive_window_start)) {
    LIBLOG_ERROR("Could not serialize receive window start.");
    return 0;
  }
  offset += used;
  remaining -= used;

  // Serialize all resend requests
  for (auto req : resend_requests) {
    used = liberate::serialization::serialize_int(offset, remaining,
        req);
    if (used != sizeof(req)) {
      LIBLOG_ERROR("Could not serialize receive window start.");
      return 0;
    }
    offset += used;
    remaining -= used;
  }

  if (remaining != 0) {
    LIBLOG_ERROR("Did not fill the entire serialized size with data.");
    return 0;
  }
  return (offset - output);
}



} // namespace channeler
