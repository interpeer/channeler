/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_CONTEXT_NODE_H
#define CHANNELER_CONTEXT_NODE_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <functional>
#include <vector>

#include "params.h"

#include "../memory/packet_pool.h"
#include "../support/timeouts.h"

namespace channeler::context {

/**
 * The node context is instanciated once for a node, and *not* per
 * connection.
 *
 * Note: in future, if we want to support multiple peer identifiers per
 *       node, we may have to split this up further.
 */
template <
  typename paramsT
>
struct node
{
  // *** Template type aliases
  using params = paramsT;

  inline node(peerid const & self,
      std::size_t packet_size,
      typename params::secret_generator generator,
      typename params::sleep_function sleep
    )
    : m_self{self}
    , m_packet_size{packet_size}
    , m_packet_pool{packet_size}
    , m_secret_generator{generator}
    , m_sleep{sleep}
    , m_message_registry{::channeler::message_registry::create()}
  {
  }

  inline std::shared_ptr<::channeler::message_registry> & message_registry()
  {
    return m_message_registry;
  }

  inline peerid const & id() const
  {
    return m_self;
  }

  inline std::size_t packet_size() const
  {
    return m_packet_size;
  }

  inline typename params::pool & packet_pool()
  {
    return m_packet_pool;
  }

  inline typename params::secret_generator &
  secret_generator()
  {
    return m_secret_generator;
  }

  inline typename params::sleep_function &
  sleep()
  {
    return m_sleep;
  }

private:
  // *** Data members
  peerid                                          m_self;
  std::size_t                                     m_packet_size;
  typename params::pool                           m_packet_pool;
  typename params::secret_generator               m_secret_generator;
  typename params::sleep_function                 m_sleep;
  std::shared_ptr<::channeler::message_registry>  m_message_registry;
};


} // namespace channeler::context

#endif // guard
