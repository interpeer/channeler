/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_SUPPORT_RANDOM_BITS_H
#define CHANNELER_SUPPORT_RANDOM_BITS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <random>
#include <chrono>
#include <limits>

namespace channeler::support {

/**
 * Return a random value of type T.
 */
template <typename T>
struct random_bits
{
  std::default_random_engine generator;
  std::uniform_int_distribution<T> distribution;

  inline random_bits()
    : generator{
        static_cast<std::default_random_engine::result_type>(
            std::chrono::system_clock::now().time_since_epoch().count()
        )
      }
    , distribution{
        std::numeric_limits<T>::min(),
        std::numeric_limits<T>::max()
      }
  {
  }

  T get()
  {
    return distribution(generator);
  }

  double get_factor()
  {
    return static_cast<long double>(get()) /
      (std::numeric_limits<T>::max() - std::numeric_limits<T>::min());
  }
};


} // namespace channeler::support

#endif // guard
