/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_PIPE_ENQUEUE_MESSAGE_H
#define CHANNELER_PIPE_ENQUEUE_MESSAGE_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <memory>

#include "../../memory/packet_pool.h"
#include "../event.h"
#include "../action.h"
#include "../event_as.h"

#include <channeler/packet.h>
#include <channeler/error.h>


namespace channeler::pipe {

/**
 * The enqueue_message filter puts messages into a channel's outgoing
 * message queue.
 *
 * This is simply so that we don't keep pushing messages through the filter
 * chain. Instead, the buffers should be deciding on which message or packet
 * to process any given time.
 */
template <
  typename channelT,
  typename next_filterT,
  typename next_eventT
>
struct enqueue_message_filter
{
  using input_event = message_out_event;
  using channel_set = ::channeler::channels<channelT>;

  inline enqueue_message_filter(next_filterT * next,
      channel_set & channels)
    : m_next{next}
    , m_channels{channels}
  {
  }


  template <typename... argsT>
  inline action_list_type
  consume(std::unique_ptr<event> unchecked, argsT && ...args)
  {
    action_list_type actions;
    auto in = event_as_or_passthrough<input_event, next_filterT, argsT...>(
        "egress:enqueue_message", unchecked, ET_MESSAGE_OUT,
        actions, m_next, std::forward<argsT>(args)...);
    if (!in) {
      return actions;
    }

    // Get the appropriate channel data
    auto ch = m_channels.get(in->channel);
    if (!ch) {
      // Uh-oh, error
      // TODO
      return {};
    }

    // Enqueue message
    ch->egress_messages().enqueue(std::move(in->message));

    // Create output event
    auto out = std::make_unique<message_out_enqueued_event>(in->channel);
    return m_next->consume(std::move(out), std::forward<argsT>(args)...);
  }


  next_filterT *  m_next;
  channel_set &   m_channels;
};


} // namespace channeler::pipe

#endif // guard
