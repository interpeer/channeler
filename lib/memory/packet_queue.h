/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_MEMORY_PACKET_QUEUE_H
#define CHANNELER_MEMORY_PACKET_QUEUE_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <optional>

#include "packet_pool.h"
#include "../policy/resend.h"
#include "../policy/ordering.h"
#include "../policy/loss.h"

#include <channeler/packet.h>

#include <liberate/logging.h>

namespace channeler::memory {

/**
 * Virtual base for packet queues; this permits run-time exchanging of policies
 * by using different concrete instantiations.
 */
template <
  typename policy_paramsT
>
struct packet_queue_base
{
  using params = policy_paramsT;

  virtual ~packet_queue_base() = default;

  /**
   * Push back onto the queue should usually result in ERR_SUCCESS. If a packet
   * is outside the receive window or will not be processed due to the queue
   * ordering policy, ERR_PACKET_REJECTED may be returned.
   */
  inline error_t
  push_back(typename params::time_point const & now,
      packet_wrapper const & packet,
      typename params::slot const & _slot)
  {
    return push_back(now, { packet, _slot });
  }


  virtual error_t
  push_back(typename params::time_point const & now,
      typename params::queue_entry const & entry) = 0;


  /**
   * Retrieving a value from the queue may succeed or not. This is indicated by
   * the first part of the result tuple, the optional queue entry.
   *
   * If the first part contains no entry, this may be a transitory or fatal
   * issue. In fatal situations, the second tuple value is set to true,
   * otherwise it remains false.
   *
   * If no sequence number is provided to the retrieve function, this means
   * that the next available packet based on the ordering policy is to be
   * retrieved.
   */
  using retrieve_result = std::tuple<
    std::optional<typename params::queue_entry>,
    bool
  >;

  virtual retrieve_result
  retrieve(typename params::time_point const & now,
      sequence_no_t const & seqno) = 0;


  /**
   * Shift the window start to the given sequence number; as a consequence,
   * truncate everything before said number. If no sequence number is given,
   * the window is advanced from window_start() to the next entry.
   */
  virtual void shift_window(typename params::time_point const & now,
      typename params::sequence_no_t new_start = 0
    ) = 0;


  /**
   * Popping a value off the beginning of the queue is a request to retrieve the
   * next available value, followed by a window shift to the next entry.
   */
  inline retrieve_result
  pop_front(typename params::time_point const & now)
  {
    auto [entry, close] = retrieve(now, 0);
    if (entry.has_value()) {
      shift_window(now, 0);
    }
    return {entry, close};
  }


  /**
   * STL-ish accessors
   */
  virtual bool empty() const = 0;
  virtual std::size_t size() const = 0;

  /**
   * Report window start
   */
  virtual sequence_no_t window_start() const = 0;

  /**
   * Report and (try to) set window size.
   */
  virtual std::size_t window_size() const = 0;
  virtual bool set_window_size(std::size_t window_size) = 0;


  /**
   * To be called *before* use (typically), set an initial clock for processing
   * timeouts.
   */
  virtual void set_initial_clock(typename params::time_point const & now) = 0;
};



/**
 * A packet queue is a queue of packets, backed by a packet pool, the behaviour
 * of which is goverend by a choice of policies for ordering packets, resending
 * missing packets, and dealing with packet loss.
 *
 * Queues follow the same fundamental algorithm, and consult policies at various
 * decision points. The bulk of the logic is in those, however. The separation
 * into separate policies and a combining algorithm is intended for easier
 * experimentation and readability of individual domains.
 *
 * The queue effectively manages a receive window; packets arriving within this
 * window are returned to the reader in the order defined by the ordering
 * policy. Normally, the window's lower bound is moved forward with the reader
 * reading a new packet. The ordering policy might also decide that it's OK to
 * skip ahead. The resend policy decides how to handle missing packets within
 * the window, while the loss policy is consulted when it decides there is no
 * more time to wait.
 *
 * In order to do this, the queue manages the next packet the *reader* should
 * read. The receive window starts at this lower bound, and extends to the end
 * of the window size.
 *
 * Upon receipt of a packet, the resend policy classifies it relative to this
 * window. It may initiate resends based on configured resend timeouts.
 *
 * The ordering policy consumes this classification. If the ordering policy
 * determines that it is correct to modify the window lower bound, it informs
 * the queue accordingly.
 *
 * Reading a packet always moves the window lower bound forward.
 */
template <
  typename resend_policyT,
  typename ordering_policyT,
  typename loss_policyT
>
class generic_packet_queue
  : public packet_queue_base<typename resend_policyT::params>
{
public:
  // Publish template parameters.
  using resend_policy = resend_policyT;
  using ordering_policy = ordering_policyT;
  using loss_policy = loss_policyT;

  // Assert own type; this is a shortcut for e.g. std::bind
  using own_type = generic_packet_queue<
    resend_policy,
    ordering_policy,
    loss_policy
  >;

  // Ensure template paramters are compatible with each other.
  static_assert(std::is_same<
      typename resend_policy::params,
      typename ordering_policy::params
    >::value,
    "All policies must share the same type specializations.");
  static_assert(std::is_same<
      typename resend_policy::params,
      typename loss_policy::params
    >::value,
    "All policies must share the same type specializations.");

  // With the static asserts done, we can use any of the policy params as our
  // authoritative params parameter.
  using params = typename resend_policy::params;

  // Also expose base type; this is useful for base pointers.
  using base_type = packet_queue_base<params>;

  // The queue entry is used by ordering policy and the queue itself.
  using queue_entry = typename params::queue_entry;

  // In order to understand that type well, we may also need the slot type.
  using pool = typename params::pool;
  using slot = typename params::slot;

  // For queues that report missing packet, we need to expose some resend
  // policy params.
  using missing_set = typename params::missing_set;
  using resend_callback = typename params::resend_callback;

  /**
   * Constructor. We need to initialize with a window size. This is the size
   * of the receive window, and it can be adjusted at run-time.
   *
   * Not all constructor parameters are used by all policy combinations. See
   * the specific queues below for details.
   */
  inline explicit generic_packet_queue(std::size_t window_size,
      typename params::duration const & roundtrip_timeout,
      std::size_t const & max_collisions = 0,
      typename params::time_point const & initial_clock = params::clock::now(),
      resend_callback _resend_callback = {}
    )
    : m_window{window_size}
    , m_next{0} // TODO: provide an initial sequence number?
    , m_resend_policy{
        m_window,
        initial_clock,
        roundtrip_timeout,
        max_collisions,
        _resend_callback,
        [this] { return m_next; }
      }
    , m_ordering_policy{m_window}
    , m_loss_policy{}
  {
  }


  virtual ~generic_packet_queue() = default;

  // See base_type above.
  inline error_t
  push_back(typename params::time_point const & now,
      packet_wrapper const & packet,
      slot const & _slot)
  {
    return push_back(now, { packet, _slot });
  }


  // See base_type above.
  virtual error_t
  push_back(typename params::time_point const & now,
      queue_entry const & entry) override final
  {
    auto sequence_no = entry.packet.sequence_no();

    // Push to resend policy. It will tell us what to do with the packet.
    auto res = m_resend_policy.packet_arrived(now, m_next, sequence_no);
    using namespace channeler::policy;
    switch (res) {
      case RR_INVALID:
        return ERR_PACKET_INVALID;

      case RR_UNEXPECTED:
        return ERR_AGAIN;

      case RR_DUPLICATE:
      case RR_OUTDATED:
        // TODO it would be possible to provide distinct return types here,
        //      but is this useful?
        return ERR_PACKET_REJECTED;

      case RR_OK:
      case RR_OUT_OF_ORDER:
        // Fall through; how to handle this is part of the ordering policy
        break;
    }
    LIBLOG_DEBUG("Packet is in receive window: "
        << static_cast<int64_t>(sequence_no));

    // If there hasn't been a lower bound set yet, then we need to set it to
    // the just arrived packet.
    if (!m_next) {
      LIBLOG_DEBUG("Initializing window lower bound to "
          << static_cast<int64_t>(sequence_no));
      m_next = sequence_no;
    }

    // The ordering policy needs to know the current window lower bound to e.g.
    // clear outdated packets. Since the sequence number is in the window as
    // per the above check, inserting must typically succeed.
    auto err = m_ordering_policy.ordered_insert(now, m_next, entry);
    if (ERR_SUCCESS != err) {
      LIBLOG_DEBUG("Packet is no longer of interest, dropping.");
      // TODO dropped? rejected? same thing?
      return ERR_PACKET_REJECTED;
    }

    // If we did manage to insert a packet, make sure that we update any
    // resend timeouts. There is a single timeout per policy, not one per
    // packet. At each expiration, it requests all missing packets (if
    // the policy so determines).
    m_resend_policy.update_timeouts(now, m_next);

    // On the insertion side of things, that should be it - if the packet is in
    // the window and the ordering works, we're fine.
    LIBLOG_DEBUG("Packet accepted.");
    return ERR_SUCCESS;
  }


  virtual typename base_type::retrieve_result
  retrieve(typename params::time_point const & now,
      sequence_no_t const & seqno) override final
  {
    // If the given sequence number is zero, we assume the m_next packet is to
    // be retrieved.
    sequence_no_t to_retrieve = seqno;
    if (!to_retrieve) {
      to_retrieve = m_next;
    }
    LIBLOG_DEBUG("Asked to retrieve packet " << to_retrieve << " with window "
        "start at " << m_next);

    // Check if we have that value
    auto result = m_ordering_policy.retrieve(m_next, to_retrieve,
        !m_resend_policy.DOES_RESEND);
    if (result.has_value()) {
      LIBLOG_DEBUG("Have the requested packet, returning it.");
      return {result, false};
    }

    // If we don't have any particular expectation and no value, this is fine.
    // We're still waiting for data to arrive.
    if (!m_next) {
      LIBLOG_DEBUG("There is no particular next packet expected, this is "
          "fine.");
      return {std::optional<typename params::queue_entry>{}, false};
    }

    // Let's see if we're supposed to keep waiting for this packet.
    if (m_resend_policy.keep_waiting_for(now, m_next, to_retrieve)) {
      LIBLOG_DEBUG("Packet is still expected to arrive, let's wait.");
      return {std::optional<typename params::queue_entry>{}, false};
    }

    // This is a packet loss condition. The loss policy will tell us
    // whether to close.
    if (m_loss_policy.emit_close_event()) {
      LIBLOG_DEBUG("Packet is lost, and we're closing.");
      return {std::optional<typename params::queue_entry>{}, true};
    }

    LIBLOG_DEBUG("Packet is lost, but we're going on.");
    return {std::optional<typename params::queue_entry>{}, false};
  }


  virtual bool empty() const override final
  {
    return m_ordering_policy.empty();
  }


  virtual std::size_t size() const override final
  {
    return m_ordering_policy.size();
  }


  virtual sequence_no_t window_start() const override final
  {
    return m_next;
  }

  virtual void shift_window(typename params::time_point const & now,
      typename params::sequence_no_t new_start = 0
    ) override final
  {
    LIBLOG_DEBUG("Window shift to " << new_start << " requested.");
    if (new_start) {
      m_next = new_start;
    }
    else {
      m_next = m_window.increment(m_next);
    }
    LIBLOG_DEBUG("Shifted window to: " << m_next);

    // Other than shfiting the window, we also update timeouts and release any
    // out of date entries.
    m_resend_policy.update_timeouts(now, m_next);
    m_ordering_policy.clear_outdated_entries(m_next);
    LIBLOG_DEBUG("Window is shifted.");
  }

  virtual void set_initial_clock(typename params::time_point const & initial) override final
  {
    m_resend_policy.set_initial_clock(initial);
  }

  virtual std::size_t window_size() const override final
  {
    return m_window.window_size();
  }

  virtual bool set_window_size(std::size_t window_size) override final
  {
    return m_window.set_window(window_size);
  }



private:

  typename params::unsigned_window  m_window;
  typename params::sequence_no_t    m_next;

  resend_policy                     m_resend_policy;
  ordering_policy                   m_ordering_policy;
  loss_policy                       m_loss_policy;
};


/**
 * We now have a variety of concrete implementations of the generic packet
 * queue, though still based off the policy params.
 *
 * - UDP-like:
 *   no_resend_fifo_ignore_loss_queue
 * - TPC-like:
 *   exponential_backoff_sequential_close_on_loss
 * - SCTP-like:
 *   exponential_backoff_fifo_close_on_loss
 * - Good for streaming:
 *   exponential_backoff_sequential_ignore_loss
 *   no_resend_sequential_ignore_loss
 *
 * See https://reset.substack.com/p/channel-capabilities for some comparison
 * on the queue types
 */

template <typename policy_paramsT>
using no_resend_fifo_ignore_loss_queue = generic_packet_queue<
  policy::no_resend_policy<policy_paramsT>,
  policy::order_of_arrival_policy<policy_paramsT>,
  policy::return_next_policy<policy_paramsT>
>;

template <typename policy_paramsT>
using exponential_backoff_fifo_ignore_loss_queue = generic_packet_queue<
  policy::exponential_backoff_resend_policy<policy_paramsT>,
  policy::order_of_arrival_policy<policy_paramsT>,
  policy::return_next_policy<policy_paramsT>
>;

template <typename policy_paramsT>
using no_resend_gapped_ignore_loss_queue = generic_packet_queue<
  policy::no_resend_policy<policy_paramsT>,
  policy::sequential_with_gaps_policy<policy_paramsT>,
  policy::return_next_policy<policy_paramsT>
>;

template <typename policy_paramsT>
using exponential_backoff_gapped_ignore_loss_queue = generic_packet_queue<
  policy::exponential_backoff_resend_policy<policy_paramsT>,
  policy::sequential_with_gaps_policy<policy_paramsT>,
  policy::return_next_policy<policy_paramsT>
>;

template <typename policy_paramsT>
using no_resend_sequential_ignore_loss_queue = generic_packet_queue<
  policy::no_resend_policy<policy_paramsT>,
  policy::sequential_without_gaps_policy<policy_paramsT>,
  policy::return_next_policy<policy_paramsT>
>;

template <typename policy_paramsT>
using exponential_backoff_sequential_ignore_loss_queue = generic_packet_queue<
  policy::exponential_backoff_resend_policy<policy_paramsT>,
  policy::sequential_without_gaps_policy<policy_paramsT>,
  policy::return_next_policy<policy_paramsT>
>;

template <typename policy_paramsT>
using no_resend_fifo_close_on_loss_queue = generic_packet_queue<
  policy::no_resend_policy<policy_paramsT>,
  policy::order_of_arrival_policy<policy_paramsT>,
  policy::abort_policy<policy_paramsT>
>;

template <typename policy_paramsT>
using exponential_backoff_fifo_close_on_loss_queue = generic_packet_queue<
  policy::exponential_backoff_resend_policy<policy_paramsT>,
  policy::order_of_arrival_policy<policy_paramsT>,
  policy::abort_policy<policy_paramsT>
>;

template <typename policy_paramsT>
using no_resend_gapped_close_on_loss_queue = generic_packet_queue<
  policy::no_resend_policy<policy_paramsT>,
  policy::sequential_with_gaps_policy<policy_paramsT>,
  policy::abort_policy<policy_paramsT>
>;

template <typename policy_paramsT>
using exponential_backoff_gapped_close_on_loss_queue = generic_packet_queue<
  policy::exponential_backoff_resend_policy<policy_paramsT>,
  policy::sequential_with_gaps_policy<policy_paramsT>,
  policy::abort_policy<policy_paramsT>
>;

template <typename policy_paramsT>
using no_resend_sequential_close_on_loss_queue = generic_packet_queue<
  policy::no_resend_policy<policy_paramsT>,
  policy::sequential_without_gaps_policy<policy_paramsT>,
  policy::abort_policy<policy_paramsT>
>;

template <typename policy_paramsT>
using exponential_backoff_sequential_close_on_loss_queue = generic_packet_queue<
  policy::exponential_backoff_resend_policy<policy_paramsT>,
  policy::sequential_without_gaps_policy<policy_paramsT>,
  policy::abort_policy<policy_paramsT>
>;


} // namespace channeler::memory

#endif // guard
