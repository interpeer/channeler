/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <channeler/message.h>
#include <channeler/message/channels.h>

#include <gtest/gtest.h>

TEST(MessageRegistry, creation)
{
  auto reg = channeler::message_registry::create();
  ASSERT_TRUE(reg);
}


TEST(MessageRegistry, basic_message)
{
  auto reg = channeler::message_registry::create();
  EXPECT_TRUE(reg);

  ASSERT_TRUE(reg->is_registered(channeler::MSG_CHANNEL_NEW));
  auto & info = reg->get_info(channeler::MSG_CHANNEL_NEW);
  ASSERT_EQ(channeler::MSG_CHANNEL_NEW, info.type);
}
