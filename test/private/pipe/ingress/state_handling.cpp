/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/pipe/ingress/state_handling.h"
#include "../lib/channel_data.h"
#include "../lib/context/node.h"
#include "../lib/context/connection.h"
#include "../lib/fsm/default.h"

#include <gtest/gtest.h>

#include "../../../packets.h"
#include "../../../messages.h"
#include "../../../clock.h"


using namespace test;

namespace {

// For testing
static constexpr std::size_t PACKET_SIZE = 200;

using test_clock_params = test::clock_params;
using test_defaults = channeler::defaults<
  typename test_clock_params::duration,
  3 // POOL_BLOCK_SIZE
>;
using address_t = uint16_t;

using test_params = ::channeler::params<
  test_clock_params,
  test_defaults,
  channeler::null_lock_policy,
  address_t
>;


using node_t = ::channeler::context::node<test_params>;

using connection_t = ::channeler::context::connection<
  test_params, node_t
>;

using registry_t = channeler::fsm::registry;

using simple_filter_t = channeler::pipe::state_handling_filter<
  test_defaults::POOL_BLOCK_SIZE,
  typename test_params::channel,
  registry_t
>;


struct event_capture
{
  std::unique_ptr<channeler::pipe::event> captured;

  channeler::pipe::action_list_type func(std::unique_ptr<channeler::pipe::event> ev)
  {
    captured = std::move(ev);
    return {};
  }
};


} // anonymous namespace


TEST(PipeIngressStateHandlingFilter, throw_on_invalid_event)
{
  using namespace channeler::pipe;

  channeler::peerid self;
  channeler::peerid peer;

  node_t node{
    self,
    PACKET_SIZE,
    []() -> std::vector<channeler::byte> { return {}; },
    [](channeler::timeout_duration d) { return d; },
  };

  channeler::timeout_config to_config;
  connection_t ctx{
    node,
    peer,
    to_config
  };

  auto reg = channeler::fsm::get_standard_registry(ctx);

  simple_filter_t::event_route_map_t routemap;
  simple_filter_t filter{reg, routemap};

  // Create a default event; this should not be handled.
  auto ev = std::make_unique<event>();
  ASSERT_THROW(filter.consume(std::move(ev)), ::channeler::exception);

  // The filter should also throw on a null event
  ASSERT_THROW(filter.consume(nullptr), ::channeler::exception);
}


TEST(PipeIngressStateHandlingFilter, create_message_on_channel_new)
{
  using namespace channeler::pipe;

  channeler::peerid self;
  channeler::peerid peer;

  node_t node{
    self,
    PACKET_SIZE,
    []() -> std::vector<channeler::byte> { return {}; },
    [](channeler::timeout_duration d) { return d; },
  };

  channeler::timeout_config to_config;
  connection_t ctx{
    node,
    peer,
    to_config
  };

  auto reg = channeler::fsm::get_standard_registry(ctx);

  // Register callbacks; in this case we need to get an egress
  // message as a result.
  simple_filter_t::event_route_map_t routemap;
  using namespace std::placeholders;
  event_capture cap_egress;
  routemap[EC_EGRESS] = std::bind(&event_capture::func, &cap_egress, _1);

  simple_filter_t filter{reg, routemap};

  // Copy packet data before parsing header
  typename test_params::pool pool{PACKET_SIZE};
  auto data = pool.allocate();
  ::memcpy(data.data(), packet_regular_channelid, packet_regular_channelid_size);
  channeler::packet_wrapper packet{data.data(), data.size()};

  auto mreg = channeler::message_registry::create();
  auto [err, msg] = mreg->parse(
      test::message_channel_new, test::message_channel_new_size, false
  );
  ASSERT_EQ(channeler::ERR_SUCCESS, err);
  auto ev = std::make_unique<simple_filter_t::input_event>(packet, data,
      typename test_params::channel_set::channel_ptr{},
      std::move(msg)
  );
  auto res = filter.consume(std::move(ev));

  // No actions
  ASSERT_EQ(0, res.size());

  // However, the egress capture function should have received an event.
  ASSERT_TRUE(cap_egress.captured);
  ASSERT_EQ(ET_MESSAGE_OUT, cap_egress.captured->type);
}
