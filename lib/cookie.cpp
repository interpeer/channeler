/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <channeler/cookie.h>

#include <cstring>
#include <vector>

#include <liberate/serialization/integer.h>
#include <liberate/checksum/crc32.h>

namespace channeler {


cookie
create_cookie_initiator(
    byte const * secret, std::size_t secret_size,
    peerid_wrapper const & initiator,
    peerid_wrapper const & responder,
    channelid::half_type initiator_part)
{
  // Buffer for the cookie inputs
  std::vector<byte> buf;
  buf.resize(secret_size + (PEERID_SIZE_BYTES * 2) + sizeof(channelid::half_type));

  // First the secret
  byte * offs = &buf[0];
  ::memcpy(offs, secret, secret_size);
  offs += secret_size;

  // Initiator and responder peer id
  ::memcpy(offs, initiator.raw, initiator.size());
  offs += initiator.size();

  ::memcpy(offs, responder.raw, responder.size());
  offs += responder.size();

  // Channel id part
  liberate::serialization::serialize_int(offs, buf.size() - (offs - buf.data()),
      initiator_part);

  // Checksum
  cookie res = liberate::checksum::crc32<liberate::checksum::CRC32>(
      buf.begin(), buf.end());
  return res;
}



cookie
create_cookie_responder(
    byte const * secret, std::size_t secret_size,
    peerid_wrapper const & initiator,
    peerid_wrapper const & responder,
    channelid const & id)
{
  // Buffer for the cookie inputs
  std::vector<byte> buf;
  buf.resize(secret_size + (PEERID_SIZE_BYTES * 2) + sizeof(channelid));

  // First the secret
  byte * offs = &buf[0];
  if (secret_size > 0) {
    ::memcpy(offs, secret, secret_size);
    offs += secret_size;
  }

  // Initiator and responder peer id
  ::memcpy(offs, initiator.raw, initiator.size());
  offs += initiator.size();

  ::memcpy(offs, responder.raw, responder.size());
  offs += responder.size();

  // Channel id part
  liberate::serialization::serialize_int(offs, buf.size() - (offs - buf.data()),
      id.full);

  // Checksum
  cookie res = liberate::checksum::crc32<liberate::checksum::CRC32>(
      buf.begin(), buf.end());
  return res;
}



} // namespace channeler
