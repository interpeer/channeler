/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <channeler/message/channels.h>
#include <channeler/message/data.h>

#include <gtest/gtest.h>

#include "../messages.h"

using namespace test;

TEST(MessageBasics, fail_parse_unknown)
{
  auto reg = channeler::message_registry::create();

  std::vector<channeler::byte> b{message_unknown, message_unknown + message_unknown_size};

  auto [ret, msg] = reg->parse(b.data(), b.size(), false);
  ASSERT_EQ(channeler::ERR_INVALID_MESSAGE_TYPE, ret);
  ASSERT_FALSE(msg);
}


TEST(MessageBasics, iterator_single_message)
{
  auto reg = channeler::message_registry::create();

  std::vector<channeler::byte> b{message_data, message_data + message_data_size};

  channeler::messages msgs{reg, b.data(), b.size()};

  std::size_t count = 0;
  auto iter = msgs.begin();
  for (; iter != msgs.end() ; ++iter, ++count) {
    // std::cout << "asdf type: " << (*iter)->type << std::endl;
    // std::cout << "  remaining: " << iter.remaining() << std::endl;
  }
  ASSERT_EQ(count, 1);
  ASSERT_EQ(iter.remaining(), 0);

  count = 0;
  for (auto x : msgs) {
    ++count;
    // std::cout << "xtype: " << x->type << std::endl;
  }
  ASSERT_EQ(count, 1);
}



TEST(MessageBasics, dynamic_message_cast)
{
  auto reg = channeler::message_registry::create();

  std::vector<channeler::byte> b{message_channel_acknowledge, message_channel_acknowledge + message_channel_acknowledge_size};

  auto [ret, msg] = reg->parse(b.data(), b.size(), true);
  ASSERT_EQ(channeler::ERR_SUCCESS, ret);
  ASSERT_TRUE(msg);

  // The message should be a basic message. Casting up should fail.
  auto x = dynamic_cast<channeler::message_channel_acknowledge *>(msg.get());
  ASSERT_EQ(nullptr, x);

  // Ok, but with a dynamic_message_cast, we should get what we want.
  auto extracted = channeler::dynamic_message_cast<channeler::message_channel_acknowledge>(msg);
  ASSERT_TRUE(extracted);

  // Ensure that decoding worked well
  ASSERT_EQ((channeler::channelid{0xd00d, 0xbeef}), extracted->id);
}




TEST(MessageBasics, iterator_message_block)
{
  auto reg = channeler::message_registry::create();

  std::vector<channeler::byte> b{message_block, message_block + message_block_size};

  channeler::messages msgs{reg, b.data(), b.size()};

  std::size_t count = 0;
  auto iter = msgs.begin();
  std::vector<channeler::message_type> types;
  for (; iter != msgs.end() ; ++iter, ++count) {
    types.push_back((*iter)->type);
    // std::cout << "asdf type: " << (*iter)->type << std::endl;
    // std::cout << "  remaining: " << iter.remaining() << std::endl;
  }
  ASSERT_EQ(count, 3);
  ASSERT_EQ(iter.remaining(), 4);
  ASSERT_EQ(channeler::MSG_DATA, types[0]);
  ASSERT_EQ(channeler::MSG_CHANNEL_NEW, types[1]);
  ASSERT_EQ(channeler::MSG_CHANNEL_COOKIE, types[2]);

  count = 0;
  types.clear();
  for (auto x : msgs) {
    ++count;
    types.push_back(x->type);
    // std::cout << "xtype: " << x->type << std::endl;
  }
  ASSERT_EQ(count, 3);
  ASSERT_EQ(iter.remaining(), 4);
  ASSERT_EQ(channeler::MSG_DATA, types[0]);
  ASSERT_EQ(channeler::MSG_CHANNEL_NEW, types[1]);
  ASSERT_EQ(channeler::MSG_CHANNEL_COOKIE, types[2]);
}
