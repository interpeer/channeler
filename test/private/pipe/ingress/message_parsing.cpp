/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/pipe/ingress/message_parsing.h"
#include "../lib/channel_data.h"
#include "../lib/params.h"

#include <gtest/gtest.h>

#include "../../../packets.h"
#include "../../../clock.h"

using namespace test;

namespace {

// For testing
std::size_t PACKET_SIZE = packet_with_messages_size;

using test_clock_params = test::clock_params;
using test_defaults = channeler::defaults<
  typename test_clock_params::duration,
  3 // POOL_BLOCK_SIZE
>;
using address_t = uint16_t;

using test_params = ::channeler::params<
  test_clock_params,
  test_defaults,
  channeler::null_lock_policy,
  address_t
>;


struct next
{
  using input_event = channeler::pipe::message_event<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  inline channeler::pipe::action_list_type consume(std::unique_ptr<channeler::pipe::event> event)
  {
    m_events.push_back(std::move(event));
    return {};
  }

  using list_t = std::list<
    std::unique_ptr<channeler::pipe::event>
  >;
  list_t m_events;
};



using simple_filter_t = channeler::pipe::message_parsing_filter<
  test_params::defaults::POOL_BLOCK_SIZE,
  next,
  next::input_event,
  typename test_params::channel
>;


} // anonymous namespace


TEST(PipeIngressMessageParsingFilter, throw_on_invalid_event)
{
  using namespace channeler::pipe;

  next n;
  simple_filter_t filter{channeler::message_registry::create(), &n};

  // Create a default event; this should not be handled.
  auto ev = std::make_unique<event>();
  ASSERT_THROW(filter.consume(std::move(ev)), ::channeler::exception);

  // The filter should also throw on a null event
  ASSERT_THROW(filter.consume(nullptr), ::channeler::exception);
}



TEST(PipeIngressMessageParsingFilter, produce_message_events)
{
  // This test also tests that the slot reference counting mechanism works as
  // intended.

  using namespace channeler::pipe;

  typename test_params::pool pool{PACKET_SIZE};

  // Copy packet data before parsing header
  auto data = pool.allocate();
  ASSERT_EQ(1, data.use_count());

  ::memcpy(data.data(), packet_with_messages, packet_with_messages_size);
  channeler::packet_wrapper packet{data.data(), data.size()};

  next n;
  simple_filter_t filter{channeler::message_registry::create(), &n};

  // No data added to event.
  auto ev = std::make_unique<simple_filter_t::input_event>(packet, data,
      typename test_params::channel_set::channel_ptr{});
  ASSERT_EQ(2, data.use_count());
  ASSERT_NO_THROW(filter.consume(std::move(ev)));

  // We expect the event to be passed on verbatim, so we'll test what there
  // is in the output event. The data slot has a use count of 4 - one for the
  // data variable in this scope, and one each for every message event.
  ASSERT_EQ(3, n.m_events.size());
  ASSERT_EQ(4, data.use_count());

  for (auto & rev : n.m_events) {
    ASSERT_EQ(channeler::pipe::ET_MESSAGE, rev->type);
    next::input_event * re = reinterpret_cast<next::input_event *>(rev.get());
    ASSERT_NE(channeler::MSG_UNKNOWN, re->message->type);
  }
}



TEST(PipeIngressMessageParsingFilter, empty_packet)
{
  // This test also tests that the slot reference counting mechanism works as
  // intended.

  using namespace channeler::pipe;

  typename test_params::pool pool{PACKET_SIZE};

  // Copy packet data before parsing header
  auto data = pool.allocate();
  ASSERT_EQ(1, data.use_count());

  ::memcpy(data.data(), packet_regular_channelid, packet_regular_channelid_size);
  channeler::packet_wrapper packet{data.data(), data.size()};

  next n;
  simple_filter_t filter{channeler::message_registry::create(), &n};

  // No data added to event. Before "consume", the data should have a use count
  // of 2, because the slot is copied to the event. Afterwards, it should be 1
  // because the event has been consumed (move)
  ASSERT_EQ(1, data.use_count());
  auto ev = std::make_unique<simple_filter_t::input_event>(packet, data,
      typename test_params::channel_set::channel_ptr{});
  ASSERT_TRUE(ev);
  ASSERT_EQ(2, data.use_count());
  ASSERT_NO_THROW(filter.consume(std::move(ev)));
  ASSERT_FALSE(ev);
  ASSERT_EQ(1, data.use_count());

  // There should not be any events produced from this.
  ASSERT_EQ(0, n.m_events.size());
}
