/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_SUPPORT_UNSIGNED_WINDOW_H
#define CHANNELER_SUPPORT_UNSIGNED_WINDOW_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <type_traits>
#include <limits>
#include <functional>

#include <liberate/logging.h>

namespace channeler::support {

/**
 * The window classes factor out a problem with comparison of larger or smaller
 * unsigned integers (within a window of accptance) and dealing with rolling
 * over values. That is, the following is vulnerable to rolling over:
 *
 * if (lower_bound <= value && value < upper_bound)
 *
 * Instead, consider (upper_bound - lower_bound) to be the size of an
 * acceptable window of values. A query of the following type can factor
 * rolling over values in, without sacrificing readbility.
 *
 * if (window.within(lower_bound, size, value)
 *
 * Some details: the window can sit comfortably within the value range
 * of the unsigned integer, or straddle the boundary (above). Let's assume
 * an integer with a value range of 0-9. The window is defined by a lower
 * bound, which may shift, and a size, which is typically fixed. Let's assume
 * the window size is fixed to 3 right now.
 *
 *           v-- lower bound
 * +---+---+---+---+---+---+---+---+---+---+
 * | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
 * +---+---+---+---+---+---+---+---+---+---+
 *                       ^-- one past window size
 *
 * The window above could is defined by the tuple of 2 and 3, the first being
 * the lower bound, the second hte window size. This can be mapped to an offset
 * range [2, 5) easily enough. But at the roll-over boundery of the integer,
 * things are more difficult:
 *
 *                                   v-- lower bound
 * +---+---+---+---+---+---+---+---+---+---+
 * | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
 * +---+---+---+---+---+---+---+---+---+---+
 *       ^-- one past window size
 *
 * A window defined by 8, 3 can make sense; the range [8, 1) less so for most
 * purposes. In other words, given that range, it is not as easy to determine
 * whether a given value lies within or without.
 *
 * Actually, that is the lesser problem. A bigger issue is to determine whether
 * a given value lies *before* or *after* the window. The range [1, 8) above is
 * clearly outside of the window, but where does "after" end and "before"
 * begin?
 *
 * We need a marker value for this, and it should be the halfway point in this
 * invalid range. We define this value to be "out of bounds" (oob).
 *
 *             oob --v               v-- lower bound
 * +---+---+---+---+---+---+---+---+---+---+
 * | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
 * +---+---+---+---+---+---+---+---+---+---+
 *       ^-- one past window size
 *
 * How about the first situation, where the window is comfortably in the value
 * range of the integer?
 *
 *           v-- lower bound         v-- oob
 * +---+---+---+---+---+---+---+---+---+---+
 * | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
 * +---+---+---+---+---+---+---+---+---+---+
 *                       ^-- one past window size
 *
 * Note: it's possible that the "outside" range does not divide so neatly; then
 * the "before" and "after" ranges will just have different sizes (by one).
 *
 * Finally, it's always useful to have an "invalid value", i.e. one that is
 * neither in, nor before, nor after the window, nor a oob marker, but
 * simply has no sense - like a nullptr. Let's always use 0 for that.
 *
 *           v-- lower bound     v-- oob (could be 8)
 * +---+---+---+---+---+---+---+---+---+---+
 * | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
 * +---+---+---+---+---+---+---+---+---+---+
 *   ^-- invalid         ^-- one past window size
 *
 * The window class then provides two pieces of functionality (and some extra
 * help):
 * a) determine whether a given value is within, before or after the window,
 *    is invalid or out-of-bounds.
 * b) determine the less-than relation between two values.
 *
 * The fundamental way for dealing with this is to shift all values such that
 * the window no longer overlaps the value range boundary, and the simplest
 * way to achieve that is to align the window on position 1 (as 0 is invalid).
 * We align the window on position 1 by substracting the lower bound from
 * all values and adding 1, letting the CPU's unsigned rollover do the rest.
 * (This is why this approach does not work well for signed integers, because
 * signed rollover is a little less uniform).
 *
 * - invalid stays invalid (0)
 * - shifted_lower_bound = lower_bound - lower_bound + 1 = 1
 * - shifted_end_of_window = lower_bound + window_size - lower_bound + 1
 *      = window_size + 1
 * - shifted_oob = oob- lower_bound + 1
 * - shifted_value = value - lower_bound + 1
 *
 *       v-- lower bound     v-- oob
 * +---+---+---+---+---+---+---+---+---+---+
 * | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
 * +---+---+---+---+---+---+---+---+---+---+
 *   ^-- invalid     ^-- one past window size
 *
 * As a result, we can work with ranges.
 *
 * - if the value is within [1, 4) it's in the window
 * - if the value is within [4, 6) it's after the window
 * - if the value is 6, it's on the oob marker
 * - if the value is within [7, 9] it's before the window
 * - if the value is 0 it is invalid before or after shifting
 *
 * Less-than comparison of values also becomes simple.
 * - Invalid values always compare false. So do oob values.
 * - Values in the range [1, 6) always compare as regular integers.
 *   would. Values in the range [7, 9] likewise.
 * - If one value lies in [1, 6) and the other in [7, 9], the second
 *   is always smaller than the first.
 *
 * First, some basic definitions. These effectively capture all the
 * shift calculations and logic for classifying values respective to the
 * window.
 */
enum result : uint8_t
{
  WIN_INVALID = 0,
  WIN_BEFORE = 1,
  WIN_WITHIN = 2,
  WIN_AFTER = 3,
  WIN_OUT_OF_BOUNDS = 4,
};


namespace detail {

/**
 * "Shifting" in these functions refers to aligning the lower bound to the
 * lowest valid value (1). "Shifted" values are realigned this way.
 * "Unshifting" is the reverse of "shifting".
 */

template <typename unsignedT>
inline constexpr unsignedT
window_shift(unsignedT const & lower_bound, unsignedT const & value)
{
  return static_cast<unsignedT>(value - lower_bound + 1);
}


template <typename unsignedT>
inline constexpr unsignedT
window_unshift(unsignedT const & lower_bound, unsignedT const & value)
{
  return static_cast<unsignedT>(value + lower_bound - 1);
}


template <typename unsignedT>
inline constexpr unsignedT
window_invalid_value()
{
  return unsignedT{0};
}


template <typename unsignedT>
inline constexpr unsignedT
window_increment(unsignedT const & value)
{
  auto tmp = value;
  do {
    ++tmp;
  } while (tmp == window_invalid_value<unsignedT>());
  return tmp;
}


template <typename unsignedT>
inline constexpr unsignedT
window_decrement(unsignedT const & value)
{
  auto tmp = value;
  do {
    --tmp;
  } while (tmp == window_invalid_value<unsignedT>());
  return tmp;
}


template <typename unsignedT>
inline constexpr unsignedT
window_shifted_start()
{
  return unsignedT{1};
}


template <typename unsignedT>
inline constexpr unsignedT
window_shifted_end(unsignedT const & window_size)
{
  return static_cast<unsignedT>(window_size + 1);
}


template <typename unsignedT>
inline constexpr unsignedT
window_shifted_out_of_bounds(unsignedT const & window_size)
{
  auto end = window_shifted_end(window_size);
  return end + ((std::numeric_limits<unsignedT>::max() - end) / 2);
}


template <typename unsignedT>
inline constexpr result
window_evaluate(unsignedT const & lower_bound, unsignedT const & window_size,
    unsignedT const & value)
{
  // Invalid values are always invalid.
  if (value == window_invalid_value<unsignedT>()) {
    return WIN_INVALID;
  }

  // Shift the value for further checks.
  auto shifted = window_shift(lower_bound, value);

  // Check if oob
  auto oob = window_shifted_out_of_bounds<unsignedT>(window_size);
  if (shifted == oob) {
    return WIN_OUT_OF_BOUNDS;
  }

  // Between start and end is within the window.
  auto start = window_shifted_start<unsignedT>();
  auto end = window_shifted_end<unsignedT>(window_size);
  if (shifted >= start && shifted < end) {
    return WIN_WITHIN;
  }

  // Between end and oob is after the window.
  if (shifted >= end && shifted < oob) {
    return WIN_AFTER;
  }

  // Between oob and the end of the value range is before the window. All
  // values are within the value range, and the above checks ensure that
  // all values are between [0, oob), so we don't have to check anything
  // any longer here.
  return WIN_BEFORE;
}


template <typename unsignedT>
inline constexpr bool
window_less(unsignedT const & lower_bound, unsignedT const & window_size,
    unsignedT const & lhs, unsignedT const & rhs)
{
  // if either is invalid, return false.
  if (rhs == window_invalid_value<unsignedT>()
      || lhs == window_invalid_value<unsignedT>())
  {
    return false;
  }

  // Shift both
  auto lhs_shifted = window_shift<unsignedT>(lower_bound, lhs);
  auto rhs_shifted = window_shift<unsignedT>(lower_bound, rhs);

  // If either is oob, return false.
  auto oob = window_shifted_out_of_bounds<unsignedT>(window_size);
  if (lhs_shifted == oob || rhs_shifted == oob) {
    return false;
  }

  // Now if both values are less than or bigger than oob, we can just
  // compare them.
  if ((lhs_shifted < oob && rhs_shifted < oob)
      || (lhs_shifted > oob && rhs_shifted > oob))
  {
    return (lhs_shifted < rhs_shifted);
  }

  // Here, one of the values is in the "before" range, the other within
  // or after. We need to know which is "before".
  if (lhs_shifted > oob) {
    // lhs is "before", so it's smaller.
    return true;
  }
  // rhs is "before", so lhs is bigger.
  return false;
}

} // namespace detail


/**
 * This class wraps the detail functionality, and makes the window size static.
 */
template <
  typename unsignedT,
  // The default window size is half the maximum range, because this
  // allows for one bit to be used for other purposes. This is maybe
  // a bit arbitrary, but that's why you can provide your own window
  // size.
  unsignedT WINDOW_SIZE = std::numeric_limits<unsignedT>::max() / 2
>
struct static_unsigned_window
{
  using unsigned_type = unsignedT;

  // Enable only for unsigned integral types
  static_assert(std::is_unsigned<unsignedT>::value,
      "unsignedT is not an unsigned integral type.");

  // Ensure that the WINDOW_SIZE is smaller than the maximum value
  // range. In fact, ensure that there is a "gap" of at least one
  // entirely invalid value given the window size. See below for how
  // this is used.
  static_assert(WINDOW_SIZE < std::numeric_limits<unsignedT>::max(),
      "WINDOW_SIZE must allow for one invalid value, so cannot be max().");


  // Basic functionality for the wrapped type's values and roll-over

  inline constexpr unsignedT
  invalid_value() const
  {
    return detail::window_invalid_value<unsignedT>();
  }

  inline constexpr unsignedT
  increment(unsignedT const & value) const
  {
    return detail::window_increment<unsignedT>(value);
  }

  inline constexpr unsignedT
  decrement(unsignedT const & value) const
  {
    return detail::window_decrement<unsignedT>(value);
  }


  // Window information

  inline constexpr unsignedT
  window_size() const
  {
    return WINDOW_SIZE;
  }

  inline constexpr unsignedT
  out_of_bounds(unsignedT const & lower_bound) const
  {
    return detail::window_unshift(lower_bound,
        detail::window_shifted_out_of_bounds(window_size()));
  }

  // Window functionality

  inline constexpr result
  evaluate(unsignedT const & lower_bound, unsignedT const & value) const
  {
    return detail::window_evaluate(lower_bound, WINDOW_SIZE, value);
  }

  inline constexpr bool within(unsignedT const & lower_bound,
      unsignedT const & value) const
  {
    return (WIN_WITHIN == evaluate(lower_bound, value));
  }

  inline constexpr bool before(unsignedT const & lower_bound,
      unsignedT const & value) const
  {
    return (WIN_BEFORE == evaluate(lower_bound, value));
  }

  inline constexpr bool after(unsignedT const & lower_bound,
      unsignedT const & value) const
  {
    return (WIN_AFTER == evaluate(lower_bound, value));
  }
};



/**
 * Dynamic version of the above. The only difference is that the window size is
 * not a compile-time, but a run-time parameter that can be adjusted.
 */
template <
  typename unsignedT
>
struct dynamic_unsigned_window
{
  using unsigned_type = unsignedT;

  // Enable only for unsigned integral types
  static_assert(std::is_unsigned<unsignedT>::value,
      "unsignedT is not an unsigned integral type.");

  /**
   * The default window size is half the maximum range, because this
   * allows for one bit to be used for other purposes. This is maybe
   * a bit arbitrary, but that's why you can provide your own window
   * size.
   */
  inline explicit dynamic_unsigned_window(std::size_t window_size
      = std::numeric_limits<unsignedT>::max() / 2)
  {
    set_window(window_size);
  }


  // Basic functionality for the wrapped type's values and roll-over

  inline constexpr unsignedT
  invalid_value() const
  {
    return detail::window_invalid_value<unsignedT>();
  }

  inline constexpr unsignedT
  increment(unsignedT const & value) const
  {
    return detail::window_increment<unsignedT>(value);
  }

  inline constexpr unsignedT
  decrement(unsignedT const & value) const
  {
    return detail::window_decrement<unsignedT>(value);
  }


  // Window information

  inline constexpr unsignedT
  window_size() const
  {
    return m_window_size;
  }

  inline constexpr unsignedT
  out_of_bounds(unsignedT const & lower_bound) const
  {
    unsignedT ret = detail::window_unshift(lower_bound,
        detail::window_shifted_out_of_bounds(window_size()));
    return ret;
  }

  /**
   * Set the window size.
   */
  inline bool set_window(std::size_t window_size)
  {
    // Ensure that the window_size is smaller than the maximum value
    // range. In fact, ensure that there is a "gap" of at least one
    // entirely invalid value given the window size. See below for how
    // this is used.
    if (window_size >= std::numeric_limits<unsignedT>::max()) {
      return false;
    }
    m_window_size = static_cast<unsignedT>(window_size);
    return true;
  }


  // Window functionality

  inline constexpr result
  evaluate(unsignedT const & lower_bound, unsignedT const & value) const
  {
    return detail::window_evaluate(lower_bound, m_window_size, value);
  }

  inline constexpr bool within(unsignedT const & lower_bound,
      unsignedT const & value) const
  {
    return (WIN_WITHIN == evaluate(lower_bound, value));
  }

  inline constexpr bool before(unsignedT const & lower_bound,
      unsignedT const & value) const
  {
    return (WIN_BEFORE == evaluate(lower_bound, value));
  }

  inline constexpr bool after(unsignedT const & lower_bound,
      unsignedT const & value) const
  {
    return (WIN_AFTER == evaluate(lower_bound, value));
  }

private:
  unsignedT m_window_size;
};




/**
 * STL-ish comparison operators.
 *
 * See description above for the inner workings. This works on the parameters
 * supplied by a window, static or dynamic.
 *
 * Note: because the out-of-bounds marker shifts with the lower bound and/or
 *       window size, two invocations of window_less() only make sense as long
 *       as these parameters are the same for both invocations.
 */
template <
  typename unsignedT,
  typename windowT
>
struct window_less
{
  static_assert(
      std::is_same<unsignedT, typename windowT::unsigned_type>::value,
      "Need the unsignedT to be identical to the window's unsigned_type."
      );

  using value_type = unsignedT;
  using window = windowT;
  using lower_bound_func = std::function<unsignedT ()>;

  inline explicit window_less(window const & win, lower_bound_func func)
    : m_window{win}
    , m_lower_bound{func}
  {
  }

  bool operator()(unsignedT const & lhs, unsignedT const & rhs) const
  {
    return detail::window_less(m_lower_bound(), m_window.window_size(),
        lhs, rhs);
  }

private:
  window const &    m_window;
  lower_bound_func  m_lower_bound;
};


} // namespace channeler::support

#endif // guard
