/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_MESSAGE_CHANNELS_H
#define CHANNELER_MESSAGE_CHANNELS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <channeler/message.h>
#include <channeler/message/basic.h>

#include <channeler/channelid.h>
#include <channeler/capabilities.h>
#include <channeler/cookie.h>

namespace channeler {


/**
 * Register messages for the channel FSMs
 */
CHANNELER_API
error_t
register_channel_types(std::shared_ptr<message_registry> registry);


/**
 * Create a new channel. The half channel identifier is the intiator's part,
 * the cookie is sent to identify a matching response.
 */
struct CHANNELER_API message_channel_new
  : public message
{
  channelid::half_type  initiator_part = DEFAULT_CHANNELID.initiator;
  cookie                cookie1 = {};

  inline message_channel_new(
      std::shared_ptr<message_registry> _registry,
      channelid::half_type const & initiator,
      cookie const & _cookie1)
    : message{_registry, MSG_CHANNEL_NEW}
    , initiator_part{initiator}
    , cookie1{_cookie1}
  {
  }

  inline message_channel_new(message && msg)
    : message{std::forward<message &&>(msg)}
  {
  }

  virtual ~message_channel_new() = default;

  virtual std::size_t serialize(byte * output, std::size_t output_max) const;
};


/**
 * Message for the responder to acknowledge a channel request. The channel
 * identifier is full, cookie1 is returned, and cookie2 is sent for the
 * following finalization message.
 */
struct CHANNELER_API message_channel_acknowledge
  : public message
{
  channelid       id = DEFAULT_CHANNELID;
  cookie          cookie1 = {};
  cookie          cookie2 = {};

  inline message_channel_acknowledge(
      std::shared_ptr<message_registry> _registry,
      channelid const & _id,
      cookie const & _cookie1,
      cookie const & _cookie2)
    : message{_registry, MSG_CHANNEL_ACKNOWLEDGE}
    , id{_id}
    , cookie1{_cookie1}
    , cookie2{_cookie2}
  {
  }

  inline message_channel_acknowledge(message && msg)
    : message{std::forward<message &&>(msg)}
  {
  }

  virtual ~message_channel_acknowledge() = default;

  virtual std::size_t serialize(byte * output, std::size_t output_max) const;
};


/**
 * Message sent by the initiator to finalize a channel. Send the channel
 * identifier, the cookie returned by the responder, and channel capabilities.
 */
struct CHANNELER_API message_channel_finalize
  : public message
{
  channelid       id = DEFAULT_CHANNELID;
  cookie          cookie2 = {};
  capabilities_t  capabilities = {};

  inline message_channel_finalize(
      std::shared_ptr<message_registry> _registry,
      channelid const & _id,
      cookie const & _cookie2,
      capabilities_t const & _capabilities)
    : message{_registry, MSG_CHANNEL_FINALIZE}
    , id{_id}
    , cookie2{_cookie2}
    , capabilities{_capabilities}
  {
  }

  inline message_channel_finalize(message && msg)
    : message{std::forward<message &&>(msg)}
  {
  }

  virtual ~message_channel_finalize() = default;

  virtual std::size_t serialize(byte * output, std::size_t output_max) const;
};


/**
 * An alternative to message_channel_acknowledge and/or message_channel_finalize
 * for when either or both sides already have pending payloads.
 */
struct CHANNELER_API message_channel_cookie
  : public message
{
  cookie          either_cookie = {};
  capabilities_t  capabilities = {};

  inline message_channel_cookie(
      std::shared_ptr<message_registry> _registry,
      cookie const & _either_cookie,
      capabilities_t const & _capabilities)
    : message{_registry, MSG_CHANNEL_COOKIE}
    , either_cookie{_either_cookie}
    , capabilities{_capabilities}
  {
  }


  inline message_channel_cookie(message && msg)
    : message{std::forward<message &&>(msg)}
  {
  }

  virtual ~message_channel_cookie() = default;

  virtual std::size_t serialize(byte * output, std::size_t output_max) const;
};


} // namespace channeler

#endif // guard
