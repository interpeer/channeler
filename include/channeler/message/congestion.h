/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_MESSAGE_CONGESTION_H
#define CHANNELER_MESSAGE_CONGESTION_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <channeler/message.h>
#include <channeler/message/basic.h>

namespace channeler {


/**
 * Register messages for the channel FSMs
 */
CHANNELER_API
error_t
register_congestion_types(std::shared_ptr<message_registry> registry);


/**
 * Report own receive window size, in numbers of packets.
 */
struct CHANNELER_API message_congestion_receive_window
  : public message
{
  std::size_t window_size;

  inline message_congestion_receive_window(
      std::shared_ptr<message_registry> _registry,
      std::size_t _window_size)
    : message{_registry, MSG_CONGESTION_RECEIVE_WINDOW}
    , window_size{_window_size}
  {
  }

  inline message_congestion_receive_window(message && msg)
    : message{std::forward<message &&>(msg)}
  {
  }

  virtual ~message_congestion_receive_window() = default;

  virtual std::size_t serialize(byte * output, std::size_t output_max) const;
};


} // namespace channeler

#endif // guard
