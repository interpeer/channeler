/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include <build-config.h>

#include <channeler/timeouts.h>

namespace channeler {

timeout_config::timeout_config()
  : channel_new_timeout{
    1000 * uint64_t{CHANNELER_DEFAULT_CHANNEL_CREATION_TIMEOUT_NSEC}
  }
  , channel_new_max_retries{
    CHANNELER_DEFAULT_CHANNEL_CREATION_RETRIES
  }
  , channel_timeout{
    1000 * uint64_t{CHANNELER_DEFAULT_CHANNEL_TIMEOUT_NSEC}
  }
  , channel_progress_timeout{
    1000 * uint64_t{CHANNELER_DEFAULT_CHANNEL_PROGRESS_TIMEOUT_NSEC}
  }
  , channel_congestion_update_timeout{
    1000 * uint64_t{CHANNELER_DEFAULT_CHANNEL_CONGESTION_UPDATE_TIMEOUT_NSEC}
  }
{
}

} // namespace channeler
