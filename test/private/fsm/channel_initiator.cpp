/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/fsm/channel_initiator.h"
#include "../lib/channel_data.h"
#include "../lib/params.h"

#include <gtest/gtest.h>

#include "../../messages.h"
#include "../../packets.h"
#include "../../clock.h"

namespace {

constexpr static std::size_t TEST_PACKET_SIZE = 120;

using test_clock_params = test::clock_params;
using test_defaults = channeler::defaults<
  typename test_clock_params::duration,
  3 // POOL_BLOCK_SIZE
>;
using address_t = uint16_t;

using test_params = ::channeler::params<
  test_clock_params,
  test_defaults,
  channeler::null_lock_policy,
  address_t
>;


} // anonymous namespace


TEST(FSMChannelInitiator, process_bad_event)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_channel_initiator<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs,
    []() { return fsm_t::secret_type{}; }
  };

  // If we feed the FSM anything other than a ET_MESSAGE event, it will return
  // false.
  action_list_type actions;
  event_list_type events;
  event ev;
  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_NE(fsm_base::PS_SUCCESS, ret);
}


TEST(FSMChannelInitiator, process_bad_message)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_channel_initiator<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;
  using event_t = message_event<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  typename test_params::pool pool{TEST_PACKET_SIZE};

  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs,
    []() { return fsm_t::secret_type{}; }
  };

  // If we feed the FSM anything other than a ET_MESSAGE event, it will return
  // false.
  action_list_type actions;
  event_list_type events;

  auto reg = channeler::message_registry::create();
  auto [err, msg] = reg->parse(test::message_data, test::message_data_size, false);
  ASSERT_EQ(channeler::ERR_SUCCESS, err);

  event_t ev{{nullptr, 0, false}, pool.allocate(), {},
    std::move(msg)
  };
  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_NE(fsm_base::PS_SUCCESS, ret);
}


TEST(FSMChannelInitiator, initiate_new_channel)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_channel_initiator<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs,
    []() { return fsm_t::secret_type{}; }
  };

  // Ok, let's create a new message event.
  peerid sender;
  peerid recipient;
  fsm_t::new_channel_event_type ev{sender, recipient};

  action_list_type actions;
  event_list_type events;

  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);

  // We expect that pretty much any new channel event will be processed
  // positively. This should produce an out event with a MSG_CHANNEL_NEW
  // entry.
  ASSERT_EQ(fsm_base::PS_SUCCESS, ret);

  ASSERT_EQ(1, events.size());
  auto & rev = *events.begin();
  ASSERT_EQ(rev->type, ET_MESSAGE_OUT);
  auto rev_conv = reinterpret_cast<message_out_event *>(rev.get());
  ASSERT_TRUE(rev_conv->message);
  ASSERT_EQ(rev_conv->channel, DEFAULT_CHANNELID);
  ASSERT_EQ(rev_conv->message->type, MSG_CHANNEL_NEW);
  auto msg = reinterpret_cast<message_channel_new *>(rev_conv->message.get());
  ASSERT_NE(msg->initiator_part, DEFAULT_CHANNELID.initiator);

  // Also, our channel set must have an approriate pending channel.
  auto res = chs.get(msg->initiator_part);
  ASSERT_TRUE(res);
}


TEST(FSMChannelInitiator, timeout_pending_channel)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_channel_initiator<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs,
    []() { return fsm_t::secret_type{}; }
  };

  // Ok, let's create a new message event.
  peerid sender;
  peerid recipient;
  auto initiator = chs.new_pending_channel();

  action_list_type actions;
  event_list_type events;

  // With this processed, we'll want to ensure that the channel set
  // loses the channel if the FSM processes a timeout for the channel
  ASSERT_TRUE(chs.has_channel(initiator));
  fsm_t::timeout_event_type to_ev{{CHANNEL_NEW_TIMEOUT_TAG, initiator}};

  auto [perr, ret] = fsm.process(to_ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_EQ(fsm_base::PS_SUCCESS, ret);

  ASSERT_FALSE(chs.has_channel(initiator));
}


TEST(FSMChannelInitiator, acknowledge_channel)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_channel_initiator<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;
  using event_t = message_event<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  auto msg_reg = channeler::message_registry::create();

  fsm_t fsm{
    msg_reg,
    t, to_config,
    chs,
    []() { return fsm_t::secret_type{}; }
  };

  // We don't want to create a new channel via the FSM. Just inject some
  // channel data into the channel set. We want a pending channel, so with
  // some initiator bits.
  peerid sender;
  peerid recipient;
  auto initiator = chs.new_pending_channel();

  action_list_type actions;
  event_list_type events;

  // With this processed, we want to acknowledge the channel. The acknowledgement
  // must have the same cookie as in the response message.
  fsm_t::secret_type secret;
  auto cookie = create_cookie_initiator(secret.data(), secret.size(),
      sender, recipient,
      initiator);

  // Need a packet buffer, even if the contents are not used
  std::vector<channeler::byte> data{test::packet_with_messages,
    test::packet_with_messages + test::packet_with_messages_size};
  channeler::packet_wrapper pkt{data.data(), data.size()};
  pkt.sender() = recipient;
  pkt.recipient() = sender;

  // Similarly, need a pool
  typename test_params::pool pool{TEST_PACKET_SIZE};
  auto ack = std::make_unique<message_channel_acknowledge>(
      msg_reg,
      channelid{initiator, 42},
      cookie,
      0xacab // cookie2
      );
  event_t ack_ev{pkt, pool.allocate(), {},
    std::move(ack)
  };

  // Process this.
  ASSERT_TRUE(chs.has_pending_channel(initiator));
  ASSERT_FALSE(chs.has_established_channel(initiator));

  actions.clear();
  events.clear();

  auto [perr, ret] = fsm.process(ack_ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_EQ(fsm_base::PS_SUCCESS, ret);

  ASSERT_EQ(1, actions.size());
  ASSERT_EQ(1, events.size());

  ASSERT_FALSE(chs.has_pending_channel(initiator));
  ASSERT_TRUE(chs.has_established_channel(initiator));

  // The returned action should notify us of this channel.
  auto & act = *actions.begin();
  ASSERT_EQ(act->type, AT_NOTIFY_CHANNEL_ESTABLISHED);
  auto actconv = reinterpret_cast<notify_channel_established_action *>(act.get());
  ASSERT_EQ(actconv->channel.initiator, initiator);

  // We should also ensure that the out event is a message event, and contains
  // a message_channel_finalize with the appropriate cookie2
  auto & rev = *events.begin();
  ASSERT_EQ(rev->type, ET_MESSAGE_OUT);
  auto rev_conv = reinterpret_cast<message_out_event *>(rev.get());
  ASSERT_TRUE(rev_conv->message);
  ASSERT_EQ(rev_conv->channel, DEFAULT_CHANNELID);
  ASSERT_EQ(rev_conv->message->type, MSG_CHANNEL_FINALIZE);
  auto msg = reinterpret_cast<message_channel_finalize *>(rev_conv->message.get());
  ASSERT_EQ(msg->id, (channelid{initiator, 42}));
  ASSERT_EQ(msg->cookie2, 0xacab);
}


TEST(FSMChannelInitiator, timeout_established_channel)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_channel_initiator<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs,
    []() { return fsm_t::secret_type{}; }
  };

  // Ok, let's create a new message event.
  peerid sender;
  peerid recipient;
  auto initiator = chs.new_pending_channel();
  auto err = chs.make_full({initiator, 42});
  ASSERT_EQ(ERR_SUCCESS, err);

  action_list_type actions;
  event_list_type events;

  // With this processed, we'll want to ensure that the channel set
  // loses the channel if the FSM processes a timeout for the channel
  ASSERT_TRUE(chs.has_channel({initiator, 42}));
  fsm_t::timeout_event_type to_ev{{CHANNEL_TIMEOUT_TAG, initiator}};

  auto [perr, ret] = fsm.process(to_ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_EQ(fsm_base::PS_SUCCESS, ret);

  ASSERT_FALSE(chs.has_channel({initiator, 42}));
}

// TODO we can and should cover more branches of the FSM
