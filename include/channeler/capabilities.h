/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_CAPABILITIES_H
#define CHANNELER_CAPABILITIES_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <bitset>

namespace channeler {

/**
 * Capabilities representation
 */
using capability_bits_t = uint16_t;
constexpr size_t CAPABILITY_COUNT = sizeof(capability_bits_t) * 8;
using capabilities_t = std::bitset<CAPABILITY_COUNT>;

/**
 * Capabilities
 *
 * This enum provides names for indices into the capability bitset.
 *
 * Note that capability indices in bitsets are LSB to MSB.
 */
enum CHANNELER_API capability_index : std::size_t
{
  // Resend lost packets.
  CAP_RESEND = 0,

  // Strict packet ordering
  CAP_ORDERED = 1,

  // Close-on-loss behaviour. Note that "loss" is defined as the final state
  // when all resend attempts have failed.
  CAP_CLOSE_ON_LOSS = 2,
};


/**
 * Some default capabilities.
 */
inline capabilities_t
capabilities_stream()
{
  capabilities_t res;
  res[CAP_RESEND] = true;
  res[CAP_ORDERED] = true;
  res[CAP_CLOSE_ON_LOSS] = true;
  return res;
}

inline capabilities_t
capabilities_datagram()
{
  capabilities_t res;
  res[CAP_RESEND] = false;
  res[CAP_ORDERED] = false;
  res[CAP_CLOSE_ON_LOSS] = false;
  return res;
}


} // namespace channeler


#endif // guard
