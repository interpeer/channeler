/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_PIPE_EGRESS_H
#define CHANNELER_PIPE_EGRESS_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include "filter_classifier.h"

#include "egress/callback.h"
#include "egress/out_buffer.h"
#include "egress/add_checksum.h"
#include "egress/message_bundling.h"
#include "egress/enqueue_message.h"

#include "../channel_data.h"

namespace channeler::pipe {

/**
 * Default egress filter pipe.
 */
template <
  typename paramsT
>
struct default_egress
{
  using params = paramsT;

  // *** Filter type aliases
  using callback = callback_filter<
    typename params::channel,
    // XXX: The only filter to which we have to pass this - it's the extra
    //      args to consume().
    typename params::clock_params::time_point const &
  >;
  using out_buffer = out_buffer_filter<
    typename params::transport_address,
    params::defaults::POOL_BLOCK_SIZE,
    typename params::channel,
    callback, typename callback::input_event
  >;
  using add_checksum = add_checksum_filter<
    typename params::transport_address,
    params::defaults::POOL_BLOCK_SIZE,
    out_buffer, typename out_buffer::input_event
  >;
  using message_bundling = message_bundling_filter<
    typename params::transport_address,
    params::defaults::POOL_BLOCK_SIZE,
    typename params::channel,
    add_checksum, typename add_checksum::input_event
  >;
  using enqueue_message = enqueue_message_filter<
    typename params::channel,
    message_bundling, typename message_bundling::input_event
  >;

  using peerid_function = typename message_bundling::peerid_function;

  inline default_egress(
      typename callback::callback cb,
      typename params::channel_set & channels,
      typename params::pool & pool,
      peerid_function own_peerid_func,
      peerid_function peer_peerid_func
    )
    : m_callback{cb}
    , m_out_buffer{&m_callback, channels}
    , m_add_checksum{&m_out_buffer}
    , m_message_bundling{&m_add_checksum, channels, pool,
      own_peerid_func, peer_peerid_func}
    , m_enqueue_message{&m_message_bundling, channels}
  {
  }


  inline action_list_type consume(
      typename params::clock_params::time_point const & now,
      std::unique_ptr<event> ev
    )
  {
    // Swap the parameters around; filters don't need to know about the time
    // point, really, except as something to pass on.
    return m_enqueue_message.consume(std::move(ev), now);
  }

  callback          m_callback;
  out_buffer        m_out_buffer;
  add_checksum      m_add_checksum;
  message_bundling  m_message_bundling;
  enqueue_message   m_enqueue_message;
};


} // namespace channeler::pipe

#endif // guard
