/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_FSM_CHANNEL_INITIATOR_H
#define CHANNELER_FSM_CHANNEL_INITIATOR_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <functional>

#include "base.h"
#include "timeouts.h"

#include <channeler/message.h>
#include <channeler/message/channels.h>
#include <channeler/timeouts.h>

#include "../macros.h"
#include "../channels.h"
#include "../channel_data.h"
#include "../support/timeouts.h"

namespace channeler::fsm {

/**
 * Implement the channel initiator part.
 *
 * Channel initiators have actual states. The state is managed per channel,
 * and can take one of the following values:
 *
 * - Start state - no channel initiation attempt has been made yet.
 * - Pending - a MSG_CHANNEL_NEW has been sent, but no response has
 *   been received.
 * - Established - a MSG_CHANNEL_ACKNOWLEDGE or correspong MSG_CHANNEL_COOKIE
 *   has been received, and a MSG_CHANNEL_FINALIZE/_COOKIE has been sent in
 *   return.
 * - The error states are reached respectively if channel establishment timed
 *   out or aborted due to an error.
 *
 * There is some complication here in that the start state applies globally;
 * the other states apply to per-channel FSMs, which technically start in the
 * CHR_PENDING state. The state machines are correspondingly *very* simple, so
 * we'll skip even something as lightweight as microfsm here.
 *
 * In addition to processing message events, the state machine can also process
 * timeout events. In particular, the Initiating state can time out.
 *
 * The only other event being handled is the explicit initialization event that
 * is supplied via user interaction.
 */
template <
  std::size_t POOL_BLOCK_SIZE,
  typename channelT
>
struct fsm_channel_initiator
  : public fsm_base
{
  using channel_set = ::channeler::channels<channelT>;
  using secret_type = std::vector<byte>;
  using secret_generator = std::function<secret_type ()>;

  using new_channel_event_type = ::channeler::pipe::new_channel_event;
  using message_event_type = ::channeler::pipe::message_event<POOL_BLOCK_SIZE, channelT>;
  using timeout_event_type = ::channeler::pipe::timeout_event<
    ::channeler::support::timeout_scoped_tag_type
  >;


  /**
   * Need to keep a reference to a channel_set as well as the function for
   * producing the cookie secret.
   */
  inline fsm_channel_initiator(
      std::shared_ptr<::channeler::message_registry> registry,
      ::channeler::support::timeouts & timeouts,
      ::channeler::timeout_config const & to_config,
      channel_set & channels,
      secret_generator generator
    )
    : m_registry{registry}
    , m_timeouts{timeouts}
    , m_to_config{to_config}
    , m_channels{channels}
    , m_secret_generator{generator}
  {
  }



  /**
   * Process function, see above.
   */
  virtual fsm_base::process_result
  process(::channeler::pipe::event const & to_process,
      ::channeler::pipe::action_list_type & result_actions,
      ::channeler::pipe::event_list_type & output_events)
  {
    namespace pipe = channeler::pipe;

    // We handle three types of events here, two of which get handled per-channel.
    switch (to_process.type) {
      case pipe::ET_NEW_CHANNEL:
        {
          auto & new_ev = ::channeler::pipe::event_cast<new_channel_event_type>(to_process);
          return initiate_new_channel(new_ev.sender, new_ev.recipient, new_ev.capabilities,
              result_actions, output_events);
        }

      case pipe::ET_MESSAGE:
        {
          auto & msg_ev = ::channeler::pipe::event_cast<message_event_type>(to_process);
          return handle_message(msg_ev, result_actions, output_events);
        }

      case pipe::ET_TIMEOUT:
        {
          auto & to_ev = ::channeler::pipe::event_cast<timeout_event_type>(to_process);
          return handle_timeout(to_ev, result_actions, output_events);
        }

      default:
        LIBLOG_WARN("Event type not handled by channel_initiator: " << to_process.type);
        break;
    }

    // Otherwise we can't really do anything here.
    return {ERR_SUCCESS, PS_UNHANDLED};
  }


  inline fsm_base::process_result
  initiate_new_channel(
      ::channeler::peerid const & sender, // ourselves!
      ::channeler::peerid const & recipient,
      ::channeler::capabilities_t const & capabilities,
      ::channeler::pipe::action_list_type & result_actions [[maybe_unused]],
      ::channeler::pipe::event_list_type & output_events [[maybe_unused]])
  {
    // Create pending channel in the channel set. This also initialises a
    // pending FSM, if the channel data holds one (it should!)
    auto id = m_channels.new_pending_channel();

    m_pending[id] = {{sender, recipient}, capabilities};
    send_channel_new(sender, recipient, id, output_events);

    // Use timout provider to set a timeout with the context being a tuple
    // of a channel tag and the initiator part.
    m_timeouts.add({CHANNEL_NEW_TIMEOUT_TAG, id},
        m_to_config.channel_new_timeout,
        m_to_config.channel_new_max_retries);

    return {ERR_SUCCESS, PS_SUCCESS};
  }


  inline void send_channel_new(
      ::channeler::peerid const & sender, // ourselves!
      ::channeler::peerid const & recipient,
      channelid::half_type id,
      ::channeler::pipe::event_list_type & output_events [[maybe_unused]])
  {
    // Create a cookie
    auto secret = m_secret_generator();
    auto cookie1 = create_cookie_initiator(secret.data(), secret.size(),
        sender, recipient,
        id);

    // Create and return MSG_CHANNEL_NEW in output_events
    auto init = std::make_unique<message_channel_new>(
          m_registry,
          id, cookie1
        );
    auto ev = std::make_unique<channeler::pipe::message_out_event>(
          DEFAULT_CHANNELID,
          std::move(init)
        );
    output_events.push_back(std::move(ev));
  }


  inline fsm_base::process_result
  handle_message(message_event_type const & event,
      ::channeler::pipe::action_list_type & result_actions [[maybe_unused]],
      ::channeler::pipe::event_list_type & output_events [[maybe_unused]])
  {
    // Check that message type is MSG_CHANNEL_ACKNOWLEDGE or abort.
    if (event.message->type != MSG_CHANNEL_ACKNOWLEDGE) {
      LIBLOG_DEBUG("Initiator only handles MSG_CHANNEL_ACKNOWLEDGE, got: " << event.message->type);
      return {ERR_SUCCESS, PS_UNHANDLED};
    }
    auto msg = reinterpret_cast<message_channel_acknowledge const *>(event.message.get());

    LIBLOG_DEBUG("MSG_CHANNEL_ACKNOWLEDGE(channel["
        << std::hex << msg->id << "]/"
        << "cookie1[" << std::hex << msg->cookie1 << "]/"
        << "cookie2[" << std::hex << msg->cookie2 << "])"
        << std::dec);

    // Pick channel identifier from message, grab channel data for it. 
    auto channel = m_channels.get(msg->id);
    if (!channel) {
      // We cannot acknowledge a channel we know nothing about.
      LIBLOG_ERROR("Initiator knows nothing about channel: " << msg->id);
      return {ERR_SUCCESS, PS_IGNORED};
    }

    // Channel must be in pending state.
    if (!m_channels.has_pending_channel(msg->id)) {
      // This is an acknowledgement for a channel that's already established.
      // This is unlikely with a well-behaving responder, so we should consider
      // this message as one we should not process.
      // XXX We may want to consider blocking this responder, but that has usage
      //     implications.
      LIBLOG_ERROR("Initiator does not have a pending channel: " << msg->id);
      return {ERR_SUCCESS, PS_IGNORED};
    }

    // At this point, we need to verify that the cookie1 sent in the message
    // is valid. If so, we can move the channel into an established state.
    // If not, we have to abort.
    auto secret = m_secret_generator();
    auto cookie1 = create_cookie_initiator(secret.data(), secret.size(),
        event.packet.recipient(), // ourselves!
        event.packet.sender(), // responder
        msg->id.initiator);

    if (cookie1 != msg->cookie1) {
      // We cannot verify that the message is in response to one of our
      // requests. This may be because the cookie secret generator changed
      // the secret value. But that's not so easy to verify, so we'll
      // abort here.
      // TODO: since we're the initiator, we can safely store the cookie we
      //       sent in the channel data. It will take up some space, but it's
      //       not as if a well-behaving initiator will create tons of
      //       channels. Let's do that in the near future instead, then
      //       we don't have to recreate the cookie above.
      //       https://codeberg.org/interpeer/channeler/issues/12
      m_channels.remove(msg->id);
      LIBLOG_ERROR("Removed pending channel due to mismatching cookie: " << msg->id
          << " calculated: " << std::hex << cookie1 << " but got "
          << std::hex << msg->cookie1 << std::dec);
      return {ERR_SUCCESS, PS_IGNORED};
    }

    // We'll upgrade the channel to full in the channel set.
    auto capabilities = m_pending[msg->id.initiator].capabilities;
    m_pending.erase(msg->id.initiator);
    auto res = m_channels.make_full(msg->id, capabilities);
    if (res != ERR_SUCCESS) {
      // This should really not happen at this point. But if it does, we'll
      // have to remove the channel and abort. Something in our channel
      // bookkeeping went very much wrong.
      m_channels.remove(msg->id);
      LIBLOG_ET("Could not turn pending channel into full channel: " << msg->id,
          res);
      return {res, PS_ERRORED};
    }

    // We have success! The caller needs to know this, so we'll use an action
    // here.
    LIBLOG_DEBUG("Channel fully established: " << msg->id);
    result_actions.push_back(std::move(
        std::make_unique<::channeler::pipe::notify_channel_established_action>(
          msg->id,
          capabilities
        )
    ));

    // At this point, we have an acknowledgement. From our point of view,
    // the channel is established. We'll just send a MSG_CHANNEL_FINALIZE or
    // MSG_CHANNEL_COOKIE to tell this to the other side.
    // However, if either of these messages were to get lost, the responder
    // may not receive data. It's now possible for the channel itself to
    // timeout. We have to cancel the CHANNEL_NEW_TIMEOUT, but start a
    // (much longer) CHANNEL_TIEMOUT.
    m_timeouts.remove({CHANNEL_NEW_TIMEOUT_TAG, msg->id.initiator});
    m_timeouts.add({CHANNEL_TIMEOUT_TAG, msg->id.initiator},
        m_to_config.channel_timeout);

    // Construct the finalize or cookie messages, respectively.
    if (channel->egress_messages().have_pending()) {
      // TODO MSG_CHANNEL_COOKIE
      //      https://codeberg.org/interpeer/channeler/issues/13
      LIBLOG_DEBUG("Sending MSG_CHANNEL_COOKIE: " << msg->id);
    }
    else {
      // MSG_CHANNEL_FINALIZE
      LIBLOG_DEBUG("Sending MSG_CHANNEL_FINALIZE: " << msg->id);
      auto response = std::make_unique<message_channel_finalize>(
            m_registry,
            msg->id, msg->cookie2,
            capabilities
          );
      auto ev = std::make_unique<channeler::pipe::message_out_event>(
            DEFAULT_CHANNELID,
            std::move(response)
          );
      output_events.push_back(std::move(ev));
    }

    return {ERR_SUCCESS, PS_SUCCESS};
  }


  inline fsm_base::process_result
  handle_timeout(timeout_event_type const & event,
      ::channeler::pipe::action_list_type & result_actions [[maybe_unused]],
      ::channeler::pipe::event_list_type & output_events [[maybe_unused]])
  {
    // The first thing to check is the tag of the timeout.
    if (event.context.tag.scope != CHANNEL_NEW_TIMEOUT_TAG
        && event.context.tag.scope != CHANNEL_TIMEOUT_TAG)
    {
      // Not a timeout for us.
      LIBLOG_DEBUG("Ignoring timeout; we didn't ask for it.");
      return {ERR_SUCCESS, PS_IGNORED};
    }

    // The tag itself is our initiator part of a channel identifier.
    channelid::half_type id = event.context.tag.name;

    // If the timeout is still active (it's using exponential backoff), then
    // we need to retry.
    auto [active, _] = m_timeouts.is_timeout_active(event.context);
    if (active) {
      // Don't delete the channel, but send another message instead.
      auto iter = m_pending.find(id);
      if (iter == m_pending.end()) {
        LIBLOG_ERROR("Got an active timeout for creating new channels, but no "
            "pending channel info.");
        return {ERR_SUCCESS, PS_IGNORED};
      }

      auto [sender, recipient] = m_pending[id].addresses;
      send_channel_new(sender, recipient, id, output_events);

      return {ERR_SUCCESS, PS_SUCCESS};
    }

    // If we have a timeout - either timeout - with a channel id we know,
    // we'll have to remove it.
    if (m_channels.has_channel(id)) {
      m_channels.remove(id);
      m_pending.erase(id);
      LIBLOG_DEBUG("Removing channel due to timeout: " << id);
      return {ERR_SUCCESS, PS_SUCCESS};
    }

    // We didn't actually handle the timeout here.
    return {ERR_SUCCESS, PS_UNHANDLED};
  }

  virtual ~fsm_channel_initiator() = default;

private:

  // We need to keep some info around for creating channels; this is transient
  // info, though.
  struct pending_data
  {
    using address_tuple = std::tuple<peerid, peerid>;
    address_tuple   addresses;
    capabilities_t  capabilities;
  };
  using pending_map = std::map<channelid::half_type, pending_data>;
  pending_map                                     m_pending = {};

  std::shared_ptr<::channeler::message_registry>  m_registry;
  ::channeler::support::timeouts &                m_timeouts;
  ::channeler::timeout_config const &             m_to_config;
  channel_set &                                   m_channels;
  secret_generator                                m_secret_generator;
};

} // namespace channeler::fsm

#endif // guard
