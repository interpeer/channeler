/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_PIPE_ADD_CHECKSUM_H
#define CHANNELER_PIPE_ADD_CHECKSUM_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <memory>

#include "../../memory/packet_pool.h"
#include "../event.h"
#include "../action.h"
#include "../event_as.h"

#include <channeler/packet.h>
#include <channeler/error.h>


namespace channeler::pipe {

/**
 * The add_checksum filter adds the checksum to a packet.
 *
 * It's separate from the message bundling filter if only because checksum
 * addition may be part of an encryption filter later.
 */
template <
  typename addressT,
  std::size_t POOL_BLOCK_SIZE,
  typename next_filterT,
  typename next_eventT
>
struct add_checksum_filter
{
  using input_event = packet_out_event<POOL_BLOCK_SIZE>;
  using pool_type = ::channeler::memory::packet_pool<
    POOL_BLOCK_SIZE
    // FIXME lock policy
  >;
  using slot_type = typename pool_type::slot;

  inline add_checksum_filter(next_filterT * next)
    : m_next{next}
  {
  }



  template <typename... argsT>
  inline action_list_type
  consume(std::unique_ptr<event> unchecked, argsT && ...args)
  {
    action_list_type actions;
    auto in = event_as_or_passthrough<input_event, next_filterT, argsT...>(
        "egress:add_checksum", unchecked, ET_PACKET_OUT,
        actions, m_next, std::forward<argsT>(args)...);
    if (!in) {
      return actions;
    }

    // Set checksum
    auto err = in->packet.update_checksum();
    if (ERR_SUCCESS != err) {
      // Uh-oh, some kind of error.
      // TODO add an action
      return {};
    }

    return m_next->consume(std::move(in), std::forward<argsT>(args)...);
  }


  next_filterT *  m_next;
};


} // namespace channeler::pipe

#endif // guard
