/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/
#ifndef CHANNELER_POLICY_RESEND_H
#define CHANNELER_POLICY_RESEND_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#include <channeler.h>

#include <set>
#include <limits>
#include <chrono>

#include "params.h"
#include "../lock_policy.h"
#include "../support/unsigned_window.h"
#include "../support/timeouts.h"

#include <channeler/packet.h>
#include <channeler/timeouts.h>

#include <liberate/logging.h>

namespace channeler::policy {

/**
 * Possible results for registering a packet as received.
 */
enum received_result : uint8_t
{
  RR_OK           = 0, // Packet has successfully been registered as received
  RR_OUTDATED     = 1, // Packet is out of date, rejected.
  RR_OUT_OF_ORDER = 2, // Packet received out-of-order, but accepted.
  RR_DUPLICATE    = 3, // Packet is out-of-order, but also duplicated. Ignoring.
  RR_UNEXPECTED   = 4, // Packet is completely unexpected, ignoring.
  RR_INVALID      = 5, // Packet is known to be invalid, ignoring.
};


/**
 * Resend policies have a fairly simple interface; it's the implementation that
 * can be complex.
 *
 * The primary function of the policy is to decide whether to wait for a packet
 * with a particular sequence number or not.
 *
 * The secondary function is to handle what should be done in this wait time,
 * e.g. to generate recovery events or not.
 *
 * The simplest policy will not attempt recovery. It still needs to implement
 * an appropriate wait strategy. A policy that does not attempt recovery needs
 * only be concerned with a simple timeout to cover network latency issues. If
 * a packet does not arrive within this timeout, it can be considered lost.
 *
 * In practice, even such a simple policy has some addtional things to deal
 * with. Packets can arrive out-of-order. If a future packet arrives before the
 * next in sequence, waiting for this future packet will never be necessary.
 * As such, even a simple policy must have a concept of the next expected
 * packet in the sequence, and maintain state on future packets that have
 * already arrived.
 *
 * To maintain such state, the policy must therefore first and foremost offer
 * a function for reporting to it the sequence number of packets that have
 * arrived.
 *
 * Second, in order to fulfil its primary function, it must provide a query
 * function for whether waiting for a particular sequence number is still
 * desired.
 *
 * A side effect of keeping this state is that policies can also report when
 * an arriving packet is outdated or duplicate, and can therefore be discarded.
 *
 * The policies are independent of an actual clock, which means the caller has
 * to provide the current time at each call.
 */
namespace detail {

template <
  typename policy_paramsT,
  template <typename, typename> class timeoutT,
  bool _DOES_RESEND
>
struct resend_policy_base
{
  using params = policy_paramsT;
  using timeout_type = timeoutT<
    typename params::time_point,
    typename params::duration
  >;

  // If the policy reports number of collisions and missing items, these are
  // the callback/data type it uses.
  using missing_set = typename params::missing_set;
  using resend_callback = typename params::resend_callback;
  using lower_bound_callback = typename params::lower_bound_callback;

  static constexpr bool DOES_RESEND = _DOES_RESEND;

  /**
   * We need to signal that there is no such thing as an expected first
   * sequence number, so we chose 0 for this - the implication is that
   * proper sequence numbers cannot start with
   * max(typename params::sequence_no_t).
   */
  inline resend_policy_base(
      typename params::unsigned_window & window,
      timeout_type && timeout_init)
    : m_window{window}
    , m_timeout{timeout_init}
  {
  }


  /**
   * Register a packet (sequence number) as arrived. The status gives an
   * indication of how the policy considers this sequence number.
   */
  inline received_result
  packet_arrived(
      typename params::time_point now [[maybe_unused]],
      typename params::sequence_no_t lower_bound,
      typename params::sequence_no_t sequence_no)
  {
    if (0 == sequence_no) {
      LIBLOG_DEBUG("Packet " << static_cast<int64_t>(sequence_no)
          << " is an invalid sequence number, ignoring.");
      return RR_INVALID;
    }

    // If we're not expecting any sequence number, then the first to arrive
    // is accepted as-is.
    if (0 == lower_bound) {
      LIBLOG_DEBUG("We're not expecting anything in particular, so we'll just "
          "accept packet " << static_cast<int64_t>(sequence_no) << ".");
      return RR_OK;
    }

    auto evaluation = m_window.evaluate(lower_bound, sequence_no);

    // Otherwise, we have to check whether the sequence number is in
    // line with our expectations, in the past, or in the future.
    if (support::WIN_BEFORE == evaluation) {
      // Ignore it
      LIBLOG_DEBUG("Outdated packet " << static_cast<int64_t>(sequence_no)
          << ", rejecting.");
      return RR_OUTDATED;
    }

    // Is it the expected?
    if (sequence_no == lower_bound) {
      // If the packet already arrived (in the future set), it's a duplicate.
      auto iter = m_future_set.find(sequence_no);
      if (iter != m_future_set.end()) {
        // We still update the sequence_no as normal, but report a duplicate.
        LIBLOG_DEBUG("Packet " << static_cast<int64_t>(sequence_no)
            << " arrived out-of-order, but is a duplicate, so rejecting.");
        return RR_DUPLICATE;
      }

      // Otherwise report ok.
      m_future_set.insert(sequence_no);
      LIBLOG_DEBUG("Packet " << static_cast<int64_t>(sequence_no)
          << " is in sequence, accepting.");
      return RR_OK;
    }

    if (support::WIN_WITHIN == evaluation) {
      // If we reach here, then we received a future packet. Let's see if it
      // already exists.
      auto iter = m_future_set.find(sequence_no);
      if (iter != m_future_set.end()) {
        // Don't do anything; we've already got this one.
        LIBLOG_DEBUG("Packet " << static_cast<int64_t>(sequence_no)
            << " arrived out-of-order, but is a duplicate, so rejecting.");
        return RR_DUPLICATE;
      }

      // Put this in the future set.
      m_future_set.insert(sequence_no);
      LIBLOG_DEBUG("Packet " << static_cast<int64_t>(sequence_no)
          << " arrived out-of-order, accepting.");
      return RR_OUT_OF_ORDER;
    }

    // If we reach here, then the packet is outside of the expected window,
    // which means we want to ignore it.
    LIBLOG_DEBUG("Packet " << static_cast<int64_t>(sequence_no)
        << " is well outside expectations, ignoring.");
    return RR_UNEXPECTED;
  }



  /**
   * Return true if we should keep waiting for the given sequence number.
   */
  inline bool keep_waiting_for(
      typename params::time_point now [[maybe_unused]],
      typename params::sequence_no_t lower_bound,
      typename params::sequence_no_t sequence_no) const
  {
    if (0 == sequence_no) {
      LIBLOG_DEBUG("Do not wait for invalid sequence number 0.");
      return false;
    }

    // A zero sequence number means we have no expectations for the next
    // in line, so we have to return true.
    if (0 == lower_bound) {
      LIBLOG_DEBUG("We don't know what we're waiting for, so we wait for any "
          "packet.");
      return true;
    }

    // Otherwise, we only want to keep waiting if the sequence number is within
    // the receive window.
    if (m_window.within(lower_bound, sequence_no)) {
      // This is a query for a future sequence number. It may be the next in
      // line, or a far future one.

      // If the packet already arrived previously, we know we do not want to
      // wait for it. That's actually relatiely simple.
      auto iter = m_future_set.find(sequence_no);
      if (iter != m_future_set.end()) {
        LIBLOG_DEBUG("Packet " << static_cast<int64_t>(sequence_no)
            << " has already arrived out-of-order, don't keep waiting.");
        return false;
      }

      // For far future packets, we also know that we want to wait - that's
      // because the next in line may not have arrived yet.
      if (lower_bound != sequence_no) {
        LIBLOG_DEBUG("Packet " << static_cast<int64_t>(sequence_no)
            << " is not expected for a while, keep waiting.");
        return true;
      }

      // It's only the next expected where we have to consider a simple timeout.
      if (m_timeout.expired(now)) {
        using support::operator<<;
        LIBLOG_DEBUG("Packet " << static_cast<int64_t>(sequence_no)
            << " timed out (at timestamp " << now << "), don't keep waiting.");
        // The interesting thing here is that if the sequence_no is timed out,
        // the best possible case for a sensible sequence number to wait for
        // is the next in line - everything before has definitely timed out.
        return false;
      }

      LIBLOG_DEBUG("Keep waiting for packet "
          << static_cast<int64_t>(sequence_no) << ".");
      return true;
    }

    // Too old or too new, ignore it
    LIBLOG_DEBUG("Packet " << static_cast<int64_t>(sequence_no)
        << " is expired or way in the future, don't keep waiting.");
    return false;
  }


  /**
   * Return the number of future packets we've got.
   */
  inline std::size_t num_future_packets_arrived() const
  {
    return m_future_set.size();
  }


  /**
   * The function advances the internal timeout, so that future calls to
   * keep_waiting_for() may error out on expired packes. At the same time, it
   * advances the window lower bound and erases all references to packets
   * now considered "before" the window (i.e. expires old packets).
   *
   * Note that is primarily functionality for ingress buffers, but the
   * principle applies either way.
   */
  inline void update_timeouts(
      typename params::time_point now,
      typename params::sequence_no_t const & lower_bound)
  {
    // Update timeout
    m_timeout.advance(now);

    // Using the given lower_bound, expire old entries. Old entries are those
    // that evaluate to *before* the lower bound.
    LIBLOG_DEBUG("Expiring old packets...");
    for (auto iter = m_future_set.begin() ; iter != m_future_set.end() ; ) {
      if (lower_bound == *iter) {
        // Treat this as a special case just for documentation purposes.
        // If we see the next sequence number in the future set, it means
        // the packet has arrived, but we can't just expire it - if we do,
        // then the policy will think that we have to wait for this packet,
        // when in fact we don't (it already arrived out-of-order).
        ++iter;
      }
      else if (m_window.before(lower_bound, *iter)) {
        LIBLOG_DEBUG("Expire old packet " << *iter << ".");
        iter = m_future_set.erase(iter);
      }
      else {
        ++iter;
      }
    }
    LIBLOG_DEBUG("Done expiring.");
  }


  /**
   * Set the timeout's initial clock, if it wasn't passed in the ctor.
   */
  inline void set_initial_clock(typename params::time_point const & initial)
  {
    m_timeout.set_initial_clock(initial);
  }


protected:

  using window_t = ::channeler::support::dynamic_unsigned_window<
    typename params::sequence_no_t
  >;
  window_t                                  m_window;
  timeout_type mutable                      m_timeout;

  std::set<typename params::sequence_no_t>  m_future_set = {};
};

} // namespace detail


/**
 * The no_resend_policy uses a simple timeout to determine whether a packet
 * arrived in time, and doesn't request any kind of resend to occur. It simply
 * decides whether packets should be waited for or not.
 */
template <
  typename policy_paramsT
>
struct no_resend_policy :
  public detail::resend_policy_base<
    policy_paramsT,
    support::simple_timeout,
    false
  >
{
  using base_type = detail::resend_policy_base<
    policy_paramsT,
    support::simple_timeout,
    false
  >;
  using params = typename base_type::params;
  using timeout_type = typename base_type::timeout_type;

  using missing_set = typename base_type::missing_set;
  using resend_callback = typename base_type::resend_callback;
  using lower_bound_callback = typename base_type::lower_bound_callback;

  /**
   * We need to signal that there is no such thing as an expected first
   * sequence number, so we chose 0 for this - the implication is that
   * proper sequence numbers cannot start with max(typename params::sequence_no_t).
   */
  inline no_resend_policy(
      typename params::unsigned_window & window,
      typename params::time_point const & initial_clock,
      typename params::duration const & roundtrip_timeout,
      std::size_t const & = 0, // unused
      resend_callback = {}, // unused
      lower_bound_callback = {} // unused
    )
    : base_type{
        window,
        timeout_type{roundtrip_timeout, initial_clock}
      }
  {
  }
};



/**
 * In contrast to no_resend_policy above, this class does issue resend
 * requests. It uses an exponential backoff algorithm to time resend
 * requests and expire them as necessary when packets do arrive.
 *
 * Resend events query for a number of "missing" sequence numbers. If packets
 * arrived out-of-order, then the lower bound for missing packets is the next
 * exected sequence number. Meanwhile, the upper bound is the last sequence
 * number that arrived. Between these bounds, there may be multiple "missing"
 * packets to resend.
 *
 * The callback type therefore expects a number of collisions that occurred,
 * as well as a set of typename params::sequence_no_t.
 */
template <
  typename policy_paramsT
>
struct exponential_backoff_resend_policy :
  public detail::resend_policy_base<
    policy_paramsT,
    support::exponential_backoff_timeout,
    true
  >
{
  using base_type = detail::resend_policy_base<
    policy_paramsT,
    support::exponential_backoff_timeout,
    true
  >;
  using params = typename base_type::params;
  using timeout_type = typename base_type::timeout_type;

  using missing_set = typename base_type::missing_set;
  using resend_callback = typename base_type::resend_callback;
  using lower_bound_callback = typename base_type::lower_bound_callback;

  /**
   * We need to signal that there is no such thing as an expected first
   * sequence number, so we chose 0 for this - the implication is that
   * proper sequence numbers cannot start with max(typename params::sequence_no_t).
   */
  inline exponential_backoff_resend_policy(
      typename params::unsigned_window & window,
      typename params::time_point const & initial_clock,
      typename params::duration const & roundtrip_timeout,
      std::size_t const & max_collisions = 0,
      resend_callback _resend_callback = {},
      lower_bound_callback _lower_bound_callback = {}
    )
    : base_type{
        window,
        timeout_type{
          roundtrip_timeout,
          initial_clock,
          max_collisions,
          std::bind(
              &exponential_backoff_resend_policy<
                params
              >::timeout_callback,
              this,
              std::placeholders::_1,
              std::placeholders::_2
          )
        }
      }
    , m_resend_callback{_resend_callback}
    , m_lower_bound_callback{_lower_bound_callback}
  {
  }

private:
  inline void timeout_callback(typename params::time_point const & now,
      std::size_t collisions)
  {
    LIBLOG_DEBUG("Timeout callback invoked: " << collisions);
    if (!m_resend_callback || !m_lower_bound_callback) {
      LIBLOG_DEBUG("Required callbacks not definde.");
      return;
    }

    // Assemble set of missing packets.
    //
    // XXX: This callback is called when the current lower bound has not yet
    //      arrived. It's therefore part of the missing set by definition.
    //      Also part of the missing set is every packet after that which is
    //      not in the future set.
    auto start = m_lower_bound_callback();
    if (!start) {
      LIBLOG_DEBUG("Cannot determine if packets are missing.");
      return;
    }

    // We count the lower bound as missing by default (see above). However,
    // we do not need to add the entire window, only gaps. Which means the
    // latest existing entry is by definition not missing, and past the end
    // of the missing set.
    missing_set missing;
    missing.insert(start);

    if (!this->base_type::m_future_set.empty()) {
      auto end = *(this->base_type::m_future_set.rbegin());

      auto cur = start;
      while (cur != end) {
        auto iter = this->base_type::m_future_set.find(cur);
        if (iter == this->base_type::m_future_set.end()) {
          // The cur value is in the window and not in the future set, so we
          // can consider it missing.
          missing.insert(cur);
        }

        cur = this->base_type::m_window.increment(cur);
      }
    }

    m_resend_callback(now, collisions, start, missing);
  }

  resend_callback       m_resend_callback;
  lower_bound_callback  m_lower_bound_callback;
};

} // namespace channeler::policy

#endif // guard
