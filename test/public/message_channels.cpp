/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <channeler/message/channels.h>

#include <gtest/gtest.h>

#include "../messages.h"

using namespace test;

TEST(MessageChannels, parse_and_serialize_channel_new)
{
  auto reg = channeler::message_registry::create();

  std::vector<channeler::byte> b{message_channel_new, message_channel_new + message_channel_new_size};

  assert_single_byte_type_fixed_size_message(reg, b, channeler::MSG_CHANNEL_NEW);

  auto [ret, msg] = reg->parse(b.data(), b.size(), false);
  ASSERT_EQ(channeler::ERR_SUCCESS, ret);
  ASSERT_TRUE(msg);
  ASSERT_EQ(msg->type, channeler::MSG_CHANNEL_NEW);

  auto ptr = reinterpret_cast<channeler::message_channel_new *>(msg.get());
  ASSERT_EQ(0xbeef, ptr->initiator_part);
  ASSERT_EQ(0xbeefb4be, ptr->cookie1);

  // Serialize
  std::vector<channeler::byte> out;
  out.resize(200);
  assert_serialization_ok(out, msg, b);
}



TEST(MessageChannels, parse_and_serialize_channel_acknowledge)
{
  auto reg = channeler::message_registry::create();

  std::vector<channeler::byte> b{message_channel_acknowledge, message_channel_acknowledge + message_channel_acknowledge_size};

  assert_single_byte_type_fixed_size_message(reg, b, channeler::MSG_CHANNEL_ACKNOWLEDGE);

  auto [ret, msg] = reg->parse(b.data(), b.size(), false);
  ASSERT_EQ(channeler::ERR_SUCCESS, ret);
  ASSERT_TRUE(msg);
  ASSERT_EQ(msg->type, channeler::MSG_CHANNEL_ACKNOWLEDGE);

  auto ptr = reinterpret_cast<channeler::message_channel_acknowledge *>(msg.get());
  ASSERT_EQ(0xbeefd00d, ptr->id.full);
  ASSERT_EQ(0xbeefb4be, ptr->cookie1);
  ASSERT_EQ(0xdeadd00d, ptr->cookie2);

  // Serialize
  std::vector<channeler::byte> out;
  out.resize(200);
  assert_serialization_ok(out, msg, b);
}



TEST(MessageChannels, parse_and_serialize_channel_finalize)
{
  auto reg = channeler::message_registry::create();

  std::vector<channeler::byte> b{message_channel_finalize, message_channel_finalize + message_channel_finalize_size};

  assert_single_byte_type_fixed_size_message(reg, b, channeler::MSG_CHANNEL_FINALIZE);

  auto [ret, msg] = reg->parse(b.data(), b.size(), false);
  ASSERT_EQ(channeler::ERR_SUCCESS, ret);
  ASSERT_TRUE(msg);
  ASSERT_EQ(msg->type, channeler::MSG_CHANNEL_FINALIZE);

  auto ptr = reinterpret_cast<channeler::message_channel_finalize *>(msg.get());
  ASSERT_EQ(0xbeefd00d, ptr->id.full);
  ASSERT_EQ(0x3987886e, ptr->cookie2);
  ASSERT_TRUE(ptr->capabilities.none());

  // Serialize
  std::vector<channeler::byte> out;
  out.resize(200);
  assert_serialization_ok(out, msg, b);
}



TEST(MessageChannels, parse_and_serialize_channel_cookie)
{
  auto reg = channeler::message_registry::create();

  std::vector<channeler::byte> b{message_channel_cookie, message_channel_cookie + message_channel_cookie_size};

  assert_single_byte_type_fixed_size_message(reg, b, channeler::MSG_CHANNEL_COOKIE);

  auto [ret, msg] = reg->parse(b.data(), b.size(), false);
  ASSERT_EQ(channeler::ERR_SUCCESS, ret);
  ASSERT_TRUE(msg);
  ASSERT_EQ(msg->type, channeler::MSG_CHANNEL_COOKIE);

  auto ptr = reinterpret_cast<channeler::message_channel_cookie *>(msg.get());
  ASSERT_EQ(0xbeefb4be, ptr->either_cookie);
  ASSERT_TRUE(ptr->capabilities.none());

  // Serialize
  std::vector<channeler::byte> out;
  out.resize(200);
  assert_serialization_ok(out, msg, b);
}
