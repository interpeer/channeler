/**
 * This file is part of channeler.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 * Copyright (c) 2022 Interpeer gUG (haftungsbeschränkt)
 *
 * SPDX-License-Identifier: GPL-3.0-only
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "../lib/fsm/channel_responder.h"
#include "../lib/channel_data.h"
#include "../lib/params.h"

#include <gtest/gtest.h>

#include "../../messages.h"
#include "../../packets.h"
#include "../../clock.h"

namespace {

constexpr static std::size_t TEST_PACKET_SIZE = 120;

using test_clock_params = test::clock_params;
using test_defaults = channeler::defaults<
  typename test_clock_params::duration,
  3 // POOL_BLOCK_SIZE
>;
using address_t = uint16_t;

using test_params = ::channeler::params<
  test_clock_params,
  test_defaults,
  channeler::null_lock_policy,
  address_t
>;

} // anonymous namespace


TEST(FSMChannelResponder, process_bad_event)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_channel_responder<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs,
    []() { return fsm_t::secret_type{}; }
  };

  // If we feed the FSM anything other than a ET_MESSAGE event, it will return
  // false.
  action_list_type actions;
  event_list_type events;
  event ev;
  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_NE(fsm_base::PS_SUCCESS, ret);
}


TEST(FSMChannelResponder, process_bad_message)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using fsm_t = fsm_channel_responder<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;
  using event_t = message_event<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  typename test_params::pool pool{TEST_PACKET_SIZE};

  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs,
    []() { return fsm_t::secret_type{}; }
  };

  // If we feed the FSM anything other than a ET_MESSAGE event, it will return
  // false.
  action_list_type actions;
  event_list_type events;

  auto reg = channeler::message_registry::create();
  auto [err, msg] = reg->parse(test::message_data, test::message_data_size, false);
  ASSERT_EQ(channeler::ERR_SUCCESS, err);

  event_t ev{{nullptr, 0, false}, pool.allocate(), {},
    std::move(msg)
  };
  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_NE(fsm_base::PS_SUCCESS, ret);
}



TEST(FSMChannelResponder, process_msg_channel_new)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using namespace test;

  // We create the packet just so we have a packet slot for the FSM.
  // This is a little unfortunate, but we need the packet's sender and
  // recipient information to generate a response.
  std::vector<channeler::byte> data{packet_with_messages,
    packet_with_messages + packet_with_messages_size};
  channeler::packet_wrapper pkt{data.data(), data.size()};

  using fsm_t = fsm_channel_responder<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;
  using event_t = message_event<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  typename test_params::pool pool{TEST_PACKET_SIZE};

  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs,
    []() { return fsm_t::secret_type{}; }
  };

  // MSG_CHANNEL_NEW should be processed, and at this point we'll expect a
  // MSG_CHANNEL_ACKNOWLEDGE in return.
  action_list_type actions;
  event_list_type events;

  auto reg = channeler::message_registry::create();
  auto [err, msg] = reg->parse(test::message_channel_new, test::message_channel_new_size, false);
  ASSERT_EQ(channeler::ERR_SUCCESS, err);

  event_t ev{pkt, pool.allocate(), {},
    std::move(msg)
  };
  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_EQ(fsm_base::PS_SUCCESS, ret);
  ASSERT_EQ(0, actions.size());
  ASSERT_EQ(1, events.size());

  auto & res = *events.begin();
  ASSERT_EQ(ET_MESSAGE_OUT, res->type);
  auto converted = reinterpret_cast<message_out_event *>(res.get());

  // The acknowledge message needs to swap sender and recipient, and be sent on
  // the deault channel.
  ASSERT_EQ(MSG_CHANNEL_ACKNOWLEDGE, converted->message->type);
  auto convmsg = reinterpret_cast<channeler::message_channel_acknowledge *>(converted->message.get());
  ASSERT_EQ(converted->channel, pkt.channel());

  // Check the cookie.
  auto secret = fsm_t::secret_type{};
  auto cookie = create_cookie_responder(secret.data(), secret.size(),
        pkt.sender(), pkt.recipient(), convmsg->id);
  ASSERT_EQ(cookie, convmsg->cookie2);
}


TEST(FSMChannelResponder, process_msg_channel_finalize)
{
  using namespace channeler::fsm;
  using namespace channeler::pipe;
  using namespace channeler;
  using namespace channeler::support;

  using namespace test;

  // We create the packet just so we have a packet slot for the FSM.
  // This is a little unfortunate, but we need the packet's sender and
  // recipient information to generate a response.
  std::vector<channeler::byte> data{packet_with_messages,
    packet_with_messages + packet_with_messages_size};
  channeler::packet_wrapper pkt{data.data(), data.size()};

  using fsm_t = fsm_channel_responder<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;
  using event_t = message_event<
    test_params::defaults::POOL_BLOCK_SIZE,
    typename test_params::channel
  >;

  typename test_params::pool pool{TEST_PACKET_SIZE};

  fsm_t::channel_set chs;
  timeout_config to_config;
  timeouts t{[] (timeouts::duration a) -> timeouts::duration { return a; }};
  fsm_t fsm{
    channeler::message_registry::create(),
    t, to_config,
    chs,
    []() { return fsm_t::secret_type{}; }
  };

  // MSG_CHANNEL_FINALIZE should be processed, but we should not get output
  // events in return. However, our channel set should afterwards contain the
  // channel being sent.
  action_list_type actions;
  event_list_type events;

  auto reg = channeler::message_registry::create();
  auto [err, msg] = reg->parse(test::message_channel_finalize, test::message_channel_finalize_size, false);
  ASSERT_EQ(channeler::ERR_SUCCESS, err);

  auto convmsg = reinterpret_cast<channeler::message_channel_finalize *>(msg.get());
  auto expected_channel = convmsg->id;
  ASSERT_FALSE(chs.has_established_channel(expected_channel));

  event_t ev{pkt, pool.allocate(), {}, std::move(msg)};
  auto [perr, ret] = fsm.process(ev, actions, events);
  ASSERT_EQ(ERR_SUCCESS, perr);
  ASSERT_EQ(fsm_base::PS_SUCCESS, ret);
  ASSERT_EQ(1, actions.size());
  ASSERT_EQ(0, events.size());

  // We need to have the channel with the given channel identifier in the set
  // now.
  ASSERT_TRUE(chs.has_established_channel(expected_channel));

  // The returned action should notify us of this channel.
  auto & act = *actions.begin();
  ASSERT_EQ(act->type, AT_NOTIFY_CHANNEL_ESTABLISHED);
  auto actconv = reinterpret_cast<notify_channel_established_action *>(act.get());
  ASSERT_EQ(actconv->channel, expected_channel);
}
